{-# OPTIONS_GHC -fno-warn-incomplete-patterns #-}
import Prelude hiding (Left, Right)
import Control.Lens ((^?), (.~))
import Control.Lens.At (ix)
import Data.Sort (sortOn)
import Control.Applicative (liftA2)
import Data.List (find)
import Debug.Trace (trace)
import Control.Concurrent

data Node = Empty | Value Int
  deriving (Eq, Show)

data Direction = Up | Down | Left | Right
  deriving (Eq, Show)

type Point = (Int, Int)
type Row = [Node]
type Board = [Row]
type Path = [Board]
type Queue = [Path]
type SortedQueue = Queue

get :: Point -> Board -> Maybe Node
get (x,y) board = board ^? (ix x . ix y)

set :: Point -> Node -> Board -> Board
set (x,y) v = (ix x . ix y) .~ v

swap :: Point -> Point -> Board -> Board
swap p1 p2 board = case (get p1 board, get p2 board) of
  (Just v1, Just v2) -> set p1 v2 . set p2 v1 $ board
  (_, _) -> board

boardSolved :: Board -> Bool
boardSolved board' = foldl (\solved (n1, n2) -> solved && n1 < n2) True ((zip <*> tail) board)
  where
    board = map value (concat board')
    value Empty = 16
    value (Value v) = v

findEmpty :: Int -> Board -> Point
findEmpty i (x:xs) = case find ((==) Empty . snd) (enumerate x) of
  Just (j, _) -> (i, j)
  Nothing -> findEmpty (i+1) xs

pointDistance :: Point -> Point -> Int
pointDistance (x,y) (a,b) = abs (x - a) + abs (y - b)

distance :: Board -> Int
distance = sum . fmap (\(i, n) -> pointDistance (indexPoint i) (nodePoint n)) . (enumerate . concat)

cost :: Path -> Int
cost = liftA2 (+) length (distance . head)

indexPoint :: Int -> Point
indexPoint = liftA2 (,) (// rowLength) (% rowLength)

valuePoint :: Int -> Point
valuePoint v = indexPoint (v - 1)

nodePoint :: Node -> Point
nodePoint Empty = (rowLength - 1, rowLength - 1)
nodePoint (Value v) = valuePoint v

insert :: SortedQueue -> Queue -> SortedQueue
insert (x:z:xs) (y:ys)
  | between = insert (x:y:z:xs) ys
  | lesser  = insert (y:x:z:xs) ys
  | greater = insert (x:z:insert xs [y]) ys
  where
    [x', y', z'] = map cost [x,y,z]

    between = y' >= x' && y' <= z'
    greater = y' >= x' && y' >= z'
    lesser  = y' <= x' && y' <= z'
insert xs [] = xs
insert [] ys = sortOn cost ys
insert [x] ys = sortOn cost (x:ys)

popAndInsert :: SortedQueue -> SortedQueue
popAndInsert (path@(board:_) : queue) = insert queue newPaths
  where
    empty@(x,y) = findEmpty 0 board
    newPath point = swap empty point board : path
    isUnique = (`notElem` path) . head
    newPaths = filter isUnique $ map newPath [(x-1, y), (x+1, y), (x, y-1), (x, y+1)]

shortestPath :: Board -> Path
shortestPath board = head $ until (boardSolved . head . head) popAndInsert [[board]]

main :: IO ()
main = print $ length (shortestPath singleShuffleBoard) - 1

rowLength = 4
enumerate = zip [0..]

(%) = mod
(//) = div
(./.) = (.) . (.)

-- TESTING
constructBoard :: [[Int]] -> Board
constructBoard = map (map (\x -> if x == -1 then Empty else Value x))

properBoard = constructBoard [[1,2,3,4], [5,6,7,8], [9,10,11,12], [13,14,15, -1]]

singleBoard = constructBoard [[1,2,3,4], [5,6,7,8], [9,10,11,12], [13,14,-1,15]]
doubleBoard = constructBoard [[1,2,3,4], [5,6,7,8], [9,10,-1,12], [13,14,11,15]]
tripleBoard = constructBoard [[1,2,3,4], [5,6,7,8], [9,-1,10,12], [13,14,11,15]]
fourBoard   = constructBoard [[1,2,3,4], [5,-1,7,8], [9,6,10,12], [13,14,11,15]]
fiveBoard   = constructBoard [[1,2,3,4], [5,7,-1,8], [9,6,10,12], [13,14,11,15]]
sixBoard    = constructBoard [[1,2,-1,4], [5,7,3,8], [9,6,10,12], [13,14,11,15]]
sevenBoard  = constructBoard [[1,-1,2,4], [5,7,3,8], [9,6,10,12], [13,14,11,15]]
eightBoard  = constructBoard [[1,7,2,4], [5,-1,3,8], [9,6,10,12], [13,14,11,15]]
nineBoard   = constructBoard [[1,7,2,4], [-1,5,3,8], [9,6,10,12], [13,14,11,15]]

doubleShuffleBoard' = constructBoard [[10,4,8,2], [1,-1,3,7], [5,6,15,14], [11,9,13,12]]
singleShuffleBoard = constructBoard [[1,-1,2,3], [5,7,8,4], [10,6,12,14], [9,11,15,13]]
doubleShuffleBoard = constructBoard [[7,5,3,1], [10,8,4,2], [11,12,-1,14], [6,9,15,13]]

trace' a = trace (show a) a

displayAll :: [Board] -> IO ()
displayAll = putStrLn . foldl (\str board -> str ++ display' board ++ "\n\n") ""

display = putStrLn . display'
display' :: Board -> String
display' [] = ""
display' (x:xs) = displayRow x ++ "\n" ++ display' xs
  where
    displayRow [] = ""
    displayRow (y:ys) = displayVal y ++ " " ++ displayRow ys

    displayVal Empty = "EE"
    displayVal (Value v)
      | v <= 9 = "0" ++ show v
      | otherwise = show v

-- NOT NEEDED
boardSolvable :: Board -> Bool
boardSolvable board = (even inversions' && odd emptyRow) || (odd inversions' && even emptyRow)
  where
    inversions' = inversions 0 (concat board)
    (emptyRow, _) = findEmpty 0 board

inversions :: Int -> [Node] -> Int
inversions total [] = total
inversions total (Empty:xs) = inversions total xs
inversions total ((Value x):xs) = inversions (total + length (filter ((>) x . v) xs)) xs
  where
    v Empty = 16
    v (Value v') = v'
