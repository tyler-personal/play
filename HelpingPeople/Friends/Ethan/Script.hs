import System.Process
import Data.List.Split
import Data.List
import Control.Exception
import GHC.IO.Exception (ExitCode)

type StdOut = String
type StdErr = String
main :: IO ()
main = install "lens"

execute :: String -> IO (ExitCode, StdOut, StdErr)
execute str = readProcessWithExitCode cmd args ""
  where (cmd:args) = splitOn " " str

getPackageName :: String -> Maybe String
getPackageName output
  | errorText `isInfixOf` output = Just . init $ takeWhile isNotNum (splitOn errorText output !! 1)
  | otherwise = Nothing
  where
    isNotNum c = c `notElem` ['0'..'9']
    errorText = "Perhaps you haven't installed the profiling libraries for package ‘"

install :: String -> IO ()
install package = do
  putStrLn $ "~~STARTED INSTALLING~~: " ++ package
  (code, out, err) <- execute command

  case (getPackageName out, getPackageName err) of
    (Just package', _) -> do
      install package'
      install package
    (_, Just package') -> do
      install package'
      install package
    _ -> putStrLn $ "~~FINISHED INSTALLING~~: " ++ package

  where
    command = "cabal install " ++ package ++ " --enable-library-profiling --force-reinstalls"

safely :: IO String -> IO String
safely stuff = catch stuff errHandler
  where
    errHandler :: SomeException -> IO String
    errHandler = pure . show

