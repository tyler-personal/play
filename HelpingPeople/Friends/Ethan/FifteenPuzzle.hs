-- NOTE: This is insanely inefficient, going to try to refactor for non-performance and see if that fixes performance anyway
module FifteenPuzzle where

import Prelude hiding (Left, Right)
import Data.List (find, minimumBy)
import Data.Maybe (fromMaybe, fromJust)
import Data.Function (on)

data Node = Empty | Value Int deriving (Eq, Show)
data Direction = Up | Down | Left | Right deriving (Eq, Show)

type Row = [Node]
type Board = [Row]
type Path = [Board]
type Point = (Int, Int)

enumerate = zip [0..]

zipWithNext = zip <*> tail

for = flip map

-- TODO: rewrite get & swap with Control.Lens
get :: Point -> Board -> Maybe Node
get (x,y) board =
  if x >= 0 && length board > x
    then if y >= 0 && length (board !! x) > y
      then Just $ (board !! x) !! y
      else Nothing
    else Nothing

swap :: Point -> Point -> Board -> Board
swap point1 point2 board =
  for (enumerate board) $ \(rowIndex, row) ->
    for (enumerate row) $ \(index, node) ->
      f (rowIndex, index) node
  where
    f point node
      | point == point1 = g point2
      | point == point2 = g point1
      | otherwise = node
      where g p = fromMaybe node (get p board)


rowIsSolved :: Bool -> Row -> Bool
rowIsSolved isFinal = and . scanl (\_ (y,z) -> compare y z) True . zipWithNext
  where
    compare Empty node = False
    compare node Empty = isFinal
    compare (Value n1) (Value n2) = n1 < n2

boardIsSolved :: Board -> Bool
boardIsSolved board' = all (rowIsSolved False) board && rowIsSolved True finalRow
  where (board, finalRow) = (init board', last board')

pathIsSolved :: Path -> Bool
pathIsSolved = boardIsSolved . last

findEmpty :: Board -> Point
findEmpty = f 0
  where
    f _ [] = error "Empty not found, you must have one empty spot to run this program"
    f i (x:xs) = case find (\(_,e) -> e == Empty) (enumerate x) of
      Just (ii, e) -> (i, ii)
      Nothing -> f (i+1) xs

swapEmpty :: Board -> Direction -> Board
swapEmpty board direction = case direction of
  Up    -> swap' (x-1, y)
  Down  -> swap' (x+1, y)
  Left  -> swap' (x, y-1)
  Right -> swap' (x, y+1)
  where
    (x,y) = findEmpty board
    swap' p = swap (x,y) p board

nextStates :: Board -> [Board]
nextStates board = [f Up, f Down, f Left, f Right]
  where f = swapEmpty board

generatePaths :: (Bool, [Path]) -> (Bool, [Path])
generatePaths (_, paths) = (any pathIsSolved newPaths, newPaths)
  where newPaths = paths >>= \p -> map (\s -> p ++ [s]) (nextStates (last p))

shortestPath :: Board -> Path
shortestPath board = fromJust $ find pathIsSolved $ snd $ until fst generatePaths (False, [[board]])
-- f path = length . snd $ until fst generatePaths (False, [path])

-- shortestPath :: Board -> Path
-- shortestPath board = undefined
--   where
--     paths :: [Path]
--     paths = map (\state -> board : [state]) (nextStates board)



main = print . length $ shortestPath board

board :: Board
board =
  [ [v 5, v 9, v 4, v 11]
  , [v 3, v 8, v 10, v 12]
  , [v 15, v 7, Empty, v 13]
  , [v 2, v 14, v 6, v 1]]
  where v = Value

solvedBoard :: Board
solvedBoard =
  [ map v [1,2,3,4]
  , map v [5,6,7,8]
  , [v 9, v 10, Empty, v 11]
  , [v 13, v 14, v 15, v 12]]
  where v = Value

displayAll :: [Board] -> IO ()
displayAll = putStrLn . foldl (\str board -> str ++ display board ++ "\n\n") ""

display :: Board -> String
display [] = ""
display (x:xs) = displayRow x ++ "\n" ++ display xs
  where
    displayRow [] = ""
    displayRow (y:ys) = displayVal y ++ " " ++ displayRow ys

    displayVal Empty = "EE"
    displayVal (Value v)
      | v <= 9 = "0" ++ show v
      | otherwise = show v
