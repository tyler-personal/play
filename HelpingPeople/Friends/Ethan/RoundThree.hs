{-# LANGUAGE RecordWildCards, TupleSections #-}
import Prelude hiding (Left, Right)
import Data.List (find)
import Control.Lens ((^?), (.~), (^.))
import Control.Lens.Tuple (_1, _2, _3)
import Control.Lens.At (ix)
import Data.Maybe (fromJust)

data Node = Empty | Value Int
  deriving (Eq, Show)

data Direction = Up | Down | Left | Right
  deriving (Eq, Show)

type Row = [Node]
type Board = [Row]
type Point = (Int, Int)
type Path = [Direction]
type State = (Board, Path)

-- type State = (Board, Path, NextPaths)

rowSolved :: Bool -> Row -> Bool
rowSolved isFinal = and . scanl (\_ (y,z) -> compare y z) True . (zip <*> tail)
  where
    compare Empty node = False
    compare node Empty = isFinal
    compare (Value n1) (Value n2) = n1 < n2

boardSolved :: Board -> Bool
boardSolved board' = all (rowSolved False) board && rowSolved True finalRow
  where (board, finalRow) = (init board', last board')

findEmpty :: Int -> Board -> Point
findEmpty i (x:xs) = case find (\(_,e) -> e == Empty) (zip [0..] x) of
  Just (j, _) -> (i, j)
  Nothing -> findEmpty (i+1) xs

swap :: Point -> Point -> Board -> Board
swap (x,y) (a,b) board = case (board ^? (ix x . ix y), board ^? (ix a . ix b)) of
  (Just v1, Just v2) -> ((ix x . ix y) .~ v2) . ((ix a . ix b) .~ v1) $ board
  (_, _) -> board

nextStates :: State -> [State]
nextStates (board, path) = filter ((/=) board . fst) [f Up, f Down, f Left, f Right]
  where
    p@(x,y) = findEmpty 0 board
    swap' p' = swap p p' board
    f d = (,path ++ [d]) $ case d of
      Up    -> swap' (x-1, y)
      Down  -> swap' (x+1, y)
      Left  -> swap' (x, y-1)
      Right -> swap' (x, y+1)

f x = x + 1
generateStates :: [State] -> [State]
generateStates = (=<<) nextStates

shortestPath :: Board -> Path
shortestPath board = snd . fromJust . find c $ until (any c) generateStates [(board, [])]
  where c = boardSolved . fst
-- shortestPath = (^. _2) . until (boardSolved . (^. _1)) generateState

main :: IO()
main = print . length $ shortestPath board3_3
singleBoard = constructBoard [[1,2,3,4], [5,6,7,8], [9,10,11,12], [13,14,-1,15]]
doubleBoard = constructBoard [[1,2,3,4], [5,6,7,8], [9,10,-1,12], [13,14,11,15]]

singleShuffleBoard =
  constructBoard [[1,-1,2,3], [5,7,8,4], [10,6,12,14], [9,11,15,13]]

doubleShuffleBoard =
  constructBoard [[7,5,3,1], [10,8,4,2], [11,12,-1,14], [6,9,15,13]]

board3_3 = constructBoard [[9,-1,2,4], [7,1,3,14], [5,12,11,15],[6,13,8,10]]

constructBoard :: [[Int]] -> Board
constructBoard = fmap (fmap f)
  where
    f (-1) = Empty
    f x = Value x

displayAll :: [Board] -> IO ()
displayAll = putStrLn . foldl (\str board -> str ++ display board ++ "\n\n") ""

display :: Board -> String
display [] = ""
display (x:xs) = displayRow x ++ "\n" ++ display xs
  where
    displayRow [] = ""
    displayRow (y:ys) = displayVal y ++ " " ++ displayRow ys

    displayVal Empty = "EE"
    displayVal (Value v)
      | v <= 9 = "0" ++ show v
      | otherwise = show v

