package com.code;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PuzzleGui {
    private JButton ButtonRandomGrid;
    private JPanel board;

    public PuzzleGui() {
        ButtonRandomGrid.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                // TODO: call the method to instantiate the boardGame, Generate Board, Update Gui
                JOptionPane.showMessageDialog(null,"Hello World.");
            }
        });
    }
    public static void main(String [] args)
    {
        JFrame frame = new JFrame("PuzzleGui");
        frame.setContentPane(new PuzzleGui().board);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
