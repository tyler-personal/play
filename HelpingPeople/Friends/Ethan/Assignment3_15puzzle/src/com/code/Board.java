package com.code;

import java.util.ArrayList;
import java.util.Stack;

public class Board {
    private int n;          // board dimension
    private int x, y;       // current xy of the blank
    private int[][] tiles;  // the copy of blocks
    boolean is_solved;
    // construct a board from an n-by-n array of blocks
    // (where blocks[i][j] = block in row i, column j)
    public Board(int[][] blocks) {
        if (blocks == null) {
            throw new IllegalArgumentException("blocks == null");
        }

        n = blocks.length;
        tiles = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n ; j++) {
                tiles[i][j] = blocks[i][j];
                if (tiles[i][j] == 0) {
                    x = i;
                    y = j;
                    continue;
                }
                int p = (tiles[i][j] - 1) / n;
                int q = (tiles[i][j] - 1) % n;
            }
        }
    }

    // board dimension n
    public int dimension() {
        return n;
    }


    // is this board the goal board?
    public boolean isGoal() {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (tiles[i][j] != (i * n + j + 1)) {
                    if (tiles[i][j] == 0) continue;
                    return false;
                }
            }
        }
        return true;
    }



    // does this board equal other?
    public boolean equals(Object other) {
        if (other == this) return true;
        if (other == null) return false;
        if (other.getClass() != this.getClass()) return false;

        Board that = (Board) other;
        if (that.n != this.n) return false;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (that.tiles[i][j] != this.tiles[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }


    public ArrayList<Board> neighbors() {
        ArrayList<Board> neighborBoards = new ArrayList<>();
        if (x > 0) neighborBoards.add((moveBlank(x - 1, y)));
        if (x < n - 1) neighborBoards.add((moveBlank(x + 1, y)));
        if (y > 0) neighborBoards.add((moveBlank(x, y - 1)));
        if (y < n - 1) neighborBoards.add((moveBlank(x, y + 1)));
        return neighborBoards;
    }

    // string representation of this board
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(n).append("\n");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                s.append(String.format("%2d ", tiles[i][j]));
            }
            s.append("\n");
        }
        return s.toString();
    }

    private void swap(int[][] arr, int x1, int y1, int x2, int y2) {
        int temp = arr[x1][y1];
        arr[x1][y1] = arr[x2][y2];
        arr[x2][y2] = temp;
    }

    private Board moveBlank(int p, int q) {

        int[][] copy = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                copy[i][j] = tiles[i][j];
            }
        }

        swap(copy, p, q, x, y);
        return new Board(copy);
    }

    public int numberOfImproperTiles() {
        int numMisplaced = 0;
        int numExpected = 0;

        for (int rowX = 0; rowX <= 3; rowX++) {
            for (int colY = 0; colY <= 3; colY++) {
                numExpected++;
                if (tiles[rowX][colY] != numExpected) {
                    numMisplaced++;
                }
            }
        }
        if (tiles[3][3] == 0) {
            numMisplaced--;
        }

        return numMisplaced;
    }

  public int[][] getTiles()
  {
      return tiles;
  }
  public int getX()
  {
      return x;
  }
    public int getY()
    {
        return y;
    }
}