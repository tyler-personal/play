package com.code;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class Node implements Comparable<Node> {
    Node prev;
    int fitness; // num of steps taken+num of tiles out of end state
    Board state;

    //int steps;
    // String tohere;
    public Node(Node previous, Board b) {
        prev = previous;
        state = b;
        if (prev == null) {
            fitness = 96; // this should be the max possible board weight naturally, i think (if in fact its solving the shortest path).
            //steps=1;
        } else {
            fitness = generateFitness();
            //   tohere=direction;
            //steps=previous.steps++;
        }
    }


//    public Node(Node previous , Board b)
//    {
//        prev = previous;
//        state = b;
//        if(prev==null) {
//            fitness = 96; // this should be the max possible board weight naturally, i think (if in fact its solving the shortest path).
//        } else
//            fitness=generateFitness();
//    }

    public int length() {
        int steps_so_far = 0;
        Node tempprev = prev;
        while (tempprev != null) {
            steps_so_far++;
            tempprev = tempprev.prev;
        }
        return steps_so_far;
    }

    public int generateFitness() {
        return length() + state.numberOfImproperTiles();
    }

    public int num_misplaced() {
        return state.numberOfImproperTiles() + 1;
    }

    public ArrayList<Node> getNeighbors() { //todo write this correctly its hella fucked up and lacking functionality
        ArrayList<Node> retList = new ArrayList<>();
        for (Board b : state.neighbors()) {

            Node n = new Node(this, b);    //check this

            retList.add(n);

        }
    //       if(prev !=null) { TODO UNCOMMENT OR IMPROVE- THIS INCREASED RUNTIME BUT IS NOT EFFECIENT AT ALL
    //           int[][] t = prev.state.getTiles();
    //           for (Node n : retList) {
    //               if (equal(n.prev.state.getTiles(), prev.state.getTiles())) {
    //                   retList.remove(n);
    //               }
    //           }
    //       }
        return retList;
    }
    // getNeighbors node = map (Node node) . neighbors
    public List<Node> getNeighbors2() {
        return state.neighbors().stream()
                .map(b -> new Node(this, b))
                .collect(Collectors.toList());
    }

    public static boolean equal(int[][] arr1, int[][] arr2) {
        if (arr1 == null) {
            return false;
        }
        if (arr2 == null) {
            return false;
        }
        if (arr1.length != arr2.length) {
            return false;
        }
        for (int i = 0; i < arr1.length; i++) {
            if (arr1[i] != arr2[i]) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int compareTo(Node node) {
        return this.fitness - node.fitness;
    }
}
