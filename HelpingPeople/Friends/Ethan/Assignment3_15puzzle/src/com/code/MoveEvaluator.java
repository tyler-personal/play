package com.code;

import java.util.*;
import java.util.concurrent.*;

public class MoveEvaluator {
    private Node root;
    private PriorityBlockingQueue<Node> pq = new PriorityBlockingQueue<>();

    public MoveEvaluator(Node n) {
        root = n;
    }

    public Node doWork() {
        var ex = Executors.newFixedThreadPool(4);
        var task = new HashSet<Callable<Node>>();

        task.add(() -> {
            pq.addAll(root.getNeighbors());
            Node retNode;
            for (; ;) {
                Node temp = pq.poll();
                if (temp != null && temp.num_misplaced() == 1) {
                    retNode = temp;
                    return retNode;
                }
                pq.addAll(temp.getNeighbors());
            }
        });

        try {
            return ex.invokeAny(task);

        } catch (InterruptedException | ExecutionException e) {
            System.exit(0);
            return null;
        }
    }
}
