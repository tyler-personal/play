{-# LANGUAGE FlexibleContexts, RankNTypes, UnicodeSyntax, ExtendedDefaultRules, ImpredicativeTypes, OverloadedStrings, TupleSections #-}
import Prelude hiding (Left, Right, zip, tail, and, scanl, all, init, last, map, length, any, (++), head, foldl, null, filter, notElem)
import Data.Maybe (fromJust)
import Control.Lens ((.~), (^?), element)
import Control.Lens.Internal.Indexed (Indexable)
import Debug.Trace (trace)
import Data.Vector

data Node = Empty | Value Int deriving (Eq, Show)
data Direction = Up | Down | Left | Right deriving (Eq, Show)

type List = Vector
type Row = List Node
type Board = List Row
type Path = List Board
type Point = ∀ p f a. (Indexable Int p, Applicative f) => p a (f a) -> List (List a) -> f (List (List a))

mkPoint :: Int -> Int -> Point
mkPoint x y = element x . element y

swap :: Point -> Point -> Board -> Board
swap p1 p2 board = case (board ^? p1, board ^? p2) of
  (Just v1, Just v2) -> (p1 .~ v2) . (p2 .~ v1) $ board
  (_, _) -> board

rowIsSolved :: Bool -> Row -> Bool
rowIsSolved isFinal = and . scanl (\_ (y,z) -> compare y z) True . zipWithNext
  where
    compare Empty node = False
    compare node Empty = isFinal
    compare (Value n1) (Value n2) = n1 < n2

boardIsSolved :: Board -> Bool
boardIsSolved board' = all (rowIsSolved False) board && rowIsSolved True finalRow
  where (board, finalRow) = (init board', last board')

pathIsSolved :: Path -> Bool
pathIsSolved = boardIsSolved . last

findEmpty :: Board -> (Int, Int)
findEmpty = f 0
  where f i xs = case find (\(_,e) -> e == Empty) (enumerate (head xs)) of
          Just (ii, _) -> (i, ii)
          Nothing -> f (i+1) (tail xs)

swapEmpty :: Board -> Direction -> Board
swapEmpty board direction = case direction of
  Up    -> swap' (x-1, y)
  Down  -> swap' (x+1, y)
  Left  -> swap' (x, y-1)
  Right -> swap' (x, y+1)
  where
    (x,y) = findEmpty board
    swap' (a,b) = swap (mkPoint x y) (mkPoint a b) board

nextStates :: Board -> List Board
nextStates board = fromList [f Up, f Down, f Left, f Right]
  where f = swapEmpty board

generatePaths :: (Bool, List Path) -> (Bool, List Path)
generatePaths (_, paths) = (any pathIsSolved allNewPaths, allNewPaths)
  where
    allNewPaths = paths >>= nextPaths

    nextPaths :: Path -> List Path
    nextPaths path = filter lastBoardIsUnique newPaths
      where
        newPaths = map (\board' -> path ++ fromList [board']) (nextStates (last path))
        lastBoardIsUnique path' = last path' `notElem` path

shortestPath :: Board -> Path
shortestPath board = fromJust $ find pathIsSolved $ snd $ until fst generatePaths (False, fromList [fromList [board]])

enumerate xs = zip (fromList [0..(length xs)]) xs
zipWithNext = zip <*> tail

main = print . length $ shortestPath board3_2

tracePaths :: List Path -> a -> a
tracePaths paths = trace . toList $ fromList sizeOfPaths ++ fromList ": " ++ fromList numberOfPaths
  where
    numberOfPaths = show (length paths)
    sizeOfPaths = show . length . head $ paths

board1 :: Board
board1 = fromList
  [ fromList [v 5, v 9, v 4, v 11]
  , fromList [v 3, v 8, v 10, v 12]
  , fromList [v 15, v 7, Empty, v 13]
  , fromList [v 2, v 14, v 6, v 1]]
  where v = Value

solvedBoard :: Board
solvedBoard = fromList
  [ fromList (fmap v [2,1,3,4])
  , fromList (fmap v [5,6,8,7])
  , fromList [v 9, v 10, Empty, v 11]
  , fromList [v 13, v 14, v 15, v 12]]
  where v = Value


board2 :: Board
board2 = constructBoard [[5,1,4,7], [9,2,10,3], [14,13,6,8], [-1, 11, 15, 12]]

board3 = constructBoard [[1,2,3,4], [9,5,7,11], [6,8,15,10], [-1,13,14,12]]

board3_2 = constructBoard [[1,2,4,11], [9,7,3,-1], [6,5,15,10], [13,8,14,12]]

board3_3 = constructBoard [[9,-1,2,4], [7,1,3,14], [5,12,11,15],[6,13,8,10]]

constructBoard :: [[Int]] -> Board
constructBoard = fmap (fromList . fmap f) . fromList
  where
    f (-1) = Empty
    f x = Value x

displayAll :: List Board -> IO ()
displayAll = putStrLn . toList . foldl (\str board -> str ++ (fromList (display' board)) ++ fromList "\n\n") (fromList "")

display = putStrLn . display'
display' :: Board -> String
display' board
  | null board = ""
  | otherwise = toList $ displayRow (head board) ++ (fromList "\n") ++ (fromList (display' (tail board)))
  where
    displayRow row
      | null row = fromList ""
      | otherwise = displayVal (head row) ++ (fromList " ") ++ displayRow (tail row)

    displayVal Empty = fromList "EE"
    displayVal (Value v)
      | v <= 9 = (fromList "0") ++ (fromList (show v))
      | otherwise = (fromList (show v))
