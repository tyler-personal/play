Node = Struct.new(:val, :left, :right)
Leaf = Struct.new(:val)

def bfs(node)
  if node.is_a?(Leaf)
    [node.val]
  else
    lVal, *left = bfs(node.left)
    rVal, *right = bfs(node.right)
    [node.val] + [lVal] + [rVal] + left + right
  end
end

def dfs(node)
  [node.val] + (node.is_a?(Leaf) ? [] : dfs(node.left) + dfs(node.right))
end

def uncons(list)
  [list[0], list[1..-1]]
end


puts (dfs Node.new(
  1,
  Node.new(
    2,
    Leaf.new(4),
    Leaf.new(5)),
  Leaf.new(3)
))
