module NodeSearch where

data Node a = Leaf a | Node a (Node a) (Node a)

bfs :: Node a -> [a]
bfs (Leaf val) = [val]
bfs (Node val left right) = val : lVal : rVal : left' ++ right'
  where (lVal:left', rVal:right') = (bfs left, bfs right)

dfs :: Node a -> [a]
dfs (Leaf val) = [val]
dfs (Node val left right) = val : dfs left ++ dfs right

main = dfs node
  where
    node = Node 1
      (Node 2 (Leaf 4) (Leaf 5))
      (Leaf 3)
