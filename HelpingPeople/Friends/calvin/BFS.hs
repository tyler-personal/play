module BFS where

main = map value (dfs node)
  where
    node = Node
      { left = Just Node
        { left = finalNode 4
        , right = finalNode 5
        , value = 2
        }
      , right = finalNode 3
      , value = 1
      }

data Node a = Node { left :: Maybe (Node a), right :: Maybe (Node a), value :: a }

bfs :: Node a -> [Node a]
bfs root = root : f root
  where
    f node = left' ++ right' ++ sub left' ++ sub right'
      where
        get Nothing = []
        get (Just n) = [n]

        left' = get $ left node
        right' = get $ right node

        sub [] = []
        sub (n:_) = f n

dfs :: Node a -> [Node a]
dfs root = root : f root
  where
    f node = get (left node) ++ get (right node)
      where
        get Nothing = []
        get (Just n) = n : get (left n) ++ get (right n)

finalNode val = Just Node { left = Nothing, right = Nothing, value = val }


-- data Node = Left (Maybe Node) | Right (Maybe Node)
-- height :: Maybe Node -> Int
-- height Nothing = 0
-- height (Just node)
--   | leftHeight > rightHeight = leftHeight + 1
--   | otherwise = rightHeight + 1
--   where
--     leftHeight = height $ left node
--     rightHeight = height $ right node
