module DeleteAndEarn2 where

import Data.List
main = print 4

type Node = (Int, [Int])
type Path = [Node]

deleteAndEarn :: [Int] -> Int
deleteAndEarn xs = maximum . map fst $ nodes [(0, xs)]

deleteAround :: Int -> [Int] -> [Int]
deleteAround x = delete x . filter (`notElem` [x - 1, x + 1])

isEmpty :: Node -> Bool
isEmpty (_, []) = True
isEmpty (_, _) = False

nodes :: [Node] -> [Node]
nodes xs
  | all isEmpty xs = xs
  | otherwise = nodes $ concatMap (\(total, rest) ->
    map (\next ->
      (total + next, deleteAround next rest))
    rest) xs
