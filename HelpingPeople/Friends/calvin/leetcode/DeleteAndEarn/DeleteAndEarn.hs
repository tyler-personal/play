module DeleteAndEarn where

import Data.List (nub)

main = print 4

deleteAround :: Int -> [Int] -> Int
deleteAround x = sum . filter (`elem` [x - 1, x, x + 1])

deleteAndEarn :: [Int] -> Int
deleteAndEarn xs = maximum $ map (`deleteAround` xs) (nub xs)
