module DeleteAndEarnShort where

import Data.List (delete)

deleteAndEarn xs = maximum . map fst $ nodes [(0, xs)]

deleteAround x = delete x . filter (`notElem` [x - 1, x + 1])

isEmpty (_, []) = True
isEmpty (_, _) = False

nodes xs
  | all (null . snd) xs = xs
  | otherwise = nodes $ concatMap (\(y, ys) -> map (\z -> (y + z, deleteAround z ys)) ys) xs
