enum Node<T> {
  case Leaf(T)
  indirect case Branch(T, Node<T>?, Node<T>?)
}

func value<T>(_ node: Node<T>) -> T {
  switch node {
  case .Leaf(let v): return v
  case .Branch(let v, _, _): return v
  }
}

func rightSideView<T>(_ nodes: List<Node<T>>) -> List<T> {
  if nodes.isEmpty {
    return []
  } else if let node = nodes.last {
    [value(node)] + rightSideView(nodes[0..nodes.count-1])
  }
}

func childNodes<T>(_ node: Node<T>) -> List<Node<T>> {

}


print(value(Node.Branch(4, Node.Leaf(5), Node.Leaf(3))))
