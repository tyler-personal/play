module BinaryTreeRight where

import Data.Maybe

main = rightValues [Branch 1
  (Just (Branch 2 Nothing (Just (Leaf 5))))
  (Just (Branch 3 Nothing (Just (Leaf 4))))]

data Node a = Leaf a | Branch a (Maybe (Node a)) (Maybe (Node a))

value :: Node a -> a
value (Leaf v) = v
value (Branch v _ _) = v

rightValues :: [Node a] -> [a]
rightValues [] = []
rightValues nodes = value (last nodes) : rightValues nextRow
  where nextRow = concatMap children nodes

children :: Node a -> [Node a]
children (Leaf _)= []
children (Branch _ left right) = catMaybes [left, right]

