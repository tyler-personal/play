{-# LANGUAGE RecordWildCards #-}
module BinaryTreeRight2 where
import Data.Maybe

main = rightValues [Node 1
  (Just (Node 2 Nothing (Just (Node 5 Nothing Nothing))))
  (Just (Node 3 Nothing (Just (Node 4 Nothing Nothing))))]

data Node a = Node
  { value :: a
  , left  :: Maybe (Node a)
  , right :: Maybe (Node a)
  }

rightValues :: [Node a] -> [a]
rightValues [] = []
rightValues nodes = value (last nodes) : rightValues nextRow
  where
    nextRow = concatMap children nodes

children :: Node a -> [Node a]
children Node {..} = catMaybes [left, right]




