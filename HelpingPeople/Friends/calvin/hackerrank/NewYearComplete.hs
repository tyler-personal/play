module NewYearComplete where

import Control.Monad

data Person = Person { bribes :: Int, position :: Int } deriving (Show)

type Queue = [Person]

noTripleMoves :: Queue -> Bool
noTripleMoves = all (\p -> bribes p < 3)

isProperOrder :: Queue -> Bool
isProperOrder = all (\(i, p) -> position p == i) . zip [1..]

isValidQueue :: Queue -> Bool
isValidQueue queue = noTripleMoves queue && isProperOrder queue

debribeIteration :: Queue -> Queue
debribeIteration [] = []
debribeIteration [x] = [x]
debribeIteration (x:y:xs)
  | position x > position y = y : rest x { bribes = bribes x + 1 }
  | otherwise = x : rest y
  where rest a = debribeIteration $ a : xs

debribe :: Queue -> Queue
debribe = until isProperOrder debribeIteration

minimumBribe :: [Int] -> Maybe Int
minimumBribe q
  | isValidQueue queue = Just . sum $ map bribes queue
  | otherwise = Nothing
  where queue = debribe $ map (Person 0) q

main :: IO()
main = do
    testCount <- readLn :: IO Int
    forM_ [1..testCount] $ \_ -> do
        _size <- readLn :: IO Int
        bribe <- minimumBribe . map read . words <$> getLine

        putStrLn $ case bribe of
          (Just x) -> show x
          Nothing -> "Too chaotic"
