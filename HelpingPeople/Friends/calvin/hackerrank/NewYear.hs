{-# LANGUAGE FlexibleInstances, UndecidableInstances, DuplicateRecordFields, ExtendedDefaultRules #-}
module NewYear where

import Control.Monad
import Data.Array
import Data.Bits
-- import Data.List (permutations)
import Debug.Trace
import System.Environment
import System.IO
import System.IO.Unsafe

zipWithNext xs = zip xs (drop 1 xs)
enumerate = zip [0..]

data Person = Person { bribes :: Int, position :: Int }
  deriving (Show)
type Queue = [Person]

noTripleMoves = all (\p -> bribes p < 3)

isProperOrder = all (\(i, p) -> position p == i + 1) . enumerate

isValidQueue :: Queue -> Bool
isValidQueue queue = noTripleMoves queue && isProperOrder queue

bribed :: Person -> Person
bribed p = p { bribes = bribes p + 1 }

debribeIteration :: Queue -> Queue
debribeIteration [] = []
debribeIteration [x] = [x]
debribeIteration (x:y:xs)
  | position x > position y = y : rest (bribed x)
  | otherwise = x : rest y
  where rest a = debribeIteration $ a : xs

debribe :: Queue -> Queue
debribe = until isProperOrder debribeIteration

minimumBribes :: [Int] -> Maybe Int
minimumBribes q
  | isValidQueue queue = Just . sum $ map bribes queue
  | otherwise = Nothing
  where queue = debribeIteration $ map (Person 0) q

-- ~~ LEFTOVER ATTEMPT 3 SHIT ~~
-- debribe queue = foldl compare [] $ zipWithNext queue
--   where
--     compare acc (current, next)
--       | current > next = acc + next
--
--   | isValidQueue queue = foldl ((. bribes) . (+)) 0 queue
--   | isValidQueue queue = foldl (\acc p -> (+) acc . bribes $ p) 0 queue

-- ~~ ATTEMPT 2 HERE ~~
-- data Person = Person { moves :: Int, number :: Int }
--
-- type Queue = [Person]
--
-- allMovesForPersonN :: Int -> Queue -> [Queue]
-- allMovesForPersonN n queue = undefined
--   where
--     person = queue !! n
--
-- swapTwo a b xs = zipWith f [0..] xs
--   where
--     f x y
--       | x == a = xs !! b
--       | x == b = xs !! a
--       | otherwise = y
--
-- permutations :: Queue -> [Queue]
-- permutations [] = []
-- permutations (x:xs) = undefined
--
-- isValidQueue :: Queue -> Bool
-- isValidQueue queue = undefined
--
-- generateValidQueues :: Int -> [Queue]
-- generateValidQueues n = undefined
--   where
--     initialQueues = map (Person 0) [1..n]
--     possibleQueues = permutations initialQueues
--
--
--
-- enumerate = zip [0..]
-- -- Complete the minimumBribes function below.
-- minimumBribes :: [Int] -> IO ()
-- minimumBribes q = undefined
--   where
--     values = map (\(i, e) -> e) $ enumerate q
--
-- ~~ ATTEMPT 1 HERE ~~
-- positive = map f . enumerate
--   where
--     f (i,x)
--       | (i - x) < 0 = 0
--       | otherwise = i - x
--
-- negative = map f . enumerate
--   where
--     f (i,x)
--       | (i - x) > 0 = 0
--       | otherwise = i - x
readMultipleLinesAsStringArray :: Int -> IO [String]
readMultipleLinesAsStringArray 0 = return []
readMultipleLinesAsStringArray n = do
    line <- getLine
    rest <- readMultipleLinesAsStringArray(n - 1)
    return (line : rest)

main :: IO()
main = do
    t <- readLn :: IO Int

    forM_ [1..t] $ \t_itr -> do
        n <- readLn :: IO Int
        qTemp <- getLine
        let q = map (read :: String -> Int) . words $ qTemp
        undefined
        -- minimumBribes q

