{-# LANGUAGE TypeApplications #-}
module StripTest where
import Data.List
import Data.Maybe

stripSuffix sfx lst =
  case stripPrefix (reverse sfx) (reverse lst) of
    Nothing -> Nothing
    Just ys -> Just (reverse ys)

stripChar :: Char -> String -> String
stripChar c = dropWhileEnd (c ==) . dropWhile (c ==)

stripWhitespace :: String -> String
stripWhitespace = strip " "

strip :: Eq a => [a] -> [a] -> [a]
strip xs = dropFromStart xs . dropFromEnd xs
  where
    dropFromX check strip xs ys
      | xs `check` ys = dropFromX check strip xs (fromJust $ strip xs ys)
      | otherwise = ys

    dropFromStart = dropFromX isPrefixOf stripPrefix
    dropFromEnd = dropFromX isSuffixOf stripSuffix

test = map (strip "\n") words
  where
    words = ["potato\n", "\noof\n"]



