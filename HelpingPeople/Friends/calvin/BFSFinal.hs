module BFSFinal where

data Node a = Leaf a | Branch a (Node a) (Node a)

bfs (Leaf val) = [val]
bfs (Branch val left right) = val : merge (bfs left) (bfs right)

dfs (Leaf val) = [val]
dfs (Branch val left right) = val : dfs left ++ dfs right

merge [] ys = ys
merge (x:xs) ys = x : merge ys xs

main = bfs node
  where
    node = Branch 1
      (Branch 2 (Leaf 4) (Leaf 5))
      (Leaf 3)
