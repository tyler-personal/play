{-# LANGUAGE ExtendedDefaultRules #-}
module Min where

import Prelude hiding (min, sum)

min :: Ord a => [a] -> a -> [a] -> (a, [a])
min [] champ ys = (champ, ys)
min (x:xs) _ [] = min xs x []
min (x:xs) champ ys
  | x < champ = min xs x (champ : ys)
  | otherwise = min xs champ (x : ys)

min2 :: Ord a => [a] -> a
min = foldl1 (\x y -> if x < y then x else y)
