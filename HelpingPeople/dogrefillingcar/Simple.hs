{-# LANGUAGE QuasiQuotes, LambdaCase, TypeSynonymInstances, FlexibleInstances #-}
module Simple where
import Text.Read (readMaybe)
import Rando (pickOne)
import Data.String.Interpolate (i)
import Control.Lens

data Option = Paper | Rock | Scissors
  deriving (Read, Show, Enum)

check Paper Rock = _1
check Paper Scissors = _2
check Rock Scissors = _1
check Rock Paper = _2
check Scissors Paper = _1
check Scissors Rock = _2
check _ _ = _3

input output = do
  putStr output
  readMaybe <$> getLine >>= \case
    Just x -> pure x
    Nothing -> input [i|Invalid input.\n#{output}|]

main = do
  putStr "Please enter Rock, Paper, or Scissors: "
  player <- readLn
  cpu <- pickOne [Paper ..]

  putStrLn [i|CPU rolled #{cpu}|]
  putStrLn $ ("You win!", "Computer wins!", "Tie!") ^. check player cpu


  --case userInput of
   -- Just x -> pure x
    --Nothing -> input [i|Invalid input.\n#{output}|]
