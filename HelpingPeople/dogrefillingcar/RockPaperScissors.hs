{-# LANGUAGE QuasiQuotes, LambdaCase #-}
import Text.Read
import Control.Applicative
import Control.Lens.Tuple
import Control.Lens
import Rando
import Data.String.Interpolate

data Option = Paper | Rock | Scissors
  deriving (Read, Show, Enum)

check Paper Rock = _1
check Paper Scissors = _2
check Rock Scissors = _1
check Rock Paper = _2
check Scissors Paper = _1
check Scissors Rock = _2
check _ _ = _3

player = putStr prompt >> readMaybe <$> getLine >>= maybe (putStrLn invalid >> player) pure
  where (prompt, invalid) = ("Please enter Rock, Paper, or Scissors: ", "Invalid option")

cpu = pickOne [Paper ..] >>= \c -> putStrLn [i|CPU rolled #{c}|] >> pure c

main = liftA2 check player cpu >>= putStrLn . (^.) ("You win!", "Computer wins!", "Tie!")

-- ALTERNATIVE
main2 = do
  player <- getPlayerAnswer
  cpu <- getComputerAnswer
  putStrLn $ ("Player wins!", "Computer wins!", "Tie!") ^. check player cpu

getPlayerAnswer = do
  putStr "Please enter Rock, Paper, or Scissors: "
  readMaybe <$> getLine >>= \case
    (Just v) -> return v
    Nothing -> putStrLn "Invalid option." >> getPlayerAnswer

getComputerAnswer = tap (pickOne [Paper ..]) (putStrLn . ("CPU rolled: " ++) . show)

tap monad f = monad >>= \m -> f m >> pure m
