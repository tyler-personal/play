import System.Random
import System.Exit


newBoard = map (\_ -> ' ') [1..9]

main :: IO ()
main = playerTurn newBoard


playerTurn :: [Char] -> IO ()
playerTurn board = do
    print "Please input the desired position (1-9; 0 shows the board)"
    position <- read <$> getLine
    if position == 0 then
        putStrLn $ showBoard board
    else do
        let newBoard = setPlayerPos board position
        if checkWon newBoard then do
            putStrLn "Player wins!"
            confirmReset      
        else aiTurn newBoard

aiTurn :: [Char] -> IO ()
aiTurn board = do
    newBoard <- setAiPos board 
    if checkWon newBoard then do
        putStrLn "AI wins!"
        confirmReset
    else playerTurn newBoard

confirmReset :: IO ()
confirmReset = do
    putStrLn "Would you like to play again? (y/n)"
    ch <- getLine
    if ch == "y" then main
    else if ch == "n" then return ()
    else do
        putStrLn "Not y or n!"
        confirmReset

setPlayerPos :: [Char] -> Int -> [Char]
setPlayerPos pos idx = replaceNth idx 'x' pos

setAiPos :: [Char] -> IO [Char]
setAiPos pos = do
    x <- randomRIO (0, 8)
    return $ replaceNth x 'o' pos

showBoard :: [Char] -> String
showBoard xs = concatMap (uncurry showLocation) (zip xs [1..])

showLocation :: Char -> Int -> [Char]
showLocation loc n 
    | n `mod` 3 == 0 = [loc] ++ "\n"
    | otherwise =      [loc] ++ " "

reset :: [Char] -> [Char]
reset [] = []
reset (x:xs) = ' ':reset xs

replaceNth :: Int -> Char -> [Char] -> [Char]
replaceNth _ _ [] = []
replaceNth n newVal (x:xs)
  | n == 0 = newVal:xs
  | otherwise = x:replaceNth (n-1) newVal xs

checkWon :: [Char] -> Bool
checkWon pos = True

-- checkHorizontal :: [Char] -> Bool

-- checkVertical :: [Char] -> Bool

-- checkDiagonal :: [Char] -> Bool