numbers = list(range(0, 100))
final_number = 0

def multiples(var):
    if var % 3 == 0 or var % 5 == 0:
        print(var)
        return var
    else:
        return 0

for i in numbers:
    final_number = final_number + multiples(i)
    print(final_number)
