multiples_of_3_or_5 = lambda x: x % 3 == 0 or x % 5 == 0

total = sum(filter(multiples_of_3_or_5, range(0, 100)))
print(total)
