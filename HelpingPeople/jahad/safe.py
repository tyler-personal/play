def safe_input(str, f, err="Invalid input, try again."):
  result = input(str)

  if f(result):
    return result
  else:
    print(err)
    return safe_input(str, f, err)

x = safe_input("What's your name?", lambda name: len(name) > 3, err="Invalid name, must be at least 4 characters long")

print(x)

