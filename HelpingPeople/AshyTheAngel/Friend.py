def friend(x):
    for name in x[:]:
        print(name)
        if len(name) == 4:
            print(f"Y: {name}")
            break
        else:
            print(f"N: {name}")
            x.remove(name)
    return x
def friend2(names):
  return [n for n in names if len(n) == 4]

def friend3(names):
  return list(filter(lambda n: len(n) == 4, names))

names = ["potato", "oof", "yaya"]
new_names = friend3(names)
print(new_names)
