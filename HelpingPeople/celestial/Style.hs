{-# LANGUAGE LambdaCase, QuasiQuotes #-}
module Style where

import Data.String.Interpolate
import Data.List.Split
import Data.List

styles = \case
  "BK" -> "Black"
  "GY" -> "Grey"
  "BL" -> "Blue"
  "CL" -> "Clear"
  "TG" -> "Tempered Glass"
  "RGD" -> "Rose Gold"
  "Metal GY" -> "Metal Grey"

rearrange input = [i|#{brand} #{productType} for #{unwords item} - #{styles style}|]
  where
    productType' : brand : style : item = words input
    productType = unwords . splitOn "-" $ productType'

main = print $ rearrange "Screen-Protector Spigen TG Google Pixel 4"
