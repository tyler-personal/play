from random import randint

input_num = input("Please enter a 2 digit number: ")
random_num = str(randint(10, 99))

print(f"Your random number is: {random_num}")

if any(x for x in input_num for y in random_num if x == y):
    print("you won")
else:
    print("you lost")
