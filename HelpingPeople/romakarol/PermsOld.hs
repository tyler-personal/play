{-# LANGUAGE FlexibleContexts #-}
module Permutations where

import Data.List
import Data.Foldable

tripleToList (x,y,z) = [x,y,z]

none :: Foldable t => (a -> Bool) -> t a -> Bool
none f = not . all f

hasDuplicates :: (Eq a, Foldable t) => t a -> Bool
hasDuplicates xs
  | length xs > 1 = length (nub (toList xs)) > 1
  | otherwise = False

uniqueTriple (a,b,c) = not $ (a == b) && (b == c)

noTriples :: String -> Bool
noTriples str = none hasDuplicates . map tripleToList $ zip3 str (drop 1 str) (drop 2 str)

generateString :: Int -> Int -> Int -> Maybe String
generateString a b c = find noTriples (permutations initialString)
  where initialString = replicate a 'a' ++ replicate b 'b' ++ replicate c 'c'

