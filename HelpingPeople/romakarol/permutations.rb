def noTriple(t)
  t[0] != t[1] and t[1] != t[2]
end

def noTriples(xs)
  xs.zip(xs.drop(1), xs.drop(2)).filter { |x| x.none? { |e| e.nil? } }.all? { |x| noTriple x }
end

def generateString(a, b, c)
  ('a' * a + 'b' * b + 'c' * c).chars.permutation.find { |x| noTriples x }&.join
end


puts generateString(4,2,2)
