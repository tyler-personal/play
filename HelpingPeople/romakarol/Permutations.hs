module Permutations where

import Data.List

noTriple (a,b,c) = a /= b && b /= c

noTriples xs = all noTriple $ zip3 xs (drop 1 xs) (drop 2 xs)

generateString a b c = find noTriples . permutations $
  replicate a 'a' ++ replicate b 'b' ++ replicate c 'c'


