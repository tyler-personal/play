from itertools import permutations

def noTriple(t):
    return not t[0] == t[1] == t[2]

def noTriples(xs):
    return all(noTriple(ys) for ys in zip(xs, xs[1:], xs[2:]))

def generateString(a, b, c):
    initialString = "a" * a + "b" * b + "c" * c
    return [''.join(p) for p in permutations(initialString) if noTriples(p)][0]

print(generateString(4,2,2))
