import numpy as np
from collections import namedtuple

data = [[1,2,3], [1, 0, 2], [3, 3, 3]]
Cell = namedtuple('Cell', 'value row column')

cells = [Cell(value=value, row=row, column=column)
         for row, rowData in enumerate(data)
         for column, value in enumerate(rowData)]
min_cell = min(cells, key=lambda c: c.value)

print(min_cell)
xs = [0,1,3,4,1,-1]
print(np.argmin(xs))
