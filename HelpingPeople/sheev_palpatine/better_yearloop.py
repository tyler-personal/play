def year_loop():
    past_year = int(input("Choose a random year in the past."))
    current_year = int(input("What's the current year?"))

    print(f"It's been {current_year-past_year} years")

    if input("Do you wish to do it again?") in ["Yes", "yes"]:
        year_loop()
    else:
        print("Alright, bye!")

year_loop()
