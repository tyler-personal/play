module FirstLast where


(<&>) :: Functor f => f a -> (a -> b) -> f b
(<&>) = flip fmap
firstLast items = [head items, last items]
f = ([head, last] <&>)

firstLasa = ([head,last] <&>)

main = print $ firstLast [1,2,3,4,5]
