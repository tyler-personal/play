from tkinter import *

import time

HEIGHT = 500
WIDTH = 600


def on_button_click():
    try:
        sec = int(entry.get())

        while sec > 0:
            m, s = divmod(sec, 60)
            h, m = divmod(m, 60)
            time_left = str(h).zfill(2) + ":" + str(m).zfill(2) + ":" + str(s).zfill(2)
            sec -= 1
            time.sleep(1)
            window.update()
            label["text"] = time_left
    except:
        label["text"] = "NaN"


window = Tk()
window.title("Countdown Timer")

canvas = Canvas(window, height=HEIGHT, width=WIDTH)
canvas.pack()

# background= tk.PhotoImage(file="night.gif")
# background_label=tk.Label(window, image=background)
# background.place(relwidth=1,relheight=1)

frame = Frame(window, bg="#80c1ff", bd=5)
frame.place(relx=0.5, rely=0.1, relwidth=0.75, relheight=0.1, anchor="n")

entry = Entry(frame, )
entry.place(relwidth=0.65, relheight=1)

button = Button(frame, text="Test Button", command=on_button_click)
button.place(relx=0.7, relheight=1, relwidth=0.3)

frame2 = Frame(window, bg="#80c1ff", bd=10)
frame2.place(relx=0.5, rely=0.25, relwidth=0.75, relheight=0.6, anchor="n")

label = Label(frame2, font=("Courier", 44))
label.place(relwidth=1, relheight=1, )

window.mainloop()
