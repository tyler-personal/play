secret_word = "Cadrega"
secret_word = secret_word.lower()
len_word = int(len(secret_word))
spaces = len_word * "._."
symbol = "._."

print(spaces)

first_letter = input("Enter your guess: ")

if first_letter == secret_word[0]:
    len_word -= 1
    spaces = len_word * "._."
    print("." + secret_word[0] + "." + spaces)
elif first_letter == secret_word[1]:
    len_word -= 2
    spaces = len_word * "._."
    print(symbol + "." + secret_word[1] + "." + spaces)
elif first_letter == secret_word[2]:
    len_word -= 3
    spaces = len_word * "._."
    print(2 * symbol + "." + secret_word[2] + "." + spaces)
elif first_letter == secret_word[3]:
    len_word -= 4
    spaces = len_word * "._."
    print(3 * symbol + "." + secret_word[3] + "." + spaces)
elif first_letter == secret_word[4]:
    len_word -= 5
    spaces = len_word * "._."
    print(4 * symbol + "." + secret_word[4] + "." + spaces)
elif first_letter == secret_word[5]:
    len_word -= 6
    spaces = len_word * "._."
    print(5 * symbol + "." + secret_word[5] + "." + spaces)
elif first_letter == secret_word[6]:
    len_word -= 7
    spaces = len_word * "._."
    print(6 * symbol + "." + secret_word[6] + "." + spaces)
elif first_letter == secret_word[7]:
    len_word -= 8
    spaces = len_word * "._."
    print(7 * symbol + "." + secret_word[7] + "." + spaces)
