def indexes(items, item_to_find):
    return [i for i, item in enumerate(items) if item == item_to_find]

word = "cadrega"
guess = ["_" for _ in word]

while "_" in guess:
    print(''.join(guess))
    char = input("Guess a letter: ")
    if char in word:
        guess = [char if i in indexes(word, char) else c for i, c in enumerate(guess)]

print("You got it!")
print(''.join(guess))


