{-# LANGUAGE NoMonomorphismRestriction #-}

module Hangman where

input = (>> getLine) . putStrLn

word = "cadrega"

complete = ('_' `notElem`)

check valid '_' new | valid == new = new
check _ found _ = found

run guess
  | complete guess = putStrLn $ "You got it!\n" ++ guess
  | otherwise = do
    c <- head <$> input (guess ++ "\nGuess a letter: ")
    run $ map (\(valid, current) -> check valid current c) (zip word guess)

main = run $ map (const '_') word
