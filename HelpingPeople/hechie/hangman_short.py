from more_itertools import locate

word = "cadrega"
guess = ["_" for _ in word]

while "_" in guess:
    print(''.join(guess))
    if (char := input("Guess: ")) in word:
        indicies = locate(word, lambda x: x == char)
        guess = [char if i in indicies else c for i,c in enumerate(guess)]
print("You win!")



