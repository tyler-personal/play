word = 'cadrega'
guesses = set()

while (guess := ''.join(char if char in guesses else '_' for char in word)) != word:
  print(guess)
  guesses.add(input('Guess a letter: '))

print('You got it!')
print(''.join(guess))

