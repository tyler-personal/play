{-# LANGUAGE NoMonomorphismRestriction #-}
module HangmanNew where
import Control.Monad.Loops

complete = ('_' `notElem`)

check guess correct current
  | guess == correct = guess
  | otherwise = current

run word state = do
  putStr $ state ++ "\nGuess a letter: "
  guess <- head <$> getLine
  pure $ zipWith (check guess) word state

main = let word = "cadrega" in iterateUntilM complete (run word) (replicate (length word) '_')
