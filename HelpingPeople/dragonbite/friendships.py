friendships = [(0, 1), (0, 2), (1, 2), (1, 3), (2, 3), (3, 4), (4, 5), (5,6), (5, 7), (6, 8), (7, 8), (8, 9)]
students = { 0: "Nynke", 1: "Lolle", 2: "Jikke", 3: "Popke", 4: "Teake", 5: "Lieuwe", 6: "Tsjabbe", 7: "Klaske", 8: "Ypke", 9: "Lobke" }

index_to_friend_count = [sum(1 for f in friendships if id in f) for id in students.keys()]
result = [(students[i], x) for i, x in enumerate(index_to_friend_count)]
sorted_result = sorted(result, key = lambda x: x[1], reverse=True)

for name, count in sorted_result:
    print(f"{name}: {count}")


# second version
result = sorted([(students[i], x) for i,x in enumerate([sum(1 for f in friendships if id in f) for id in students.keys()])], key = lambda x: x[1], reverse = True)
print(result)
