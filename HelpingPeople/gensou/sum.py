# short
n = int(input("number: "))
print(f"sum: {sum(x for x in range(n+1))}\nsum of cubes: {sum(x**3 for x in range(n+1))}")

# mine
def sum_of_1_to_n(n):
    return sum(x for x in range(n + 1))

def sum_of_cubes_of_1_to_n(n):
    return sum(x**3 for x in range(n + 1))

n = int(input("val: "))
print(f"sum of first {n} numbers: {sum_of_1_to_n(n)}")
print(f"sum of first {n} cubes: {sum_of_cubes_of_1_to_n(n)}")

# his (refactored a bit)
def SumN(number):
    numberx = 1
    x = number
    Snum = 0
    count = 1
    if number <= 0:
        print("please input a positive number.")
    if number == 1:
        print("The sum is 1")
    if number >= 2:
        while (count <= number):
            Snum = (Snum+number)
            number = (number-1)
            if (count-1) != number:
                continue
            print("Sum of the First", x, "Natural Numbers is", Snum)


def SumNCubes(number):
    numberx = 1
    x = number
    Snum = 0
    count = 1
    if number <= 0:
        print("please input a positive number.")
    if number == 1:
        print("The sum is 1")
    if number >= 2:
        for x in range(1, number+1):
            Snum = number**3+Snum
            number = (number-1)
            if (count-1) != number:
                continue
            print("Sum of the First", x, "Natural Numbers is", Snum)

number = int(input("Sum of the First n Natural Numbers: "))
SumN(number)
SumNCubes(number)
