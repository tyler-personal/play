import System.Exit

input :: Read a => String -> IO a
input str = do
  putStrLn str
  read <$> getLine

type BenefitLevel = Char
type Coverage = Char
type Children = Char

main = do
  clientID <- input "Enter client ID or 0 to quit"
  benefitLevel <- input "Enter plan level as L, R or H (Low, Regular, High)"
  coverage <- input "Enter S for single or F for family"

  children <- if coverage == 'S'
    then return Nothing
    else do
      result <- input "Enter Y for children or N for no children"
      return $ Just result

  print $ loop benefitLevel coverage children
  putChar clientID

  putStrLn "test"

loop :: BenefitLevel -> Coverage -> Maybe Children -> Double
loop 'L' 'S' Nothing = 36.25
loop 'L' 'F' (Just 'N') = 56.50
loop 'L' 'F' (Just 'Y') = 98.35
loop 'R' 'S' Nothing = 48.90
loop 'R' 'F' (Just 'N') =  74.70
loop 'R' 'F' (Just 'Y') = 136.75
loop 'H' 'S' Nothing = 69.80
loop 'H' 'F' (Just 'N') = 99.45
loop 'H' 'F' (Just 'Y') = 174.55
loop _ _ _ = error "You can't have children if you don't select family"


