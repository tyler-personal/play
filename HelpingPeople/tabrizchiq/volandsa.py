from math import pi

def processWhat(r,h):
  if h == 0:
    processSphere(r)
  else:
    processCylinder(r,h)
    volume = getConeVolume(r,h)
    area = getConeArea(r,h)
    return volume,area

def processSphere(r):
  volume = (4/3)*pi*r**3
  area = 4*pi*r**2
  print("The volume of a sphere with radius %.2f" % r,"cm is %.2f" % volume,"cm^3")
  print("The surface area of a sphere with radius %.2f" % r,"cm is %.2f" % area,"cm^2")

def processCylinder(r,h):
  volume = (pi*r*r*h)
  area = (2*pi*r*h + 2*pi*r*r)
  print("The volume of a cylinder with radius", r,"cm and height", h,"is %.2f" % volume,"cm^3")
  print("The surface area of a cylinder with radius %.2f" % r, "cm and height %.2f" % h,"is %.2f" % area,"cm^2")
  print()

def getConeArea(r,h):
  return pi*r*(r+(h*h+r*r)**(.5))


def getConeVolume(r,h):
  return pi*r*r*h/3


def main():
  print("Computing the volume and surface area of a sphere or cylinder (and cone): ")
  r = float(input("Enter radius: "))
  h = float(input("Enter height (0 for sphere): "))
  if h !=0:
    conevolume,conearea = processWhat(r,h)
    print("The volume of a cone with radius %.2f" % r, "cm and height %.2f" % h, "cm is %.2f" % conevolume, "cm^3")
    print("The surface area of a cone with radius %.2f" % r, "cm and height %.2f" % h, "is %.2f" % conearea,"cm^2")
  else:
    processWhat(r,h)




main()
