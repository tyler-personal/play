from functools import reduce
from operator import mul
from typing import *

def prod(nums): # remove in Python 3.8
    return reduce(mul, nums)

# for requirements
def processStr(digits: str):
    nums = map(int, digits)
    for kind, value in [("Sum", sum(nums)), ("Product", prod(nums))]:
        print(f"{kind} of the digits in {digits} is {value}")

def getHighest(digits: str):
    return max(map(int, digits))

def getHasRepeatDigit(digits: str):
    has_repeat = any(set(x for x in digits if digits.count(x) > 1))
    return getHighest(digits) if has_repeat else 0


# way I'd actually do it

def print_sum_and_product(nums: List[int]):
    for kind, value in [("Sum", sum(nums)), ("Product", prod(nums))]:
        print(f"{kind} of the digits in {nums} is {value}")

def has_repeat(xs):
    return (len(xs) - len(set(xs))) > 0

def main_loop():
    user_input = input("Enter a string of digits or 'quit' to stop: ")
    if user_input != "quit":
        nums = list(map(int, user_input))

        print_sum_and_product(nums)
        print(f"The highest digit is: {max(nums)}")

        repeat = "" if has_repeat(nums) else "no "
        print(f"The string has {repeat}repeated digits")

        main_loop()

main_loop()
