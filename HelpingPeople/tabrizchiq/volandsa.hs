{-# LANGUAGE DuplicateRecordFields, RecordWildCards, NamedFieldPuns #-}
module Volandsa where
import Numeric

data Shape = Shape
  { volume :: Double
  , area :: Double
  , name :: String
  }

sphere radius = Shape
  { volume = (4 / 3) * pi * radius
  , area = 4 * pi * radius ^ 2
  , name = "sphere"
  }

cylinder radius height = Shape
  { volume = pi * radius ^ 2 * height
  , area = (2 * pi * radius * height) + (2 * pi * radius ^ 2)
  , name = "cylinder"
  }

cone radius height = Shape
  { volume = pi * radius ^ 2 * height / 3
  , area = pi * radius * (radius + sqrt (height ^ 2 + radius ^ 2))
  , name = "cone"
  }

displayFunc str f s@Shape {..} radius height =
  "The " ++ str ++ " of a " ++ name ++ " with radius "
  ++ showFloat radius ++ "cm and height "
  ++ showFloat height ++ "cm is " ++ showFloat (f s) ++ "cm^3"
  where showFloat num = showFFloat (Just 2) num ""

display radius height shape = do
  putStrLn $ displayFunc "volume" volume shape radius height
  putStrLn $ displayFunc "area" area shape radius height

main = do
  radius <- input "Enter radius: "
  height <- input "Enter height (0 for sphere): "

  display radius height $ determineShape radius height
  display radius height $ cone radius height

  where
    determineShape radius 0 = sphere radius
    determineShape radius height = cylinder radius height

input str = do
  putStr str
  readLn

