def processStr(x):
  sum = 0
  product = 1
  for i in x:
    sum +=int(i)
    product *=int(i)
  print("Sum of the digits in", x,"is",sum)
  print("Product of the digits in", x,"is", product)


def getHighest(x):
  highest = 0
  for i in x:
    if int(i) > highest:
      highest = int(i)
  print("The highest digit in the given string is:", highest)


def getHighestRepeatDigit(highest):
  found_repeat = False
  for i, x in enumerate(highest):
    for ii, y in enumerate(highest):
      if x == y and i != ii:
        found_repeat = True
        break
  if found_repeat:
    print("The string has repeated digits.")
  else:
    print("This string has no repeated digits.")


def main():
  x = ''
  while x != 'quit':
    x = input("Enter a string of digits to process or 'quit' to stop: ")
    if x != 'quit':
      processStr(x)
      getHighestRepeatDigit(x)
      getHighest(x)

main()
