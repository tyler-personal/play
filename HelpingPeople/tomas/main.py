xs = [1,2]
ys = [0] * 5
new_ys = [1 if i in xs else 0 for i, e in enumerate(ys)]
print(new_ys)

def track_indicies(xs, max_length = 5):
  return [1 if i in xs else 0 for i,e in enumerate([0] * max_length)]

def track_all(ys):
    max_num = max(sum(ys, []))
    print(max_num)
    return list(map(lambda xs: track_indicies(xs, max_num), ys))

data = [[1], [0, 3], [], [1]]
print(track_all(data))
