module Last where

data MyName = Jim | John
myFunc Jim = "Hi I'm Jim!"
myFunc John = "Hi I'm John!"
main = print 4

myLast (x : xs) = case xs of
  [] -> error "empty"
  (y : ys) -> myLast(ys)
