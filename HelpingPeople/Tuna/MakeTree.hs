module MakeTree where

data Tree a = Nil | Node a (Tree a) (Tree a)

isLeaf (Node _ Nil Nil) = True
isLeaf _ = False

deleteValue v Nil = Nil
deleteValue v node@(Node val left right)
  | v == val = killRoot node
  | otherwise = Node val (deleteValue v left) (deleteValue v right)

killRoot Nil = Nil
killRoot (Node _ Nil Nil) = Nil
killRoot (Node _ Nil right) = right
killRoot (Node _ left Nil) = left
killRoot (Node _ left@(Node lVal _ _) right) =
  Node lVal (deleteValue lVal left) right



