{-# LANGUAGE LambdaCase #-}
module Fold where

import Prelude hiding (foldl, foldr)

foldl _ initial [] = initial
foldl f initial (x:xs) = foldl f (f initial x) xs

foldl2 f initial = \case
  [] -> initial
  (x:xs) -> foldl f (f initial x) xs

foldr _ i [] = i
foldr f i (x:xs) = f x (foldr f i xs)
