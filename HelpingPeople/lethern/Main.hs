{-# LANGUAGE RecordWildCards, LambdaCase, MultiWayIf #-}
import Data.Map
import Data.List (find)
import Control.Lens
import Control.Monad.State


data FormState = FormState { formType :: Int, companyID :: Int, stepID :: Int, processId :: Int, form :: Form }
data Form = Form { header :: Row, lines :: [Row], rules :: [Rule] }
data Field = Field { label :: String, fieldType :: String, value :: Maybe String }

type Row = [Field]
type Rule = (Form -> Form)

mField Form{..} = find ((== "supplier_dax_id") . label) header

companyBsRule :: [FormState] -> [FormState]
companyBsRule = traverse %~ \state@FormState{..} -> if
  | formType `elem` [6,16] && stepID == 191 -> undefined
  | otherwise -> state

main = print 4
