def get_num(msg):
    num = input(msg)
    if not num.isdigit():
        print("You have entered an invalid number.")
        return get_num(msg)
    return int(num)

get_salary = lambda: get_num("Please enter your beginning salary: ")

def main():
    salary = get_salary()

    while salary <= 10000:
        print("Sorry your beginning salary must be at least 10,000")
        salary = get_salary()
