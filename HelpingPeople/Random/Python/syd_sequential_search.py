first_booth_voters = [1,2,3,4,5]
second_booth_voters = [3,4,5,6,7]
fraud_votes = []
comparisons = 0

def f1(xs, ys, z):
    pos_x = 0
    pos_y = 0

    while pos_x < len(xs):
        while pos_y < len(ys):
            if xs[pos_x] == ys[pos_y] == z:
                return True
            pos_y += 1
        pos_x += 1
        pos_y = 0
    return False

def f2(xs, ys, z):
    for x in xs:
        for y in ys:
            if x == y == z:
                return True
    return False

def f3(xs, ys, z):
    return any(x == y == z for x in xs for y in ys)

data = [int(x) for x in input().split()]
print(data)

data2 = [int(x) for x in input().split()]
print(data2)

print(f1(data, data2, 4))
print(f2(data, data2, 4))
print(f3(data, data2, 4))

f = lambda xs, ys, z: any(x == y == z for x in xs for y in ys)
