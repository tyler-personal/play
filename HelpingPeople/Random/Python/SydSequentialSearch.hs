f1 xs ys z = do
  (pos_x, pos_y) <- get
f xs ys z = any (\x -> any (\y -> (x == y) && (y == z)) ys) xs
main = print $ f [1,2,3,4,5] [2,3,4,5] 4

