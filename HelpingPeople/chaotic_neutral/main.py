class Node:
    def __init__(self, val):
        self.val = val
        self.next = None

def is_empty(head):
    return head == None

def last(head):
    while head.next != None:
        head = head.next
    return head

def merge(head1, head2):
    last(head1).next = head2

def filter(predicate, head):
    previous = None
    current = head

    while current != None:
        next = current.next

        if not predicate(current) and previous == None:
            head = next
        elif not predicate(current):
            previous.next = next
        else:
            previous = current

        current = next

    return head

# nonfunctional, quicksort on greater causes infinite loop.
def quicksort(head):
    if is_empty(head):
        return None
    else:
        lesser = quicksort(filter(lambda node: node.val < head.val, head.next))
        greater = filter(lambda node: node.val >= head.val, head.next)

        if is_empty(lesser) and is_empty(greater):
            return head
        elif is_empty(lesser):
            head.next = greater
            return head
        elif is_empty(greater):
            last(lesser).next = head
            return lesser
        else:
            last(lesser).next = head
            head.next = greater

            return lesser

def show(head):
    while head != None:
        print(f"Node: {head.val}")
        head = head.next

list_1 = Node(4)
node2 = Node(7)
node3 = Node(14)
node4 = Node(17)

list_1.next = node2
node2.next = node3
node3.next = node4

list_2 = Node(1)
node6 = Node(4)
node7 = Node(2)
node8 = Node(114)

list_2.next = node6
node6.next = node7
node7.next = node8

show(quicksort(list_2))
