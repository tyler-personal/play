module His where

throttleBy minInterval f [] = []
throttleBy minInterval f (x : xs) = x : throttleBy minInterval f (dropWhile (\x' -> f x' < f x + minInterval) xs)

