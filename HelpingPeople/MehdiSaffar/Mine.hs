{-# LANGUAGE ExtendedDefaultRules #-}
module Mine where


throttleBy minInterval f = foldl next []
  where
    next acc x
      | null acc = [x]
      | f (last acc) + minInterval > f x = acc
      | otherwise = acc ++ [x]

throttleZy minInterval f = foldl next []
  where
    next acc x
      | null acc || f (last acc) + minInterval < f x = acc ++ [x]
      | otherwise = acc

-- not the same
throttleBy2 interval f = foldl (\xs x -> if null xs || f (last xs) + interval < f x then [x] else []) []


throttleBy3 interval f = foldl (\xs x -> xs ++ [x | null xs || f (last xs) + interval <= f x]) []

-- not the same
throttleBy4 interval f = concat . scanl (\xs x -> [x | null xs || f (last xs) + interval <= f x]) []

