import Data.Char

mock' :: String -> String
mock' = snd . foldl (\(flag,ans) c -> (not flag, ans++[cap flag c])) (True, "")
  where
    cap True = toUpper
    cap False = id


main = print 4
