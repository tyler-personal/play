{-# LANGUAGE LambdaCase, QuasiQuotes, NoMonomorphismRestriction, ExtendedDefaultRules #-}
import Data.List (permutations, find)
import Control.Monad (replicateM)
import Control.Applicative (liftA2)
import Data.String.Interpolate

-- SHARED
input = (>> readLn) . putStrLn

-- READABLE
f xs target = extractIndexes <$> find properNumbers combinations
  where
    extractIndexes [(i, _), (j, _)] = (i,j)
    properNumbers [(i, x), (j, y)] = x + y == target && i /= j
    combinations = replicateM 2 (zip [0..] xs)

main' = do
  nums <- input "Enter a list of nums"
  target <- input "Enter a target value"

  case g nums target of
    Just indicies -> putStrLn [i|Indexes that add to target value: #{indicies}|]
    Nothing -> putStrLn "No valid combination"

-- SHORT
g xs z = (\[(i,_), (j,_)] -> (i,j)) <$> find (\[(i,x), (j,y)] -> x + y == z && i /= j) (replicateM 2 (zip [0..] xs))


main = liftA2 g (input "Enter a list of nums") (input "Enter a target num") >>= \case
  Just indicies -> putStrLn [i|Indexes that add to target value: #{indicies}|]
  Nothing -> putStrLn "No valid combination"

-- SUPER SHORT (does values not indicies)
_h xs z = find (\[x,y] -> x + y == z) (replicateM 2 xs)
main2 = liftA2 (\xs z -> find (\[x,y] -> x + y == z) (replicateM 2 xs)) readLn readLn >>= print
main3 = print $ find (\[x,y] -> x + y == 9) (replicateM 2 [2,7,11,15])
