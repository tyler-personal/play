module Triple where

square x = x * x

pyth x y = square x + square y

isTriple x y z = pyth x y == square z

