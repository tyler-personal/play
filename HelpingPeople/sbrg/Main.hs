module Fact where

import Text.Printf(printf)
import Data.List (intersperse)

fact :: Int -> String
fact 0 = "0! = 1"
fact n = printf "%d! = %d = %s" n res expr
 where
  ns = [n, n - 1 .. 1]
  res = product ns
  expr = unwords . intersperse "*" . map show $ ns
