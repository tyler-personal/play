from collections import Counter

def count(filename, k):
    words = open(filename).read().split(' ')
    c = Counter(words)

    for word in words[:k]:
        print(f"{word}: {c[word]}")
def count2(filename, k):
    all_words = open(filename).read().split(' ')
    c = Counter(all_words)
    print(c)
    search_words = all_words[:k]
    counts = {word : 0 for word in search_words}

    for word in all_words:
        if word in search_words:
            counts[word] += 1

    for word, count in counts.items():
        print(f"{word}: {count}")


count("test.txt", 2)
