from inspect import cleandoc

SEPARATOR = "=" * 80

def get_input(message, validation, error):
    user_input = input(message)
    if not validation(user_input):
        print(error)
        return get_input(message, validation, error)
    return user_input

def check_for_collecting_bugs():
    return get_input("Do you want to collect bugs today? (y/n)",
                     validation=lambda x: x.lower() in ["y", "n"],
                     error="You can only enter y/n")

def get_bugs_for_today(days):
    return int(get_input(f"Enter the number of bugs collected for Day #{days + 1}",
                         validation=lambda x: x.isdigit() and int(x) > 0,
                         error="Please enter a positive number."))

def output_results(totalBugs, days):
    outputFile.write(cleandoc(f"""
        {SEPARATOR}
        A total of {totalBugs} bugs were collected in {days} days.
        {SEPARATOR}
        {SEPARATOR}
        Assignment #07 - WHILE LOOP VERSION
        {SEPARATOR}
    """))
    outputFile.close()

answer = check_for_collecting_bugs()
totalBugs = 0
days = 0
outputFile = open("PYTHON_WHILE_LOOP_BUGS_CHECK_DATA_OUTPUT.txt", "w")

while answer.lower() == "y":
    bugs = get_bugs_for_today(days)

    totalBugs += bugs
    outputFile.write(f"{SEPARATOR}\nBugs Collected for Day #{days} = {format(bugs, '4d')}\n")
    days = (days + 1)

    answer = check_for_collecting_bugs()

output_results(totalBugs, days)


# OBSOLETE below here
def old_get_user_input():
    user_input = input("Do you want to collect bugs today? (y/n)")
    if user_input.lower() not in ["y", "n"]:
        print("You can only enter y/n")
        return old_get_user_input()
    return user_input

def old_get_bugs_for_today(days: int):
    try:
        bugs = int(input(f"Enter the number of bugs collected for Day #{days + 1} : "))
    except ValueError:
        print("You entered the wrong type of data! Please enter a positive number.")
        return get_bugs_for_today(days)
    else:
        if bugs > 0:
            return bugs
        else:
            print("Please enter a positive number")
            return old_get_bugs_for_today(days)from inspect import cleandoc
