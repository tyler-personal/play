import Data.Char
main = putStr "ye"

separator = replicate 80 '='

input msg validation err = do
  putStrLn msg
  user_input <- read <$> getLine
  if validation user_input
    then return user_input
    else do
      putStrLn err
      input msg validation err

checkForCollectingBugs = input "Do you want to collect bugs today? (y/n)" f "You can only enter y/n"
  where f x = toLower x `notElem` ['y', 'n']

