# INITIALIZE PROCESSED VARIABLES
totalBugs = 0
days = 0
# OUTPUT FILE
outFile = open("PYTHON_WHILE_LOOP_BUGS_CHECK_DATA_OUTPUT.txt", "w")
# =======================================================================================================
# Defining a function
def loop():
    while True:
        try:
            print("Do you want to collect bugs today? (Y or y mean yes, N or n mean no) ")
            loopVariable = (input())
        except ValueError:
            continue
        else:
            if (loopVariable == "y") or (loopVariable == "Y"):
                break
            else:
                if (loopVariable == "n") or (loopVariable == "N"):
                    break
                else:
                    print ("You can only put these answers. Y or y mean yes, N or n mean no")
                    continue
loop()
# =======================================================================================================
# FOR LOOP TO ACCUMULATE THE NUMBER OF BUGS COLLECTED IN A DESIGNATED NUMBER OF DAYS
while (loopVariable == "y") or (loopVariable == "Y"):
    days = (days +1)
    while True:
        try:
            # Input the number of bugs collected per day
            print("Enter the number of bugs collected for Day # " + str(days) + " : ")
            bugs = int(input())
        except ValueError:
            print ("You entered the wrong type of data! Please enter a positive number.")
            continue
        else:
            if (bugs <= 0):
                print("Please enter a positive number.")
                continue
            else:
                break
    # Accumulates or keeps a running sum of the number of bugs collected
    totalBugs = totalBugs + bugs
    outFile.write("="*80 + "\n")
    outFile.write("Bugs Collected for Day # " + format(days, "d") + " = " +format(bugs, "4d") + "\n")
    loop()
# Output the results
# =======================================================================================================
outFile.write("="*80 + "\n")
outFile.write("A total of " + str(totalBugs) + " bugs were collected in " + str(days) + " days.\n")
outFile.write("="*80 + "\n")
outFile.write(" \n")
outFile.write("="*80 + "\n")
outFile.write("Assignment #07 - WHILE LOOP VERSION\n")
outFile.write("="*80 + "\n")
outFile.close()

# =======================================================================================================
# END PROGRAM
