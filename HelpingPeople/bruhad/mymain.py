
# INITIALIZE PROCESSED VARIABLES
totalBugs = 0
days = 0
# OUTPUT FILE
outFile = open("PYTHON_WHILE_LOOP_BUGS_CHECK_DATA_OUTPUT.txt", "w")


# =======================================================================================================
# Defining a function
def loop():
    global loopVariable
    while True:
        try:
            print(
                "Do you want to collect bugs today? (Y or y mean yes, N or n mean no) "
            )
            loopVariable = (input())
        except ValueError:
            continue
        else:
            # we can do
            if loopVariable.lower() not in ["y", "n"]:
                # code
            else:
                break
            # the only reason I introduced `not` is so we could flip the order. So instead of
            if loopVariable.lower() in ["y", "n"]:
                break
            else:
                # code
            # this is to shorten it up a bit more, instead of checking very similar conditions, we're just checking if the lowercased variable is in the list of values
            if loopVariable.lower() in ["y", "n"]
            # the reason we're doing .lower() here is so we don't have to check for upper & lowercase, we can just lowercase the value and check against `y` or `n`
            if loopVariable.lower() == "y" or loopVariable.lower() == "n"
            # so below this line is what we're going to replace
            if (loopVariable == "y") or (loopVariable == "Y"):
                break
            else:
                if (loopVariable == "n") or (loopVariable == "N"):
                    break
                else:
                    print(
                        "You can only put these answers. Y or y mean yes, N or n mean no"
                    )
                    continue


loop()
# =======================================================================================================
# FOR LOOP TO ACCUMULATE THE NUMBER OF BUGS COLLECTED IN A DESIGNATED NUMBER OF DAYS
while (loopVariable == "y") or (loopVariable == "Y"):
    days = (days + 1)
    while True:
        try:
            # Input the number of bugs collected per day
            print("Enter the number of bugs collected for Day # " + str(days) +
                  " : ")
            bugs = int(input())
        except ValueError:
            print(
                "You entered the wrong type of data! Please enter a positive number."
            )
            continue
        else:
            if (bugs <= 0):
                print("Please enter a positive number.")
                continue
            else:
                break
    # Accumulates or keeps a running sum of the number of bugs collected
    totalBugs = totalBugs + bugs
    outFile.write("=" * 80 + "\n")
    outFile.write("Bugs Collected for Day # " + format(days, "d") + " = " +
                  format(bugs, "4d") + "\n")
    loop()
# Output the results
# =======================================================================================================
outFile.write("=" * 80 + "\n")
outFile.write("A total of " + str(totalBugs) + " bugs were collected in " +
              str(days) + " days.\n")
outFile.write("=" * 80 + "\n")
outFile.write(" \n")
outFile.write("=" * 80 + "\n")
outFile.write("Assignment #07 - WHILE LOOP VERSION\n")
outFile.write("=" * 80 + "\n")
outFile.close()

# =======================================================================================================
# END PROGRAM
