board = {letter: {number: '.' for number in range(8)} for letter in 'abcdefgh'}

for _, row in board.items():
  for _, dot in row.items():
    print(dot, end=" ")
  print()
