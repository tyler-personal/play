module Iterate where

myFunc f [] = []
myFunc f (x:xs) = f x : myFunc f xs

main = myFunc (\item -> item * 2) [1,2,3,4,5]


