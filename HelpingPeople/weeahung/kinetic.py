kinetic_energy = lambda mass, velocity: (mass * velocity**2) // 2

result = []

while True:
    try:
        x, y = (int(x) for x in input().split())

        if x < 0 or y < 0:
            break
        result.append(str(kinetic_energy(x,y)))
    except:
        result.append("Invalid")

print('\n' + '\n'.join(result))
