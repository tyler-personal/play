def diff(xs, ys):
    xs = list(xs)
    for y in ys:
        xs.remove(y)
    return xs

def threeSum(nums):
    options = {tuple(sorted([x,y,z]))
        for x in nums
        for y in diff(nums, [x])
        for z in diff(nums, [x,y])
        }

    return list(filter(lambda x: sum(x) == 0, options))

print(threeSum([-1,0,0,1,0,-1,-4,-2]))

