{-# LANGUAGE OverloadedLists #-}
module Sum where
import Prelude hiding (filter)
import Data.List

threeSum nums = filter ((==) 0 . sum) options
  where options = nub [sort [x,y,z] | x <- nums, y <- nums \\ [x], z <- nums \\ [x,y]]

main = print $ threeSum2 [-1,0,0,1,0,-1,-4,-2]


threeSum2 :: (Num a, Ord a) => [a] -> [[a]]
threeSum2 nums = filter(\x -> sum x == 0) options
  where
    options = nub $ concatMap (\x ->
      concatMap (\y ->
        map (\z -> sort [x,y,z]
          ) (nums \\ [x,y])
        ) (nums \\ [x])
      ) nums
