from collections import namedtuple

Device = namedtuple("Device", "address name result")

class Property:
    def __init__(number):
        self.number = number

        self.router = Device(f'10.100.{number}.1', 'router', 'null')
        self.dvr = Device(f'10.100.{number}.101', 'dvr', 'null')
        self.gate_controller = Device(f'10.100.{number}.103', 'gate_controller', 'null')

        self.devices = [router, dvr, gate_controller]

    def start_ping(self):
        for device in devices:
            process = subprocess.call("ping " + device.address)

            if process == 0:
                device.result = "green"
            else:
                device.result = "red"

            threading.Timer(1, start_ping).start()

class Dashboard(Screen):
    properties = [Property(x) for x in range(1, 6)]

    def start_ping(self):
        for prop in properties:
            threading.Timer(1, prop.start_ping).start()

    def check_color(self):
        for prop in properties:
            for device in prop.devices:
                if device.result == "green":
                    res = (0, 1, 0, 1)
                elif device.result == "red":
                    res = (1, 0, 0, 1)
                else:
                    res = (1, .5, .2, 1)

                setattr(self, f'router{prop.number.ToString("000")}', res)






class Dashboard(Screen):
    res1a = "null"
    res2a = "null"
    res3a = "null"

    res1b = "null"
    res2b = "null"
    res3b = "null"

    res1c = "null"
    res2c = "null"
    res3c = "null"

    res1d = "null"
    res2d = "null"
    res3d = "null"

    res1e = "null"
    res2e = "null"
    res3e = "null"

    def start_ping(self):
        threading.Timer(1, self.check_color).start()
        threading.Timer(1, self.prop_001).start()
        threading.Timer(1, self.prop_002).start()
        threading.Timer(1, self.prop_003).start()
        threading.Timer(1, self.prop_004).start()
        threading.Timer(1, self.prop_006).start()

    def check_color(self):
        # Property 001
        if Dashboard.res1a == "green":
            self.router001.color = (0, 1, 0, 1)
        elif Dashboard.res1a == "red":
            self.router001.color = (1, 0, 0, 1)
        else:
            self.router001.color = (1, .5, .2, 1)
        if Dashboard.res2a == "green":
            self.dvr001.color = (0, 1, 0, 1)
        elif Dashboard.res2a == "red":
            self.dvr001.color = (1, 0, 0, 1)
        else:
            self.dvr001.color = (1, .5, .2, 1)
        if Dashboard.res3a == "green":
            self.gate001.color = (0, 1, 0, 1)
        elif Dashboard.res3a == "red":
            self.gate001.color = (1, 0, 0, 1)
        else:
            self.gate001.color = (1, .5, .2, 1)

        # Property 002
        if Dashboard.res1b == "green":
            self.router002.color = (0, 1, 0, 1)
        elif Dashboard.res1b == "red":
            self.router002.color = (1, 0, 0, 1)
        else:
            self.router002.color = (1, .5, .2, 1)
        if Dashboard.res2b == "green":
            self.dvr002.color = (0, 1, 0, 1)
        elif Dashboard.res2b == "red":
            self.dvr002.color = (1, 0, 0, 1)
        else:
            self.dvr002.color = (1, .5, .2, 1)
        if Dashboard.res3b == "green":
            self.gate002.color = (0, 1, 0, 1)
        elif Dashboard.res3b == "red":
            self.gate002.color = (1, 0, 0, 1)
        else:
            self.gate002.color = (1, .5, .2, 1)

        # Property 003
        if Dashboard.res1c == "green":
            self.router003.color = (0, 1, 0, 1)
        elif Dashboard.res1c == "red":
            self.router003.color = (1, 0, 0, 1)
        else:
            self.router003.color = (1, .5, .2, 1)
        if Dashboard.res2c == "green":
            self.dvr003.color = (0, 1, 0, 1)
        elif Dashboard.res2c == "red":
            self.dvr003.color = (1, 0, 0, 1)
        else:
            self.dvr003.color = (1, .5, .2, 1)
        if Dashboard.res3c == "green":
            self.gate003.color = (0, 1, 0, 1)
        elif Dashboard.res3c == "red":
            self.gate003.color = (1, 0, 0, 1)
        else:
            self.gate003.color = (1, .5, .2, 1)

        # Property 004
        if Dashboard.res1d == "green":
            self.router004.color = (0, 1, 0, 1)
        elif Dashboard.res1d == "red":
            self.router004.color = (1, 0, 0, 1)
        else:
            self.router004.color = (1, .5, .2, 1)
        if Dashboard.res2d == "green":
            self.dvr004.color = (0, 1, 0, 1)
        elif Dashboard.res2d == "red":
            self.dvr004.color = (1, 0, 0, 1)
        else:
            self.dvr004.color = (1, .5, .2, 1)
        if Dashboard.res3d == "green":
            self.gate004.color = (0, 1, 0, 1)
        elif Dashboard.res3d == "red":
            self.gate004.color = (1, 0, 0, 1)
        else:
            self.gate004.color = (1, .5, .2, 1)

        # Property 006
        if Dashboard.res1e == "green":
            self.router006.color = (0, 1, 0, 1)
        elif Dashboard.res1e == "red":
            self.router006.color = (1, 0, 0, 1)
        else:
            self.router006.color = (1, .5, .2, 1)
        if Dashboard.res2e == "green":
            self.dvr006.color = (0, 1, 0, 1)
        elif Dashboard.res2e == "red":
            self.dvr006.color = (1, 0, 0, 1)
        else:
            self.dvr006.color = (1, .5, .2, 1)
        if Dashboard.res3e == "green":
            self.gate006.color = (0, 1, 0, 1)
        elif Dashboard.res3e == "red":
            self.gate006.color = (1, 0, 0, 1)
        else:
            self.gate006.color = (1, .5, .2, 1)
        threading.Timer(1, self.check_color).start()

    def prop_001(self):
        address = '10.100.1.1'
        res1 = subprocess.call("ping " + address)
        if res1 == 0:
            Dashboard.res1a = "green"
        else:
            Dashboard.res1a = "red"
        address = '10.100.1.101'
        res2 = subprocess.call("ping " + address)
        if res2 == 0:
            Dashboard.res2a = "green"
        else:
            Dashboard.res2a = "red"
        address = '10.100.1.103'
        res3 = subprocess.call("ping " + address)
        if res3 == 0:
            Dashboard.res3a = "green"
        else:
            Dashboard.res3a = "red"
        threading.Timer(1, self.prop_001).start()

    def prop_002(self):
        address = '10.100.2.1'
        res1 = subprocess.call("ping " + address)
        if res1 == 0:
            Dashboard.res1b = "green"
        else:
            Dashboard.res1b = "red"
        address = '10.100.2.101'
        res2 = subprocess.call("ping " + address)
        if res2 == 0:
            Dashboard.res2b = "green"
        else:
            Dashboard.res2b = "red"
        address = '10.100.2.103'
        res3 = subprocess.call("ping " + address)
        if res3 == 0:
            Dashboard.res3b = "green"
        else:
            Dashboard.res3b = "red"
        threading.Timer(1, self.prop_002).start()

    def prop_003(self):
        address = '10.100.3.1'
        res1 = subprocess.call("ping " + address)
        if res1 == 0:
            Dashboard.res1c = "green"
        else:
            Dashboard.res1c = "red"
        address = '10.100.3.101'
        res2 = subprocess.call("ping " + address)
        if res2 == 0:
            Dashboard.res2c = "green"
        else:
            Dashboard.res2c = "red"
        address = '10.100.3.103'
        res3 = subprocess.call("ping " + address)
        if res3 == 0:
            Dashboard.res3c = "green"
        else:
            Dashboard.res3c = "red"
        threading.Timer(1, self.prop_003).start()

    def prop_004(self):
        address = '10.100.4.1'
        res1 = subprocess.call("ping " + address)
        if res1 == 0:
            Dashboard.res1d = "green"
        else:
            Dashboard.res1d = "red"
        address = '10.100.4.101'
        res2 = subprocess.call("ping " + address)
        if res2 == 0:
            Dashboard.res2d = "green"
        else:
            Dashboard.res2d = "red"
        address = '10.100.4.103'
        res3 = subprocess.call("ping " + address)
        if res3 == 0:
            Dashboard.res3d = "green"
        else:
            Dashboard.res3d = "red"
        threading.Timer(1, self.prop_004).start()

    def prop_006(self):
        address = '10.100.6.1'
        res1 = subprocess.call("ping " + address)
        if res1 == 0:
            Dashboard.res1e = "green"
        else:
            Dashboard.res1e = "red"
        address = '10.100.6.101'
        res2 = subprocess.call("ping " + address)
        if res2 == 0:
            Dashboard.res2e = "green"
        else:
            Dashboard.res2e = "red"
        address = '10.100.6.103'
        res3 = subprocess.call("ping " + address)
        if res3 == 0:
            Dashboard.res3e = "green"
        else:
            Dashboard.res3e = "red"
        threading.Timer(1, self.prop_006).start()

