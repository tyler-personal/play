# -----level 1-----

print("Storage Express is Cool")

# -----level 2-----

dog = 3
bird = 4
if dog > bird:
    print("Dogs are better than birds")
else:
    print("Dogs are not better than birds")

# -----Level 3-----

animals = ["Dog", "Cat", "Rabbit", "Horse", "Lion"]

for animal in animals:
    print(animal)

# -----level 4-----

for number in range(1, 100):
    print(number)

# -----level 5-----

numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9]

for i in numbers:
    if i == 7:
        print("This number ate the number 9")
    elif (i % 2) == 0:
        print(str(i)+" is an even number")
    else:
        print(str(i)+" is an odd number")

# -----Bonus Level-----


def error():
    print("There was an error")

counters = [f"The {person} counts to " for person in "boy girl man woman dog cat CEO".split()]

for index, counter in enumerate(counters):
    if index in [0, 1]:
        maximum = 5
    elif index in [2, 3]:
        maximum = 25
    elif index in [4, 5]:
        maximum = 10
    elif index == 6:
        maximum = 100
    else:
        raise Exception("There was an error")

    for number in range(1, maximum):
        print(f"{counter} {number}")
    print("-------------------------------------")

