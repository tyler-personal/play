{-# LANGUAGE LambdaCase, MultiWayIf #-}
import Data.List
import Data.Monoid
import Control.Monad
import Data.IORef

data Coin = P1 | P2 | P5 | P10 | P20 | P50 | P100 | P200
  deriving (Enum, Show, Ord, Eq)

value :: Coin -> Int
value = \case
  P1   -> 1
  P2   -> 2
  P5   -> 5
  P10  -> 10
  P20  -> 20
  P50  -> 50
  P100 -> 100
  P200 -> 200

permutationsUpTo :: Coin -> [[Coin]]
permutationsUpTo _coin = nub $ f [] (value _coin) [] _coin
  where
    f :: [Coin] -> Int -> [Int] -> Coin -> [[Coin]]
    f coins target hitCoins coin
      | total + val > target  = []
      | total + val == target = if
        | val `elem` hitCoins -> [coin : coins]
        | otherwise           -> [coin : coins] <> lowerFs
      | otherwise             = nextCoins >>= f (coin : coins) target (val : hitCoins)
      where
        total = sum (map value coins)
        val = value coin
        nextCoins = reverse [P1 .. coin]
        lowerFs = tail nextCoins >>= f [] target (val : hitCoins)

main :: IO ()
main = do
  let result = permutationsUpTo P50
  print $ length result
--  forM_ result print

f :: [Coin] -> Int -> Coin -> [[Coin]]
f coins target largest
  | total + value largest > target = lowerFs
  | total + value largest == target = lowerFs <> [largest : coins]
  | otherwise = (nextCoins >>= f (largest : coins) target)

--  | total + value largestCoin > 200  = f current nextCoin valids
--  | current `elem` valids = error "invalid state, this shouldn't happen"
--  | total + value largestCoin == 200 = f [] (maximum current) (current : valids)

  where
    lowerFs = tail nextCoins >>= f [] target
    total = sum (map value coins)
    nextCoins = reverse [P1 .. largest]
--    nextCoin = reverse [P1 .. largestCoin] !! 1


originalMain = print . length . filter ((/= 200) . sum . map value) . permutations $ [P1 .. P200] >>= replicate (200)

main2 = do
--  print $ length coins
  print $ length coins2

coins2 = permutations $ r 20 P1 ++ r 10 P2 ++ r 4 P5 ++ r 2 P10 ++ r 1 P20 ++ r 4 P50 ++ r 2 P100 ++ [P200]
  where r = replicate

coins3 = undefined
