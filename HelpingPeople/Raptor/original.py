x = 200

no_of_configs_for_1_and_2 = {}

for i in range (0, x + 1) :

    no_of_configs_for_1_and_2[i] = i // 2 + 1

no_of_configs_for_1_2_and_5 = no_of_configs_for_1_and_2

for i in range (5, x + 1) :

    j = i
    k = 0

    while j >= 0 :
        k += no_of_configs_for_1_and_2[j]
        j -= 5

    no_of_configs_for_1_2_and_5.update({i : k})

no_of_configs_for_1_2_5_and_10 = no_of_configs_for_1_2_and_5

for i in range(10, x + 1) :

    j = i
    k = 0

    while j >= 0 :
        k += no_of_configs_for_1_2_and_5[j]
        j -= 10

    no_of_configs_for_1_2_5_and_10.update({i: k})

no_of_configs_for_1_2_5_10_and_20 = no_of_configs_for_1_2_5_and_10

for i in range(20, x + 1) :

    j = i
    k = 0

    while j >= 0 :
        k += no_of_configs_for_1_2_5_and_10[j]
        j -= 20

    no_of_configs_for_1_2_5_10_and_20.update({i: k})

no_of_configs_for_1_2_5_10_20_and_50 = no_of_configs_for_1_2_5_10_and_20

for i in range(50, x + 1):

    j = i
    k = 0

    while j >= 0:
        k += no_of_configs_for_1_2_5_10_and_20[j]
        j -= 50

    no_of_configs_for_1_2_5_10_20_and_50.update({i: k})

no_of_configs_for_1_2_5_10_20_50_and_100 = no_of_configs_for_1_2_5_10_20_and_50

for i in range(100, x + 1):

    j = i
    k = 0

    while j >= 0:
        k += no_of_configs_for_1_2_5_10_20_and_50[j]
        j -= 100

    no_of_configs_for_1_2_5_10_20_50_and_100.update({i: k})

no_of_configs_for_1_2_5_10_20_50_100_and_200 = no_of_configs_for_1_2_5_10_20_50_and_100

for i in range(200, x + 1):

    j = i
    k = 0

    while j >= 0:
        k += no_of_configs_for_1_2_5_10_20_50_and_100[j]
        j -= 200

    no_of_configs_for_1_2_5_10_20_50_100_and_200.update({i: k})

print(no_of_configs_for_1_2_5_10_20_50_100_and_200[200])
