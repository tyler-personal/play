{-# LANGUAGE NoMonomorphismRestriction #-}
module Main where

import Data.List (intercalate)

main = print 2

type Line = String

lines :: [Int] -> [Line]
lines numbers = map (line (length numbers)) allNumbers
  where
    allNumbers = map (`take` numbers) [1..length numbers - 1]

line :: Int -> [Int] -> Line
line longestNums nums = problem ++ spaces ++ " = " ++ solution
  where
    nums' = map show nums

    numberOfSpaces = (longestNums - length nums) * 4
    spaces = replicate numberOfSpaces ' '
    problem = intercalate " + " nums'
    solution = show $ sum nums

