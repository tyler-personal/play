def print_weather(check, weather, sunny, rainy, snowy, cloudy, _else):
    if check:
        if weather == "sunny":
            print(sunny)
        elif weather == "rainy":
            print(rainy)
        elif weather == "snowy":
            print(snowy)
        elif weather == "cloudy":
            print(cloudy)
        else:
            print(otherwise)

temp = int(input("temp?"))
weather = input("weather?")

print_weather(temp > 80, weather,
        sunny = "bring sunscreen",
        rainy = "bring your umbrella",
        snowy = "and no its not snowing!",
        cloudy = "", # THIS WAS MISSING
        _else = "It is a HOT day")

print_weather(temp <= 32, weather
        sunny = "wear your boots!",  # why
        rainy = "are you sure its now snowing?", # not?
        snowy = "wear your boots!",
        cloudy = "are you sure its now snowing?", # not?
        _else = "It is freezing today")

print_weather(temp <= 80 and temp > 32, weather,
        sunny = "It's a great day today!",
        rainy = "Be sure to bring your umbrella",
        snowy = "I don't think its snowing!",
        cloudy = "I don't think its snowing!", # wut
        _else = "") # THIS WAS MISSING
