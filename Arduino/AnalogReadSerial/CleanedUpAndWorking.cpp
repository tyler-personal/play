void on(int num) {
  digitalWrite(num, HIGH);
}

void off(int num) {
  digitalWrite(num, LOW);
}

enum Segment {
  A = 4, B = 3, C = 7, D = 9, E = 8, F = 5, G = 6, Last
};

const int segments[] {A,B,C,D,E,F,G};

void executeOnAll(void f(int)) {
  for (auto segment : segments) {
    f(segment);
  }
}

int zero[] {A, B, C, D, E, F, Last};
int one[] {B, C, Last};
int two[] {A, B, D, E, G, Last};
int three[] {A, B, C, D, G, Last};
int four[] {B, C, F, G, Last};
int five[] {A, C, D, F, G, Last};
int six[] {A, C, D, E, F, G, Last};
int seven[] {A, B, C, Last};
int eight[] {A, B, C, D, E, F, G, Last};
int nine[] {A, B, C, D, F, G, Last};

void show(int* segments) {
  executeOnAll([](int x) { off(x); });
  for(int i = 0; i != Last; i++) {
    on(segments[i]);
  }
}

void setup() {
  Serial.begin(9600);
  executeOnAll([](int a) { pinMode(a, OUTPUT); });
}

void loop() {
  int sensorValue = analogRead(A0);
  int num = map(sensorValue, 0, 1023, 0, 9);

  if (num == 0)
    show(zero);
  else if (num == 1)
    show(one);
  else if (num == 2)
    show(two);
  else if (num == 3)
    show(three);
  else if (num == 4)
    show(four);
  else if (num == 5)
    show(five);
  else if (num == 6)
    show(six);
  else if (num == 7)
    show(seven);
  else if (num == 8)
    show(eight);
  else if (num == 9)
    show(nine);
  Serial.println(sensorValue);
  delay(100);
}
