/*
  AnalogReadSerial

  Reads an analog input on pin 0, prints the result to the Serial Monitor.
  Graphical representation is available using Serial Plotter (Tools > Serial Plotter menu).
  Attach the center pin of a potentiometer to pin A0, and the outside pins to +5V and ground.

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/AnalogReadSerial
*/
void on(int num) {
  digitalWrite(num, HIGH);
}

void off(int num) {
  digitalWrite(num, LOW);
}

/* TODO: Write a HOF to abstract iterating over all */
void allOff() {
  for(int x = 3; x <= 9; x++) {
    off(x);
  }
}

void allOn() {
  for(int x = 3; x <= 9; x++) {
    on(x);
  }
}

enum Segment {
  A = 4, B = 3, C = 7, D = 9, E = 8, F = 5, G = 6
};

void showZero() {
  allOff();
  on(A);
  on(B);
  on(C);
  on(D);
  on(E);
  on(F);
}

void showOne() {
  allOff();
  on(B);
  on(C);
}

void showTwo() {
  allOff();
  on(A);
  on(B);
  on(G);
  on(D);
  on(E);
}

void showThree() {
  allOff();
  on(A);
  on(B);
  on(G);
  on(C);
  on(D);
}

void showFour() {
  allOff();
  on(B);
  on(C);
  on(G);
  on(F);
}

void showFive() {
  allOff();
  on(A);
  on(F);
  on(G);
  on(C);
  on(D);
}

void showSix() {
  allOff();
  on(A);
  on(C);
  on(D);
  on(E);
  on(F);
  on(G);
}
void showSeven() {
  allOff();
  on(3);
  on(4);
  on(7);
}

void showEight() {
  allOn();
}
void showNine() {
  allOff();
  on(3);
  on(4);
  on(5);
  on(6);
  on(7);
  on(9);

}


// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:X
  for(int x = 3; x <= 9; x++) {
    pinMode(x, OUTPUT);
  }
  allOff();
  Serial.begin(9600);
}

// the loop routine runs over and over again forever:
void loop() {
  // read the input on analog pin 0:
  int sensorValue = analogRead(A0);
  // int voltage = map(sensorValue, 0, 1023, 0, 5);
  // print out the value you read:
  int mappedNum = map(sensorValue, 0, 1023, 0, 9);
  if (mappedNum == 1)
    showOne();
  else if (mappedNum == 2)
    showTwo();
  else if (mappedNum == 3)
    showThree();
  else if (mappedNum == 4)
    showFour();
  else if (mappedNum == 5)
    showFive();
  else if (mappedNum == 6)
    showSix();
  else if (mappedNum == 7)
    showSeven();
  else if (mappedNum == 8)
    showEight();
  else if (mappedNum == 9)
    showNine();
  Serial.println(sensorValue);
  delay(100);        // delay in between reads for stability
}
