data Segment = A | B | C | D | E | F | G
  deriving (Enum, Show, Eq)

segments = [A .. G]
type SegmentPin = (Int, Segment)

on :: Int -> IO ()
on = undefined

off :: Int -> IO ()
off = undefined

zero = [A .. F]
one = [B, C]
two = [A, B, G, D, E]
three = [A, B, G, C, D]
four = [B, C, F, G]
five = [A, C, D, F, G]
six = [A, C, D, E, F, G]
seven = [A, B, C]
eight = [A .. G]
nine = [A, B, C, D, F, G]

show :: [SegmentPin] -> IO ()
show segments = do
  mapM_ off

main = undefined
