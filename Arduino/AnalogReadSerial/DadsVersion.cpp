
enum Segment { a, b, c, d, e, f, g };

// SegmentWriter encapsulates writing out HIGH or LOW values to individual pins. It is
// constructed with an array of pins whose indices correspond with Segment enum values.

class SegmentWriter {

private:

  int *pins;
  int pinsLength;

public:

  SegmentWriter(int *pins, int pinsLength) {
    this->pins = pins;
    this->pinsLength = pinsLength;
  }

  void enable(Segment* segments, int length) {
    disableAll();
    toggle(segments, length, HIGH);
  }

  void disable(Segment* segments, int length) {
    toggle(segments, length, LOW);
  }

private:

  void disableAll() {
    for (int i = 0; i < pinsLength; i++) {
      int pin = pins[i];
      digitalWrite(pin, LOW);
    }
  }

  void toggle(Segment* segments, int length, int value) {
    for (int i = 0; i < length; i++) {
      int segment = segments[i];
      int pin = pins[segment];
      digitalWrite(pin, value);
    }
  }
};

// DigitDisplay represents a single digit of a seven segment display. It knows how to
// display digits by enabling the required segments on the display.

class DigitDisplay {

private:

  SegmentWriter* writer;

public:

  DigitDisplay(SegmentWriter* writer) {
    this->writer = writer;
  }

  void show(int digit) {
    switch (digit) {
      case 0:
        showZero();
        break;
       case 1:
         showOne();
         break;
       case 2:
         showTwo();
         break;
       case 3:
         showThree();
         break;
       case 4:
         showFour();
         break;
       case 5:
         showFive();
         break;
       case 6:
         showSix();
         break;
       case 7:
         showSeven();
         break;
       case 8:
         showEight();
         break;
       case 9:
         showNine();
         break;
    }
  }

private:

  void showZero() {
    int length = 7;
    Segment segments[length] = { a, b, c, d, e, f, g };
    writer->enable(segments, length);
  }

  void showOne() {
    int length = 2;
    Segment segments[length] = { a, b };
    writer->enable(segments, length);
  }

  void showTwo() {
    int length = 5;
    Segment segments[length] = { a, b, g, e, d };
    writer->enable(segments, length);
  }

  void showThree() {
    int length = 5;
    Segment segments[length] = { a, b, g, c, d };
    writer->enable(segments, length);
  }

  void showFour() {
    int length = 4;
    Segment segments[length] = { b, c, f, g };
    writer->enable(segments, length);
  }

  void showFive() {
    int length = 5;
    Segment segments[length] = { a, c, d, f, g };
    writer->enable(segments, length);
  }

  void showSix() {
    int length = 6;
    Segment segments[length] = { c, d, e, f, g };
    writer->enable(segments, length);
  }

  void showSeven() {
    int length = 3;
    Segment segments[length] = { a, b, c };
    writer->enable(segments, length);
  }

  void showEight() {
    int length = 7;
    Segment segments[length] = { a, b, c, d, e, f, g };
    writer->enable(segments, length);
  }

  void showNine() {
    int length = 5;
    Segment segments[length] = { a, b, c, f, g };
    writer->enable(segments, length);
  }
};

// ************
// Main Program
// ************

DigitDisplay *digitDisplay;

void setup() {
  int pinsLength = 7;
  int pins[pinsLength];
  pins[Segment::a] = 4;
  pins[Segment::b] = 3;
  pins[Segment::c] = 7;
  pins[Segment::d] = 9;
  pins[Segment::e] = 8;
  pins[Segment::f] = 5;
  pins[Segment::g] = 6;

  initializePins(pins, pinsLength);

  SegmentWriter writer(pins, pinsLength);
  digitDisplay = new DigitDisplay(&writer);
  digitDisplay->show(2);
}

void loop() {
  int sensorValue = analogRead(A0);
  int mappedNum = map(sensorValue, 0, 1023, 0, 9);
  digitDisplay->show(mappedNum);
  Serial.println(mappedNum);
  delay(100);        // delay in between reads for stability
}

void initializePins(int *pins, int pinsLength) {
  Serial.begin(9600);
  for (int i = 0; i < pinsLength; i++) {
    pinMode(pins[i], OUTPUT);
  }
}
