
void on(int num) {
  digitalWrite(num, HIGH);
}

void off(int num) {
  digitalWrite(num, LOW);
}


enum Segment {
  A = 4, B = 3, C = 7, D = 9, E = 8, F = 5, G = 6, Last
};

void executeOnAll(void f(int)) {
  for(int x = A; x != Last; x++) {
    f(x);
  }
}

void allOff() {
  executeOnAll([](int x) { off(x); });
}


int zero[] {A, B, C, D, E, F};
int one[] {B, C};
int two[] {A, B, D, E, G};
int three[] {A, B, C, D, G};
int four[] {B, C, F, G};
int five[] {A, C, D, F, G};
int six[] {A, C, D, E, F, G};
int seven[] {A, B, C};
int eight[] {A, B, C, D, E, F, G};
int nine[] {A, B, C, D, F, G};

void show(int* segments, int segmentsSize) {
  allOff();
  for(int x = 0; x < segmentsSize; x++) {
    on(segments[x]);
  }
}

void setup() {
  Serial.begin(9600);
  executeOnAll([](int x) { pinMode(x, OUTPUT); });
}

void loop() {
  int sensorValue = analogRead(A0);
  int mappedNum = map(sensorValue, 0, 1023, 0, 9);
  allOff();

  if (mappedNum == 0)
    show(zero, 6);
  else if (mappedNum == 1)
    show(one, 2);
  else if (mappedNum == 2)
    show(two, 5);
  else if (mappedNum == 3)
    show(three, 5);
  else if (mappedNum == 4)
    show(four, 4);
  else if (mappedNum == 5)
    show(five, 5);
  else if (mappedNum == 6)
    show(six, 6);
  else if (mappedNum == 7)
    show(seven, 3);
  else if (mappedNum == 8)
    show(eight, 7);
  else if (mappedNum == 9)
    show(nine, 6);
  //Serial.println(sensorValue);
  delay(100);
}
