{-# LANGUAGE ExtendedDefaultRules #-}
import Data.Maybe
import Data.List
-- public class M{public static void main(String[] args){System.out.println("Hello World");}
fib = 1 : scanl (+) 2 fib

isMultiple x y = y `mod` x == 0

f x = fromJust $ find (isMultiple x) fib

sumList sum t = foldr (+) sum t

a x=let g=1:scanl(+)1 g in fromJust$find((0==).(`mod` x))g
b x=let g=1:scanl(+)1 g in filter((0==).(`mod` x))g!!0
main=readLn>>=print.a
