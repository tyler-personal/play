{-# LANGUAGE TupleSections #-}
module Snail where

import Data.Ix
import Data.List (nub)
main = print 4

-- 0.0 0.1 0.2 0.3 1.3 2.3 3.3 3.2 3.1 3.0 2.0 1.0
-- 1.1 1.2 2.2 2.1
--
--f x y = concatMap (\c -> scanl (\b a -> (c, a)) (c, 0) [1..x]) [1..y]

-- 0.0 1.0 2.0 3.0
-- 3.1 3.2 3.3
-- 2.3 1.3 0.3
-- 0.2 0.1

-- 1.1 2.1 2.2 1.2

-- f1Fake x y = ff1 0 0 x y ++ ff1 1 1 (x-1) (y-1)

buildSnail x y = nub $ concatMap (\a -> buildRing a a (x-a-1) (y-a-1)) [0..(x `div` 2)]

buildRing xStart yStart xEnd yEnd = nub $ concat [top, right, bottom, left]
  where
    top    = map (,yStart) [xStart..xEnd]
    right  = map (xEnd,) [yStart..yEnd]
    bottom = map (,yEnd) (reverse [xStart..xEnd])
    left   = map (xStart, ) (reverse [yStart..yEnd])


-- separate
f x y = nub $ concatMap (\a -> g a a (x-a-1) (y-a-1)) [0..(div x 2)]
  where g b c d e = concat [(,c)<$>[b..d],(d,)<$>[c..e],(,e)<$>reverse [b..d],(b,)<$>reverse [c..e]]


