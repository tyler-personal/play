module BruteForceLeavingEarth where
import Control.Monad
import Data.List

data Rocket = Juno | Atlas | Soyuz | Saturn
  deriving (Eq, Enum, Show, Ord)

data Action = Move Difficulty | Add Mass
  deriving (Show)

data Build = Build
  { actions :: [Action]
  , totalMass :: Int
  , launches :: [Launch]
  }

type Difficulty = Int
type Mass = Int
type Launch = [Rocket]

emptyBuild = Build { actions = [], totalMass = 0, launches = [] }

mkBuilds :: Mass -> [Action] -> [Build]
mkBuilds mass (Move move:actions) = undefined
mkBuilds mass (Add mass':actions) = undefined
--mkBuilds mass actions = scanl f emptyBuild actions
mkBuilds mass actions = undefined
  where
    f build action = undefined

cost :: Rocket -> Int
cost Juno = 1
cost Atlas = 5
cost Soyuz = 8
cost Saturn = 15

mass :: Rocket -> Mass
mass Juno = 1
mass Atlas = 4
mass Soyuz = 9
mass Saturn = 20

thrust :: Rocket -> Int
thrust Juno = 4
thrust Atlas = 27
thrust Soyuz = 80
thrust Saturn = 200

launchOptions :: [Launch]
launchOptions = takeWhile ((< 2) . length) filteredOptions
  where filteredOptions = concatMap (nub . map sort . (`replicateM` [Juno ..])) [0..]

buildOptions :: [[Launch]]
buildOptions = replicateM 10 launchOptions

isValid :: Build -> Bool
isValid build = True
  where
    --testMove (Move x:xs, rockets:ys) = map cost rockets
    --testMove (Mass o:os, r:rs) = undefined

main = print . length $ buildOptions
