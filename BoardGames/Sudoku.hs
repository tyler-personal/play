{-# LANGUAGE NamedFieldPuns, RecordWildCards, NoMonomorphismRestriction #-}
module Sudoku where
import Prelude hiding (or)
import Data.List ((\\))
import Control.Monad (join)

data Node = Node { row :: Int, col :: Int, group :: Int, value :: Int } deriving Show

solve :: [Node] -> [Node]
solve = until (all ((/= 0) . value)) solveBoard
  where solveBoard nodes = map (\n -> n { value = solveNode nodes n }) nodes

solveNode :: [Node] -> Node -> Int
solveNode grid node@Node{value=currentVal}
  | solved || unsolvable = currentVal
  | otherwise = head possibleValues
  where
    solved = currentVal > 0
    unsolvable = length possibleValues /= 1
    possibleValues = [1..9] \\ map value
      (filter (\n -> any (\f -> f n == f node) [row, col, group]) grid)

gridToNodes :: [[Int]] -> [Node]
gridToNodes grid = join $ map (\(row, dat) -> map (\(col, value) ->
  Node { row, col, group = solveGroup row col, value }
  ) (zip [0..] dat)) (zip [0..] grid)
  where
    groups = [[3,3,1], [3,6,2], [3,9,3], [6,3,4], [6,6,5], [6,9,6], [9,3,7], [9,6,8], [9,9,9]]
    solveGroup row col = val
      where [_,_,val] = head (filter (\[r,c,_] -> row < r && col < c) groups)

main :: IO ()
main = putStrLn . showNodes $ solve startingNodes

showNodes :: [Node] -> String
showNodes = f 0
  where
    f _ [] = []
    f n (Node{..}:nodes) = (if n < row then "\n" else "") ++ show value ++ " " ++ f row nodes

startingNodes :: [Node]
startingNodes = gridToNodes
  [[5,3,0,0,7,0,0,0,0]
  ,[6,0,0,1,9,5,0,0,0]
  ,[0,9,8,0,0,0,0,6,0]
  ,[8,0,0,0,6,0,0,0,3]
  ,[4,0,0,8,0,3,0,0,1]
  ,[7,0,0,0,2,0,0,0,6]
  ,[0,6,0,0,0,0,2,8,0]
  ,[0,0,0,4,1,9,0,0,5]
  ,[0,0,0,0,8,0,0,7,9]]
