{-# LANGUAGE NamedFieldPuns, RecordWildCards #-}
import Data.List
import Data.Function
import Debug.Trace
import Control.Applicative
main = print 4

t' a = trace (show a) a

data Rocket = Juno | Atlas | Soyuz | Saturn
  deriving Show

type Move = [Rocket]

bestAction :: [Rocket] -> Double -> Double -> Double -> Move

bestMove :: [Rocket] -> Double -> Double -> Move
bestMove [] _ _ = error "You need to have rockets available. Some logic error occurred."
bestMove _ _ 0 = []
bestMove availableRockets difficulty mass'
  | otherwise = undefined
  where
    [lRocket, hRocket] = map (\f -> f availableRockets difficulty mass') [lowerRocket, higherRocket]
    nextRockets = bestMove availableRockets difficulty ((t' mass') - mass lRocket)

xRocket f g rockets difficulty mass' = f (compare `on` mass) rocketsRelativeToMass
  where rocketsRelativeToMass = filter ((`g` mass') . maxPayload difficulty) rockets
lowerRocket = xRocket maximumBy (<)
higherRocket = xRocket minimumBy (>)

lowerRockets rockets difficulty mass' thrust
  | mass' < 0 = error "invalid"
  | thrust == (mass' * difficulty) = []
  | otherwise  = rocket : rockets'
  where
    rocket = maximumBy (compare `on` mass) $ filter ((< mass') . maxPayload difficulty) rockets
    rockets' = lowerRockets rockets difficulty (mass' - thrust rocket)


maxPayload :: Double -> Rocket -> Double
maxPayload difficulty rocket = (thrust rocket - (mass rocket * difficulty)) / difficulty

cost :: Rocket -> Double
cost Juno = 1
cost Atlas = 5
cost Soyuz = 8
cost Saturn = 15

mass :: Rocket -> Double
mass Juno = 1
mass Atlas = 4
mass Soyuz = 9
mass Saturn = 20

thrust :: Rocket -> Double
thrust Juno = 4
thrust Atlas = 27
thrust Soyuz = 80
thrust Saturn = 200
