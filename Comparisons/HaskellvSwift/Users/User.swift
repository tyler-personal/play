struct User : Equatable {
  let name: String
  var age: Int

  var canVote: Bool {
    age >= 18
  }
}

var user1 = User(name: "Potato", age: 40)
let user2 = User(name: "Potato", age: 40)

print(user1)
print(user1 == user2)

print(user1.canVote)
user1.age = 12
print(user1.canVote)

let fred = User(name: "Fred", age: 20)
let bobi = User(name: "Bobi", age: 37)
let yoyo = User(name: "Yoyo", age: 12)

let users = [fred, bobi, yoyo]
let voters = users.filter(\.canVote)
print(voters)
