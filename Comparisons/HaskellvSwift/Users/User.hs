{-# LANGUAGE RecordWildCards #-}
data User = User { name :: String, age :: Int } deriving (Eq, Show)

canVote = (>= 18) . age
canVota User {..} = age >= 18

main = do
  let user1 = User { name = "Potato", age = 40 }
  let user2 = User { name = "Potato", age = 40 }

  print user1
  print $ user1 == user2

  print $ canVote user1
  let user1_new = user1 { age = 12 }
  print $ canVote user1_new

  main2

fred = User { name = "Fred", age = 20 }
bobi = User { name = "Bobi", age = 37 }
yoyo = User { name = "Yoyo", age = 12 }

users = [fred, bobi, yoyo]
voters = filter canVote users

main2 = print voters
