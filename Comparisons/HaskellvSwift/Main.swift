enum Dog {
  case mastiff(Int, String)
  case beagle(String, Int)
}

func printAge(puppy: Dog) {
  switch puppy {
  case let Dog.mastiff(age, name):
    print(age)
  case let Dog.beagle(name, age):
    print(age)
  }
}

printAge(puppy: Dog.beagle("yeeter", 14))
