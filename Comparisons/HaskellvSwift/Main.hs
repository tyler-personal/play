data Dog = Mastiff Int String | Beagle String Int

printAge :: Dog -> IO ()
printAge (Mastiff age _) = print age
printAge (Beagle _ age) = print age

mlength [] = 0
mlength (_:xs) = 1 + mlength xs

main = printAge $ Beagle "yeeter" 14
