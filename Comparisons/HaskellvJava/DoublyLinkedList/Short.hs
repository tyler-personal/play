{-# LANGUAGE NamedFieldPuns, RecordWildCards, NoMonomorphismRestriction #-}
import Data.List (intercalate)

data N v = N | E { v :: v, prev :: N v, next :: N v }
  deriving (Show, Eq)

push N v = E v N N
push n v = let n' = E v N (n { prev = n' }) in n'

pop N = (Nothing, N)
pop E{..} = (Just v, next)

append N v = E v N N
append n@(E _ _ N) v = E v n N
append n v = n { next = append (next n) v }

showNodes = intercalate "," . map (show . v) . takeWhile (/= N) . iterate next

main = undefined
