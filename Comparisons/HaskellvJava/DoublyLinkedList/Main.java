package DoublyLinkedList;

import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Java8<T> {
    Node head;

    class Node {
        T value;
        @Nullable Node previous;
        @Nullable Node next;

        Node(T value, @Nullable Node previous, @Nullable Node next) {
            this.value = value;
            this.previous = previous;
            this.next = next;
        }

        @Override
        public String toString() {
            return this.value.toString();
        }
    }

    void push(T value) {
        Node node = new Node(value, null, head);

        if (head != null)
            head.previous = node;

        head = node;
    }

    void append(T value) {
        Node node = new Node(value, null, null);

        if (head == null) {
            head = node;
            return;
        }

        Node last = Stream
            .iterate(head, n -> n.next != null, n -> n.next)
            .reduce((a,b) -> b)
            .get();

        last.next = node;
        node.previous = last;
    }

    @Override
    public String toString() {
        return Stream.iterate(head, Objects::nonNull, node -> node.next)
                .map(Node::toString)
                .collect(Collectors.joining(", "));
    }


    public static void main(String[] args) {
        Java8<Integer> list = new Java8<>();
        list.append(3);
        list.push(2);
        list.push(1);
        list.append(4);
        System.out.println(list);
    }
}
