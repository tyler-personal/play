{-# LANGUAGE RecordWildCards, QuasiQuotes #-}
import Data.String.Interpolate

data Node a = Nil | Node { value :: a, prev :: Node a, next :: Node a }
  deriving Eq

instance Show a => Show (Node a) where
  show Nil = "Nil"
  show node = f "  " node
    where
      f _ Nil = "Nil"
      f spaces Node{..} = [i|Node #{value}\n#{spaces}prev: |] ++ (case prev of
        Nil -> "Nil"
        Node{..} -> [i|Node #{value}|]) ++ [i|\n#{spaces}next: #{f (spaces ++ " ") next}|]

push :: Node a -> a -> Node a
push Nil value = Node value Nil Nil
push node value = newNode
  where newNode = Node value Nil (node { prev = newNode })

pop :: Node a -> (Maybe a, Node a)
pop Nil = (Nothing, Nil)
pop node = (Just (value node), next node)

append :: Node a -> a -> Node a
append Nil value = Node value Nil Nil
append node@(Node _ _ Nil) value = node { next = Node value node Nil }
append node value = node { next = append (next node) value }

xs :: Node Integer
xs = append (append (append Nil 1) 2) 3

main = undefined
