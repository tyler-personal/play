{-# LANGUAGE NamedFieldPuns, RecordWildCards, FlexibleInstances, MultiParamTypeClasses #-}
import Data.List (intercalate)
import Control.Lens
import Control.Monad.Loops
import Control.Category ((>>>))

data Node a = Nil | Node { value :: a, prev :: Node a, next :: Node a }
  deriving (Show, Eq)

instance Functor Node where
  fmap _ Nil = Nil
  fmap f (Node v p n) = Node { value=f v, prev=fmap f p, next=fmap f n }

instance Foldable Node where
  foldMap _ Nil = mempty
  foldMap f (Node value prev next) = f value <> foldMap f next

--instance Snoc [a] [b] a b where
--  _Snoc = prism (\(as,a) -> as Prelude.++ [a]) $ \aas -> if Prelude.null aas
--    then Left []
--    else Right (Prelude.init aas, Prelude.last aas)
instance Eq a => Snoc (Node a) (Node b) a b where
  _Snoc = prism (\(xs, x) -> append x xs) $ \xxs -> if xxs == Nil
    then Left Nil
    else Right (xxs, undefined)

push :: Node a -> a -> Node a
push Nil value = Node value Nil Nil
push node value = newNode
  where newNode = Node value Nil (node { prev = newNode })

pop :: Node a -> (Maybe a, Node a)
pop Nil = (Nothing, Nil)
pop Node{..} = (Just value, next)

--initLast :: Node a -> (Node a, a)
initLast node = f (node, node, value node)
  where
    f :: (Node a, Node a, a) -> (Node a, a)
    f (new, Nil, val) = (new, val)
    f (new, old, val) = f (append val new, next old, value old)

append :: a -> Node a -> Node a
--append node value = node |> value
append value Nil = Node value Nil Nil
append value node@(Node _ _ Nil) = node { next = Node value node Nil}
append value node = node { next = append value (next node) }

showNodes :: (Eq a, Show a) => Node a -> String
showNodes = intercalate "," . map (show . value) .
            takeWhile (/= Nil) . iterate next

--ys = append Nil >>> append 1
xs :: Node Int
xs = as
  where
    as = Node 1 Nil bs
    bs = Node 2 as cs
    cs = Node 3 bs ds
    ds = Node 4 cs Nil

main = undefined
