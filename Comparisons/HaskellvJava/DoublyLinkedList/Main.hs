{-# LANGUAGE NamedFieldPuns, RecordWildCards #-}
import Data.List (intercalate)

data Node a = Nil | Node { value :: a, prev :: Node a, next :: Node a }
  deriving (Show, Eq)

push :: Node a -> a -> Node a
push Nil value = Node value Nil Nil
push node value = newNode
  where newNode = Node value Nil (node { prev = newNode })

pop :: Node a -> (Maybe a, Node a)
pop Nil = (Nothing, Nil)
pop Node{..} = (Just value, next)

append :: Node a -> a -> Node a
append Nil value = Node value Nil Nil
append node@(Node _ _ Nil) value = node { next = Node value node Nil }
append node value = node { next = append (next node) value }

showNodes :: (Eq a, Show a) => Node a -> String
showNodes = intercalate "," . map (show . value) .
            takeWhile (/= Nil) . iterate next

main = undefined
