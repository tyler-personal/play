{-# LANGUAGE NamedFieldPuns, RecordWildCards, UnboxedSums, UnicodeSyntax #-}
import Data.List (intercalate)
import Data.Either (fromLeft)
import Control.Monad.Loops (untilM)

type Nodeʔ a = Either (Node a) ()

data Node a = Node { value :: a, prev :: Nodeʔ a, next :: Nodeʔ a }

push :: Node a -> a -> Node a
push node value = newNode
  where newNode = Node value (Right ()) (Left (node { prev = Left newNode }))

pop :: Node a -> (Maybe a, Nodeʔ a)
pop Node{..} = (Just value, next)

append node value = fromLeft $ untilM next (== (Right ())) node

--showNodes :: (Eq a, Show a) => Node a -> String
--showNodes = intercalate "," . map (show . value) .
--            takeWhile (/= Nil) . iterate next

main = undefined
