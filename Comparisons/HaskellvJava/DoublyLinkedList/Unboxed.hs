{-# LANGUAGE NamedFieldPuns, RecordWildCards, UnboxedSums #-}
import Data.List (intercalate)
-- Failed due to boxing issues
type Node' a = (# Node a | () #)
data Node a = Node { value :: a, prev :: Node' a, next :: Node' a }
data T a = T (Maybe a) (Node' a)

mkList :: a -> Node a
mkList value = Node value (#|()#) (#|()#)

push :: Node a -> a -> Node a
push node value = newNode
  where newNode = Node value (#|()#) ((# node { prev = (#newNode| #) }| #))

pop :: Node a -> T a
pop Node{..} = T (Just value) next

append :: Node a -> a -> Node a
append node@(Node _ _ (# | _ #)) value = Node value node Nil
append node value = until (== (#|()#)) next node

showNodes :: (Eq a, Show a) => Node a -> String
showNodes = intercalate "," . map (show . value) .
            takeWhile (/= Nil) . iterate next

main = undefined
