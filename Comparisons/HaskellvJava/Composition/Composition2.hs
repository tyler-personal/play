module Composition2 where

showEvens = map show . filter even

toRadix b n = map (`mod` b) (takeWhile (> 0) (iterate (`div` b) n))

toRadiz b = map (`mod` b) . takeWhile (> 0) . iterate (`div` b)
main = print $ showEvens [1..10]
