multiplyBy2 = (* 2)
add3 = (+ 3)

add3ThenMultiplyBy2 = multiplyBy2 . add3

main = print (add3ThenMultiplyBy2 4)
