main = do
  n <- readLn
  x <- sum . map read . words <$> getLine
  print $ n * (n + 1) / 2 - x
