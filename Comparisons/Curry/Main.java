// Java - 308 characters
// Pros:
//   - Lambda style
// Cons:
//   - NO Native currying support
//   - NO Type inference for lambdas
//   - Lambdas DON'T behave like functions at the call site
import java.util.function.Function;

public class Main {
    static Function<Integer, Function<Integer, Integer>> add =
        x -> y -> x + y;
    static Function<Integer, Integer> add4 =
        add.apply(4);

    public static void main(String[] args) {
        System.out.println(add4.apply(7));
    }
}
