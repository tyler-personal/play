// Swift -- 81 characters
let add = { (x: Int) in { (y: Int) in x + y } }
let add4 = add(4)

print(add4(7))
