# Python - 61 characters
# Pros:
#   - Lambdas behave like functions at the call site
# Cons:
#   - Lambda style
add = lambda x: lambda y: x + y
add4 = add(4)

print(add4(7))
