// Scala - 96 characters
// Pros:
//   - Native currying support
def add(x: Int)(y: Int) = x + y
def add4 = add(4)

class Main extends App {
  println(add4(7))
}
