# Ruby - 67 characters
# Pros:
#   -
# Cons:
#   - Lambdas DON'T behave like functions at the call site
add = -> x { -> y { x + y } }
add4 = add.call(4)

puts add4.call(7)
