// Kotlin - 79 characters
fun add(x: Int) = { y: Int -> x + y }
val add4 = add(4)

println(add4(7))
