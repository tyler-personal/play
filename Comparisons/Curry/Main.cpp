// C++ - 168 characters
// Pros:
//   - Type inference for lambdas
//   - Lambdas behave like functions at the call site.
// Cons:
//   - Lambda style
#include<iostream>
using namespace std;

auto add = [](int x) { return [x](int y) { return x + y; }; };
auto add4 = add(4);

int main() {
    cout << add4(7) << '\n';
}
