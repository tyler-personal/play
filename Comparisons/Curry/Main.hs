-- Haskell - 51 characters
-- Pros:
--   - Currying is the default behavior.
add x y = x + y
add4 = add 4

main = print $ add4 7
