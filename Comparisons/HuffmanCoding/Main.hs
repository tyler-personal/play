{-# LANGUAGE TupleSections #-}
import Control.Arrow
import Data.Map (fromListWith, toList)
import Data.List (group, sortOn)

newtype SortedCounter a = SortedCounter [(Int, a)]

frequency'' :: Eq a => [a] -> HuffmanTree a
frequency'' xs = undefined
  where sortedCounter = sortOn fst . map (length &&& head) . group $ xs

huffmanTree' :: SortedCounter a -> HuffmanTree a -> HuffmanTree a
huffmanTree' (SortedCounter []) _ = error "heap can't be empty"
huffmanTree' (SortedCounter [_]) tree = tree
-- huffmanTree' (SortedCounter (x:y:xs)) tree = Node


data HuffmanTree a = Leaf a Int | Node Int (HuffmanTree a) (HuffmanTree a)
  deriving (Show)
type Nodes a = [HuffmanTree a]

frequency :: Ord a => [a] -> Nodes a
frequency = sortNodes . map (uncurry Leaf) . toList . fromListWith (+) . map (,1)

count :: HuffmanTree v -> Int
count (Leaf _ c) = c
count (Node c _ _) = c

nodesToTree :: Nodes a -> HuffmanTree a
nodesToTree [] = error "nodes can't be empty"
nodesToTree [x] = x
nodesToTree (x:y:xs) = nodesToTree . sortNodes $ newNode : xs
  where newNode = Node (count x + count y) x y

sortNodes :: Nodes a -> Nodes a
sortNodes = sortOn count

huffmanTree :: Ord a => [a] -> HuffmanTree a
huffmanTree = nodesToTree . frequency

main :: IO ()
main = do
  val <- input "Enter a word or phrase."
  print $ huffmanTree val

input :: String -> IO String
input str = do
  print str
  getLine
