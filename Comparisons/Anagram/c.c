#include <stdio.h>
#include <string.h>

int main(void) {
  const char names[][8] = {"potato", "muffin", "oofsted", "alamony", "ruff"};
  int lengths[5];

  for(int i = 0; i < 5; i++)
    lengths[i] = strlen(names[i]);

  for (int i = 0; i < sizeof(names)/8; i++)
    printf("%d ", lengths[i]);
  printf("\n");

  return 0;
}
