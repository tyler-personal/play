module Haskell where
import Data.Char (toLower)
import Data.List (sort)
import Data.Function (on)

anagramsFor :: String -> [String] -> [String]
anagramsFor = filter . on (==) sorted
  where sorted = sort . map toLower

anagrams = filter . on (==) (sort . map toLower)
