#1/usr/bin/perl

use strict;

use constant NAMES => qw(potato muffin oofsted alamony ruff);
use constant LENGTHS => map { length $_ } NAMES;

while (my ($length, $name) = each LENGTHS) {
  print "'$name' has $length characters\n";
}
