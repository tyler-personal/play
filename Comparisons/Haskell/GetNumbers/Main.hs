getNumbers :: String -> [Int]
getNumbers = map read . words

main = getNumbers <$> getLine >>= print
