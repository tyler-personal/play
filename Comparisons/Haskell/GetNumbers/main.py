from typing import *

def getNumbers(s: str) -> List[int]:
    return [int(x) for x in s.split()]

if __name__ == '__main__':
    print(getNumbers(input()))

