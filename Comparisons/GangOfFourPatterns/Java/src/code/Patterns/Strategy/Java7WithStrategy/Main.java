package Patterns.Strategy.Java7WithStrategy;


public class Main {
    public static void main(String[] arguments) {
        BillingStrategy normalStrategy = BillingStrategy.normalStrategy;
        BillingStrategy happyHourStrategy = BillingStrategy.happyHourStrategy;

        Customer firstCustomer = new Customer(normalStrategy);

        firstCustomer.addDrink(100, 1);

        firstCustomer.setStrategy(happyHourStrategy);
        firstCustomer.addDrink(100, 2);

        Customer secondCustomer = new Customer(happyHourStrategy);
        secondCustomer.addDrink(80, 1);
        System.out.println(firstCustomer.getBill());

        secondCustomer.setStrategy(normalStrategy);
        secondCustomer.addDrink(130, 2);
        secondCustomer.addDrink(250, 1);
        System.out.println(secondCustomer.getBill());
    }
}