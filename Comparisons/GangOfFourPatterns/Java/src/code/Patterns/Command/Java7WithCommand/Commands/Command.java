package Patterns.Command.Java7WithCommand.Commands;

public interface Command {
    void execute();
}
