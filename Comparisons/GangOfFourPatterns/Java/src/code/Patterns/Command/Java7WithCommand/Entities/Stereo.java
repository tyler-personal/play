package Patterns.Command.Java7WithCommand.Entities;


import Boundary.IO;

public class Stereo {
    private final IO io;

    public Stereo(IO io) {
        this.io = io;
    }

    public void on() {
        io.show("Stereo is on");
    }

    public void off() {
        io.show("Stereo is off");
    }

    public void setCD() {
        io.show("Stereo is set for CD input");
    }

    public void setDVD() {
        io.show("Stereo is set for DVD input");
    }

    public void setRadio() {
        io.show("Stereo is set for Radio");
    }

    public void setVolume(int volume) {
        io.show("Stereo volume set to " + volume);
    }
}