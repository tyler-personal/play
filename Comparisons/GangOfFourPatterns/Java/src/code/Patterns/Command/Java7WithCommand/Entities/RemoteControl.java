package Patterns.Command.Java7WithCommand.Entities;

import Patterns.Command.Java7WithCommand.Commands.Command;

import java.util.function.Function;

public class RemoteControl {
    private Command button;

    public RemoteControl() { }

    public void setCommand(Command command) {
        button = command;
    }

    public void pressButton() {
        button.execute();
    }
}
