package Patterns.Command.Java8WithoutCommand;

import Boundary.IO;
import Patterns.Command.Java8WithoutCommand.Entities.Light;
import Patterns.Command.Java8WithoutCommand.Entities.RemoteControl;
import Patterns.Command.Java8WithoutCommand.Entities.Stereo;
import Presentation.PresentationDependencies;

public class Main {
    public static void main(String[] args) {
        RemoteControl remote = new RemoteControl();
        IO io = PresentationDependencies.io;

        Light light = new Light(io);
        Stereo stereo = new Stereo(io);

        remote.setCommand(light::on);
        remote.pressButton();
        remote.setCommand(stereo::onWithCD);
        remote.pressButton();
        remote.setCommand(stereo::off);
        remote.pressButton();
    }
}
