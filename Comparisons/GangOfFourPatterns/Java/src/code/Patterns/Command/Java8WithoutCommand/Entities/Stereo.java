package Patterns.Command.Java8WithoutCommand.Entities;

import Boundary.IO;

public class Stereo {
    private final IO io;

    public Stereo(IO io) {
        this.io = io;
    }

    public void onWithCD() {
        on();
        setCD();
        setVolume(10);
    }


    public void off() {
        io.show("Stereo is off");
    }

    private void on() {
        io.show("Stereo is on");
    }

    private void setCD() {
        io.show("Stereo is set for CD input");
    }

    private void setVolume(int volume) {
        io.show("Stereo volume set to " + volume);
    }

    private void setDVD() {
        io.show("Stereo is set for DVD input");
    }

    private void setRadio() {
        io.show("Stereo is set for Radio");
    }
}
