package Presentation;

import Boundary.IO;

class ConsoleIO implements IO {
    @Override
    public void show(String str) {
        System.out.println(str);
    }
}
