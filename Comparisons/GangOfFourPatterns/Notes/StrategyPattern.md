This pattern is largely obsolete for modern languages

Below are some points in favor of the strategy pattern, and their counterpoints.

# You can select different implementations of an action based on runtime details

### This is true of functions too, and you can do it with the same level of flexibility via HOFs.

# Strategies are encapsulated and separated from the user of a strategy

### Functions can do the same thing. Even in a language without support for top level functions, you can simply use something like an interactor which is essentially just a wrapper for a function, or static "top level" lambdas.

### In Java, you could even create a FunctionalInterface and then implement the interface with these lambdas. You'd be essentially mixing the strategy pattern with the functional approach, and having that mixed approach is sort of the foundation of Java's Function/lambda implementations, which comes with its own set of semantic issues.
