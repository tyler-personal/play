This pattern is largely obsolete for modern languages.

Below are some points in favor of the command pattern, and their counterpoints.

# You can encapsulate the manipulation of some entity in a specific way in a single class.

### This manipulation could be done inside a single function of the entity instead.

# With a class you can separate method setup (via instantiation) from the execution of the method

### You can do this with functions as well, by returning a function to be executed. Currying is essentially this but simplified

# You could access multiple entities in one class with a command pattern, but if you put it on the entity then you'd be coupling classes together

### This isn't the command pattern. The command pattern has one receiver for any given command.

### Even if this was the command pattern, first class functions would answer this anyway (since they could exist at the top level, independent of a class)

# With a class you can store the objects in a list or places to be executed at a later time.

### The same is true of functions, even in languages that lack first class support of them.

# A large part of the command pattern is the inheritance aspect. You can have multiple commands and not know what the specific command is, only that it adheres to an interface

### Functions implicitly allow for this. If you're passing around a function of `() -> Int` all you know is that the function conforms to that specification, and that specification is incredibly similar to a single function interface (in Java 8+ that specification is literally identical actually, lambdas can implicitly conform to single method interfaces even without the FunctionalInterface annotation)

# __Other notes__

## Haskell

There are definitely levels of indirection/verbosity here that aren't necessary.

Note in the main function Haskell implicitly enforces that we can't make the mistake of calling `pressButton` before (Java's) `setCommand`.
