{-# LANGUAGE RecordWildCards, DuplicateRecordFields #-}
-- This is mimicking the Java8WithoutCommand package
module CommandPattern where

data Light = Light { onLight  :: IO (), offLight :: IO () }

mkLight :: IO' -> Light
mkLight IO'{..} = Light
  { onLight = display "Light is on"
  , offLight = display "Light is off"
  }

data Stereo = Stereo { onWithCDStereo :: IO (), offStereo :: IO () }

mkStereo :: IO' -> Stereo
mkStereo IO'{..} = Stereo
  { onWithCDStereo = on >> setCD >> setVolume 10
  , offStereo = display "Stereo is off"
  }
  where
    on = display "Stereo is on"
    setCD = display "Stereo is set for CD input"
    setVolume volume = display $ "Stereo volume set to " ++ show volume
    setDVD = display "Stereo is set for DVD input"
    setRadio = display "Stereo is set for Radio"

data RemoteControl = RemoteControl { command :: IO () }

pressButton :: RemoteControl -> IO ()
pressButton = command

data IO' = IO' { display :: String -> IO () }

consoleIO :: IO'
consoleIO = IO' { display = putStrLn }

main :: IO ()
main = do
  let io = consoleIO

  let Light{..} = mkLight io
  let Stereo{..} = mkStereo io

  let remote = RemoteControl { command = onLight }
  pressButton remote
  let remote = remote { command = onWithCDStereo }
  pressButton remote
  let remote = remote { command = offStereo }
  command remote
