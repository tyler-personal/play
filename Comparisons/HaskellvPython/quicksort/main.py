import random
## Random generated data for testing
unsorted = [random.randint(0,100) for i in range(50)]
print(unsorted)

def qs(xs):
    if xs == []:
        return []
    else:
        (x,xs) = (xs[0], xs[1:])
        return qs(list(filter(lambda a: a < x, xs))) + [x] + qs(list(filter(lambda a: a >= x, xs)))
## The One-Liner
q = lambda l: (q([x for x in l[1:] if x <= l[0]]) + [l[0]] + q([x for x in l if x > l[0]]) if l else [])

## The Result
print(q(unsorted))
