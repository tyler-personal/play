{-# LANGUAGE NoMonomorphismRestriction #-}
import Data.List
quicksort [] = []
quicksort (x:xs) = lesser ++ [x] ++ greater
  where (lesser, greater) = partition (< x) xs

qs [] = []
qs (x:xs) = qs (filter (< x) xs) ++ [x] ++ qs (filter (> x) xs)
main = undefined
q [] = []
q (x:xs) = l ++ x : g where (l,g) = partition (< x) xs
q2 l = if null l then [] else q (filter (< x) xs) ++ x : q (filter (> x) xs) where (x:xs) = l

q3 = \l -> if length l > 0 then (q ([x | x <- tail l, x <= head l]) ++ [head l] ++ q([x | x <- l, x > head l])) else []
