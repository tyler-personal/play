cities = {'Beijing': 27.74, 'New Delhi': 999, 'Tokyo': 21.99, 'Manila': 31.78, 'Moscow': 9.7, 'Cairo': 28.44, 'Jakarta': 29.18, 'Kinshasa': 31.85, 'Seoul': 19.81, 'Mexico City': 999, 'Tehran': 21.93, 'London': 15.2, 'Lima': 22.75, 'Bangkok': 35.26, 'Berlin': 15.75, 'Hanoi': 26.33, 'Hong Kong': 999, 'Baghdad': 32.06, 'Singapore': 28.51, 'Ankara': 16.77}

max_cities = {k:v for k,v in cities.items() if v == 999}
other_cities = {k:v for k,v in cities.items() if v != 999}

result = dict({k:v for k,v in sorted(other_cities.items(), key=lambda x: x[1])}, **max_cities)
print(result)
result["Jakarta"] = 400
print(result)
