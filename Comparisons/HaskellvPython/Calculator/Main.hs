{-# LANGUAGE NoMonomorphismRestriction, FlexibleContexts, OverloadedLists, GADTs #-}
import Control.Monad
import qualified Data.Map as M

ops = [("+", (+)), ("-", (-)), ("*", (*)), ("/", (/)), ("**", (**))]

main = do
  [x,y] <- replicateM 2 (read <$> (putStr "Enter a number: " >> getLine))
  op <- putStr "Enter an operator: " >> getLine
  print $ (\f -> f x y) <$> M.lookup op ops
