from operator import *

ops = {"+": add, "-": sub, "*": mul, "/": truediv, "**": pow}

[x,y] = [float(input("Enter a number: ")) for _ in range(2)]
op = input("Enter an operator: ")

if op in ops:
    print(ops[op](x,y))
else:
    print("Missing operator")
