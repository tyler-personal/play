{-# LANGUAGE NoMonomorphismRestriction, FlexibleContexts, OverloadedLists, GADTs #-}
import Control.Monad
import qualified Data.Map as M

main = replicateM 2 (read <$> (putStr "Enter a number: " >> getLine))
  >>= \[x,y] -> (putStr "Enter an operator: " >> getLine)
  >>= \op -> print $ (\f -> f x y) <$> M.lookup op
    [("+", (+)), ("-", (-)), ("*", (*)), ("/", (/)), ("**", (**))]
