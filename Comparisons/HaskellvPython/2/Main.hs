{-# LANGUAGE ParallelListComp #-}
addDiffs x y = sum [a - b | a <- x | b <- y]

main = print $ map (*2) [1,2,3,4]

