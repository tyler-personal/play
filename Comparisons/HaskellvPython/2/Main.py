def addDiffs(x, y):
    return sum(a - b for (a,b) in zip(x,y))

print(addDiffs([1,2,3,4], [0,1,2,3]))
