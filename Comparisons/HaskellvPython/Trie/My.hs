module My where

import Prelude hiding (lookup)
import Data.Map hiding (map)

data Trie a = Trie (Map a (Trie a)) Bool

put [] (Trie m _) = Trie m True
put (c : w) (Trie m b) = case lookup c m of
  Nothing -> put (c : w) $ Trie (insert c (Trie empty False) m) b
  Just tr -> Trie (insert c (put w tr) m) b

find [] (Trie _ b) = b
find (c : w) (Trie m _) = maybe False (find w) $ lookup c m

complete [] (Trie m b) = [[] | b] ++ concat [map (c :) (complete [] tr) | (c, tr) <- toList m]
complete (c : w) (Trie m _) = maybe [] (map (c :) . complete w) $ lookup c m
