module PythonStyle where

import Data.Map hiding (map)

data Trie value = Trie (Map value (Trie value)) Bool

newTrie value = Trie value False

