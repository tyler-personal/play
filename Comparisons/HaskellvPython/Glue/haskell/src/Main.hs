module Main where

import Data.Text
import Language.Java
import Language.Java.Inline
import Foreign.C.String
import qualified Language.C.Inline as C

C.include "<stdio.h>"
C.include "<stdlib.h>"

getNameFromC = [C.block| char* {
    int len = 20;
    char* name = malloc(len);

    printf("What's your name?\n");
    fgets(name, len, stdin);

    return name;
  }|] >>= peekCString

displayInJavaPopup msg = withJVM @() [] $ do
  javaMsg <- reflect (pack msg)
  [java| { javax.swing.JOptionPane.showMessageDialog(null, "Hello, " + $javaMsg); }|]

main = do
  name <- getNameFromC
  displayInJavaPopup name

