{-# LANGUAGE CPP #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
{-# OPTIONS_GHC -fno-warn-implicit-prelude #-}
module Paths_glue_test (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/tylerwbrown/Code/Play/Comparisons/HaskellvPython/Glue/haskell/.stack-work/install/x86_64-linux-tinfo6/lts-11.22/8.2.2/bin"
libdir     = "/home/tylerwbrown/Code/Play/Comparisons/HaskellvPython/Glue/haskell/.stack-work/install/x86_64-linux-tinfo6/lts-11.22/8.2.2/lib/x86_64-linux-ghc-8.2.2/glue-test-0.1-LwfkzTnGKDl3TykryrQa4C-glue-test"
dynlibdir  = "/home/tylerwbrown/Code/Play/Comparisons/HaskellvPython/Glue/haskell/.stack-work/install/x86_64-linux-tinfo6/lts-11.22/8.2.2/lib/x86_64-linux-ghc-8.2.2"
datadir    = "/home/tylerwbrown/Code/Play/Comparisons/HaskellvPython/Glue/haskell/.stack-work/install/x86_64-linux-tinfo6/lts-11.22/8.2.2/share/x86_64-linux-ghc-8.2.2/glue-test-0.1"
libexecdir = "/home/tylerwbrown/Code/Play/Comparisons/HaskellvPython/Glue/haskell/.stack-work/install/x86_64-linux-tinfo6/lts-11.22/8.2.2/libexec/x86_64-linux-ghc-8.2.2/glue-test-0.1"
sysconfdir = "/home/tylerwbrown/Code/Play/Comparisons/HaskellvPython/Glue/haskell/.stack-work/install/x86_64-linux-tinfo6/lts-11.22/8.2.2/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "glue_test_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "glue_test_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "glue_test_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "glue_test_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "glue_test_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "glue_test_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
