let
  pkgs = import <nixpkgs> {};
  newPkgs = pkgs // {jdk = pkgs.adoptopenjdk-jre-openj9-bin-11; };
in
with newPkgs;
with import (builtins.fetchGit {
    url = https://github.com/NixOS/nixpkgs-channels;
    ref = "nixos-18.03";
    rev = "cb0e20d6db96fe09a501076c7a2c265359982814";
}) {};
haskell.lib.buildStackProject {
    name = "glue-test";
    buildNativeInputs = [ jdk ];
    buildInputs = [ ghc ];
}
