{-# LANGUAGE LambdaCase #-}
module Main2 where
import System.Exit
import Control.Monad
import Control.Monad.State
import Data.Traversable

main = do
  statements <- words <$> getLine
  void $ runStateT (code statements) 0

code statements = do
  n <- get

  for statements (\case
    "inc" -> put $ n + 1
    "dec" -> put $ n - 1
    "double" -> put $ n * 2
    "half" -> put $ n / 2
    "print" -> lift $ print n
    "exit" -> lift exitSuccess)

