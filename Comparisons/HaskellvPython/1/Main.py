statements = input().split(' ')
n = 0

for statement in statements:
    if statement == "inc":
        n = n + 1
    elif statement == "dec":
        n = n - 1
    elif statement == "double":
        n = n * 2
    elif statement == "half":
        n = n / 2
    elif statement == "print":
        print(n)
    elif statement == "exit":
        exit()
