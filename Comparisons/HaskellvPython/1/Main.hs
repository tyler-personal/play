import System.Exit
import Control.Monad

main = (words <$> getLine) >>= foldM_ f 0

f n "print" = n <$ print n
f _ "exit" = exitSuccess
f n a = return $ case a of
  "inc" -> n + 1
  "dec" -> n - 1
  "double" -> n * 2
  "half" -> n / 2
  _ -> n
