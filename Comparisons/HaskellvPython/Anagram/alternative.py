from toolz import curry
from collections import Counter
from typing import List

def word_count(word: str):
    return Counter(i.lower() for i in word)

def lazy_equals(x: str, y: str):
    return len(x) == len(y) and any(i.lower() != j.lower() for i, j in zip(x, y))

def find_anagrams(word: str, candidates: List[str]): -> List[str]
    count = None
    return (c for c in candidates
              if lazy_equals(c, word)
              and word_count(c) == (count if count else count := word_count(word)))
