{-# LANGUAGE TupleSections #-}
module Alternative where

import Data.Map (fromListWith, Map)
import Data.Char (toLower)
import Data.Function

characterCount :: String -> Map Char Int
characterCount = fromListWith (+) . map (,1) . map toLower

isAnagram :: String -> String -> Bool
isAnagram =
  on (==) length <&&>
  on (/=) (map toLower) <&&>
  on (==) characterCount

findAnagrams :: String -> [String] -> [String]
findAnagrams = filter . isAnagram

(f <&&> g) a b = f a b && g a b
