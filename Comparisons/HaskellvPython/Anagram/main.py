from toolz import curry
from functools import lru_cache
from collections import Counter
from typing import List

@lru_cache
def frequency_of_characters(word: str) -> Counter:
    return Counter(word)

def lazy_not_equals(x: str, y: str) -> bool:
    return any(i != j for i, j in zip(x, y))

@curry
def is_anagram(word: str, candidate: str) -> bool:
    return len(word) == len(candidate) and \
           lazy_not_equals(word, candidate) and \
           frequency_of_characters(word) == frequency_of_characters(candidate)

def find_anagrams(word: str, candidates: List[str]) -> List[str]:
    return list(filter(is_anagram(word), candidates))
