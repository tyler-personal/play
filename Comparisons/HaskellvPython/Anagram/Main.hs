{-# LANGUAGE TupleSections, NoMonomorphismRestriction #-}
import Data.Map (fromListWith, Map)
import Data.Function

frequencyOfCharacters :: String -> Map Char Int
frequencyOfCharacters = fromListWith (+) . map (,1)

isAnagram :: String -> String -> Bool
isAnagram = same length <&&> (/=) <&&> same frequencyOfCharacters

findAnagrams :: String -> [String] -> [String]
findAnagrams = filter . isAnagram

same = on (==)

(f <&&> g) a b = f a b && g a b

main = undefined
