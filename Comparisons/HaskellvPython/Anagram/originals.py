from collections import Counter

def find_anagrams(word, candidates):
    word_counter = Counter(i.lower() for i in word)
    return (candidate for candidate in candidates if len(word) == len(candidate) and
        any(i.lower() != j.lower() for i, j in zip(word, candidate)) and
        Counter(i.lower() for i in candidate) == word_counter)

# helped him get to here (to cache word_counter)
def find_anagrams_2(word, candidates):
    word_counter = None
    return (candidate for candidate in candidates if len(word) == len(candidate) and
        any(i.lower() != j.lower() for i, j in zip(word, candidate)) and
        Counter(i.lower() for i in candidate) == (word_counter if word_counter else word_counter := Counter(i.lower() for i in word))
