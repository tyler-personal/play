def check(str):
    valid = len(input(f"Write your {str}: ")) >= 10
    if not valid:
        print(f"That {str} is too small")
    return valid

while True:
    answer = input('\nOnly say yes/no\nDo you have an account? ')
    if answer in ['yes', 'no'] and all(map(check, ["email", "username", "password"])):
        break
