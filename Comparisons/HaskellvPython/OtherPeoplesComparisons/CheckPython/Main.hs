import Control.Monad
import Control.Monad.Loops
import System.Exit

input str = putStr str >> getLine

check str = do
  valid <- (>= 10) . length <$> input ("Write your " ++ str ++ ": ")
  when valid . putStrLn $ "That " ++ str ++ "is too small"
  return valid

main = forever $ do
  answerValid <- (`elem` ["yes", "no"]) <$> input "\nOnly say yes/no\nDo you have an account? "
  allCheck <- andM $ map check ["email", "username", "password"]
  when (answerValid && allCheck) exitSuccess

--  x <- liftA2 (&&) ((`elem` ["yes", "no"]) <$> input "\nOnly say yes/no\nDo you have an account?") (andM $ map check ["email", "username", "password"])
