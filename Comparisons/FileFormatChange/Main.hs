import System.Directory

input str = do
  putStrLn str
  getLine

main = do
  src <- input "What's the file?"
  newSrc <- input "What's the new name?"
  renameFile src newSrc

