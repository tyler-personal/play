data Tree a = Leaf a | Node a (Tree a) (Tree a)

bfs (Leaf val) = [val]
bfs (Node val left right) = val : merge (bfs left) (bfs right)

dfs (Leaf val) = [val]
dfs (Node val left right) = val : dfs left ++ dfs right

merge [] ys = ys
merge (x:xs) ys = x : merge ys xs

main = print $ bfs node
  where
    node = Node 1
      (Node 2 (Leaf 4) (Leaf 5))
      (Leaf 3)

fact 1 = 1
fact n = n * fact (n - 1)


f n = product [1..n]
