module HaskellSafe where

import Prelude hiding ((+))
data Tree a = Nil | Node a (Tree a) (Tree a)

leaf a = Node a Nil Nil

bfs Nil = []
bfs (Node val left right) = val : merge (bfs left) (bfs right)

dfs Nil = []
dfs (Node val left right) = val : dfs left ++ dfs right

merge [] ys = ys
merge (x:xs) ys = x : merge ys xs

tree = Node 1
  (Node 2 (Node 4 (leaf 5) (leaf 6)) (leaf 7))
  (leaf 3)
