{-# LANGUAGE TypeApplications #-}
import Data.List

qs [] = []
qs (x:xs) = qs less ++ [x] ++ qs more
  where (less, more) = partition (<x) xs

main = do
  vals <- map (read @Int) . words <$> getLine
  print $ qs vals
