module Main1 where
import Control.Lens

type OwnerName = Int
type SlaveName = Int
type Owner = [SlaveName]

createOwners :: Int -> [(SlaveName, OwnerName)] -> [Owner]
createOwners personCount = foldl assignSlave (replicate (personCount + 1) [])

assignSlave :: [Owner] -> (SlaveName, OwnerName) -> [Owner]
assignSlave xs (slave, owner) = xs & element owner %~ (++ [slave])

slaveCounts :: [Owner] -> [Int]
slaveCounts xs = map (slaveCountFor xs) [1..length xs-1]

slaveCountFor :: [Owner] -> OwnerName -> Int
slaveCountFor xs owner = length slaves + sum (map (slaveCountFor xs) slaves)
  where slaves = xs !! owner

main :: IO ()
main = do
  n <- readLn
  xs <- zip [2..] . map read . words <$> getLine
  print . slaveCounts $ createOwners n xs
