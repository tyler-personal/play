import Data.Map hiding (map, foldl)
import Control.Category hiding ((.))
import Control.Applicative
import Data.Function

type Owner = Int
type Slave = Int
type Owners = Map Owner [Slave]

createOwners :: [(Slave, Owner)] -> Owners
createOwners = foldl assignSlave (fromList [])
  where
    assignSlave xs (slave, owner) = insertWith (++) owner [slave] xs

slaveCounts :: Int -> Owners -> [Int]
slaveCounts n xs = map slaveCount [1..n]
  where
    slaveCount owner = length slaves + sum (map slaveCount slaves)
      where slaves = findWithDefault [] owner xs

main :: IO ()
main = do
  n <- readLn
  xs <- zip [2..] . map read . words <$> getLine
  print $ createOwners xs
  -- let n = 200000
  -- xs <- zip [2..] . map read . words <$> readFile "data.txt"
  putStrLn . unwords . map show . slaveCounts n $ createOwners xs

  createOwners xs & (
    slaveCounts n >>>
    map show >>>
    unwords >>>
    putStrLn)

