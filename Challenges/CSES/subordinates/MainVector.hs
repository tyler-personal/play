import Prelude hiding (foldl, map, length, sum, replicate, (++))
import qualified Prelude as P
import Data.Vector hiding (zip)

vec = fromList

type OwnerName = Int
type SlaveName = Int
type Owner = Vector SlaveName

createOwners :: Int -> Vector (SlaveName, OwnerName) -> Vector Owner
createOwners personCount = foldl assignSlave (replicate (personCount + 1) (vec []))

assignSlave :: Vector Owner -> (SlaveName, OwnerName) -> Vector Owner
assignSlave xs (slave, owner) = update xs (fromList [(owner, cons slave slaves)])
  where slaves = xs ! owner
--assignSlave xs (slave, owner) = xs & element owner %~ (++ vec [slave])

slaveCounts :: Vector Owner -> Vector Int
slaveCounts xs = map (slaveCountFor xs) (vec [1..length xs-1])

slaveCountFor :: Vector Owner -> OwnerName -> Int
slaveCountFor xs owner = length slaves + sum (map (slaveCountFor xs) slaves)
  where slaves = xs ! owner

main :: IO ()
main = do
  n <- readLn
  xs <- vec . zip [2..] . P.map read . words <$> readFile "data.txt"
  print . slaveCounts $ createOwners n xs
