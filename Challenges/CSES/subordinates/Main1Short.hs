module Main1Short where
import Control.Lens
g x(s,o)=x&element o%~(++[s])
h x=map(q x)[1..length x-1]
q x o=length s+sum(map(q x)s)where s=x!!o
main=do
  n<-readLn
  zip[2..].map read.words<$>getLine>>=print.h.foldl g(replicate(n+1)[])
