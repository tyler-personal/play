class Slave
  attr_accessor :owner, :count

  def initialize(owner = nil, count = 0)
    @owner = owner
    @count = count
  end

  def inc
    @count += 1
    @owner&.inc
  end
end

_ = gets

nums = gets.split.map(&:to_i)
slaves = { 1 => Slave.new }

nums.each_with_index do |n, i|
  s = Slave.new(owner = slaves[n])
  slaves[i+2] = s
  s.owner.inc
end

puts slaves.map { |_,v| v.count }.join(" ")

