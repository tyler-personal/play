class Slave
  attr_accessor :owner, :count

  def initialize(owner = nil, count = 0)
    @owner = owner
    @count = count
  end

  def inc
    @count += 1
    @owner&.inc
  end
end

_ = gets

nums = gets.split.map(&:to_i)
slaves = [Slave.new, Slave.new]

nums.each do |n|
  s = Slave.new(owner = slaves[n])
  slaves << s
  s.owner.inc
end

puts slaves.drop(1).map(&:count).join(" ")

