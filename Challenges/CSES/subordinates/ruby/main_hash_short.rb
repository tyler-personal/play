gets
s = { 1 => [!0,0] }

gets.split.map.with_index { |n,i|
  n = n.to_i
  x = s[n]
  s[i+2] = [n, 0]

  while x
    x[1] += 1
    x = s[x[0]]
  end
}
puts s.map { |_,v| v[1] }*' '

