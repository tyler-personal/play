require 'stackprof'
StackProf.run(mode: :cpu, out: 'mainprof.dump') do
  n=5
  xs=(1..n).map { |x| [x,[]] }.to_h.merge((1..5000).map(&:to_s).map
    .with_index { |x, i| [x.to_i, i+2] }
    .inject({}) { |xs, (owner, slave)| xs.merge({owner => [slave]}) { |k, a, b| a + b } }
    .to_h)

  f = ->(o) { xs[o].length + xs[o].map { |x| f.call x }.sum }

  puts (1..n).map { |x| f.call x }.join(" ")
end

# n=gets.to_i
# xs=(1..n).map { |x| [x,[]] }.to_h.merge(gets.split.map
#   .with_index { |x, i| [x.to_i, i+2] }
#   .inject({}) { |xs, (owner, slave)| xs.merge({owner => [slave]}) { |k, a, b| a + b } }
#   .to_h)
#
# f = ->(o) { xs[o].length + xs[o].map { |x| f.call x }.sum }
#
# puts (1..n).map { |x| f.call x }.join(" ")
