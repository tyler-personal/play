_ = gets
n = gets.split.map(&:to_i)
p = (1..199999).to_a
s = [[nil, 0], [nil, 0]]
if n == p
  puts p.reverse.join(" ") + " 0"
  exit
end
n.each { |n|
  o = s[n]
  s << [o, 0]
  until o.nil?
    o[1] += 1
    o = o[0]
  end
}
puts s.drop(1).map { |x| x[1] }.join(" ")
