module Zeros where
import Control.Applicative

zeros = sum . liftA2 map div powerOfFives

powerOfFives n = takeWhile (<= n) $ iterate (*5) 5
