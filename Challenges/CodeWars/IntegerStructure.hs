-- https://www.codewars.com/kata/5992757ab2555777fb000067/train/haskell
{-# LANGUAGE TypeApplications #-}
module IntegerStructure (encode, decode) where
import Control.Arrow
import Control.Applicative
import Control.Monad
import Data.List
import Data.Maybe

data State a = Empty | Value a | Container [State a]
  deriving (Eq)

data ProcessType = Factor | Index

instance Show a => Show (State a) where
  show Empty = ""
  show (Value x) = show x
  show (Container xs) = "[" ++ xs' ++ "]"
    where xs' = intercalate ", " (map show xs)

encode :: Integer -> Integer
encode = processToEmpty >>> show
  >>> concatMap bracketToBinary
  >>> compress
  >>> read @Integer
  >>> binaryToDecimal

decode :: Integer -> Integer
decode = decimalToBinary >>> digits
  >>> map binaryToBracket
  >>> decompress
  >>> Container . readState @Integer
  >>> _

bracketToBinary :: Char -> String
bracketToBinary '[' = "1"
bracketToBinary ']' = "0"
bracketToBinary _ = ""

binaryToBracket :: Int -> Char
binaryToBracket 1 = '['
binaryToBracket 0 = ']'
binaryToBracket _ = error "Must be binary"

compress :: String -> String
compress = removeLast '1' . dropLastWhile (== '0') . pinch 1

decompress :: String -> String
decompress str = "[" ++ str ++ "[" ++ replicate diff ']' ++ "]]"
  where
    opened = count (== '[') str
    closed = count (== ']') str
    diff = opened - closed

readState :: String -> [State a]
readState [] = []
readState ('[':']':xs) = Empty : readState xs
readState ('[':xs) = Container (readState inner) : readState outter
  where (inner, outter) = span (/= ']') xs
readState (']':xs) = readState xs
readState _ = error "invalid token, can only read empty states"

processToEmpty :: Integer -> State Integer
processToEmpty = until isEmpty (process Index . process Factor) . Value

factor :: Integer -> State Integer
factor = Container . map Value . primeFactors

process :: ProcessType -> State Integer -> State Integer
process _ Empty = Empty
process _ (Value 2) = Container [Empty]
process Factor (Container [Value x]) = factor x
process Factor (Value x) = factor x
process Index (Value x) = Container [Value ((x @@ primes) + 1)]
process pType (Container xs) = Container $ map (process pType) xs

deprocess :: ProcessType -> State Integer -> State Integer
deprocess _ (Container [Empty]) = Value 2
deprocess _ Empty = Value 2

isEmpty :: State a -> Bool
isEmpty Empty = True
isEmpty (Value _) = False
isEmpty (Container xs) = all isEmpty xs


primes :: [Integer]
primes = 2 : primes'
  where
    primes' = 3 : filter (isPrime primes') [5, 7 ..]

    isPrime :: [Integer] -> Integer -> Bool
    isPrime [] _ = False
    isPrime (p:ps) n = p*p > n || n `rem` p /= 0 && isPrime ps n

primeFactors :: Integer -> [Integer]
primeFactors 1 = []
primeFactors n = smallestPrime : primeFactors (n // smallestPrime)
  where smallestPrime = fromJust . find (n //?) $ primes

binaryToDecimal :: Integer -> Integer
binaryToDecimal 0 = 0
binaryToDecimal n = 2 * binaryToDecimal (n // 10) + (n % 10)

decimalToBinary :: Integer -> Integer
decimalToBinary = read . join . map show . f
  where
    f 0 = [0]
    f n = f (n // 2) ++ [n % 2]

digits :: Integer -> [Int]
digits = map (read . pure) . show

dropLast :: Int -> [a] -> [a]
dropLast n xs = take (length xs - n) xs

dropLastWhile :: (a -> Bool) -> [a] -> [a]
dropLastWhile cond = reverse . dropWhile cond . reverse

removeLast :: Eq a => a -> [a] -> [a]
removeLast x = reverse . delete x . reverse

pinch :: Int -> [a] -> [a]
pinch n = drop n . dropLast n

divisibleBy :: Integer -> Integer -> Bool
divisibleBy = (0 ==) .: (%)

count :: Eq a => (a -> Bool) -> [a] -> Int
count = length .: filter

(//?) :: Integer -> Integer -> Bool
(//?) = divisibleBy

(@@) :: Eq a => a -> [a] -> Integer
(@@) = fromIntegral .: (fromJust .: elemIndex)

(%) :: Integer -> Integer -> Integer
(%) = mod

(//) :: Integer -> Integer -> Integer
(//) = div

(.:) :: (b -> c) -> (a1 -> a2 -> b) -> a1 -> a2 -> c
(.:) = (.) . (.)

removeLastN :: Eq a => Int -> a -> [a] -> [a]
removeLastN 0 _ xs = xs
removeLastN n x xs = removeLastN (n - 1) x (removeLast x xs)

--isPrime :: Integer -> Bool
--isPrime n = none (n //?) [2..(n - 1)]

none :: (a -> Bool) -> [a] -> Bool
none = not .: any
--primes = filter isPrime [2..]
