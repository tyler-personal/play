module Squares where
import Control.Monad
import Control.Category hiding ((.))
import Control.Lens
import Data.List
import Text.Read

(?) b x y = if b then x else y

squareDigitsPipe :: Int -> Int
squareDigitsPipe = show >>> map (pure >>> read >>> (^ 2) >>> show) >>> join >>> read

-- OR
squareDigs :: Int -> Int
squareDigs = read . join . map (show . (^ 2) . read . pure) . show

-- OR
squareDigits :: Int -> Int
squareDigits = read . join . map (show . (^ 2)) . digits'

digits :: Int -> [Int]
digits = map (read . pure) . show

digits' 0 = []
digits' x = digits' (x `div` 10) |> x `mod` 10

