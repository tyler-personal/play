module LengthOfMissingArray where
import Data.List

lengthOfMissingArray xs
  | null xs || any null xs = 0
  | otherwise = head ([minimum lens..maximum lens] \\ lens)
  where lens = map length xs

