module Second where
import Data.List (intercalate)

data Range = Range Int Int

instance Show Range where
  show (Range x y)
    | x == y     = show x
    | x + 1 == y = show x ++ "," ++ show y
    | otherwise  = show x ++ "-" ++ show y

solution :: [Int] -> String
solution = intercalate "," . map show . merge . map (\x -> Range x x)

merge :: [Range] -> [Range]
merge (Range x y : Range y' z : xs)
  | y' `elem` [y,y+1] = merge $ Range x z : xs
  | otherwise = Range x y : merge (Range y' z : xs)
merge xs = xs
