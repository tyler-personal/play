module Third where
import Data.List (intercalate)
import Control.Monad (join, forever)

f = forever (,)
display (x,y)
  | x == y     = show x
  | x + 1 == y = show x ++ "," ++ show y
  | otherwise  = show x ++ "-" ++ show y

solution = intercalate "," . map display . merge . map (join (,))

merge ((x, y) : (y', z) : xs)
  | y' `elem` [y,y+1] = merge $ (x, z) : xs
  | otherwise = (x, y) : merge ((y', z) : xs)
merge xs = xs
