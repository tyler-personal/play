{-# LANGUAGE NoMonomorphismRestriction #-}
module Original where
import Control.Lens
import Control.Lens.At
import Control.Monad
import Control.Monad.Loops
import System.Random
import Data.Maybe
import Control.Applicative

data Tile = E | X | O deriving (Show, Eq)
type Board = [[Tile]]
type Position = (Int, Int)

newBoard :: Board
newBoard = replicate 3 (replicate 3 E)

update :: Tile -> Position -> Board -> Board
update tile (x,y) = ix x . ix y .~ tile

tileSolved :: Tile -> Bool
tileSolved = (`elem` [X, O])

findRandomEmpty :: Board -> IO (Maybe Position)
findRandomEmpty board = do
  x <- randomRIO (0,2)
  y <- randomRIO (0,2)
  case (any (elem E) board, (board !! x !! y) == E) of
    (False, _) -> return Nothing
    (_, True) -> return . Just $ (x,y)
    _ -> findRandomEmpty board

solved :: Maybe Board -> Bool
solved Nothing = True
solved (Just board) = any (liftA2 (&&) allSame ((/= E) . head)) [row1, row2, row3, col1, col2, col3, diag1, diag2]
  where
    row1 = board !! 0
    row2 = board !! 1
    row3 = board !! 2
    col1 = map (!! 0) board
    col2 = map (!! 1) board
    col3 = map (!! 2) board

    topLeft = board !! 0 !! 0
    topRight = board !! 0 !! 2
    bottomLeft = board !! 2 !! 0
    bottomRight = board !! 2 !! 2
    middle = board !! 1 !! 1

    diag1 = [topLeft, middle, bottomRight]
    diag2 = [topRight, middle, bottomLeft]

turn :: Board -> IO (Maybe Board)
turn board = do
  putStr "Input row,column (1-3): "
  (x,y) <- read . (\x -> "(" ++ x ++ ")") <$> getLine

  let playerBoard = update X (x-1, y-1) board
  print playerBoard
  randomPosition <- findRandomEmpty playerBoard

  case randomPosition of
    (Just p) -> do
      let aiBoard = update O p playerBoard
      print aiBoard
      pure . Just $ aiBoard
    Nothing -> pure Nothing

main :: IO ()
main = do
  result <- iterateUntilM solved (turn . fromJust) (pure newBoard)
  putStrLn "Game ended."
  print result


allSame :: Eq a => [a] -> Bool
allSame [] = True
allSame (x:xs) = all (== x) xs
