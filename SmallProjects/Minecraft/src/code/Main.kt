import net.minecraft.init.Blocks
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.common.event.FMLInitializationEvent
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent
import org.apache.logging.log4j.Logger

private const val MOD_ID = "mymod"
private const val NAME = "My Mod"
private const val VERSION = "1.0"

@Mod(modid = MOD_ID, name = NAME, version = VERSION) class ExampleMod {
    private lateinit var logger: Logger

    @Mod.EventHandler fun preInit(event: FMLPreInitializationEvent) {
        logger = event.modLog
    }

    @Mod.EventHandler fun init(event: FMLInitializationEvent) {
        // some example code
        logger.info("DIRT BLOCK >> {}", Blocks.DIRT.registryName)
    }
}
