module Main where
import Lib
import Data.Maybe

data Move = X | O
  deriving (Eq, Show)

type Row = [Maybe Move]
type Board = [Row]

main :: IO ()
main = do
  putStrLn "hello world"

rowSolved :: Row -> Bool
rowSolved = liftA2 (&&) allSame (isJust . head)

solved :: Board -> Bool
solved board = any rowSolved possibleSolutions
  where
    cols = map (\x -> map (!! x) board) [0..2]
    diag1 = map (\i -> board !! i !! i) [0..2]
    diag2 = map (\i -> board !! i !! (2-i)) [0..2]
    possibleSolutions = diag1 : diag2 : board ++ cols
