module Lib
  ( module Control.Lens
  , module Control.Lens.At
  , module Control.Applicative
  , module Lib
  ) where

import Control.Lens.At
import Control.Lens
import Control.Applicative

type Position = (Int, Int)

update :: Position -> a -> [[a]] -> [[a]]
update (x,y) = (ix x . ix y .~)

allSame :: Eq a => [a] -> Bool
allSame [] = True
allSame (x:xs) = all (== x) xs
