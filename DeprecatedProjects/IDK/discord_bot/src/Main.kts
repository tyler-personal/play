import org.javacord.api.DiscordApiBuilder
import org.javacord.api.entity.user.UserStatus
import java.io.File
import java.util.*

val bot = DiscordApiBuilder().setToken(File("token.txt").readText()).login().join()
val t: String = ""

val afkChannel = bot.getServerVoiceChannelById("afkChannelID").get()
bot.serverVoiceChannels.forEach { voiceChannel ->
    val users = voiceChannel.connectedUsers.toList()
    if (users.size == 1 && users[0].status == UserStatus.IDLE)
        users[0].move(afkChannel)
}
bot.command("!tally") { event ->
    val channel = event.channel
    val user = event.messageAuthor.asUser().get()
    user.status
    event.channel.type()
    event.channel.sendMessage("Hello ${event.message.author.name}")
}

interface Test {
    fun code()
}