import org.javacord.api.DiscordApi
import org.javacord.api.event.message.MessageCreateEvent

fun DiscordApi.command(name: String, block: (MessageCreateEvent) -> Unit) {
    addMessageCreateListener { event ->
        if (event.message.content == name)
            block(event)
    }
}