module Main where
import ClassyPrelude
import Web.Scotty

main = scotty 3000 $ do
  get "/" home

home = do
  maybeName <- safeParam @Text "name"
  maybePassword <- safeParam @Text "password"

  user <- liftIO $ case (maybeName, maybePassword) of
    (Just name, Just password) -> do
      user <- get name password
      when (isNothing user) $ save User { name, password, token="16" }
      return user
    (_, _) -> return Nothing

  createTemplate $ homeTemplate maybeName

safeParam :: Parsable a => Text -> ActionM (Maybe a)
safeParam x = do
  p <- param (fromStrict x)
  return (Just p) `rescue` (\_ -> return Nothing)
