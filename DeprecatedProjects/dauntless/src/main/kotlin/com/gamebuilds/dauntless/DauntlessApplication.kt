package com.gamebuilds.dauntless

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DauntlessApplication

fun main(args: Array<String>) {
	runApplication<DauntlessApplication>(*args)
}
