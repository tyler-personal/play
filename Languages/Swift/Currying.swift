func addA() -> (Int) -> (Int) -> (Int) {
  return { a in { b in a + b}}
}

let addB: (Int) -> (Int) -> (Int) =
  { a in { a + $0 } }

// this one is invalid as of Swift 3
// func addC(a: Int)(b: Int) -> Int {
//  return a + b
//}

let addD =
  { a in { b in a + b } }
print(addD(6)(5))
