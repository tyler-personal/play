protocol NumberGetter {
  mutating func getNum() -> Int
}

class Mutable: NumberGetter {
  private var value = 0

  mutating func getNum() -> Int {
    self.value += 1
    return value
  }
}

class Immutable: NumberGetter {
  func getNum() -> Int { 0 }
}

let constant = (Immutable(), Mutable())
print(constant.1.getNum())
