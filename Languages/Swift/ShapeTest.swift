protocol Shape {
  func draw() -> String
}

class Triangle: Shape {
  func draw() -> String {
    return "I'm a triangle."
  }
}

func shape() -> some Shape {
  return secondShape()
}

func secondShape() -> Shape {
  return Triangle()
}

func isEmpty(xs: Array) -> Bool {
  return xs.count == 0
}

print(isEmpty([1,2,3]))

