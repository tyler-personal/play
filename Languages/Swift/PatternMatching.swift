func qs <T: Comparable> (_ li: [T]) -> [T] {
  switch(li) {
    case []:
      return []
    default:
      let x = li[0]
      let xs = li.dropFirst(1)
      return qs(xs.filter { $0 < x }) + [x] + qs(xs.filter { $0 >= x })
  }
}

print(qs([4,3,1,7,8]))
