{-# LANGUAGE NoMonomorphismRestriction #-}
import System.TimeIt
-- import Data.Vector
import Control.Applicative

xs = [1..500]

a = [x | x <- xs, y <- xs, z <- xs, x + y + z == 1600]
b = sequence [xs, xs, xs]

main = timeIt $ print (length b)
