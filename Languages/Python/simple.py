def f(x, y):
    result = []
    for a in reversed(range(x)):
        b = a * 7
        if b > y:
            result.append(b + " " + b)
    return result
