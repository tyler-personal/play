from random import randint
from time import time


def filter_version(xs):
    neg_xs = [x for x in xs if x < 0]
    pos_xs = [x for x in xs if x >= 0]

    for x in xs:
        for y in xs:
            if x >= 0 and y >= 0:
                _xs = neg_xs
            elif x < 0 and y < 0:
                _xs = pos_xs
            else:
                _xs = xs

            for z in _xs:
                if x + y + z == 0:
                    return (x,y,z)
    return None

def normal_version(xs):
    for x in xs:
        for y in xs:
            for z in xs:
                if x + y + z == 0:
                    return (x,y,z)


def time_f(f):
    s = time()
    x = f()
    print(time() - s)
    return x

xs = [randint(-109871234, 109871234) for _ in range(8000)]
time_f(lambda: filter_version(xs))
time_f(lambda: normal_version(xs))
