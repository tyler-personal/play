def f(x, /, y, *, z):
    return [f"{b} {b} :: {z}" for a in range(x)[:1:-2] if (b := a * 7) > y]

print(f(10, 20))

# g = lambda a, b: a * b
