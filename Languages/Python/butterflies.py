print("Welcome")

total = 0
days = 0

while (butterflies := int(input("Enter # of butterflies or -1: "))) != -1:
    days += 1
    total += butterflies

print(f"You have collected {total} over {days} days")
print(f"The average is: {total / days}")
