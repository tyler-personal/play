import asyncio
import websockets

async def hello():
    uri = "ws://192.168.1.32:8001"
    async with websockets.connect(uri) as websocket:
        x = input("What's your name? ")
        await websocket.send(f"I am {x}")
        result = await websocket.recv()
        print("Server:", result)
        while True:
            await websocket.send(input("> "))

asyncio.get_event_loop().run_until_complete(hello())
