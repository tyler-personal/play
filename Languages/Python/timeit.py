from itertools import repeat
from time import time

N = 44222111

def f1():
    return list(map(lambda x: x * 2, range(N)))

def f2():
    return [x + x for x in range(N)]

def f3():
    return list(repeat(1 + 1, N))

def time_it(f):
    start = time()
    print(f()[-1])
    end = time()
    print(f"{end - start}s")

if __name__ == "__main__":
    for f in [f1, f2, f3]:
        time_it(f)
