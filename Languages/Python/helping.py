:def getNumberSafely(message, 
        default = None, 
        errorMessage = "You can only input numbers!", 
        condition = None,
        conditionMessage = "Condition failed"):
    try:
        val = int(input(message))
        if condition != None:
            if condition(val):
                return val
            else:
                print(conditionMessage)
        else:
            return val
    except ValueError:
        if default != None:
            return default
        else:
            print(errorMessage)
    return getNumberSafely(message, default, errorMessage, condition, conditionMessage)

beginningSalary = getNumberSafely(
        "Please enter your beginning salary: ", 
        condition = lambda x: x > 10000, 
        conditionMessage = "Sorry your beginning salary must be at least 10000")
