class Person:
  def __init__(self, name):
    self.name = name

  def say_name(self):
    print(f"Hi, my name is {self.name}")

people = map(Person, ["Bob", "Joe", "Billy"])

for person in people:
    person.say_name()
