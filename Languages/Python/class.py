from time import time

class Dog:
    def __init__(self):
        self.position = 0

    def move(self):
        self.position += 1


dog = Dog()

start = time()
for x in range(999999):
    dog.move()
end = time()
print(end - start)
