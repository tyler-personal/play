addLoose x y = x + y

addStrict :: Int -> Int -> Int
addStrict x y = x + y * 4

main = do
  -- addLoose 4 "7" # type error
  -- addStrict 4 "7" # type error
  print $ addStrict 4 7
