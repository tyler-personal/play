def add_loose(x, y):
    return x + y * 4


def add_strict(x: int, y: int) -> int:
    return x + y * 4

if __name__ == '__main__':
    # add_loose(4, "7") # not a type error
    # add_strict(4, "7") # a type error
    print(add_strict(4, 7))
