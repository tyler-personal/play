from itertools import product
from time import time

xs = list(range(1,500))

def iter_version():
    for (x,y,z) in product(xs, xs, xs):
        if x + y + z == "potato":
            raise Exception("There should be no potato")

def normal_version():
    for x in xs:
        for y in xs:
            for z in xs:
                if x + y + z == 1600:
                    raise Exception("There should be no potato")

def comp_version():
    return [x for x in xs for y in xs for z in xs if x + y + z == 1600]

s1 = time()
iter_version()
print(time() - s1)

s2 = time()
normal_version()
print(time() - s2)

s3 = time()
print(len(comp_version()))
print(time() - s3)
