def add_imports(dir_name):
    for file_name in [f"{dir_name}/{f}" for f in set(proto_file_dict.values())]:
        with open(file_name, "r") as file:
            (lines, import_list) = get_imports(file)

        lines.insert(3, "\n".join(f'import "{item}";' for item in import_list))

        with open(file_name, "w") as file:
            file.writelines(lines)


def get_imports(file):
    lines = file.readlines()
    return (lines, set(
        [proto_file_dict[message_type]
         for line in lines
         for message_type in proto_file_dict.keys()
         if line_contains_message_type(line, message_type, file)]))


def line_contains_message_type(line, message_type, file):
    return line.find(message_type) != NOT_FOUND and \
           line.find(MESSAGE_TAG) == NOT_FOUND and \
           proto_file_dict[message_type] != os.path.basename(file.name)
