import Data.Time
import Data.Traversable

n = 44222111

f1 = map (*2) [0..n]

f2 = [x + x | x <- [0..n]]

f3 = replicate n $ 1 + 1

timeIt f = do
  start <- getCurrentTime
  print $ last f
  end <- getCurrentTime
  print $ diffUTCTime end start

main = for [f1, f2, f3] timeIt
