def add_imports(dir_name):
    for file_name in file_names():
        with open(f"{dir_name}/{file_name}", "r+") as file:
            write_import_to_file(file)


def write_import_to_file(file):
    (lines, import_list) = get_imports(file)
    imports = "\n".join(f'import "{item}";' for item in import_list)
    lines.insert(3, imports)

    file.seek(0, 0)
    file.truncate()
    file.writelines(lines)


def get_imports(file):
    lines = file.readlines()
    return (lines, set(
        [proto_file_dict[message_type]
         for line in lines
         for message_type in message_types()
         if line_contains_message_type(line, message_type, file)]))


def line_contains_message_type(line, message_type, file):
    return line.find(message_type) != NOT_FOUND and \
           line.find(MESSAGE_TAG) == NOT_FOUND and \
           proto_file_dict[message_type] != name(file)



def message_types():
    return proto_file_dicts.keys()

def file_names():
    return proto_file_dicts.values()
name = lambda file: os.path.basename(file.name)
