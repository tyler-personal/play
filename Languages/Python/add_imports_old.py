def add_imports(dir_name):

    import_list = set()
    import_buf = ""
    for file_name in set(proto_file_dict.values()):
        with open(dir_name + file_name, "r+") as proto_file:
            lines = []
            for line in proto_file:
                for message_type in set(proto_file_dict.keys()):
                    if line.find(message_type != NOT_FOUND and line.find(MESSAGE_TAG) == NOT_FOUND):
                        if proto_file_dict[message_type] != file_name:
                            import_list.add(proto_file_dict[message_type])
                lines += [line]

            for item in import_list:
                import_buf += f"import \"{item}\";\n"

            lines.insert(3, import_buf)
            import_buf = ""
            import_list = set()
            proto_file.seek(0, 0)
            proto_file.truncate()

            for line in lines:
                proto_file.write(line)
            print("Wrote imports to " + str(file_name))