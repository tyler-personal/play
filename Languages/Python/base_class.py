from toolz import curry
class BaseClass:
    def run(self):
        self.method(User())

class DerivedClass(BaseClass):
    def method(self):
        return User.say_name

class User:
    def __init__(self):
        self.name = "yeet"

    def say_name(self):
        print(self.name)

DerivedClass().run()
