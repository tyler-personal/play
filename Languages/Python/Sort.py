if x := 0:
    print(4)
else:
    print("yacht")
def s(a):
    if len(a) > 1 and (i := a.index(min(a))):
        print(a)
        print(f"INDEX: {i}. VALUE: {a[i]}")
        return ([a[i]] + s(a[:i]+a[i+1:]))
    else:
        return a
sort = lambda a: ([a[i-1]] + sort(a[:i-1]+a[i:])) if len(a) > 1 and (i := (a.index(min(a)) + 1)) else a

def sortLong(items):
    if not items:
        return []
    items = items[:]
    minimum = min(items)
    items.remove(minimum)
    return [minimum] + sortLong(items)


xs = [1,4,0,2,3,1,0,-1,7]
print(sort(xs))
print(sortLong(xs))
