class A:
    def __init__(self):
        self.name = "potato"

    @staticmethod
    def main():
        a = A()
        print(a.name)

A.main()
