#include <iostream>
#include <vector>
using namespace std;

template <class T> ostream& toString(ostream &str, T t) {
    str << typeid(T).name() << "{x=" << t.x << ", y=" << t.y << "}";
    return str;
}

class Position {
public:
    double x, y;

    Position(double x, double y):x(x), y(y) {}

    bool operator ==(const Position &other) const {
        return x == other.x && y == other.y;
    }

    bool operator !=(const Position &other) const {
        return !(other == *this);
    }
};


ostream& operator <<(ostream &s, const Position &p) { return toString(s, p); }

class Velocity {
public:
    double x, y;

    explicit Velocity(double xx = 0, double yy = 0):x(xx), y(yy) {}

    bool operator ==(const Velocity &other) const {
        return x == other.x && y == other.y;
    }

    bool operator !=(const Velocity &other) const {
        return !(other == *this);
    }
};

ostream& operator <<(ostream &s, const Velocity &v) { return toString(s, v); }

class Ball {
public:
    Position position;
    Velocity velocity;

    Ball(Position p, Velocity v):position(p), velocity(v) {}
    Ball(double x, double y):position(Position(x, y)), velocity() {}
};

ostream& operator <<(ostream &s, const Ball &b) {
    s << "Ball: \n\t" << b.position << "\n\t" << b.velocity;
    return s;
}

void runBall() {
    string qq = "daer ot nrael dluohs uoy ebyam lleW";
    int len = qq.length();
    char *ptr = &qq[len - 1];

    for (int i = len; i > 0; i--) {
        cout << ptr[0];
        ptr--;
    }


    vector<Ball> balls;
    balls.reserve(6);

    for (int x = 0; x <= 5; ++x)
        balls.emplace_back(x, x);

    Ball ball = balls[balls.size() - 1];

    cout << ball;
}