#include <vector>
#include <string>
#include <algorithm>
#include <iostream>
#include <array>

template <typename X>
using namespace std;
void runTransformations() {
    const vector<string> names {"potato", "muffin", "oofsted", "alamony", "ruff"};
    vector<int> _lengths;
    transform(names.begin(), names.end(), back_inserter(_lengths), [](const auto& s) { return s.length(); });
    const auto lengths = _lengths;

    for (int length : lengths)
        cout << length << " ";

    cout << "\n";
}

void runTransformations2() {
    const string names[] {"potato", "muffin", "oofsted", "alamony", "ruff"};
    int lengths[5];

    for(int i = 0; i < 5; i++)
        lengths[i] = names[i].length();
}

void runTransformations3() {
    const string names[] {"potato", "muffin", "oofsted", "alamony", "ruff"};
    int lengths[5];
    // transform(names.begin(), names.end(), back_inserter(lengths), [](const auto& s) { return s.length(); });

}

void runTransformations4() {
    const auto names = std::array<std::string, 5>{"potato", "muffin", "oofsted", "alamony", "ruff"};
    const auto lengths = ([&](){
        std::vector<int> lengths;
        transform(names.cbegin(), names.cend(), back_inserter(lengths), [](const auto& s) { return s.length(); });
        return lengths;
    })();

    for (int length : lengths)
        cout << length << " ";

    cout << "\n";
}

template<typename F, typename FX, typename FY>
auto get(F f, FX fx, FY fy) {
    return std::array<std::result_of_t<F>, 2>{f(fx), f(fy)};
}

void test() {
    get(2, 3, 4);
}