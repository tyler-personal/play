cmake_minimum_required(VERSION 3.14)
project(main)

set(CMAKE_CXX_STANDARD 14)


add_executable(main main.cpp Ball.cpp ball.h comparisons/transformations.cpp comparisons/transformations.h)