//#include "ball.h"
//#include "comparisons/transformations.h"
#include <iostream>

auto f(auto x, auto y) {
    std::cout << x + y << "\n";
}

int main() {
    double a = 4.2;
    double b = 4.7;

    int c = 3;
    int d = 7;

    std::string x = "41";
    std::string y = "73";

    f(a, b);
    f(c, d);
    f(x, y);
}
//int main() {
//    runTransformations();
//}
