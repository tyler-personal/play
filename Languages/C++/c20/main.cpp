#include <iostream>


template<typename T, typename U>
U execute(U f(T), T x) {
    return f(x);
}

int g(int x) { return x + x; }

auto f(auto x, auto y) {
    std::cout << x + y << "\n";
}

int main() {
    double a = 4.2;
    double b = 4.7;

    int c = 3;
    int d = 7;

    std::string x = "41";
    std::string y = "73";

    f(a, b);
    f(c, d);
    f(x, y);
    f(a, c);

    int q = execute(g, 3);
    std::cout << execute(g, 3);
}
