interface Runner
interface Jumper
interface Swimmer
interface Diver

fun <T> testAllAthleticism(t: T) where T : Runner, T : Jumper, T : Swimmer, T : Diver {}
fun <T> testGroundAthleticism(t: T) where T : Runner, T : Jumper {}
fun <T> testWaterAthleticism(t: T) where T : Swimmer, T : Diver {}
fun <T> testSwimming(t: T) where T : Swimmer {}

inline fun <reified T> runTest(t: T) {

    when {
        t is Runner && t is Jumper && t is Swimmer && t is Diver -> testAllAthleticism(t) // compiler error
        t is Runner && t is Jumper -> testGroundAthleticism(t) // compiler error
        t is Swimmer && t is Diver -> testWaterAthleticism(t) // compiler error
        t is Swimmer -> testSwimming(t) // works
    }
}