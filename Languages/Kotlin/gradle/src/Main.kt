import kastree.ast.Node
import kastree.ast.Visitor
import kastree.ast.Writer
import kastree.ast.psi.Parser
import org.jetbrains.kotlin.com.intellij.psi.PsiElement
import org.jetbrains.kotlin.psi.KtDotQualifiedExpression
import org.jetbrains.kotlin.psi.KtFile
import org.jetbrains.kotlin.psi.KtStringTemplateExpression
import org.jetbrains.kotlin.psi.psiUtil.allChildren
import org.jetbrains.kotlin.psi.psiUtil.isDotReceiver

val code = """
    package test

    class Test(val name: String)
    fun aye() {
        println("Hello world!")
        "Test".toUpperCase()
        "Test".toUpper
        2.
        false.
        "Test".
    }

    fun baz() = println("Hello, again!")
""".trimIndent()


// println(Writer.write(file))
//    (file2.declarations[0] as KtNamedFunction).bodyBlockExpression.statements
//        .filterIsInstance<KtDotQualifiedExpression>()
//        .map { it.children[1].isValid }
// (file2.declarations[0] as KtNamedFunction).bodyBlockExpression.statements.map { it.text }

// file2.declarations.flatMap { it.children.toList() }.flatMap { it.children.toList() }.filterIsInstance<KtDotQualifiedExpression>().map { it.children
fun main() {
    autoCompleteSuggestions(code, 3)
}

data class Suggestion(val name: String, val type: String)

fun autoCompleteSuggestions(code: String, lineNum: Int): List<Suggestion> {
    val file2 = Parser.parsePsiFile(code)
    val dotExpressions = file2
        .allChildrenList
        .filterIsInstance<KtDotQualifiedExpression>()
        .filterNot { it.isDotReceiver() }
    print(dotExpressions)
    return listOf()
}

val KtFile.allChildrenList: List<PsiElement> get() =
    generateSequence(listOf<PsiElement>() to children.toList()) { (acc, e) ->
        val new = e.flatMap { it.children.toList() }
        acc + new to new
    }.takeWhile { (_, e) -> e.isNotEmpty() }.last().first

