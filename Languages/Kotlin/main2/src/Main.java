import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiFunction;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        System.out.println("Greetings");
        System.out.println(this.clone());
        System.out.println(Rotation.left(List.of(1,2,3,4)));
        Byte x = 125;
        //var nxs = List.of(x);
//        List<Number> nums = List.of(1,2.2,3,4);
//        List<Number> nums2 = List.of(0,1,2,3);
//
//        System.out.println(addDiffs(nums, nums2, Number.class));
//        Scanner scanner = new Scanner(System.in);
//
//        int[] result = subtractNumbers(scanner.nextLine());
//        System.out.print("[");
//        List<Integer> xs = List.of(1,2);
//
//        final var res = xs.stream().reduce(Integer::sum).orElse(0);
//
//
//        for (int i = 0; i < result.length; i++) {
//            if (i < result.length - 1)
//                System.out.print(result[i] + ", ");
//            else
//                System.out.print(result[i]);
//        }
//
//        System.out.print("]");
    }



    public static int[] subtractNumbers(String str) {
        int resultCount = (int) Math.ceil(str.length() / 4.0);
        int[][] numbers = new int[2][resultCount];
        int numbersIndex = 0;
        boolean splitFound = false;
        int countOfNumbersPrecedingThis = 0;

        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            int x = Character.getNumericValue(c);
            if (!splitFound && c == '|') {
                splitFound = true;
                numbersIndex = 0;
                countOfNumbersPrecedingThis = 0;
            }
            else if (c != ',' && c != ' ') {
                int index = splitFound ? 1 : 0;
                if (countOfNumbersPrecedingThis > 0) {
                    int value = numbers[index][numbersIndex - 1];
                    int valueToAdd = (int) Math.pow((x), countOfNumbersPrecedingThis + 1);
                    numbers[index][numbersIndex - 1] = value + valueToAdd;
                    numbersIndex -= 1;
                } else {
                    numbers[index][numbersIndex] = x;
                }
                numbersIndex++;
                countOfNumbersPrecedingThis++;
            }
            else {
                countOfNumbersPrecedingThis = 0;
            }
        }

        int[] results = new int[resultCount];

        int[] xs = numbers[0];
        int[] ys = numbers[1];

        for (int i = 0; i < resultCount; i++)
            results[i] = xs[i] - ys[i];

        return results;
    }

    // Potential generic solution that doesn't work :(
//    final Function<Number, Function<Number, Function<BiFunction<Number, Number, T>, T>>> convert = x -> y -> f -> {
//        if (klass == Double.class)
//            return klass.cast(f.apply(x.doubleValue(), y.doubleValue()));
//        else if (klass == Float.class)
//            return klass.cast(f.apply(x.floatValue(), y.floatValue()));
//        else if (klass == Long.class)
//            return klass.cast(f.apply(x.longValue(), y.longValue()));
//        else if (klass == Integer.class)
//            return klass.cast(f.apply(x.intValue(), y.intValue()));
//        else if (klass == Byte.class)
//            return klass.cast(f.apply(x.byteValue(), y.byteValue()));
//        else if (klass == Short.class)
//            return klass.cast(f.apply(x.shortValue(), y.shortValue()));
//        else
//            return klass.cast(f.apply(x.doubleValue(), y.doubleValue()));
//    };

public static <T extends Number> T addDiffs(
        List<T> xs, List<T> ys, Class<T> klass
) {
    final BiFunction<Number, Number, T> subtract = (x, y) -> {
        if (klass == Double.class)
            return klass.cast(x.doubleValue() - y.doubleValue());
        else if (klass == Float.class)
            return klass.cast(x.floatValue() - y.floatValue());
        else if (klass == Long.class)
            return klass.cast(x.longValue() - y.longValue());
        else if (klass == Integer.class)
            return klass.cast(x.intValue() - y.intValue());
        else if (klass == Byte.class)
            return klass.cast(x.byteValue() - y.byteValue());
        else if (klass == Short.class)
            return klass.cast(x.shortValue() - y.shortValue());
        else
            return klass.cast(x.doubleValue() - y.doubleValue());
    };
    
    final BiFunction<Number, Number, T> add = (x, y) -> {
        if (klass == Double.class)
            return klass.cast(x.doubleValue() + y.doubleValue());
        else if (klass == Float.class)
            return klass.cast(x.floatValue() + y.floatValue());
        else if (klass == Long.class)
            return klass.cast(x.longValue() + y.longValue());
        else if (klass == Integer.class)
            return klass.cast(x.intValue() + y.intValue());
        else if (klass == Byte.class)
            return klass.cast(x.byteValue() + y.byteValue());
        else if (klass == Short.class)
            return klass.cast(x.shortValue() + y.shortValue());
        else
            return klass.cast(x.doubleValue() + y.doubleValue());
    };

    final Supplier<T> orElse = () -> {
        if (klass == Double.class)
            return klass.cast(0D);
        else if (klass == Float.class)
            return klass.cast(0F);
        else if (klass == Long.class)
            return klass.cast(0L);
        else if (klass == Integer.class)
            return klass.cast(0);
        else if (klass == Byte.class)
            return klass.cast(0);
        else if (klass == Short.class)
            return klass.cast(0);
        else
            return klass.cast(0D);
    };

    return IntStream.range(0, xs.size())
        .mapToObj(i -> subtract.apply(xs.get(i), ys.get(i)))
        .reduce(add::apply).orElse(orElse.get());
}

    public static <T extends Number> double addDiffs2(List<T> xs, List<T> ys) {
        return IntStream.range(0, xs.size())
            .mapToDouble(i -> xs.get(i).doubleValue() - ys.get(i).doubleValue())
            .sum();
    }
}
