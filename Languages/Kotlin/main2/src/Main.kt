import java.util.*
import java.io.*
import java.math.*
import kotlin.reflect.KClass

data class Tuple<T, Q>(val _1: T, val _2: Q)
fun main() {
    val xs = listOf(1,2,3,4)
    val ys = listOf(0,1,2,3)

    val result = xs.zip(ys).map { (x,y) -> x - y }.sum()

    println(result)

    val res2 = addDiffs(xs, ys)
    println(res2)
}

inline fun <reified T: Number> addDiffs(xs: List<T>, ys: List<T>): T {
    val convert = when (T::class) {
        Double::class -> Number::toDouble
        Float::class -> Number::toFloat
        Long::class -> Number::toLong
        Int::class -> Number::toInt
        Byte::class -> Number::toByte
        Short::class -> Number::toShort
        else -> Number::toDouble
    }
    throw Exception("shrug")
    // return xs.zip(ys).map { (x,y) -> convert(x) - convert(y) }.sum()
}

fun addDifferences(xs: List<Number>, ys: List<Number>) =
    xs.zip(ys).map { (x, y) -> x.toDouble() - y.toDouble() }.sum()