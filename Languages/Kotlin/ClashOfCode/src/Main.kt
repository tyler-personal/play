import java.util.*

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
fun main(args : Array<String>) {
    val input = Scanner(System.`in`)
    val a = input.nextInt()
    val b = input.nextInt()
    val n = input.nextInt()
    for (i in 0 until n) {
        val x = input.nextInt()
        println(a*x + b)
    }

    // Write an action using println()
    // To debug: System.err.println("Debug messages...");

    println("answer")
}