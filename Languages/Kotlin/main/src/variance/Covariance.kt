package variance

private class Tuple2<T, Q>(val _1: T, val _2: Q) {
    operator fun component1() = _1
    operator fun component2() = _2
}

private interface CovariantList<T> {
    fun addAll(list: CovariantList<out T>)
    fun <Q: T> addAllAndReturnNewOld(list: CovariantList<Q>): Tuple2<CovariantList<T>, CovariantList<Q>>
}

private fun testCovariance(all: CovariantList<Any>, names: CovariantList<String>) {
    val (newAll: CovariantList<Any>, newNames: CovariantList<String>) = all.addAllAndReturnNewOld(names)
}

private fun <T, Q: T> testCovariance2(ye1: CovariantList<T>, ye2: CovariantList<Q>) {
    ye1.addAll(ye2)
}