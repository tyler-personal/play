package variance;

public class Covariance {
    private class Tuple2<T, Q> {
        private T _1;
        private Q _2;

        public Tuple2(T _1, Q _2) {
            this._1 = _1;
            this._2 = _2;
        }

        T get_1() { return _1; }
        Q get_2() { return _2; }
    }

    private interface CovariantList<T> {
        <Q extends T> Tuple2<CovariantList<T>, CovariantList<Q>> addAllAndReturnNewOld(CovariantList<Q> list);
    }

    private void testCovariance(CovariantList<Object> all, CovariantList<String> names) {
        final var tuple = all.addAllAndReturnNewOld(names);
        final CovariantList<Object> newAll = tuple.get_1();
        final CovariantList<String> newNames = tuple.get_2();

    }
}
