package cinchapi;

class FavoriteType<T> {
    enum Type {
        COLOR, NUMBER, NAME
    }

    static final FavoriteType<String> COLOR =
            new FavoriteType<>(Type.COLOR);

    static final FavoriteType<Integer> NUMBER =
            new FavoriteType<>(Type.NUMBER);

    static final FavoriteType<String> NAME =
            new FavoriteType<>(Type.NAME);

    final Type type;

    private FavoriteType(Type type) {
        this.type = type;
    }
}

class FavoriteThing<T> {
    FavoriteType<T> type;
    T value;

    FavoriteThing(FavoriteType<T> type, T value) {
        this.type = type;
        this.value = value;
    }
}

class Runner {
    public static void main(String[] args) {
        final var thing = new FavoriteThing<>(FavoriteType.COLOR, "Blue");
    }
}