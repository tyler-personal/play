fun <T> getInput(output: String, invalidOutput: String, validation: (String) -> T?): T {
    println(output)
    return validation(readLine()!!) ?: getInput("$invalidOutput\n$output", "", validation)
}

fun <T> getInput2(invalid: String, validation: (String) -> T?): T =
    validation(readLine()!!) ?: { println("Invalid Input; $invalid"); getInput2(invalid, validation) }()