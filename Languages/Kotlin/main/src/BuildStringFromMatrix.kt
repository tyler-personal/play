typealias Matrix<T> = List<List<T>>

fun <T> List<T>.chopStartEnd(x: Int = 1) = drop(x).dropLast(x)
fun <T> List<T>.takeIfNotDuplicate(block: (List<T>) -> T) = if (size > 1) block(this) else null

fun <T: Any> Matrix<T>.buildOutside(): Pair<String, Matrix<T>> {
    val topRow = get(0)
    val rightColumn = map { it.last() }.chopStartEnd()
    val bottomRow = takeIfNotDuplicate { last() } ?: listOf()
    val leftColumn = this.mapNotNull { it.takeIfNotDuplicate { it[0] } }.chopStartEnd()

    val remaining = chopStartEnd().map { it.chopStartEnd() }
    return (topRow + rightColumn + bottomRow.reversed() + leftColumn).fold("") { x, y -> "$x$y " } to remaining
}

fun <T: Any> List<List<T>>.buildString() = generateSequence("" to this) { (_, remaining) ->
    remaining.takeIf { it.isNotEmpty() }?.buildOutside()
}.map { it.first }.reduce { x, y -> x + y }


fun main() {
    println(listOf(listOf(2,3,4,8), listOf(5,7,9,12), listOf(1,0,6,10)).buildString())
}
