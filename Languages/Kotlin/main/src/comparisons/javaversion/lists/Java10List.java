package comparisons.javaversion.lists;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Java10List {
    public static void main(String[] args) {
        final var names = List.of("potato", "muffin", "oofsted", "alamony", "ruff");
        final var lengths = names.stream().map(String::length).collect(Collectors.toUnmodifiableList());
        System.out.println(lengths);
    }
}
