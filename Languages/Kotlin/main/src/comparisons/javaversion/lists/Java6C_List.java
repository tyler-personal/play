package comparisons.javaversion.lists;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Java6C_List {
    public static void main(String[] args) {
        final String[] names = {"potato", "muffin", "oofsted", "alamony", "ruff"};
        int[] lengths = new int[5];

        for (int i = 0; i < names.length; i++)
            lengths[i] = names[i].length();

    }
}
