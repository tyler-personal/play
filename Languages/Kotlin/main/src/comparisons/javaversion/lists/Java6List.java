package comparisons.javaversion.lists;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Java6List {
    public static void main(String[] args) {
        List<String> _names = new ArrayList<String>();
        _names.add("potato");
        _names.add("muffin");
        _names.add("oofsted");
        _names.add("alamony");
        _names.add("ruff");

        final List<String> names = Collections.unmodifiableList(_names);

        List<Integer> _lengths = new ArrayList<Integer>();

        for (String name : names)
            _lengths.add(name.length());

        final List<Integer> lengths = Collections.unmodifiableList(_lengths);
    }
}
