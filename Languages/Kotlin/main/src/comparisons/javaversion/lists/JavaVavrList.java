package comparisons.javaversion.lists;

import io.vavr.collection.List;
import lombok.val;

public class JavaVavrList {
    public static void main(String[] args) {
        final var names = List.of("potato", "muffin", "oofsted", "alamony", "ruff");
        final var lengths = names.map(String::length);
    }
}
