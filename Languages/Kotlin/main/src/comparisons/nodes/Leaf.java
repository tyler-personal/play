package comparisons.nodes;

public class Leaf<T> extends Node<T> {
    public Leaf(T val) {
        this.val = val;
    }
}