package comparisons.nodes;

public class ParentNode<T> extends Node<T> {
    public Node<T> left;
    public Node<T> right;

    public ParentNode(T val, Node<T> left, Node<T> right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}