package comparisons.nodes;

import java.util.AbstractMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NodeUtils {
    public static <T> List<T> bfs(Node<T> node) {
        if (node instanceof Leaf)
            return List.of(node.val);
        else {
            final var _left = uncons(bfs(((ParentNode<T>) node).left));
            final var _right = uncons(bfs(((ParentNode<T>) node).right));

            final var lVal = _left.getKey();
            final var left = _left.getValue();

            final var rVal = _right.getKey();
            final var right = _right.getValue();

            return concat(List.of(node.val), List.of(lVal), List.of(rVal), left, right);
        }
    }

    public static <T> List<T> dfs(Node<T> node) {
        final List<T> newList = node instanceof Leaf
                ? List.of()
                : concat(dfs(((ParentNode<T>) node).left), dfs(((ParentNode<T>) node).right));
        return concat(List.of(node.val), newList);
    }

    @SafeVarargs
    private static <T> List<T> concat(List<T> ... lists) {
        return Stream.of(lists).flatMap(List::stream).collect(Collectors.toUnmodifiableList());
    }

    private static <T> AbstractMap.SimpleImmutableEntry<T, List<T>> uncons(List<T> list) {
        final var head = list.get(0);
        final var tail = list.subList(1, list.size());
        return new AbstractMap.SimpleImmutableEntry<>(head, tail);
    }
}

//    public static void main(String[] args) {
//        var node = new ParentNode<>(1,
//                new ParentNode<>(2,
//                        new Leaf<>(4), new Leaf<>(5)),
//                new Leaf<>(3));
//        System.out.println(dfs(node));
//    }