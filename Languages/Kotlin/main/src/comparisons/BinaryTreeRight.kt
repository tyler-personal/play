package comparisons

//sealed class Node<T>(val value: T)
//
//class Leaf<T>(value: T) : Node<T>(value)
//
//class Branch<T>(value: T, val left: Node<T>?, val right: Node<T>?) : Node<T>(value)
//
//fun <T> rightValues(nodes: List<Node<T>>) = when {
//    nodes.isEmpty() -> listOf()
//    else -> listOf(nodes.last().value) + rightValues(nodes.flatMap { it.children() }
//}
//
//fun <T> Node<T>.children() = when(this) {
//    is Leaf -> listOf()
//    is Branch -> listOfNotNull(left, right)
//}

class TreeNode(var `val`: Int) {
    var left: TreeNode? = null
    var right: TreeNode? = null
}

class Solution {
    fun rightSideView(root: TreeNode?): List<Int> {
        return rightValues(listOfNotNull(root))
    }
}

fun rightValues(nodes: List<TreeNode>): List<Int> {
    val nextRow = nodes.flatMap { it.children() }
    return when {
        nodes.isEmpty() -> listOf()
        else -> listOf(nodes.last().`val`) + rightValues(nextRow)
    }
}

fun TreeNode.children() = listOfNotNull(left, right)