package comparisons.idris_conditions;


import java.util.AbstractMap;
import java.util.List;

public class Take {
    /*
     * @param items generic list from which result will be derived
     * @param index number used for retrieving items
     *
     * @pre (items.size() / 2) > index
     * @pre index > 0
     * @pre items cannot be null
     *
     * @post result != null
     * @post result does not contain nulls
     *
     * @inv items
     *
     * @return tuple of item at index and twice the index
     * @throws IllegalArgumentException if items contains a null
     * @throws IllegalArgumentException if index is out of the precondition bounds
     */
    public static <T> AbstractMap.SimpleImmutableEntry<T, T> take(List<T> items, int index) {
        for (var item : items)
            if (item == null)
                throw new IllegalArgumentException("Items cannot be null.");

        if (index > items.size() / 2 || index < 0)
            throw new IllegalArgumentException("Index is out of precondition bounds.");

        return new AbstractMap.SimpleImmutableEntry<>(items.get(index), items.get(index * 2));
    }
}
