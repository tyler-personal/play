package comparisons;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Range {
    public static List<Integer> range(int low, int high) {
        if (low > high)
            throw new IllegalArgumentException("high must be higher than low");
        else if (low < 0 || high < 0)
            throw new IllegalArgumentException("Low and high must be greater than or equal to 0");

        return IntStream.rangeClosed(low, high).boxed().collect(Collectors.toList());
    }
}