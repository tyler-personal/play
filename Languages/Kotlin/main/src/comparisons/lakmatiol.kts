package comparisons

val c = "hello worL aNd all reLated".toLowerCase()
val d = c.split(" ").mapIndexed { i, b -> if (i % 2 == 0) b.toUpperCase() else b }

val e = c.split(" ").map {
    it.mapIndexed { i, b -> if (i % 2 == 0) b.toUpperCase() else b }
}

fun <T> List<T>.onEven(f: (T) -> T) =
    mapIndexed { i, t -> if (i % 2 == 0) f(t) else t }

fun <T, R> List<T>.map(onEven: (T) -> R, onOdd: (T) -> R) =
    mapIndexed { i, t -> if (i % 2 == 0) onEven(t) else onOdd(t) }
