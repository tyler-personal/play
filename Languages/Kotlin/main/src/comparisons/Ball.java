package comparisons;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

class Position {
    public Number x;
    public Number y;

    public Position(Number x, Number y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        var position = (Position) o;
        return x.equals(position.x) &&
                y.equals(position.y);
    }

    @Override
    public String toString() {
        return "Position{" + "x=" + x + ", y=" + y + '}';
    }
}

class Velocity {
    public Number x;
    public Number y;

    public Velocity(Number x, Number y) {
        this.x = x;
        this.y = y;
    }

    public Velocity() {
        this.x = 0;
        this.y = 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        var velocity = (Velocity) o;
        return x.equals(velocity.x) &&
                y.equals(velocity.y);
    }

    @Override
    public String toString() {
        return "Velocity{" + "x=" + x + ", y=" + y + '}';
    }
}

class Ball {
    public Position position;
    public Velocity velocity;

    public Ball(Position position, Velocity velocity) {
        this.position = position;
        this.velocity = velocity;
    }

    public Ball(Number x, Number y) {
        this(new Position(x, y), new Velocity());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        var ball = (Ball) o;
        return position.equals(ball.position) &&
                velocity.equals(ball.velocity);
    }

    @Override
    public String toString() {
        return "Ball{" + "position=" + position +
                ", velocity=" + velocity + '}';
    }
}

//public class Main {
//    public static void main(String[] args) {
//        final var balls = IntStream.rangeClosed(0, 5)
//                .mapToObj(x -> new Ball(x, x)).collect(Collectors.toList());
//        final var ball = balls.get(balls.size() - 1);
//
//        System.out.println(ball);
//    }
//}