package comparisons.anagram

"potato".anagrams("otatop", "potato", "oofsted", "tatopo").forEach { println("yaa:$it") }

fun String.anagrams(vararg candidates: String) =
    candidates.filter { it.sortAndLower() == sortAndLower() }

fun String.sortAndLower() =
    toCharArray().map { it.toLowerCase() }.sorted().joinToString()