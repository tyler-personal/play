package comparisons.anagram;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

public class StripTest {
    public static void main(String[] args) {
        var list1 = List.of(1, 2, 3, 1, 2, 3, 4, 4, 5, 5);
        var list2 = dropFromStart(List.of(1, 2, 3), list1);
        var list3 = dropFromEnd(List.of(4, 5, 5), list2);
        System.out.println(list3);

        var list4 = List.of(1, 2, 3, 1, 2, 3, 4, 4, 5, 5, 1, 2, 3);
        var result = strip(List.of(1, 2, 3), list4);
        System.out.println(dropFromEnd(List.of(1,2,3), list4));
        System.out.println(result);

        var potato = "         oof   ";
        System.out.println(strip(List.of(' '), potato.chars().mapToObj(e -> (char) e).collect(Collectors.toList())));
    }

    public static <A> List<A> strip(List<A> xs, List<A> ys) {
        return dropFromStart(xs, dropFromEnd(xs, ys));
    }

    private static <A> List<A> dropFromStart(List<A> xs, List<A> ys) {
        var result = new ArrayList<A>();
        boolean stillDropping = true;

        for (int i = 0; i < ys.size(); i++) {
            var x = xs.get(i % xs.size());
            var y = ys.get(i);

            if (x != y)
                stillDropping = false;

            if (!stillDropping)
                result.add(y);
        }

        return result;
    }

    private static <A> List<A> dropFromEnd(List<A> xs, List<A> ys) {
        var result = new ArrayList<A>();
        boolean stillDropping = true;

        for (int i = 0; i < ys.size(); i++) {
            var x = xs.get(xs.size() - 1 - (i % xs.size()));
            var y = ys.get(ys.size() - 1 - i);

            if (x != y)
                stillDropping = false;
            if (!stillDropping)
                result.add(y);
        }

        return reverse(result);
    }

    private static <A> List<A> reverse(List<A> xs) {
        var result = new ArrayList<A>();

        for (int i = xs.size() - 1; i >= 0; i--)
            result.add(xs.get(i));

        return result;
    }
}

//        for (int i = ys.size() - 1; i >= 0; i--) {
//            var x = xs.get((ys.size() - i) % xs.size());
//            var y = ys.get(i);
//
//            if (x != y)
//                stillDropping = false;
//            if (!stillDropping)
//                result.add(y);
//        }
