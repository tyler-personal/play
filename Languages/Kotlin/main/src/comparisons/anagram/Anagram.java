package comparisons.anagram;

import java.util.AbstractMap;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Anagram {
    int x, y;
    // public static <T, R> AbstractMap.SimpleEntry<T, R> get(Function<Object, Object> f)
    public static void main(String[] args) {
        for (String s: anagramsFor("potato", List.of("otatop", "oofsted", "potato", "tatopo")))
            System.out.println(s);
    }

    public static List<String> anagramsFor(String word, List<String> candidates) {
        return candidates.stream()
                .filter(candidate -> sortAndLower(candidate).equals(sortAndLower(word)))
                .collect(Collectors.toList());
    }

    public static String sortAndLower(String word) {
        return word.chars()
                .mapToObj(c -> ((Character)(char) c).toString().toLowerCase())
                .sorted()
                .collect(Collectors.joining());
    }
}
