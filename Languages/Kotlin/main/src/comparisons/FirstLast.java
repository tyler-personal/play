package comparisons;

import java.util.AbstractMap;
import java.util.List;

public class FirstLast {
    public static void main(String[] args) {
        System.out.println(firstLast(List.of(1,2,3,4,5)));
    }

    public static <T> T first(List<T> items) {
        return items.get(0);
    }

    public static <T> T last(List<T> items) {
        return items.get(items.size() - 1);
    }

    public static <T> List<T> firstLast(List<T> items) {
        return List.of(first(items), last(items));
    }
}
