package comparisons;

import io.vavr.collection.List;

import java.util.function.Function;

import static io.vavr.API.*;
import static io.vavr.Patterns.$Cons;
import static io.vavr.Patterns.$Nil;

public class Sort {

    public static <T extends Comparable<T>> List<T> quicksort(List<T> _xs) {
        if (_xs.isEmpty())
            return List.of();

        var x = _xs.get(0);
        var xs = _xs.drop(1);

        var lesser = quicksort(xs.filter(y -> y.compareTo(x) <= 0));
        var greater = quicksort(xs.filter(y -> y.compareTo(x) > 0));

        return lesser.append(x).appendAll(greater);
    }
}
