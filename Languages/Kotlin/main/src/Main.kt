import java.util.*
import java.io.*
import java.math.*
import kotlin.math.pow
import kotlin.reflect.KClass

fun List<String>.repeat(times: Int) =
    flatMap { item -> List(times) { item } }

fun test2() {
    val nums = listOf(1,2,3,4)
    val num = nums.filter { it % 2 == 0 }.max()!!
}
data class Tuple<T, Q>(val _1: T, val _2: Q)
fun main2() {
    val mountains = mapOf<String, Any>()
    val xs = listOf(1,2,3,4)
    val ys = listOf(0,1,2,3)

    val addition = { x: Int, y: Int -> x + y }
    val result = xs.zip(ys).map { (x,y) -> x - y }.sum()

    println(result)

    val res2 = addDiffs(xs, ys)
    println(res2)
}

inline fun <reified T: Number> addDiffs(xs: List<T>, ys: List<T>): T {
    val convert = when (T::class) {
        Double::class -> Number::toDouble
        Float::class -> Number::toFloat
        Long::class -> Number::toLong
        Int::class -> Number::toInt
        Byte::class -> Number::toByte
        Short::class -> Number::toShort
        else -> Number::toDouble
    }

    return 2 as T
    //return xs.zip(ys).map { (x,y) -> convert(x) - convert(y) }.sum()
}

fun f(x: Int, y: Int) = x + y
fun addDifferences(xs: List<Number>, ys: List<Number>) =
    xs.zip(ys).map { (x, y) -> x.toDouble() - y.toDouble() }.sum()

private fun pluralize(word: String, num: Long) =
    "$num $word" + if (num != 1L) "s" else ""
fun f(x: Long) = x != 1L

fun main() {
    println(pluralize("Potato", 1))
    f(4)
    val str = "one, two, three, four"
    val array = str.split(",").let { listOf("${it[0]},${it[1]}") + it.drop(2) }
    val brray = str.split(",").let { (x,y,xs) -> listOf("$x,$y") + xs.drop(2) }
    println(array[0])
    println(listOf("hello", "world").repeat(3))
    val input = Scanner(System.`in`)
    val N = input.nextInt()
    if (input.hasNextLine()) {
        input.nextLine()
    }
    for (i in 0 until N) {
        val ROW = input.nextLine()
        println(ROW.mapNotNull { it.toString().toIntOrNull() }.size)
    }

    // Write an action using println()
    // To debug: System.err.println("Debug messages...");

    println("answer")
}

fun squares(data: List<Double>): List<Double> {
    val listo = mutableListOf<Double>()
    if (data.isNotEmpty()) {
        listo.add(data[0].pow(2))
        //listo.add(squares(data.subList(1)))
    }
    return listo
}
