import java.io.*
import java.math.*
import java.text.*
import java.util.*
import java.util.regex.*
fun simpleArraySum(numbers: Array<Int>): Int {
    var yeet = 0
    for (n in numbers)
        yeet += n
    return yeet
}

fun main() {
    val scan = Scanner(System.`in`)
    val arCount = scan.nextLine().trim().toInt()
    val ar = scan.nextLine().split(" ").map{ it.trim().toInt() }.toTypedArray()
    val result = simpleArraySum(ar)
    println(result)
}
