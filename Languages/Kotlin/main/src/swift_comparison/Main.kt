package swift_comparison

interface Shape {
    fun draw(): String
}

data class Rectangle(val isSquare: Boolean) : Shape {
    override fun draw() = "tis me, a rectangle."
}

data class Triangle(val favoriteSide: Int) : Shape {
    override fun draw() = "look at all 3 o' me sides"
}

fun main() {
    val t1 = getTriangle()
    val t2 = getTriangle()

    println(t1 == t2)
}

fun getTriangle(): Shape = Triangle(3)
