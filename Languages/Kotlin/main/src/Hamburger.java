
class Hamburger {
    final boolean cheese;

    Hamburger(boolean cheese) {
        this.cheese = cheese;
    }
}

class Cheeseburger extends Hamburger {
    Cheeseburger(boolean cheese) {
        super(cheese);
    }
    
    public void test() {

    }
}

