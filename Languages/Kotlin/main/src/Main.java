import kotlin.Pair;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {
    //static Function<List<Integer>, List<String>> showEvens =

    static Function<Integer, Integer> multiplyBy2 = x -> x * 2;
    static Function<Integer, Integer> add3 = x -> x + 3;

    static Function<Integer, Integer> add3ThenMultiplyBy2 = multiplyBy2.compose(add3);

    public static void main(String[] args) {
        int x = 1;
        double y = 1.0;
        System.out.println(x == y);
    }

    public boolean isValid(String braces) {
        var bracesLeft = new ArrayList<Character>();
        for (var brace : braces.toCharArray()) {
            if (bracesLeft.isEmpty()) {
                bracesLeft.add(brace);
            } else if (isMatching(last(bracesLeft), brace)) {
                bracesLeft.remove(bracesLeft.size() - 1);
            } else {
                bracesLeft.add(brace);
            }
        }
        return bracesLeft.isEmpty();
    }

    private boolean isMatching(char last, char next) {

        return (last == '(' && next == ')') ||
               (last == '{' && next == '}') ||
               (last == '[' && next == ']');
    }

    private <T> T last(List<T> items) {
        return items.get(items.size() - 1);
    }

    public static Stream<Integer> fib() {
        return Stream.iterate(List.of(1, 2), list ->
                List.of(list.get(1), list.get(0) + list.get(1))
        ).map(list -> list.get(0));
    }
    public static <T> List<Pair<Integer, T>> enumerate(List<T> items) {
        return IntStream.range(0, items.size())
                .mapToObj(i -> new Pair<>(i, items.get(i)))
                .collect(Collectors.toList());
    }

    static String toCamelCase(String s){
        var result = new StringBuilder();
        var capitalize = false;
        for (int i = 0; i < s.length(); i++) {
            var c = s.charAt(i);
            if (List.of('_', '-').contains(c)) {
                capitalize = true;
                continue;
            }
            if (capitalize) {
                result.append(Character.toUpperCase(c));
                capitalize = false;
            } else {
                result.append(c);
            }
        }
        return result.toString();
    }

    static String toCamleCase2(String s) {
        var stream = IntStream.range(0, s.length())
                .mapToObj(s::charAt)
                .collect(Collectors.toList());
        return null;

    }

    public void test2() {
        var nums = List.of(1,2,3);

        var max = Integer.MIN_VALUE;

        for (var n : nums)
            if (n > max && n % 2 == 0)
                max = n;

        var max2 = nums.stream()
            .filter(n -> n % 2 == 0)
            .mapToInt(n -> n)
            .max().getAsInt();
    }
    public static void main3(String[] args) {
        var names = List.of(null, null);
        System.out.println(names.get(1));
    }
    public static void main2(String[] args) {
        var resultz = Rotation.left(Arrays.stream(new int[] {1,2,3,4,5}).boxed().collect(Collectors.toList()));
        System.out.println(resultz);
        System.out.println(Rotation.right(List.of(1,2,3,4)));
        System.out.println(Rotation.left("1235"));
        int xx = 4;
        int y = 0;
        double z = (double) xx / y;

        System.out.println(toCamelCase("potato-runner_oofsted"));
        System.out.println(z);
        fib().limit(10).forEach(System.out::println);
        List<Number> nums = List.of(1,2.2,3,4);
        List<Number> nums2 = List.of(0,1,2,3);



        System.out.println(addDiffs(nums, nums2, Number.class));
        Scanner scanner = new Scanner(System.in);

        int[] result = subtractNumbers(scanner.nextLine());
        System.out.print("[");
        List<Integer> xs = List.of(1,2);

        final var res = xs.stream().reduce(Integer::sum).orElse(0);


        for (int i = 0; i < result.length; i++) {
            if (i < result.length - 1)
                System.out.print(result[i] + ", ");
            else
                System.out.print(result[i]);
        }

        System.out.print("]");
    }
    public static void test() {
        boolean x;
        if (x = func()) { }
    }
    public static boolean func() { return true; }

    public static int[] subtractNumbers(String str) {
        int resultCount = (int) Math.ceil(str.length() / 4.0);
        int[][] numbers = new int[2][resultCount];
        int numbersIndex = 0;
        boolean splitFound = false;
        int countOfNumbersPrecedingThis = 0;

        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            int x = Character.getNumericValue(c);
            if (!splitFound && c == '|') {
                splitFound = true;
                numbersIndex = 0;
                countOfNumbersPrecedingThis = 0;
            }
            else if (c != ',' && c != ' ') {
                int index = splitFound ? 1 : 0;
                if (countOfNumbersPrecedingThis > 0) {
                    int value = numbers[index][numbersIndex - 1];
                    int valueToAdd = (int) Math.pow((x), countOfNumbersPrecedingThis + 1);
                    numbers[index][numbersIndex - 1] = value + valueToAdd;
                    numbersIndex -= 1;
                } else {
                    numbers[index][numbersIndex] = x;
                }
                numbersIndex++;
                countOfNumbersPrecedingThis++;
            }
            else {
                countOfNumbersPrecedingThis = 0;
            }
        }

        int[] results = new int[resultCount];

        int[] xs = numbers[0];
        int[] ys = numbers[1];

        for (int i = 0; i < resultCount; i++)
            results[i] = xs[i] - ys[i];

        return results;
    }

    // Potential generic solution that doesn't work :(
//    final Function<Number, Function<Number, Function<BiFunction<Number, Number, T>, T>>> convert = x -> y -> f -> {
//        if (klass == Double.class)
//            return klass.cast(f.apply(x.doubleValue(), y.doubleValue()));
//        else if (klass == Float.class)
//            return klass.cast(f.apply(x.floatValue(), y.floatValue()));
//        else if (klass == Long.class)
//            return klass.cast(f.apply(x.longValue(), y.longValue()));
//        else if (klass == Integer.class)
//            return klass.cast(f.apply(x.intValue(), y.intValue()));
//        else if (klass == Byte.class)
//            return klass.cast(f.apply(x.byteValue(), y.byteValue()));
//        else if (klass == Short.class)
//            return klass.cast(f.apply(x.shortValue(), y.shortValue()));
//        else
//            return klass.cast(f.apply(x.doubleValue(), y.doubleValue()));
//    };

public static <T extends Number> T addDiffs(
        List<T> xs, List<T> ys, Class<T> klass
) {
    final BiFunction<Number, Number, T> subtract = (x, y) -> {
        if (klass == Double.class)
            return klass.cast(x.doubleValue() - y.doubleValue());
        else if (klass == Float.class)
            return klass.cast(x.floatValue() - y.floatValue());
        else if (klass == Long.class)
            return klass.cast(x.longValue() - y.longValue());
        else if (klass == Integer.class)
            return klass.cast(x.intValue() - y.intValue());
        else if (klass == Byte.class)
            return klass.cast(x.byteValue() - y.byteValue());
        else if (klass == Short.class)
            return klass.cast(x.shortValue() - y.shortValue());
        else
            return klass.cast(x.doubleValue() - y.doubleValue());
    };

    final BiFunction<Number, Number, T> add = (x, y) -> {
        if (klass == Double.class)
            return klass.cast(x.doubleValue() + y.doubleValue());
        else if (klass == Float.class)
            return klass.cast(x.floatValue() + y.floatValue());
        else if (klass == Long.class)
            return klass.cast(x.longValue() + y.longValue());
        else if (klass == Integer.class)
            return klass.cast(x.intValue() + y.intValue());
        else if (klass == Byte.class)
            return klass.cast(x.byteValue() + y.byteValue());
        else if (klass == Short.class)
            return klass.cast(x.shortValue() + y.shortValue());
        else
            return klass.cast(x.doubleValue() + y.doubleValue());
    };

    final Supplier<T> orElse = () -> {
        if (klass == Double.class)
            return klass.cast(0D);
        else if (klass == Float.class)
            return klass.cast(0F);
        else if (klass == Long.class)
            return klass.cast(0L);
        else if (klass == Integer.class)
            return klass.cast(0);
        else if (klass == Byte.class)
            return klass.cast(0);
        else if (klass == Short.class)
            return klass.cast(0);
        else
            return klass.cast(0D);
    };

    return IntStream.range(0, xs.size())
        .mapToObj(i -> subtract.apply(xs.get(i), ys.get(i)))
        .reduce(add::apply).orElse(orElse.get());
}

    public static <T extends Number> double addDiffs2(List<T> xs, List<T> ys) {
        return IntStream.range(0, xs.size())
            .mapToDouble(i -> xs.get(i).doubleValue() - ys.get(i).doubleValue())
            .sum();
    }
}
