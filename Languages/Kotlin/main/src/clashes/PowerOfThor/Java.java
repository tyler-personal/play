package clashes.PowerOfThor;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Board {
    final int x;
    final int y;
    final int endX;
    final int endY;

    Board(int x, int y, int endX, int endY) {
        this.x = x;
        this.y = y;
        this.endX = endX;
        this.endY = endY;
    }
}

class Move {
    final String direction;
    final Board board;

    Move(String direction, Board board) {
        this.direction = direction;
        this.board = board;
    }
}

public class Java {
    static List<Integer> getNumbers(String s) {
        var x = "test";
        return Arrays.stream(s.split(" ")).map(Integer::parseInt).collect(Collectors.toList());
    }

    private static Function<Move, List<Move>> f; // Java can't compile local recursive Functions.

    static List<Move> game(Board b) {
        f = m -> m.direction.equals("")
            ? List.of(m)
            : Stream.concat(Stream.of(m), f.apply(calculate(m.board)).stream()).collect(Collectors.toList());
        return f.apply(calculate(b));
    }

    static Move calculate(Board b) {
        BiFunction<Integer, Integer, Board> newB = (x, y) -> new Board(x, y, b.endX, b.endY);

        if (b.x > b.endX && b.y > b.endY)
            return new Move("NW", newB.apply(b.x - 1, b.y - 1));
        else if (b.x > b.endX && b.y < b.endY)
            return new Move("SW", newB.apply(b.x - 1, b.y + 1));
        else if (b.x < b.endX && b.y > b.endY)
            return new Move("NE", newB.apply(b.x + 1, b.y - 1));
        else if (b.x < b.endX && b.y < b.endY)
            return new Move("SE", newB.apply(b.x + 1, b.y + 1));
        else if (b.x > b.endX)
            return new Move("W", newB.apply(b.x - 1, b.y));
        else if (b.x < b.endX)
            return new Move("W", newB.apply(b.x - 1, b.y));
        else if (b.y > b.endY)
            return new Move("N", newB.apply(b.x, b.y - 1));
        else if (b.y < b.endY)
            return new Move("S", newB.apply(b.x, b.y + 1));
        else
            return new Move("", b);
    }

    public static void main(String[] args) {
        final var n = getNumbers(new Scanner(System.in).nextLine());
        final var board = new Board(n.get(2), n.get(3), n.get(0), n.get(1));
        game(board).forEach(move -> System.out.println(move.direction));
    }
}