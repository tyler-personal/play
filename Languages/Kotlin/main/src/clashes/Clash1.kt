package clashes

fun main() {
    val statements = readLine()!!.split(" ")
    statements.fold(0, ::f)
}

fun f(n: Int, statement: String) = when(statement) {
    "inc" -> n + 1
    "dec" -> n - 1
    "half" -> n / 2
    "double" -> n * 2
    "print" -> n.also(::println)
    "exit" -> n.also(System::exit)
    else -> n
}