package clashes

fun main() {
    val x = "sAmMyJr" // readLine()!!
    val y = "Xxxxx, Xx." // readLine()!!

    println(generateSequence(Triple(0, 0, "")) { (ai, bi, acc) ->
        val a = x.getOrNull(ai)
        val b = y.getOrNull(bi)
        when {
            b == null || a == null -> null
            b == 'X' -> Triple(ai+1, bi+1, acc + a.toUpperCase())
            b == 'x' -> Triple(ai+1, bi+1, acc + a.toLowerCase())
            else -> Triple(ai, bi+1, acc + b)
        }
    }.last().third)
}

fun main2() { // this one is wrong
    val x = "sAmMyJr" // readLine()!!
    val y = "Xxxxx, Xx." // readLine()!!

    println(0.until(y.length).map { 0 to "${y[it]}" }.reduce { (i, z), (_,b) ->
        println("$i $z $b")
        val a = x.getOrNull(i)?.toString()
        when(b) {
            "X" -> (i+1) to (z + a?.toUpperCase() ?: " ")
            "x" -> (i+1) to (z+a?.toLowerCase() ?: " ")
            else -> (i) to (z+b)
        }
    }.second)
}

fun main3() {
    val x = "sAmMyJr" // readLine()!!
    val y = "Xxxxx, Xx." // readLine()!!

    var i = 0

    for (b in y) {
        val a = x.getOrNull(i)
        print(when(b) {
            'X' -> a?.toUpperCase().also { i++ } ?: ' '
            'x' -> a?.toLowerCase().also { i++ } ?: ' '
            else -> b
        })
    }
}
fun main4() {
    val x = "sAmMyJr" // readLine()!!
    val y = "Xxxxx, Xx." // readLine()!!

    var i = 0
    var ii = 0

    do {
        val a = x.getOrNull(i)
        val b = y.getOrNull(ii)
        print(when (b) {
            'X' -> a?.toUpperCase().also { i++; ii++ } ?: ' '
            'x' -> a?.toLowerCase().also { i++; ii++ } ?: ' '
            null -> a.also { i++ } ?: ' '
            else -> b.also { ii++ }
        })
    } while(a != null || b != null)
}