package clashes.dont_panic

// Can't construct a true readAll
// This version would pretty much crash 100% of the time
fun <T> readAll(s: String): List<T> {
    return s.split(" ").map { it as T }
}

fun readNums() = readLine()!!.split(" ").map { it.toInt() }

class Exit(val floor: Int, val position: Int)
class Elevator(val floor: Int, val position: Int)
class Clone(val floor: Int, val position: Int, val direction: String)

class GameInfo(val elevators: List<Elevator>, val exit: Exit)
class GameState(var startPosition: Int?)
class GameLoopInfo(val clone: Clone)

fun main() {
    
    val (floorCount, width, roundsMax, exitFloor, exitPosition, cloneCount, _, elevatorCount) = readNums()
    val elevators = 0.until(elevatorCount).map {
        val (elevatorFloor, elevatorPosition) = readNums()
        Elevator(elevatorFloor, elevatorPosition)
    }

    val exit = Exit(exitFloor, exitPosition)
    val gameInfo = GameInfo(elevators, exit)
    val gameState = GameState(startPosition = null)

    gameLoop(gameInfo, gameState)
}

fun gameLoop(gameInfo: GameInfo, gameState: GameState) {
    fun findElevator(cloneFloor: Int) = gameInfo.elevators.find { it.floor == cloneFloor }!!
    fun determineOutputType(clone: Clone, exit: Exit, startPosition: Int?) = when {
        exit.floor == clone.floor -> exitOutput(exit, clone)
        clone.floor >= 0 -> floorOutput(findElevator(clone.floor), clone, startPosition)
        else -> "WAIT"
    }

    val gameLoopInfo = gameLoopInfo()
    val clone = gameLoopInfo.clone

    if (gameState.startPosition == null)
        gameState.startPosition = clone.position

    println(determineOutputType(clone, gameInfo.exit, gameState.startPosition))

    return gameLoop(gameInfo, gameState)
}

fun gameLoopInfo(): GameLoopInfo {
    val input = readLine()!!.split(" ")
    val cloneFloor = input[0].toInt()
    val clonePosition = input[1].toInt()
    val cloneDirection = input[2]
    return GameLoopInfo(Clone(cloneFloor, clonePosition, cloneDirection))
}

fun floorOutput(elevator: Elevator, clone: Clone, startPosition: Int?) = when {
    startPosition == null -> "WAIT"
    startPosition == clone.position -> "WAIT"
    clone.direction == "RIGHT" && elevator.position < clone.position -> "BLOCK"
    clone.direction == "LEFT" && elevator.position > clone.position -> "BLOCK"
    else -> "WAIT"
}

fun exitOutput(exit: Exit, clone: Clone) = when {
    clone.direction == "RIGHT" && exit.position < clone.position -> "BLOCK"
    clone.direction == "LEFT" && exit.position > clone.position -> "BLOCK"
    else -> "WAIT"
}

private operator fun <E> List<E>.component6() = this[5]
private operator fun <E> List<E>.component7() = this[6]
private operator fun <E> List<E>.component8() = this[7]