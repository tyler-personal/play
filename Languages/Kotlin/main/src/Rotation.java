import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Rotation {
    public static char[] left(char[] items) {
        return rotation(items, i -> Rotation.left(i, Character.class));
    }

    public static short[] left(short[] items) {
        return rotation(items, i -> Rotation.left(i, Short.class));
    }

    public static float[] left(float[] items) {
        return rotation(items, i -> Rotation.left(i, Float.class));
    }

    public static boolean[] left(boolean[] items) {
        return rotation(items, i -> Rotation.left(i, Boolean.class));
    }

    public static byte[] left(byte[] items) {
        return rotation(items, i -> Rotation.left(i, Byte.class));
    }

    public static int[] left(int[] items) {
        return rotation(items, i -> Rotation.left(i, Integer.class));
    }

    public static double[] left(double[] items) {
        return rotation(items, i -> Rotation.left(i, Double.class));
    }

    public static long[] left(long[] items) {
        return rotation(items, i -> Rotation.left(i, Long.class));
    }

    public static char[] right(char[] items) {
        return rotation(items, i -> Rotation.right(i, Character.class));
    }

    public static short[] right(short[] items) {
        return rotation(items, i -> Rotation.right(i, Short.class));
    }

    public static float[] right(float[] items) {
        return rotation(items, i -> Rotation.right(i, Float.class));
    }

    public static boolean[] right(boolean[] items) {
        return rotation(items, i -> Rotation.right(i, Boolean.class));
    }

    public static byte[] right(byte[] items) {
        return rotation(items, i -> Rotation.right(i, Byte.class));
    }

    public static int[] right(int[] items) {
        return rotation(items, i -> Rotation.right(i, Integer.class));
    }

    public static double[] right(double[] items) {
        return rotation(items, i -> Rotation.right(i, Double.class));
    }

    public static long[] right(long[] items) {
        return rotation(items, i -> Rotation.right(i, Long.class));
    }

    public static String left(String items) {
        return rotation(items, i -> Rotation.left(i, Character.class));
    }

    public static String right(String items) {
        return rotation(items, i -> Rotation.right(i, Character.class));
    }

    public static <T> Iterable<T> left(Iterable<T> items) {
        var resultList = new ArrayList<T>();
        var iterator = items.iterator();
        var initial = iterator.next();

        iterator.forEachRemaining(resultList::add);
        resultList.add(initial);
        return resultList;
    }

    public static <T> T[] left(T[] items, Class<T> klass) {
        var resultArray = (T[]) Array.newInstance(klass, items.length);

        for (int i = 1; i < items.length; i++) {
            resultArray[i-1] = items[i];
        }
        resultArray[items.length - 1] = items[0];
        return resultArray;
    }

    public static <T> Iterable<T> right(Iterable<T> items) {
        var initialList = new ArrayList<T>();
        items.forEach(initialList::add);
        var last = initialList.remove(initialList.size() - 1);

        var resultList = new ArrayList<T>();
        resultList.add(last);
        resultList.addAll(initialList);
        return resultList;
    }

    public static <T> T[] right(T[] items, Class<T> klass) {
        var resultArray = (T[]) Array.newInstance(klass, items.length);

        for (int i = 0; i < items.length - 1; i++) {
            resultArray[i+1] = items[i];
        }
        resultArray[0] = items[items.length - 1];
        return resultArray;
    }

    private static char[] rotation(char[] items, Function<Character[], Character[]> rotate) {
        var boxedItems = IntStream.range(0, items.length).mapToObj(i -> items[i]).toArray(Character[]::new);
        var rotatedItems = rotate.apply(boxedItems);
        var unboxedItems = new char[items.length];

        for (int i = 0; i < items.length; i++) {
            unboxedItems[i] = rotatedItems[i];
        }
        return unboxedItems;
    }

    private static short[] rotation(short[] items, Function<Short[], Short[]> rotate) {
        var boxedItems = IntStream.range(0, items.length).mapToObj(i -> items[i]).toArray(Short[]::new);
        var rotatedItems = rotate.apply(boxedItems);
        var unboxedItems = new short[items.length];

        for (int i = 0; i < items.length; i++) {
            unboxedItems[i] = rotatedItems[i];
        }
        return unboxedItems;
    }

    private static float[] rotation(float[] items, Function<Float[], Float[]> rotate) {
        var boxedItems = IntStream.range(0, items.length).mapToObj(i -> items[i]).toArray(Float[]::new);
        var rotatedItems = rotate.apply(boxedItems);
        var unboxedItems = new float[items.length];

        for (int i = 0; i < items.length; i++) {
            unboxedItems[i] = rotatedItems[i];
        }
        return unboxedItems;
    }

    private static boolean[] rotation(boolean[] items, Function<Boolean[], Boolean[]> rotate) {
        var boxedItems = IntStream.range(0, items.length).mapToObj(i -> items[i]).toArray(Boolean[]::new);
        var rotatedItems = rotate.apply(boxedItems);
        var unboxedItems = new boolean[items.length];

        for (int i = 0; i < items.length; i++) {
            unboxedItems[i] = rotatedItems[i];
        }
        return unboxedItems;
    }

    private static byte[] rotation(byte[] items, Function<Byte[], Byte[]> rotate) {
        var boxedItems = IntStream.range(0, items.length).mapToObj(i -> items[i]).toArray(Byte[]::new);
        var rotatedItems = rotate.apply(boxedItems);
        var unboxedItems = new byte[items.length];

        for (int i = 0; i < items.length; i++) {
            unboxedItems[i] = rotatedItems[i];
        }
        return unboxedItems;
    }

    private static int[] rotation(int[] items, Function<Integer[], Integer[]> rotate) {
        return Arrays.stream(rotate.apply(
                Arrays.stream(items).boxed().toArray(Integer[]::new)
        )).mapToInt(Integer::intValue).toArray();
    }

    private static double[] rotation(double[] items, Function<Double[], Double[]> rotate) {
        return Arrays.stream(rotate.apply(
                Arrays.stream(items).boxed().toArray(Double[]::new)
        )).mapToDouble(Double::doubleValue).toArray();
    }

    private static long[] rotation(long[] items, Function<Long[], Long[]> rotate) {
        return Arrays.stream(rotate.apply(
                Arrays.stream(items).boxed().toArray(Long[]::new)
        )).mapToLong(Long::longValue).toArray();
    }

    private static String rotation(String items, Function<Character[], Character[]> rotate) {
        var result = new StringBuilder();
        var boxedItems = IntStream.range(0, items.length()).mapToObj(items::charAt).toArray(Character[]::new);
        for (Character c : rotate.apply(boxedItems)) {
            result.append(c);
        }
        return result.toString();
    }
}

//    public static <T> Iterable<T> leftRotationOriginal(Iterable<T> items) {
//        AtomicBoolean gotInitial = new AtomicBoolean(false);
//        AtomicReference<T> initial = new AtomicReference<>();
//        var newList = new ArrayList<T>();
//
//        items.forEach(item -> {
//            if (!gotInitial.get()) {
//                initial.set(item);
//                gotInitial.set(true);
//            } else {
//                newList.add(item);
//            }
//        });
//        newList.add(initial.get());
//        return newList;
//    }
//    public static <T> List<T> leftRotationList(List<T> items) {
//        var newList = items.subList(1, items.size() - 1);
//        newList.add(items.get(0));
//        return newList;
//    }
//
//    public static <T> List<T> rightRotationList(List<T> items) {
//        var newList = List.of(items.get(items.size() - 1));
//        newList.addAll(items.subList(0, items.size() - 2));
//        return newList;
//    }
    /*
        if (!items.iterator().hasNext()) {
            return new ArrayList<T>();
        }
     */
