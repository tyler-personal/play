package haskell_test;

import java.util.function.BiFunction;

interface Animal {
    Boolean running();
}

interface Bipedal {
    Boolean kneeling();
}

interface Swimmer {
    Boolean afraid();
}

public class HaskellOverOOP {
    public static <A extends Animal & Bipedal & Swimmer> void check(A a) {
        BiFunction<Boolean, Boolean, String> f = ((x, y) -> {
            if (x && y)
                return "You're kneeling and running";
            else if (!x && !y)
                return "Maybe do something";
            else
                return "Wow you're normal";
        });

        System.out.println(f.apply(a.kneeling(), a.running()));

        if (a.afraid())
            System.out.println("You're a big baby");
        else
            System.out.println("Well that was expected");
    }

    // Concrete Run
    static class John implements Animal, Bipedal, Swimmer {
        public Boolean running() { return true; }

        public Boolean kneeling() { return false; }

        public Boolean afraid() { return true; }
    }

    public static void main(String[] args) { check(new John()); }
}
