package haskell_test

interface Animal {
    val running: Boolean
}

interface Bipedal {
    val kneeling: Boolean
}

interface Swimmer {
    val afraid: Boolean
}

fun <A> check(a: A) where A : Animal, A : Bipedal, A : Swimmer {
    fun f(x: Boolean, y: Boolean) = when(x to y) {
        true to true -> "You're kneeling and running"
        false to false -> "Maybe do something"
        else -> "Wow you're normal"
    }

    print(f(a.kneeling, a.running))
    print(if (a.afraid)
        "You're a big baby"
    else
        "Well that was expected")
}

fun <A> simple(a: A) where A : Animal {}

fun <A> runCheck(a: A) {
    when {
        a is Animal && a is Bipedal && a is Swimmer -> simple(a)
        a is Animal -> simple(a)
    }
}

// Concrete Run
class John : Animal, Bipedal, Swimmer {
    override val running = true
    override val kneeling = false
    override val afraid = true
}

check(John())

