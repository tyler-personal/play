import java.util.Objects;

public class Java13Main {
    public static void main(String[] args) {
        var user1 = new User("Potato", 40);
        var user2 = new User("Potato", 40);

        System.out.println(user1);
        System.out.println(user1.equals(user2));
    }

//    char[] guesses = new char[10];
//    public char[] getRemainingGuesses() {
//        int counter = 0;
//        for (char guess : guesses) {
//            if (guess == null) {
//                counter++;
//            }
//        }
//    }
    private static class User {
        final String name;
        final int age;

        User(String name, int age) {
            this.name = name;
            this.age = age;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            User user = (User) o;
            return age == user.age &&
                    Objects.equals(name, user.name);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name, age);
        }

        @Override
        public String toString() {
            return "User[" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    ']';
        }
    }
}
