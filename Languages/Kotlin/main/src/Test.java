import java.util.List;
import java.util.Objects;

public class Test {
    double x, y;

    @Override
    public boolean equals(Object o) {
        return (o != null && o.getClass() == getClass()) && (((Test) o).x == x && ((Test) o).y == y);
    }
}
