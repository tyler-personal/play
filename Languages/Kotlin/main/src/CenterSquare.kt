// typealias Matrix<T> = List<List<T>>
class Point(val x: Int, val y: Int)
fun <T> Matrix<T>.center(start: Point, end: Point) =
    this.subList(start.y, end.y + 1).map { it.subList(start.x, end.x + 1) }

fun main() {
    val matrix = listOf(listOf(1, 2, 3, 4), listOf(5, 6, 7, 8), listOf(9, 1, 2, 3), listOf(4, 5, 6, 7))

    println(matrix)
    println(matrix.center(Point(1, 1), Point(1, 2)))
}

fun <T> matrixOf(vararg xs: T) = listOf(xs.toList())

val regions = mutableListOf<Region>()
val cells = mutableListOf<Cell>()
class ComplexNumber(val real: Double, val imaginary: Double)
class Cell(val number: ComplexNumber, val row: Int, val column: Int)

open class Part(val cells: List<Cell>)
class Region(cells: List<Cell>, val parts: List<Part>, val row: Int, val column: Int) : Part(cells) {
    constructor(parts: List<Part>, row: Int, column: Int) : this(parts.flatMap { it.cells }, parts, row, column)

    fun composeFromColumn(next: Int): Region =
        if (row == 0) {
            val parts = regions.filter { it.column == column }.drop(1)
            Region(parts, row, column)
        } else {
            // TODO: Other code
            Region(parts, row, column)
        }
}
