package helping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Zinbeeno {
public static List<List<Integer>> threeSumE(int[] nums) {
    List<List<Integer>> options = new ArrayList<>();
    List<Integer> xs = new ArrayList<>();
    for (int x : nums)
        xs.add(x);

    for (int x : xs) {
        boolean xUnique = false;
        for (int y : xs) {
            boolean yUnique = false;
            if (x == y)
                xUnique = true;
            else {
                for (int z : xs)
                    if (x == z)
                        xUnique = true;
                    else if (y == z)
                        yUnique = true;
                    else
                        options.add(Arrays.asList(x, y, z).stream().sorted().collect(Collectors.toList()));
            }
        }
    }
    options = options.stream().distinct().collect(Collectors.toList());

    return options.stream()
            .filter(x -> x.stream().mapToInt(a -> a).sum() == 0)
            .collect(Collectors.toList());
}
    public static List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> options = new ArrayList<>();
        var xs = new ArrayList<Integer>();
        for (var x : nums)
            xs.add(x);

        for (var x : xs) {
            var ys = new ArrayList<>(xs);
            ys.remove(x);

            for (var y : ys) {
                var zs = new ArrayList<>(ys);
                zs.remove(y);

                for (var z : zs)
                    options.add(List.of(x, y, z).stream().sorted().collect(Collectors.toList()));
            }
        }
        options = options.stream().distinct().collect(Collectors.toList());

        return options.stream()
                .filter(x -> x.stream().mapToInt(a -> a).sum() == 0)
                .collect(Collectors.toList());
    }

    public static void main(String[] args) {
        System.out.println(threeSum(new int[]{0, 0, 0, 0}));
    }
}
