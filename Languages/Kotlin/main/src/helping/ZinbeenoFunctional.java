package helping;

import io.vavr.collection.List;

public class ZinbeenoFunctional {
    public static List<List<Integer>> threeSum(List<Integer> nums) {
        final var options = nums.flatMap(x ->
            nums.remove(x).flatMap(y ->
                nums.remove(x).remove(y).map(z ->
                    List.of(x, y, z).sorted()
        ))).distinct();

        return options.filter(x -> x.sum().intValue() == 0);
    }

    public static void main(String[] args) {
        System.out.println(threeSum(List.of(-1, 0, 0, 1, 0, -1, -4, -2)));
    }
}
