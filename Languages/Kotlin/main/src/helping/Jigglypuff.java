package helping;

import java.util.Scanner;

public class Jigglypuff {
    public static void main(String[] args) {
        var input = new Scanner(System.in);
        System.out.print("Enter the coordinates for two points (x,y)(x,y): ");

        var x1 = input.nextDouble();
        var y1 = input.nextDouble();
        var x2 = input.nextDouble();
        var y2 = input.nextDouble();


        var slope = (y2-y1)/(x2-x1);
        var yintercept = y1-slope * x1;

        if (slope!=1 && yintercept!=0) {
            System.out.print("y=" + slope+"x + "+ yintercept);
        }
        else if (slope==1 && yintercept!=0) {
            System.out.print("y=x" + yintercept);
        }
        else if (slope!=1 && yintercept ==0){
            System.out.print("y=" + slope+"x");
        }
        else {
            System.out.print("y=x");
        }
    }
}
