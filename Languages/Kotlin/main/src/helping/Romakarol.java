package helping;


import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Romakarol {
    public static <T> boolean noTriple(Tuple3<T, T, T> t) {
        return !(t._1.equals(t._2) && t._2.equals(t._3));
    }

    public static void main(String[] args) {
        System.out.println(generateString(4,2,2).get());
    }
    public static <T> boolean noTriples(List<T> xs) {
        var ys = xs.subList(1, xs.size());
        var zs = xs.subList(1, ys.size());
        return zip3(xs, ys, zs).stream().allMatch(Romakarol::noTriple);
    }

    public static Optional<String> generateString(int a, int b, int c) {
        var initialString = "a".repeat(a) + "b".repeat(b) + "c".repeat(c);
        var stringAsList = initialString.chars()
                .mapToObj(cc -> (char) cc)
                .collect(Collectors.toList());

        var resultAsList = permutations(stringAsList).stream()
                .filter(Romakarol::noTriples)
                .findFirst();

        return resultAsList.map(list ->
                list.stream().map(Object::toString).collect(Collectors.joining()));
    }

    public static <T> List<List<T>> permutations(List<T> initial) {
        return null;
//        return switch(initial.size()) {
//            case 0 -> List.of();
//            case 1 -> List.of(initial);
//            default -> permutations(initial.subList(1, initial.size())).stream()
//                    .flatMap(perm ->
//                        IntStream.rangeClosed(0, perm.size()).mapToObj(i -> {
//                            var newPerm = new ArrayList<T>();
//                            newPerm.addAll(perm.subList(0, i));
//                            newPerm.addAll(initial.subList(0, 1));
//                            newPerm.addAll(perm.subList(i, perm.size()));
//                            return newPerm;
//                        }))
//                    .collect(Collectors.toList());
//        };
    }

    public static <T> List<Tuple3<T, T, T>> zip3(List<T> xs, List<T> ys, List<T> zs) {
        var min = IntStream.of(xs.size(), ys.size(), zs.size()).min().getAsInt();
        return Stream.iterate(0, x -> x < min, x -> x + 1)
                .map(i -> new Tuple3<>(xs.get(i), ys.get(i), zs.get(i)))
                .collect(Collectors.toList());
    }

    public static class Tuple3<A, B, C> {
        public final A _1;
        public final B _2;
        public final C _3;

        public Tuple3(A a, B b, C c) {
            _1 = a;
            _2 = b;
            _3 = c;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Tuple3<?, ?, ?> tuple3 = (Tuple3<?, ?, ?>) o;
            return Objects.equals(_1, tuple3._1) &&
                    Objects.equals(_2, tuple3._2) &&
                    Objects.equals(_3, tuple3._3);
        }

        @Override
        public int hashCode() {
            return Objects.hash(_1, _2, _3);
        }

        @Override
        public String toString() {
            return "Tuple3{" +
                    "_1=" + _1 +
                    ", _2=" + _2 +
                    ", _3=" + _3 +
                    '}';
        }
    }

    // IGNORE
    public static ArrayList<String> computeAllPossiblePermutations(String str) {
        ArrayList<String> perms = new ArrayList<>();
        if (str.length() == 1) {
            perms.add(str);
        } else {
            String chr = str.substring(0,1);
            String rest = str.substring(1);
            ArrayList<String> subPerms = computeAllPossiblePermutations(rest);
            for (String s : subPerms) {
                for (int j = 0; j <= s.length(); j++) {
                    String newPerm = s.substring(0,j) + chr + s.substring(j);
                    perms.add(newPerm);
                }
            }
        }
        return perms;
    }
//
//    int[] heapPermutation(int a[], int size) {
//        // if size becomes 1 then prints the obtained
//        // permutation
//        if (size == 1)
//            return a;
//
//        for (int i = 0; i < size; i++) {
//            heapPermutation(a, size - 1);
//
//            // if size is odd, swap first and last
//            // element
//            if (size % 2 == 1) {
//                int temp = a[0];
//                a[0] = a[size - 1];
//                a[size - 1] = temp;
//            }
//
//            // If size is even, swap ith and last
//            // element
//            else {
//                int temp = a[i];
//                a[i] = a[size - 1];
//                a[size - 1] = temp;
//            }
//        }
//    }
}
