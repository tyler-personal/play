package helping;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Schnuckelig {

    private static class Subject {
        Subject(String code, String name) { }
        String getCode() { return ""; }
    }
    public static void main(String[] args) throws Exception {
        Subject s = new Subject("", "");

        Scanner input = new Scanner(new File("Subjects.txt"));
        ArrayList<Subject> subjectList = new ArrayList<Subject>();
        while (input.hasNext()) {
            String cl = input.next();
            String nl = input.nextLine();
            subjectList.add(new Subject(cl, nl));
        }

        var subjects = Files.lines(Paths.get("Subjects.txt"))
                .map(line -> {
                    var codeThenName = line.split(" ");
                    return new Subject(codeThenName[0], codeThenName[1]);
                }).collect(Collectors.toList());
    }

    public static ArrayList<String> allDisciplines(ArrayList<Subject> subjectList) {
        ArrayList<String> disList = new ArrayList<String>();
        for (int i = 0; i < subjectList.size(); i++) {
            String iCode = subjectList.get(i).getCode();
            disList.add(iCode);
        }
        Collections.sort(disList);
        return disList;
    }

    public static List<String> allDisciplines(List<Subject> subjects) {
        return subjects.stream()
                .map(Subject::getCode)
                .sorted()
                .collect(Collectors.toList());
    }
}
