package helping

import java.io.File

class Subject(val code: String, val name: String) {
}

fun allDisciplines(subjects: List<Subject>): List<String> =
    subjects.map { it.code }.sorted()

fun Sequence<Subject>.disciplines() = map { it.code }.sorted()

val subjects = File("Subjects.txt").readLines()
    .map { it.split(" ") }
    .map { (code, name) -> Subject(code, name) }
