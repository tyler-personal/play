package helping;

import java.util.ArrayList;
import java.util.List;

public class Romakarol5 {
    public static <T> boolean hasTriple(Tuple3<T, T, T> t) {
        return t._1.equals(t._2) && t._2.equals(t._3);
    }

    public static <T> boolean noTriples(List<T> xs) {
        List<T> ys = xs.subList(1, xs.size());
        List<T> zs = xs.subList(1, ys.size());

        List<Tuple3<T, T, T>> items = zip3(xs, ys, zs);

        for (Tuple3<T, T, T> item : items) {
            if (hasTriple(item)) {
                return false;
            }
        }
        return true;
    }

    public static String generateString(int a, int b, int c) {
        StringBuilder initialStringBuilder = new StringBuilder();

        for (int i = 0; i < a; i++) {
            initialStringBuilder.append('a');
        }

        for (int i = 0; i < b; i++) {
            initialStringBuilder.append('b');
        }

        for (int i = 0; i < c; i++) {
            initialStringBuilder.append('c');
        }

        String initialString = initialStringBuilder.toString();
        List<Character> initialCharacters = new ArrayList<Character>();

        for (int i = 0; i < initialString.length(); i++) {
            initialCharacters.add(initialString.charAt(i));
        }

        List<List<Character>> permutations = permutations(initialCharacters);
        List<Character> resultPermutation = null;

        for (List<Character> permutation : permutations) {
            if (noTriples(permutation)) {
                resultPermutation = permutation;
                break;
            }
        }

        if (resultPermutation == null) {
            return null;
        }

        StringBuilder resultStringBuilder = new StringBuilder();

        for (Character character : resultPermutation) {
            resultStringBuilder.append(character);
        }

        return resultStringBuilder.toString();
    }

    public static <T> List<List<T>> permutations(List<T> initial) {
        List<List<T>> result = new ArrayList<List<T>>();

        if (initial.size() == 1) {
            result.add(initial);
        }
        else if (initial.size() > 1) {
            List<T> x = initial.subList(0, 1);
            List<T> xs = initial.subList(1, initial.size());
            List<List<T>> perms = permutations(xs);

            for (List<T> perm : perms) {
                for (int i = 0; i <= perm.size(); i++) {
                    List<T> newPerm = new ArrayList<T>();
                    newPerm.addAll(perm.subList(0, i));
                    newPerm.addAll(x);
                    newPerm.addAll(perm.subList(i, perm.size()));
                    result.add(newPerm);
                }
            }
        }

        return result;
    }

    public static <T> List<Tuple3<T, T, T>> zip3(List<T> xs, List<T> ys, List<T> zs) {
        int min = xs.size();

        if (ys.size() < min) {
            min = ys.size();
        }

        if (zs.size() < min) {
            min = zs.size();
        }

        List<Tuple3<T, T, T>> result = new ArrayList<Tuple3<T, T, T>>();

        for (int i = 0; i < min; i++) {
            result.add(new Tuple3<T, T, T>(xs.get(i), ys.get(i), zs.get(i)));
        }

        return result;
    }

    public static class Tuple3<A, B, C> {
        public A _1;
        public B _2;
        public C _3;

        public Tuple3(A a, B b, C c) {
            _1 = a;
            _2 = b;
            _3 = c;
        }
    }
}
