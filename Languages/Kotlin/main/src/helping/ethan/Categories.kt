//package helping.ethan
//
//class Category(val name: String)
//class SubCategory(val name: String)
//class Keywords(val name: String)
//
//class Study(val categories: List<Category?>?,
//            val subCategories: List<SubCategory?>?,
//            val keywords: List<Keywords?>?)
//
//fun run(studies: List<Study>): Triple<List<Category>, List<SubCategory>, List<Keywords>> {
//    val categories = studies.flatMap { it.categories ?: listOf() }
//    val subCategories = studies.flatMap { it.subCategories ?: listOf() }
//    val keywords = studies.flatMap { it.keywords ?: listOf() }
//
//    return Triple(categories.filterNotNull(), subCategories.filterNotNull(), keywords.filterNotNull())
//}
//
//fun run2(studies: List<Study>): Triple<List<Any>, List<Any>, List<Any>> = // Could cast to fix this
//    listOf(Study::categories, Study::subCategories, Study::keywords).map { f ->
//        studies.map { f(it) ?: listOf() }.flatten().filterNotNull()
//    }.let { (_1, _2, _3) -> Triple(_1, _2, _3) }
//
//fun run3(studies: List<Study>): Triple<List<Category>, List<SubCategory>, List<Keywords>> {
//    fun <T> f(g: (Study) -> List<T?>?) = studies.flatMap { g(it) ?: listOf() }.filterNotNull()
//
//    return Triple(f(Study::categories), f(Study::subCategories), f(Study::keywords))
//}