package helping.schnuckelig.triangle;

public class Triangle extends GeometricObject {
    private final double side1;
    private final double side2;
    private final double side3;

    public Triangle(double side1, double side2, double side3, String color, boolean filled) {
        super(color, filled);
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    public Triangle(double side1, double side2, double side3, String color) {
        this(side1, side2, side3, color, true);
    }

    public double getPerimeter() {
        return side1 + side2 + side3;
    }

    public double getArea(){
        double s = getPerimeter() / 2;
        return Math.sqrt(s *(s - side1) * (s - side2) * (s - side3));
    }

    public String toString(){
        return "Side 1 = " + side1 + " Side 2 = " + side2 + " Side 3 = " + side3
                + "\nFilled = " + isFilled() + "\nArea = " + getArea() + "\nPerimeter = " + getPerimeter();
    }
}
