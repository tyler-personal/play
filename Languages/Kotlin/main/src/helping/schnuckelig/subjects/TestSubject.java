package helping.schnuckelig.subjects;

import java.io.File;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TestSubject {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws Exception {
        var subjects = Files.lines(Paths.get("Subjects.txt"))
            .map(line -> {
                var codeThenName = line.split(" ");
                return new Subject2(codeThenName[0], codeThenName[1]);
            }).collect(Collectors.toList());


        var addNewSubject = input("Do you wish to add a new subject? (y/n): ");

        while (addNewSubject.equals("y")) {
            printHeader("EXISTING DISCIPLINES:");

            var disciplines = Subject2.allCodes();
            display(disciplines);

            var discipline = input("Enter the discipline (eg ITC): ");
            printHeader("SUBJECTS IN THIS DISCIPLINE: ");

            var cpd = Subject2.codesForDiscipline(discipline);
            display(cpd);

            addNewSubject(subjects);
            addNewSubject = input("\nDo you want to enter another subject? (y/n): ");
        }

        PrintWriter output = new PrintWriter(new File("Output.txt"));

        subjects.forEach(output::println);
        output.close();
    }

    public static <T> void display(List<T> list) {
        for (T t : list)
            print(t);
    }

    public static void addNewSubject(List<Subject2> subjects) {
        boolean subjectExists = true;
        while (subjectExists) {
            var name = input("Enter the subject name: ");
            var code = input("Enter the subject code (eg ITC421): ");

            subjectExists = Subject2.exists(name, code);

            if (!Subject2.isValidCode(code))
                print("Invalid subject code. Must be a 6 digit code");
            else if (subjectExists)
                print("Subject already exists. Please enter new code: ");
            else
                subjects.add(new Subject2(name, code));
        }
    }

    private static void print(Object s, String ending) { System.out.print(s + ending); }
    private static void print(Object s) { print(s, "\n"); }
    private static void printHeader(Object s) { print(s, "\n____________________\n"); }
    private static String input(Object s) { print(s); return scanner.nextLine(); }

    public static String input(String s, String error, Function<String, Boolean> validation) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(s);

        final var line = scanner.nextLine();

        if (validation.apply(line))
            return line;
        else {
            System.out.println(error);
            return input(s, error, validation);
        }
    }

    public static String input(String s, Function<String, Boolean> validation) {
        return input(s, "", validation);
    }

    public static String input(String s, String error) {
        return input(s, error, ss -> true);
    }

    public static String input(String s) {
        return input(s, "");
    }
}