package helping.schnuckelig.subjects;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Subject2 {
    final static private List<Subject2> subjects = new ArrayList<>();
    final private String code;
    final private String name;

    public Subject2(String code, String name) {
        this.code = code;
        this.name = name;

        subjects.add(this);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getDiscipline() {
        return code.substring(0, 3);
    }

    @Override
    public String toString() {
        return code + ' ' + name;
    }

    public static boolean exists(String name, String code) {
        return allCodes().contains(code) && allNames().contains(name);
    }

    public static boolean isValidCode(String code) {
        return code.length() == 6;
    }

    public static List<String> allNames() {
        return subjectsMapped(Subject2::getName);
    }

    public static List<String> allCodes() {
        return subjectsMapped(Subject2::getCode);
    }

    public static List<String> codesForDiscipline(String discipline) {
        return subjects.stream()
                .map(Subject2::getDiscipline)
                .filter(discipline::equals)
                .collect(Collectors.toList());
    }

    private static <T> List<T> subjectsMapped(Function<Subject2, T> f) {
        return subjects.stream().map(f).sorted().collect(Collectors.toList());
    }
}
