package helping.schnuckelig

import java.io.File

val subjects = File("Subjects.txt").readLines()
    .map { it.split(Regex("\\s+")) }
    .map { (code, name) -> Subject(name, code) }
    .toMutableList()

val q = listOf(1,2,3).map(::f)
fun f(x: Int)  = x * 2
println(subjects)

while (askToAddNewSubject() == "y")
    addNewSubject()

File("Output.txt").writeText(subjects.joinToString("\n"))

fun askToAddNewSubject() =
    input("Do you wish to add a new subject?: ", "Must be y/n") { it == "y" || it == "n" }

fun addNewSubject() {
    println("~~Existing Subjects~~")
    subjects.forEach(::println)

    val name = input("Enter a subject name: ", "Must be unique") { name -> subjects.none { it.name == name } }

    val codeError = "Must be unique and exactly 3 capital letters followed by 3 numbers"
    val code = input("Enter a subject code: ", codeError) { code ->
        subjects.none { it.code == code } && code.validCode
    }

    subjects.add(Subject(name, code))
}

fun input(s: String, error: String = "", validation: ((String) -> Boolean) = { true }): String {
    println(s)
    return readLine()?.takeIf(validation) ?: {
        println(error)
        input(s, error, validation)
    }()
}

val String.validCode get() = matches(Regex("^[A-Z]{3}\\d{3}\$"))

data class Subject(val name: String, val code: String)

//val discipline = input("Enter a new discipline: ", "Must be 3 letters") { it.length == 3 }
//
//printHeader("Existing subjects in discipline")
//subjects.filter { it.discipline in disciplines }.forEach(::println)