import java.util.Arrays;
import java.util.List;

public class ottar {
    public void checkChar(char c) {
        final var validGrades = List.of('a', 'b', 'c', 'd', 'f');
        if (!validGrades.contains(Character.toLowerCase(c)))
            throw new IllegalArgumentException("character is not a letter grade");
    }
}
