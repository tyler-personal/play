typealias Path = List<Node>
typealias TreeData = List<List<Int>>

fun <T, R> compareAll(xs: List<T>, ys: List<R>, block: (T, R) -> Boolean) =
    xs.any { x -> ys.any { y -> block(x, y) } }
class Node(val index: Int, val connectedNodes: MutableList<Node> = mutableListOf()) {
    fun findLargestComponentOrder() = generateSequence(connectedNodes.map { listOf<Node>(this, it) }) { paths ->
        paths.flatMap { path ->
            val lastNode = path.last()
            lastNode.connectedNodes
                .filterNot { newNode -> newNode in path }
                .map { newNode -> path + listOf(newNode) }
        }.takeIf { newPaths ->
            newPaths.size > paths.size || // compareAll(newPaths, paths) { (newPath, path) -> newP > path.size }
                    newPaths.any { newPath -> paths.any { path -> newPath.size > path.size } } }
    }.last().map { it.size }.max() ?: 1


    override fun equals(other: Any?) = when(other) {
        is Node -> other.index == index
        else -> false
    }
}

fun getNextNumberList() = readLine()!!.split(" ")
    .filter { it.isNotEmpty() }.map { it.toInt() }

fun buildTreeData(length: Int) =
    0.until(length).mapNotNull { getNextNumberList() }

fun buildAllTreeData() =
    generateSequence {
        val nextNumber = readLine()!!.toInt()
        nextNumber to buildTreeData(nextNumber)
    }.takeWhile { (num, _) -> num > 0 }.fold(listOf<TreeData>()) { acc, e -> acc + listOf(e.second) }

buildAllTreeData().forEachIndexed { graphIndex, treeData ->
    val nodes = 0.until(treeData.size).map { Node(it) }
    treeData.forEachIndexed { index, otherNodeIndexes ->
        otherNodeIndexes.forEach {
            nodes[index].connectedNodes.add(nodes[it])
        }
    }

    val largestOrder = nodes.map { node ->
        node.findLargestComponentOrder()
    }.max()

    println("Graph ${graphIndex + 1} has a component of order $largestOrder")
}

//fun findLargestComponentOrder2() = generateSequence(
//    listOf<Node>() to connectedNodes.toList()
//) { (checkedNodes, nodes) ->
//    val newNodes = nodes.filterNot { it in checkedNodes }.flatMap { node ->
//        node.connectedNodes
//    }
//    println(checkedNodes)
//    (checkedNodes + nodes to newNodes)
//}.takeWhile { (oldNodes, newNodes) -> newNodes.isNotEmpty() }.toList().size