package helping;

import java.util.ArrayList;
import java.util.Scanner;

public class Maxxx {
    public static void main(String args[]) {
        var scanner = new Scanner(System.in);
        var numbers = new ArrayList<Integer>();

        for (int i = 0; i < 10; i++) {
            numbers.add(scanner.nextInt());
        }

        checkIfFree(numbers);
    }

    public static void checkIfFree(ArrayList<Integer> numbers) {
        for (var number : numbers) {
            if (number == 0) {
                System.out.println("This place is free");
            } else {
                System.out.println("This place is not free");
            }
        }
    }
}