package helping;


import io.vavr.Tuple3;
import io.vavr.collection.List;

public class Main {
    public static <T> boolean noTriple(Tuple3<T, T, T> t) {
        return !(t._1.equals(t._2) && t._2.equals(t._3));
    }

    public static <T> boolean noTriples(List<T> xs) {
        return zip3(xs, xs.drop(1), xs.drop(2)).forAll(Main::noTriple);
    }

    public static String generateString(int a, int b, int c) {
        return List.ofAll(("a".repeat(a) + "b".repeat(b) + "c".repeat(c)).toCharArray())
                .permutations()
                .filter(Main::noTriples)
                .map(xs -> xs.map(Object::toString))
                .get(0).mkString();
    }

    public static <T> List<Tuple3<T, T, T>> zip3(List<T> xs, List<T> ys, List<T> zs) {
        return xs.zip(ys).zip(zs).map(t -> new Tuple3<>(t._1._1, t._1._2, t._2));
    }
}
