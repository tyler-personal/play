package comparisons

fun main() {
    val x = 4
    val y = 7

    val data = Array(x) { Array(y) { "Hello" } }
    data.forEach { it.forEach(::println) }
}