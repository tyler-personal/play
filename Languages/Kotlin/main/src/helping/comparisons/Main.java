package comparisons;

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        var scanner = new Scanner(System.in);

        var result = scanner.nextInt();
        System.out.println(result);
    }
}
