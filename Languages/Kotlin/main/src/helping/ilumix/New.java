package helping.ilumix;

import java.util.Optional;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

import static helping.ilumix.New.Player.*;
import static helping.ilumix.New.Roll.*;
public class New {
    private static Scanner scanner = new Scanner(System.in);
    enum Roll { ROCK, PAPER, SCISSORS }
    enum Player { PERSON, COMPUTER, TIE }

    private static Player getWinner(Roll person, Roll computer) {
        return switch(person) {
            case ROCK -> switch(computer) {
                case ROCK -> TIE;
                case PAPER -> COMPUTER;
                case SCISSORS -> PERSON;
            };
            case PAPER -> switch(computer) {
                case ROCK -> PERSON;
                case PAPER -> TIE;
                case SCISSORS -> COMPUTER;
            };
            case SCISSORS -> switch(computer) {
                case ROCK -> COMPUTER;
                case PAPER -> PERSON;
                case SCISSORS -> TIE;
            };
        };
    }

    private static Optional<Roll> getPersonRoll() {
        try {
            var line = scanner.nextLine().toUpperCase();
            return line.equals("END") ? Optional.empty() : Optional.of(Roll.valueOf(line));
        } catch(Exception e) {
            System.out.print("Invalid input, try again: ");
            return getPersonRoll();
        }
    }

    public static void main(String[] args) throws Exception {
        while(true) {
            System.out.print("Enter Rock/Paper/Scissors/End: ");
            var personOptional = getPersonRoll();
            if (personOptional.isEmpty())
                break;

            var person = personOptional.get();
            var computer = switch(ThreadLocalRandom.current().nextInt(0, 3)) {
                case 0 -> ROCK;
                case 1 -> PAPER;
                case 2 -> SCISSORS;
                default -> throw new Exception("ThreadLocalRandom produced value outside range.");
            };

            System.out.println("Person: " + person.toString());
            System.out.println("Computer: " + computer.toString());
            System.out.println("Winner: " + getWinner(person, computer));
        }
    }


}
