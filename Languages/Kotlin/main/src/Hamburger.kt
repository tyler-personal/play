open class Hamburger(val cheese: Boolean = false)
class Cheeseburger(cheese: Boolean = true) : Hamburger(cheese)
fun <T> combine(x: T, y: T): Pair<T, T> = Pair(x,y)

fun main() {
    combine(2, 4)
    combine("6", "7)")
    val garrettBurger = Cheeseburger(cheese = false)
    val jordanBurger = Hamburger()

    println(garrettBurger == jordanBurger)
}