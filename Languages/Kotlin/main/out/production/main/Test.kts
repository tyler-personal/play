fun String.subtractNumbers() =
    split("|").map { it.split(",").map { it.trim().toInt() } }
        .let { (xs, ys) -> xs.zip(ys).map { (x, y) -> x - y  } }

println(readLine()!!.subtractNumbers())

interface Runnable {
    fun run(): Unit
}

open class Runnable2 (
    val run: () -> Unit
)

class Human : Runnable {
    override fun run() = println("yote")
}

class Human2 : Runnable2(
    run = { println("yote") }
)

// Like Kotlin / OOP
interface Employee {
    fun say(s: String): String
    fun run(): Unit
}

class DepressedEmployee: Employee {
    override fun say(s: String) = s.toLowerCase()
    override fun run() = println("I'm not running.")
}

class NormalEmployee: Employee {
    override fun say(s: String) = s
    override fun run() = println("I'm running.")
}

class IntenseEmployee: Employee {
    override fun say(s: String) = s.toUpperCase()
    override fun run() = println("I'M RUNNING!!")
}

// Like Haskell / FP
class Employee2(
    val say: (String) -> String,
    val run: () -> Unit
)

val depressedEmployee2 = Employee2(
    say = { it.toLowerCase() },
    run = { println("I'm not running.") }
)

val normalEmployee2 = Employee2(
    say = { it },
    run = { println("I'm running") }
)

val intenseEmployee2 = Employee2(
    say = { it.toUpperCase() },
    run = { println("I'M RUNNING!!") }
)

