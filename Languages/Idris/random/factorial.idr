module Factorial

-- %default total

factorial : Nat -> Nat
factorial Z = 1
factorial (S n) = (n + 1) * factorial n

main : IO ()
main = print $ factorial (-1)
