module TakeJavaComparison

import Data.Vect

take : Vect n a -> (m : Nat) -> {auto p: m * 2 `LT` n} -> (a, a)
take items i {p}= (index i items, index (i) items)

--take : Vect n a -> Fin n -> (a, a)
--take items i = ((index i items), (index i items))
