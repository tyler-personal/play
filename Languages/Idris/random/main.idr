module Main

import Data.Vect

-- %default partial

factorial : (x : Int) -> {auto p: x > 0 = True} -> Int
factorial x = f x where
  f 0 = 1
  f n = n * f (n - 1)

divide : (a : Int) -> (b: Int) -> {auto p: b /= 0 = True} -> Int
divide a b = a `div` b

main : IO()
main = print (divide 4 0)

sum : (x : Int) -> {auto p: x >= 0 = True} -> Int
sum x = f x where
  f 0 = 0
  f n = n + f (n - 1)

fibonacci : (n : Nat) -> Nat
fibonacci Z = 1
fibonacci (S k) = S k * fibonacci k


-- append : Vect n a -> {auto p: n > 0 = True} ->
--          Vect m a -> {auto pp: m > 0 = True } -> Vect (n + m) a
-- append xs ys = f xs ys where
--   f : Vect n a -> Vect m a -> Vect (n + m) a
--   f [] [] = []
--   f xs [] {n=Z} = xs
--   f [] ys = ys
--   f (x :: xs) (y :: ys) = x :: y :: (f xs ys)
--

