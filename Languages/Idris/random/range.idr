import Data.Vect

range : (low:Int) -> (high:Int) -> {auto p: high > low = True} -> List Int
range low high = [low .. high]

range' : (low : Nat) -> (high : Nat) -> {auto p: low `LTE` high} -> Vect (length (enumFromTo low high)) Nat
range' low high = fromList [low .. high]
