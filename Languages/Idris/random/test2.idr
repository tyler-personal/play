
add : Nat -> Nat -> Nat
add Z x = x
add (S x) y = add x (S y)

factorial : (x : Int) -> {auto p: x > 0 = True} -> Int
factorial x = f x where
  f 0 = 1
  f n = n * f (n - 1)
