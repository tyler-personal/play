module Main

import Data.Vect

-- fib : Nat -> List Nat
-- fib n = List.take (n) (seq 0 1) where
--   seq : Nat -> Nat -> List Nat
--   seq a b = a :: seq b (a + b)

add : Num a => Vect n a -> Vect n a -> Vect n a
add [] [] = []
add (x :: xs) (y :: ys) = x + y :: add xs ys

main = putStrLn "yes"

test : 4 = 5
test = Refl

factorial : (n : Nat) -> {auto p: n `GT` 0} -> Nat
factorial n = n * factorial n
-- fibonacci : (n : Nat) -> {auto p: n `GT` 0} -> Vect p n
-- fibonacci = [0, 1] ++ zipWith (+) (fibonacci) (

-- Note that mSmallest is accepted as total with just this one case!
total mSmallest : (n : Nat) -> (m : Nat) -> {auto p : n `GT` m} -> Vect m (Fin n)
mSmallest (S k) m = replicate m FZ
