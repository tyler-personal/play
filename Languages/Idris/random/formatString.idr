module FormatString

import Data.Vect

format :
  (value : String)
  -> (format : String)
  -> {auto p: length value > length format=True}
  -> String
format value format = f 0 0 ""
  where
    f : Int -> Int -> String -> String
    f ai bi acc = case (strIndex value ai, strIndex format bi) of
      (Nothing, Nothing) => acc
      (Just a, Just 'X') => f (ai+1) (bi+1) (acc + [toUpper a])
      (Just a, Just 'x') => f (ai+1) (bi+1) (acc + [toLower a])
      (_, Just b) => f ai (bi+1) (ac + [b])
