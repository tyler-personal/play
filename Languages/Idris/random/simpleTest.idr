module SimpleTest

import Data.Vect

f : Maybe Int -> Maybe Int -> Int
f a b = ff a b
  where
    ff aa bb = case (aa, bb) of
      (Just aa, Just bb) => aa + bb
      (Just aa, _) => aa
      (_, Just bb) => bb
      (_, _) => 0
