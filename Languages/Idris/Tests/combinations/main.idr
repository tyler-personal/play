import Data.Vect
-- factorial : (x : Int) -> (y : Int) -> {auto p1: y >= 1 = True} -> {auto p2: x >= y = True} -> Int
-- factorial x y with (x == y)
--   | True = x
--   | False = x * factorial (x - 1) y
-- permutations : Vec n a -> {x : Nat} -> Vec () (Vec x a)
-- permutations list count =

splitBy : (a -> Bool) -> List a -> (List a, List a)
splitBy f [] = ([], [])
splitBy f (x :: xs) = let (a, b) = splitBy f xs in
  if f x
     then (x :: a, b)
     else (a, x :: b)

qs : Ord a => List a -> List a
qs [] = []
qs (x :: xs) = let (a, b) = splitBy (< x) xs in
  (qs a) ++ [x] ++ (qs b)

sort : Ord a => Vect n a -> Vect n a
sort [] = []
sort (x :: xs) =
