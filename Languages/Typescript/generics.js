function process(text) {
    return text && text.replace(/f/g, "p");
}
// function f<T extends boolean>(x: T): T extends true ? string : number {
//   if x is true
//     return "potato";
//   else
//     return 4;
// }
// function run<T>(arg: T): T {
//   return arg;
// }
//
// const namea = "potato";
// const name2 = run(namea);
// console.log(namea);
