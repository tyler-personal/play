type IsString<T> = T extends string ? "yes" : "no";
type A = IsString<number>;
const a: A = "no";
// not working? >:(
// function f<T extends string | null>(x: T): T extends string ? string : null {
//   return x && x.replace("_", " ");
// }
//
// f("potato");
// f(null);
//
// function f<T extends boolean>(x: T): T extends true ? string : number {
//   if x is true
//     return "potato";
//   else
//     return 4;
// }
// function run<T>(arg: T): T {
//   return arg;
// }
//
// const namea = "potato";
// const name2 = run(namea);
// console.log(namea);
