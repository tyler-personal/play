type MPredicate<T> = (value: T) => boolean;
type MCallback<T, U> = (value: T) => U;
type Tuple<T, U> = [T, U];
interface MRouteResponse<M, N> {
  defineRoute: (
    predicate: Predicate<M>,
    callback: Callback<M, N>
  ) => RouteResponse<M, N>;
  runRoute: (val: M) => any;
}

const McomposeRouteCallbacks = <T, U>(
  [prevPred, prevCallback]: Tuple<Predicate<T>, Callback<T, U> | undefined>,
  callback: Callback<T, U>
) => (x: T) => (prevPred(x) && prevCallback ? prevCallback(x) : callback(x));

const composePredicates = <T>(prevPred: Predicate<T>, pred: Predicate<T>) => (
  x: T
) => prevPred(x) || pred(x);

const recursiveRoute = <T, U>(
  prevPred: Predicate<T> = () => false,
  prevCall?: Callback<T, U>
) => (pred: Predicate<T>, cb: Callback<T, U>): RouteResponse<T, U> => {
  const newPred = composePredicates(prevPred, pred);
  const newCallback = composeRouteCallbacks([prevPred, prevCall], callback);
  return {
    defineRoute: recursiveRoute(newPred, newCallback),
    runRoute: (val: T) => {
      return newPred(val)
        ? newCallback(val)
        : console.log("no match found " + val);
    }
  };
};

const route = <T, U>(
  pred: Predicate<T>,
  callback: Callback<T, U>
): RouteResponse<T, U> => {
    const noParentRoute: (pred: Predicate<T>, cb: Callback<T, U>) => RouteResponse<T, U> = recursiveRoute();
    return noParentRoute(pred, callback)
};
