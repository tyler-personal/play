type Predicate<T> = (t: T) => boolean;
type Callback<T, U> = (t: T) => U;
type Route<T, U> = (p: Predicate<T>, cb: Callback<T, U>) => RouteResponse<T, U>;

interface RouteResponse<T, U> {
  defineRoute: Route<T, U>;
  runRoute: (t: T) => U
}

const composeRouteCallbacks = <T, U>(
  [prevPred, prevCallback]: [Predicate<T>, Callback<T, U> | undefined],
  callback: Callback<T, U>
) => (x: T) => (prevPred(x) && prevCallback ? prevCallback(x) : callback(x));

