import Prelude hiding (unwords)
import Network.Discord hiding (get)
import Data.Text (isPrefixOf, strip, splitOn, stripPrefix)
import Data.Maybe

{-@ sub :: x:a -> {y:a | y < x} -> a @-}
sub a b = a - b

removePrefix prefix text = fromMaybe text $ stripPrefix prefix text

token = "NDc5Nzk5ODYyNzU0Mjc5NDI1.D3meVw.Xty3zrpoiMqefSbSPVfC79h1IJk"

runDiscordBot = runBot (Bot token) $ with MessageCreateEvent $
  \_m@Message{messageContent=text, messageChannel=channel} -> do
    let args cmd = filter (/= "") . splitOn " " . strip $ removePrefix cmd text
    let command cmd f = when (cmd `isPrefixOf` text) $ f $ args cmd
    let reply msg = fetch' $ CreateMessage channel msg Nothing

    reply $ r text
    command "!ping" (\_ -> reply "yaaaaa")

r "Hey" = "Hi"
r "Yo" = "Sup"
r "Kay" = "Shut up, dude."
r "Aight" = "yaated"
r _ = ""


main = do
  runDiscordBot
