{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_brick_tests (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/tylerwbrown/Code/Play/Languages/Haskell/brick-tests/.stack-work/install/x86_64-linux-tinfo6/ghc-8.4.1/8.4.1/bin"
libdir     = "/home/tylerwbrown/Code/Play/Languages/Haskell/brick-tests/.stack-work/install/x86_64-linux-tinfo6/ghc-8.4.1/8.4.1/lib/x86_64-linux-ghc-8.4.1/brick-tests-0.1.0.0-7NUkP6X6hFHFG4lgiT9vyn-brick-tests"
dynlibdir  = "/home/tylerwbrown/Code/Play/Languages/Haskell/brick-tests/.stack-work/install/x86_64-linux-tinfo6/ghc-8.4.1/8.4.1/lib/x86_64-linux-ghc-8.4.1"
datadir    = "/home/tylerwbrown/Code/Play/Languages/Haskell/brick-tests/.stack-work/install/x86_64-linux-tinfo6/ghc-8.4.1/8.4.1/share/x86_64-linux-ghc-8.4.1/brick-tests-0.1.0.0"
libexecdir = "/home/tylerwbrown/Code/Play/Languages/Haskell/brick-tests/.stack-work/install/x86_64-linux-tinfo6/ghc-8.4.1/8.4.1/libexec/x86_64-linux-ghc-8.4.1/brick-tests-0.1.0.0"
sysconfdir = "/home/tylerwbrown/Code/Play/Languages/Haskell/brick-tests/.stack-work/install/x86_64-linux-tinfo6/ghc-8.4.1/8.4.1/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "brick_tests_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "brick_tests_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "brick_tests_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "brick_tests_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "brick_tests_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "brick_tests_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
