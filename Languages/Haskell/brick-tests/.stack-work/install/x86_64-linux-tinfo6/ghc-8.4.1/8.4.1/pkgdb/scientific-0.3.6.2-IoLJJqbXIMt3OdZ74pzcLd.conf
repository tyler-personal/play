name: scientific
version: 0.3.6.2
id: scientific-0.3.6.2-IoLJJqbXIMt3OdZ74pzcLd
key: scientific-0.3.6.2-IoLJJqbXIMt3OdZ74pzcLd
license: BSD-3-Clause
maintainer: Bas van Dijk <v.dijk.bas@gmail.com>
author: Bas van Dijk
homepage: https://github.com/basvandijk/scientific
synopsis: Numbers represented using scientific notation
description:
    "Data.Scientific" provides the number type 'Scientific'. Scientific numbers are
    arbitrary precision and space efficient. They are represented using
    <http://en.wikipedia.org/wiki/Scientific_notation scientific notation>.
    The implementation uses a coefficient @c :: 'Integer'@ and a base-10 exponent
    @e :: 'Int'@. A scientific number corresponds to the
    'Fractional' number: @'fromInteger' c * 10 '^^' e@.
    .
    Note that since we're using an 'Int' to represent the exponent these numbers
    aren't truly arbitrary precision. I intend to change the type of the exponent
    to 'Integer' in a future release.
    .
    The main application of 'Scientific' is to be used as the target of parsing
    arbitrary precision numbers coming from an untrusted source. The advantages
    over using 'Rational' for this are that:
    .
    * A 'Scientific' is more efficient to construct. Rational numbers need to be
    constructed using '%' which has to compute the 'gcd' of the 'numerator' and
    'denominator'.
    .
    * 'Scientific' is safe against numbers with huge exponents. For example:
    @1e1000000000 :: 'Rational'@ will fill up all space and crash your
    program. Scientific works as expected:
    .
    >>> read "1e1000000000" :: Scientific
    1.0e1000000000
    .
    * Also, the space usage of converting scientific numbers with huge exponents to
    @'Integral's@ (like: 'Int') or @'RealFloat's@ (like: 'Double' or 'Float')
    will always be bounded by the target type.
category: Data
abi: 909dbba9293b1e6ad87b6e1a1eac2c01
exposed: True
exposed-modules:
    Data.ByteString.Builder.Scientific Data.Scientific
    Data.Text.Lazy.Builder.Scientific
hidden-modules: GHC.Integer.Compat Utils
import-dirs: /home/tylerwbrown/Code/Play/Languages/Haskell/brick-tests/.stack-work/install/x86_64-linux-tinfo6/ghc-8.4.1/8.4.1/lib/x86_64-linux-ghc-8.4.1/scientific-0.3.6.2-IoLJJqbXIMt3OdZ74pzcLd
library-dirs: /home/tylerwbrown/Code/Play/Languages/Haskell/brick-tests/.stack-work/install/x86_64-linux-tinfo6/ghc-8.4.1/8.4.1/lib/x86_64-linux-ghc-8.4.1/scientific-0.3.6.2-IoLJJqbXIMt3OdZ74pzcLd
dynamic-library-dirs: /home/tylerwbrown/Code/Play/Languages/Haskell/brick-tests/.stack-work/install/x86_64-linux-tinfo6/ghc-8.4.1/8.4.1/lib/x86_64-linux-ghc-8.4.1
data-dir: /home/tylerwbrown/Code/Play/Languages/Haskell/brick-tests/.stack-work/install/x86_64-linux-tinfo6/ghc-8.4.1/8.4.1/share/x86_64-linux-ghc-8.4.1/scientific-0.3.6.2
hs-libraries: HSscientific-0.3.6.2-IoLJJqbXIMt3OdZ74pzcLd
depends:
    base-4.11.0.0 integer-logarithms-1.0.3-DikCH1hZ0v84YEnGjq00Ft
    deepseq-1.4.3.0 text-1.2.3.0
    hashable-1.3.0.0-1aQ2oPfbYUd6SKQfrFxBj9
    primitive-0.7.0.0-4uR7vNoyuT87ELsVp5roty containers-0.5.11.0
    binary-0.8.5.1 bytestring-0.10.8.2 integer-gmp-1.0.1.0
abi-depends: base-4.11.0.0=fbe01d431ed12096d1c6602fe9bad91f
             integer-logarithms-1.0.3-DikCH1hZ0v84YEnGjq00Ft=57fc5bce106c7b23e876c2ceb48ca693
             deepseq-1.4.3.0=4870cb4da84d4be96d4aa4e0870cea5d
             text-1.2.3.0=6dfe24cd3de4b11b00c272428413ef75
             hashable-1.3.0.0-1aQ2oPfbYUd6SKQfrFxBj9=82b3f0f9131480d761c1dde39b8d3c49
             primitive-0.7.0.0-4uR7vNoyuT87ELsVp5roty=761cc9be9b5029b6a5f1ed2972f264e2
             containers-0.5.11.0=0ce4757dfb5f9d3ddc7bdaaa03b58763
             binary-0.8.5.1=12e9dce050b7c2b521da35f5d3f7db45
             bytestring-0.10.8.2=06e12565f8797f849f260240ec004cfa
             integer-gmp-1.0.1.0=ad0c57726092288ddb5a3647a560f62b
haddock-interfaces: /home/tylerwbrown/Code/Play/Languages/Haskell/brick-tests/.stack-work/install/x86_64-linux-tinfo6/ghc-8.4.1/8.4.1/doc/scientific-0.3.6.2/scientific.haddock
haddock-html: /home/tylerwbrown/Code/Play/Languages/Haskell/brick-tests/.stack-work/install/x86_64-linux-tinfo6/ghc-8.4.1/8.4.1/doc/scientific-0.3.6.2
