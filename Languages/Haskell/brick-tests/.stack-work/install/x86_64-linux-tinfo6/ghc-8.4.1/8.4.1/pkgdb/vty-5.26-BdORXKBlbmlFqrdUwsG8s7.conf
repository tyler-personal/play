name: vty
version: 5.26
id: vty-5.26-BdORXKBlbmlFqrdUwsG8s7
key: vty-5.26-BdORXKBlbmlFqrdUwsG8s7
license: BSD-3-Clause
maintainer: Jonathan Daugherty (cygnus@foobox.com)
author: AUTHORS
homepage: https://github.com/jtdaugherty/vty
synopsis: A simple terminal UI library
description:
    vty is terminal GUI library in the niche of ncurses. It is intended to
    be easy to use, have no confusing corner cases, and good support for
    common terminal types.
    .
    See the @vty-examples@ package as well as the program
    @test/interactive_terminal_test.hs@ included in the @vty@ package for
    examples on how to use the library.
    .
    Import the "Graphics.Vty" convenience module to get access to the core
    parts of the library.
    .
    &#169; 2006-2007 Stefan O'Rear; BSD3 license.
    .
    &#169; Corey O'Connor; BSD3 license.
    .
    &#169; Jonathan Daugherty; BSD3 license.
category: User Interfaces
abi: 3885b6c4b98a8eab80b85ff2f5c00655
exposed: True
exposed-modules:
    Codec.Binary.UTF8.Debug Data.Terminfo.Eval Data.Terminfo.Parse
    Graphics.Text.Width Graphics.Vty Graphics.Vty.Attributes
    Graphics.Vty.Attributes.Color Graphics.Vty.Config
    Graphics.Vty.Debug Graphics.Vty.DisplayAttributes
    Graphics.Vty.Error Graphics.Vty.Image Graphics.Vty.Image.Internal
    Graphics.Vty.Inline Graphics.Vty.Inline.Unsafe Graphics.Vty.Input
    Graphics.Vty.Input.Classify Graphics.Vty.Input.Classify.Parse
    Graphics.Vty.Input.Classify.Types Graphics.Vty.Input.Events
    Graphics.Vty.Input.Focus Graphics.Vty.Input.Loop
    Graphics.Vty.Input.Mouse Graphics.Vty.Input.Paste
    Graphics.Vty.Input.Terminfo Graphics.Vty.Output
    Graphics.Vty.Output.Interface Graphics.Vty.Output.Mock
    Graphics.Vty.Output.TerminfoBased Graphics.Vty.Output.XTermColor
    Graphics.Vty.Picture Graphics.Vty.PictureToSpans Graphics.Vty.Span
hidden-modules: Graphics.Vty.Attributes.Color240
                Graphics.Vty.Debug.Image Graphics.Vty.Input.Terminfo.ANSIVT
import-dirs: /home/tylerwbrown/Code/Play/Languages/Haskell/brick-tests/.stack-work/install/x86_64-linux-tinfo6/ghc-8.4.1/8.4.1/lib/x86_64-linux-ghc-8.4.1/vty-5.26-BdORXKBlbmlFqrdUwsG8s7
library-dirs: /home/tylerwbrown/Code/Play/Languages/Haskell/brick-tests/.stack-work/install/x86_64-linux-tinfo6/ghc-8.4.1/8.4.1/lib/x86_64-linux-ghc-8.4.1/vty-5.26-BdORXKBlbmlFqrdUwsG8s7
dynamic-library-dirs: /home/tylerwbrown/Code/Play/Languages/Haskell/brick-tests/.stack-work/install/x86_64-linux-tinfo6/ghc-8.4.1/8.4.1/lib/x86_64-linux-ghc-8.4.1
data-dir: /home/tylerwbrown/Code/Play/Languages/Haskell/brick-tests/.stack-work/install/x86_64-linux-tinfo6/ghc-8.4.1/8.4.1/share/x86_64-linux-ghc-8.4.1/vty-5.26
hs-libraries: HSvty-5.26-BdORXKBlbmlFqrdUwsG8s7
depends:
    base-4.11.0.0 blaze-builder-0.4.1.0-1lQcalGyz5S87Rx0azM34
    bytestring-0.10.8.2 containers-0.5.11.0 deepseq-1.4.3.0
    directory-1.3.1.5 filepath-1.4.2
    microlens-0.4.11.2-Fvejwz4j3pA27OFmGdCGu2
    microlens-mtl-0.2.0.1-2l0VnXJqQx175bRSVicauf
    microlens-th-0.4.3.2-8ntYPg9lNRPBdfxIPm7TG1
    hashable-1.3.0.0-1aQ2oPfbYUd6SKQfrFxBj9 mtl-2.2.2
    parallel-3.2.2.0-CbfRUWA8fGRCBFhhHtlOlD parsec-3.1.13.0 stm-2.4.5.0
    terminfo-0.4.1.1 transformers-0.5.5.0 text-1.2.3.0 unix-2.7.2.2
    utf8-string-1.0.1.1-2cokAEWVuL98LKJDrRjVdP
    vector-0.12.0.3-IUzzKO2TZqDLT8zP6i1nro
abi-depends: base-4.11.0.0=fbe01d431ed12096d1c6602fe9bad91f
             blaze-builder-0.4.1.0-1lQcalGyz5S87Rx0azM34=7441c715533891e673e3fca9fbb10a89
             bytestring-0.10.8.2=06e12565f8797f849f260240ec004cfa
             containers-0.5.11.0=0ce4757dfb5f9d3ddc7bdaaa03b58763
             deepseq-1.4.3.0=4870cb4da84d4be96d4aa4e0870cea5d
             directory-1.3.1.5=66918f2e0c83f948e3cd9a249082bb3c
             filepath-1.4.2=f2eb5d523aab8e473a701881752edbe1
             microlens-0.4.11.2-Fvejwz4j3pA27OFmGdCGu2=a9d6200914fe1fbb9cccb4c3c7944c0a
             microlens-mtl-0.2.0.1-2l0VnXJqQx175bRSVicauf=b90f056deb0db333557abc830713685f
             microlens-th-0.4.3.2-8ntYPg9lNRPBdfxIPm7TG1=aefb864cf6fc2a2e3ac3185b06926d22
             hashable-1.3.0.0-1aQ2oPfbYUd6SKQfrFxBj9=82b3f0f9131480d761c1dde39b8d3c49
             mtl-2.2.2=55e606487e7872e7f63c78736ab1e97c
             parallel-3.2.2.0-CbfRUWA8fGRCBFhhHtlOlD=9fe2bd4665ec4c0456384de01c34051b
             parsec-3.1.13.0=a2e213cbede2b742bc5c34e3d51e3d64
             stm-2.4.5.0=ed35a147f6b4f8cfd5e5dc5b09d7420b
             terminfo-0.4.1.1=6c87624b964f65a02bf9cc118f36cadf
             transformers-0.5.5.0=62bac3ffc2e24ea3f0041962ee243eda
             text-1.2.3.0=6dfe24cd3de4b11b00c272428413ef75
             unix-2.7.2.2=b525d0055bfee89b5235d4c4142bf990
             utf8-string-1.0.1.1-2cokAEWVuL98LKJDrRjVdP=f4d6057067e36e7c4bd148e0e68d4272
             vector-0.12.0.3-IUzzKO2TZqDLT8zP6i1nro=cf854a8825018104f0570624ae69d940
haddock-interfaces: /home/tylerwbrown/Code/Play/Languages/Haskell/brick-tests/.stack-work/install/x86_64-linux-tinfo6/ghc-8.4.1/8.4.1/doc/vty-5.26/vty.haddock
haddock-html: /home/tylerwbrown/Code/Play/Languages/Haskell/brick-tests/.stack-work/install/x86_64-linux-tinfo6/ghc-8.4.1/8.4.1/doc/vty-5.26
