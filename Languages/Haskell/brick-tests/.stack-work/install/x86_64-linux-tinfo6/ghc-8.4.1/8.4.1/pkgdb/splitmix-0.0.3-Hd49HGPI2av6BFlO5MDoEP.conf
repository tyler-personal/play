name: splitmix
version: 0.0.3
id: splitmix-0.0.3-Hd49HGPI2av6BFlO5MDoEP
key: splitmix-0.0.3-Hd49HGPI2av6BFlO5MDoEP
license: BSD-3-Clause
maintainer: Oleg Grenrus <oleg.grenrus@iki.fi>
synopsis: Fast Splittable PRNG
description:
    Pure Haskell implementation of SplitMix described in
    .
    Guy L. Steele, Jr., Doug Lea, and Christine H. Flood. 2014.
    Fast splittable pseudorandom number generators. In Proceedings
    of the 2014 ACM International Conference on Object Oriented
    Programming Systems Languages & Applications (OOPSLA '14). ACM,
    New York, NY, USA, 453-472. DOI:
    <https://doi.org/10.1145/2660193.2660195>
    .
    The paper describes a new algorithm /SplitMix/ for /splittable/
    pseudorandom number generator that is quite fast: 9 64 bit arithmetic/logical
    operations per 64 bits generated.
    .
    /SplitMix/ is tested with two standard statistical test suites (DieHarder and
    TestU01, this implementation only using the former) and it appears to be
    adequate for "everyday" use, such as Monte Carlo algorithms and randomized
    data structures where speed is important.
    .
    In particular, it __should not be used for cryptographic or security applications__,
    because generated sequences of pseudorandom values are too predictable
    (the mixing functions are easily inverted, and two successive outputs
    suffice to reconstruct the internal state).
category: System, Random
abi: 4dadbba01474d5640549de99b9a69bfc
exposed: True
exposed-modules:
    System.Random.SplitMix System.Random.SplitMix32
hidden-modules: Data.Bits.Compat
import-dirs: /home/tylerwbrown/Code/Play/Languages/Haskell/brick-tests/.stack-work/install/x86_64-linux-tinfo6/ghc-8.4.1/8.4.1/lib/x86_64-linux-ghc-8.4.1/splitmix-0.0.3-Hd49HGPI2av6BFlO5MDoEP
library-dirs: /home/tylerwbrown/Code/Play/Languages/Haskell/brick-tests/.stack-work/install/x86_64-linux-tinfo6/ghc-8.4.1/8.4.1/lib/x86_64-linux-ghc-8.4.1/splitmix-0.0.3-Hd49HGPI2av6BFlO5MDoEP
dynamic-library-dirs: /home/tylerwbrown/Code/Play/Languages/Haskell/brick-tests/.stack-work/install/x86_64-linux-tinfo6/ghc-8.4.1/8.4.1/lib/x86_64-linux-ghc-8.4.1
data-dir: /home/tylerwbrown/Code/Play/Languages/Haskell/brick-tests/.stack-work/install/x86_64-linux-tinfo6/ghc-8.4.1/8.4.1/share/x86_64-linux-ghc-8.4.1/splitmix-0.0.3
hs-libraries: HSsplitmix-0.0.3-Hd49HGPI2av6BFlO5MDoEP
depends:
    base-4.11.0.0 deepseq-1.4.3.0 time-1.8.0.2
    random-1.1-Bzf9YW9nFUS5UwW0OIfLBC
abi-depends: base-4.11.0.0=fbe01d431ed12096d1c6602fe9bad91f
             deepseq-1.4.3.0=4870cb4da84d4be96d4aa4e0870cea5d
             time-1.8.0.2=07baeb2afc7eae548db8241f1b7986b4
             random-1.1-Bzf9YW9nFUS5UwW0OIfLBC=732c79fb2dd88d42e09436e0e57a9a54
haddock-interfaces: /home/tylerwbrown/Code/Play/Languages/Haskell/brick-tests/.stack-work/install/x86_64-linux-tinfo6/ghc-8.4.1/8.4.1/doc/splitmix-0.0.3/splitmix.haddock
haddock-html: /home/tylerwbrown/Code/Play/Languages/Haskell/brick-tests/.stack-work/install/x86_64-linux-tinfo6/ghc-8.4.1/8.4.1/doc/splitmix-0.0.3
