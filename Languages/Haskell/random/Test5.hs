module Test5 where

data Formula p = Prop p | X p
  deriving (Show, Eq)
