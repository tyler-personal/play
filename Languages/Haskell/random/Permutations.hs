module Permutations where

{-@ factorial :: {x:Nat} -> {y:Nat}  @-}
factorial n low
  | n == low = n
  | otherwise = n * factorial (n - 1) low
