{-# LANGUAGE TypeApplications #-}
import System.IO.Unsafe
add = (+) $ unsafePerformIO (read @Int <$> getLine)

main = print $ add 4
