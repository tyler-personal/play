{-# LANGUAGE GADTs, LambdaCase, DataKinds, TypeOperators, PolyKinds #-}

module GADT where


data Expr a where
  EBool  :: Bool -> Expr Bool
  EInt   :: Int -> Expr Int
  EEqual :: Expr Int -> Expr Int -> Expr Bool
  EMaybe :: Maybe a -> Expr (Maybe a)

eval :: Expr a -> a
eval = \case
    EBool a -> a
    EInt a -> a
    EMaybe a -> a
    EEqual a b -> eval a == eval b

expr1 :: Expr Bool
expr1 = EEqual (EInt 2) (EInt 3)

res :: Bool
res = eval expr1

data HList types where
  Nil :: HList '[]
  (:>) :: x -> HList y -> HList (x ': y)
infixr 5 :>

data Index xs x where
  Z :: Index (x ': xs) x
  S :: Index xs x -> Index (y ': xs) x

get :: Index ts t -> HList ts -> t
get Z     (x :> _)  = x
get (S e) (_ :> xs) = get e xs

myList :: HList '[Int, String, Char, Bool]
myList = 4 :> "potato" :> 'e' :> True :> Nil

res1 = get (S Z) myList

-- Second Example

-- data Expr'
--   = EBool' Bool
--   | EInt' Int
--   | EEqual' Expr' Expr'
--
-- eval' :: Expr' -> a
-- eval' = \case
--   EBool' a -> a
--   EInt' a -> a
--   EEqual' a b -> eval' a == eval' b
