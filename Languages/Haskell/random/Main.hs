{-# LANGUAGE NoMonomorphismRestriction, FlexibleContexts #-}
import Data.Char (toUpper)
import Data.List
import Control.Applicative
import Control.Lens
x a b c d = sort . g a b c d
y a b c d = sort $ g a b c d
g = undefined
hasLast char = filter ((==) char . last)

main = print $ hasLast 'o' ["tomato", "potato", "oof"]

f = fmap . replicate

toCamelCase (x:y:xs)
  | x `elem` "_-" = toUpper y : toCamelCase xs
  | otherwise = x : toCamelCase (y:xs)
toCamelCase x = x

sum' = sum $ filter (\x -> x %? 3 || x %? 5) [1..999]
su'' = sum $ filter (liftA2 (||) (%? 3) (%? 5)) [1..999]
s''' = sum $ filter ((||) <$> (%? 3) <*> (%? 5)) [1..999]
sumb = sum $ filter ((||) <$> ((== 0) . (`mod` 3)) <*> ((== 0) . (`mod` 5))) [1..999]
--suma = sum $ filter ((||) <$> ((== 0) . (flip mod)) <$> (3 <*> 5)) [1..999]
-- suma = sum $ filter (over both ((== 0) . (flip mod)))
suma = sum $ filter ((||) . (%? 3) <*> (%? 5)) [1..999]
sumc = sum $ filter (\x -> any (x %?) [3,5]) [1..999]
sumd = sum $ filter (flip any [3,5] . (%?)) [1..999]
sume = sum $ filter (flip any [3,5] . ((== 0) .: mod)) [1..999]
sumg = sum $ filter (\x -> any ((== 0) . mod x) [3,5]) [1..999]

x %? n = x `mod` n == 0
(.:) = (.) . (.)
