{-# LANGUAGE RecordWildCards #-}
module Test3 where

x :: Num a => a
x = 6

main = print x

data Item = Item { name :: String, age :: Int }

f :: Item
f = Item { name = "potato", age = 4 }
