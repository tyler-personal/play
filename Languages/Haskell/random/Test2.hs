module Test2 where
import Data.List
import Control.Monad

infixr 9 .:
(.:) = (.) . (.)
count = length .: filter

maaa = getLine >>= print . ap (count . (1 ==) .: gcd) (enumFromTo 1) . read
maiz = getLine >>= print . (\n -> count ((== 1) . gcd n) [1..n]). read
main = getLine >>= print . (\n -> length [x | x <- [1..n], gcd x n == 1]) . read

f :: (Num a, Enum a) => (a -> a -> Bool) -> a -> Int
f cond n = length [x | x <- [1..n], cond x n]
