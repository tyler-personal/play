{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -w #-}

module RandomGrammar where
import Prelude hiding (unwords)
import Data.Text
import Data.List hiding (unwords)
import Control.Monad


main = (splitOn " " <$> readLn) >>= \xs -> forM_ (permutations xs) (print . unwords)

main2 = do
  words <- splitOn " " <$> readLn
  forM_ (permutations words) (print . unwords)

main3 = do
  line <- readLn

  let words = splitOn " " line
  let combinations = permutations words

  forM_ combinations printCombination

printCombination = print . unwords

