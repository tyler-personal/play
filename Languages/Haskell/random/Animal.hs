{-# LANGUAGE DuplicateRecordFields, RecordWildCards #-}
data AnimalType = Dog | Cat | Beagle

data Animal = Animal { name :: String, animalType :: AnimalType }
data User = User { name :: String, age :: Int }

userWhatAmI = print "I am a user class"

run User{..} = print (name ++ " is running, and he's " ++ show age ++ " years old")

main = do
  userWhatAmI

  let user = User "Kyle" 20
  run user

  let user2 = User "Tyler" 22
  run user

