{-# LANGUAGE QuasiQuotes, TypeApplications #-}
module Test4 where

import Control.Monad
import Data.String.Interpolate

f :: [a] -> Int
f = do
  l <- length
  return l
pluralize word num = [i|#{num} #{word}#{guard (num /= 1) >> "s"}|]
--pluralize word num = [i|#{num} #{word}|] ++ (guard (num /= 1) >> "s")
--pluralize word num = (show num) ++ " " ++ word ++ (guard (num /= 1) >> "s")

main = print $ pluralize "potato" 4
