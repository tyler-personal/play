{-# LANGUAGE TypeApplications #-}
module Main2 where

main = do
  print "Enter the first number"
  num1 <- getNum
  print "Enter the second number"
  num2 <- getNum
  print  $ filter odd [num1..num2]

getNum = read @Int <$> getLine
