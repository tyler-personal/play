data Currency = Penny | Nickle | Dime | Quarter | OneBill | FiveBill | TenBill | TwentyBill
  deriving (Eq, Ord, Show)

getChange :: Int -> [Currency] -> [Currency]
getChange owed givenCurrency
  | owed > given = error "There is no change"
  | given > owed = convertToCurrency $ given - owed
  | otherwise = []
    where
      given = convertToValue givenCurrency

convertToValue :: [Currency] -> Int
convertToValue = foldr ((+) . value) 0

convertToCurrency :: Int -> [Currency]
convertToCurrency 0 = []
convertToCurrency n
  | n >= 2000 = TwentyBill : rest 2000
  | n >= 1000 = TenBill : rest 1000
  | n >= 500 = FiveBill : rest 500
  | n >= 100 = OneBill : rest 100
  | n >= 25 = Quarter : rest 25
  | n >= 10 = Dime : rest 10
  | n >= 5 = Nickle : rest 5
  | n >= 1 = Penny : rest 1
  | otherwise = error "Can't convert debt to currency"

  where rest removed = convertToCurrency (n - removed)

value :: Currency -> Int
value Penny = 1
value Nickle = 5
value Dime = 10
value Quarter = 25
value OneBill = 100
value FiveBill = 500
value TenBill = 1000
value TwentyBill = 2000

main = do
  let before = [TenBill, OneBill, Nickle, Penny]
  let charge = 512
  let after = getChange charge before
  print after
