{-# LANGUAGE TypeApplications #-}
import Control.Monad

readAll :: Read a => String -> [a]
readAll = map read . words


input str = do
  putStrLn str
  getLine


main = do
  smarty <- read @Bool <$> input "Are you a smarty?"
  nums <- readAll @Int <$> input "Really? Then tell me your favorite numbers."

  when (2 `elem` nums) $
    putStrLn "Dayum 2 is a good number."

  putStrLn "Bye"
