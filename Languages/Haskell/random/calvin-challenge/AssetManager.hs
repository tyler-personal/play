{-# LANGUAGE NamedFieldPuns, RecordWildCards #-}
module AssetManager where
import Data.List.Split (splitOn)
import Data.List (sortOn, intercalate)
import Data.Maybe (mapMaybe)

data Asset = Asset { name :: String, kind :: String, amount :: Int }

instance Show Asset where
  show Asset{..} = name ++ "," ++ kind ++ "," ++ show amount

instance Eq Asset where
  a1 == a2 = name a1 == name a2 && kind a1 == kind a2

createAsset it = Asset { name = it!!0, kind = it!!1, amount = read (it!!2) }

main = do
  line <- getLine
  let [before, after] = map (map (createAsset . splitOn ",") . splitOn "|") (splitOn ":" line)

  let befores = createResults before after (\it -> (Just it, Nothing))
  let afters = createResults after before (\it -> (Nothing, Just it))
  let both = before >>= \b -> mapMaybe (\a -> createResult(Just b, Just a)) after

  putStrLn . intercalate "\n" . map (\(t, a) -> t ++ "," ++ show a) .
    sortOn (name . snd) . sortOn (kind . snd) $
      (befores ++ afters ++ both)

createResults x y f = mapMaybe (createResult . f) (filter (`notElem` y) x)

newAsset a@Asset{amount=a1} Asset{amount=a2} = a { amount = abs (a1 - a2) }

createResult (Nothing, Just a) = Just ("BUY", a)
createResult (Just b, Nothing) = Just ("SELL", b)
createResult (Nothing, Nothing) = Nothing
createResult (Just b, Just a)
  | b /= a = Nothing
  | amount b == amount a = Nothing
  | amount b > amount a = Just ("SELL", newAsset b a)
  | otherwise = Just ("BUY", newAsset b a)
