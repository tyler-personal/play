{-# LANGUAGE NoMonomorphismRestriction #-}
import Prelude hiding ((^))
main = print 4

(^) _ 0 = 1
(^) b n
  | n < 0 = b / b ^ abs (n - 1)
  | n > 0 = b * b ^ (n - 1)

pow _ 0 = 1
pow b n = b * pow b (n - 1)

(//) = div

(^!) b n
  | n == 0 = 1
  | n < 0  = b / b ^! abs (n - 1)
  | even n = (b * b) ^! (n // 2)
  | odd n  = b * (b * b) ^! ((n - 1) // 2)

