module Main where

main = do
  print "Enter the first number"
  line <- getLine
  print "Enter the second number"
  line2 <- getLine
  let num1 = read line :: Int
  let num2 = read line2 :: Int
  print $ filter (\x -> x `mod` 2 == 1) [num1..num2]

