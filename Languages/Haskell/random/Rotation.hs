--import Data.Sequence hiding ((|>), (<|), (:<), (:>))
import Control.Lens
import Data.Vector
--import Data.Array

leftRotation (x:<xs) = xs |> x
rightRotation (xs:>x) = x <| xs

--leftRotation (uncons -> Just (x,xs)) = xs |> x

--leftRotation' :: Seq a -> Seq a
--leftRotation'  (x:<|xs) = xs |> x

--rightRotation' :: Seq a -> Seq a
--rightRotation' (xs:|>x) = x <| xs

main = print 4
