import Prelude hiding (head, subtract, div)

{-@ type NonEmpty a = {v:[a] | 0 < len v} @-}

{-@ head :: NonEmpty a -> a @-}
head (x:_) = x

{-@ div :: Num a => a -> {y:a | y != 0} -> a @-}
div a b = a / b

{-@ fib :: {x:a | x >= 0} -> b @-}
fib 0 = 1
fib 1 = 1
fib n = fib (n - 1) + fib (n - 2)

{-@ subtract :: Num a => x:a -> {y : a | x > y} -> a @-}
subtract a b = a - b

add a b = a + b

main :: IO ()
main = do
  let x = "4"
  let y = "yeet"
  let q = bam x
  putStrLn "test"
  putStrLn "A: "
  xIn <- getLine
  print $ div 4 2
  putStrLn "B: "
  yIn <- getLine
  let x = read xIn :: Double
  let y = read yIn :: Double

  print $ div 4 0
  print $
    if y == 0 then
      "That's dumb"
    else
      show $ div x y

quicksort [] = []
quicksort (x:xs) = lesser ++ [x] ++ greater
  where
    lesser = quicksort (filter (< x) xs)
    greater = quicksort (filter (>= x) xs)

bam :: String -> Int
bam x = length x


