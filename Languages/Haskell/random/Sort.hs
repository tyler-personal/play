module Sort where
import Data.List hiding (sort)
import Control.Lens

qs1 [] = []
qs1 (x:xs) = qs1 less ++ [x] ++ qs1 more
  where
    less = filter (< x) xs
    more = filter (>= x) xs

qs2 [] = []
qs2 (x:xs) = qs2 less ++ [x] ++ qs2 more
  where (less, more) = partition (<x) xs

qs3 [] = []
qs3 (x:xs) = less ++ [x] ++ more
  where (less, more) = (each %~ qs3) (partition (<x) xs)

quicksort :: Ord a => [a] -> [a]
quicksort [] = []
quicksort (x:xs) = quicksort (filter (<= x) xs) ++ [x] ++ quicksort (filter (> x) xs)

a f xs = map $ f xs

sort1 [] = []
sort1 xs = x : delete x xs
  where x = minimum xs

sort a = if null a then a else x : sort (delete x a) where x = minimum a

sortLong [] = []
sortLong xs = min : sortLong (delete min xs)
  where min = minimum xs

main = print $ sort [1,4,0,2,3,1,0,-1,7]
