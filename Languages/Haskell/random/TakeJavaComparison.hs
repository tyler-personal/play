module TakeJavaComparison where

-- {-@ take :: xs:[a] -> {i:Int | len xs > (i * 2)} -> (a, a) @-}
-- take xs i = (xs !! i, xs !! (i))

{-@ test :: {xs:[a] | len xs > 4} -> a @-}
test xs = xs !! 4

-- {-@ test2 :: forall a. xs[a] -> {i:Int | len xs > i} -> (a,a) @-}
-- test2 xs i = (xs !! i, xs !! i)
