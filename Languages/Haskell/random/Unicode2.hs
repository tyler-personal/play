{-# LANGUAGE UnicodeSyntax #-}
{-# OPTIONS_GHC -w #-}

(∈) ∷ (Foldable t, Eq a) ⇒ a → t a -> Bool
(∈) = elem

main = print $ 2 ∈ [2, 4, 7]
