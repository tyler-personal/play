module Third where

purr str = str ++ "rrr"

roar str = purr $ str ++ "yaaa"

main = print $ roar "test"
