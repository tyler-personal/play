module Second where

shout :: String -> IO ()
shout = print

purr str = shout $ str ++ "rrr"

roar str = purr $ str ++ "yaaa"

main = roar "test"
