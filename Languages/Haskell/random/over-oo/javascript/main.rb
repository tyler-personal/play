fib = Enumerator.new { |y|
  a, b = 0, 1
  loop {
    y << b
    a, b = b, a + b
  }
}

x = gets.chomp.to_i
puts fib.take(x).last()
