Javascript:
```Javascript
const fib = n =>
  Array.from({ length : n }).reduce(
    (acc, e, i) => acc.concat(i > 1 ? acc[i - 1] + acc[i - 2] : i), []);

const fib2 = n => {
  const arr = [0, 1];
  for (let i = 2; i < n; i++)
    arr.push(arr[i - 2] + arr[i - 1]);
  return arr;
}

const fib3 = n =>
  n === 0 ? 0 :
    n === 1 ? 1 :
      fib3(n - 1) + fib3(n - 2);
```
Haskell:
```Haskell
fib = 0 : scanl (+) 1 fib

fib2 = f 0 1
  where f x y = x : f y (x + y)

fib3 0 = 0
fib3 1 = 1
fib3 n = fib3 (n - 1) + fib3 (n - 2)
```

`fib` & `fib2` have linear time complexity. `fib3` has expontential time complexity,

respective character counts:
Haskell:
  `fib` - 25
  `fib2` - 44
  `fib3` - 58
Javascript:
  `fib` - 126
  `fib2` - 124
  `fib3` - 84

Interestingly enough, having a lower time complexity in Haskell actually results in simpler/shorter code, where in Javascript it results in more complex code.

The Haskell is also lazily evaluated, which means you can do things like `fib !! 7` instead of `fib(8)[7]` in JS.
