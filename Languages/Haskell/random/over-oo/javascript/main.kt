val fib = generateSequence(0 to 1) { (a, b) ->
  b to a + b
}.map { it.second }

fun main() {
  val x = readLine()!!.toInt()
  println(fib.take(x).last())
}
