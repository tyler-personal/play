fib :: [Int]
fib = 0 : scanl (+) 1 fib

main = ((fib !!) <$> readLn) >>= print
