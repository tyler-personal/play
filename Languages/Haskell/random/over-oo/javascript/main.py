def fib():
    a, b = 0, 1
    while True:
        yield b
        a, b = b, a + b

def indexGenerator(generator, index):
    result = 0
    g = generator()
    for _ in range(index):
        result = next(g)
    return result

if __name__ == '__main__':
    print(indexGenerator(
        generator=fib,
        index=int(input())
    ))
