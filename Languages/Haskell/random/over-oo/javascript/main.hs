module Main2 where
fib = 0 : scanl (+) 1 fib

fib2 = f 0 1
  where f x y = x : f y (x + y)

fib3 0 = 0
fib3 1 = 1
fib3 n = fib3 (n - 1) + fib3 (n - 2)

doubles = scanl (+) 1 doubles

factorial n
  | n <= 1 = 1
  | otherwise = n * factorial (n - 1)

average xs = sum xs / fromIntegral (length xs)

main = print "yeet"
