module Main3 where
fib = f 0 1
  where f x y = x : f y (x + y)

main = do
  x <- readLn
  print $ fib !! x

