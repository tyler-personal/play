const fib = n =>
  Array.from({ length : n }).reduce(
    (acc, e, i) => acc.concat(i > 1 ? acc[i - 1] + acc[i - 2] : i), []);

const fib2 = n => {
  const arr = [0, 1];
  for (let i = 2; i < n; i++)
    arr.push(arr[i - 2] + arr[i - 1]);
  return arr;
}

const fib3 = n =>
  n === 0 ? 0 :
    n === 1 ? 1 :
      fib3(n - 1) + fib3(n - 2);

const factorial = n =>
  n <= 1 ? 1 : n * factorial

const average = xs =>
  xs.reduce((x, y) => x + y, 0) / xs.length

