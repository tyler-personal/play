{-# LANGUAGE GADTs, TypeOperators, DataKinds, PolyKinds, RankNTypes #-}
{-# LANGUAGE FlexibleInstances, FlexibleContexts, PatternSynonyms #-}
{-# LANGUAGE OverloadedStrings #-}

module H2
  ( HListT(Nil, (:~), (:~~)), HList
  , mapNatural, extract
  , Copointed(..), (<~>)
  ) where

import Data.Kind (Type)
import Data.Functor.Identity
import Data.Maybe
import Data.Text

data HListT :: (k -> Type) -> [k] -> Type where
  Nil  :: HListT f '[]
  (:~) :: f a -> HListT f l -> HListT f (a ': l)
infixr 6 :~

instance Show (HListT f '[]) where
  show _ = "Nil"
instance (Show (HListT f xs), Show (f x)) => Show (HListT f (x ': xs)) where
  showsPrec d (fx :~ xs)
    = showParen (d >= 11)
    $ showsPrec 6 fx . (" :~ " ++) . showsPrec 6 xs

instance Eq (HListT f '[]) where
  _ == _ = True
instance (Eq (HListT f xs), Eq (f x)) => Eq (HListT f (x ': xs)) where
  fx :~ xs == fy :~ ys = fx == fy && xs == ys


type HList = HListT Identity

{-# COMPLETE (:~~) #-}
pattern (:~~) :: () => al ~ (a ': l) => a -> HList l -> HList al
pattern x :~~ xs = Identity x :~ xs


mapNatural :: (forall a. f a -> g a) -> HListT f xs -> HListT g xs
mapNatural _ Nil       = Nil
mapNatural f (x :~ xs) = f x :~ mapNatural f xs

extract :: (forall a. f a -> a) -> HListT f xs -> HList xs
extract f = mapNatural (Identity . f)


class Copointed f where
  copoint :: f a -> a

(<~>) :: Copointed f => f x -> HList xs -> HList (x ': xs)
fx <~> hl = copoint fx :~~ hl
infixr 5 <~>

testHList :: HList '[Int, Text]
testHList = 2 :~ "3" :~ Nil

testData :: HListT Maybe '[Int, Text]
testData = Just 2 :~ Just "3" :~ Nil

testData2 :: HListT [] '[Int, Text]
testData2 = [2] :~ ["3"] :~ Nil

test :: IO ()
test = print $ extract fromJust testData

