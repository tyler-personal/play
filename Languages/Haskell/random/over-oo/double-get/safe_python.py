from typing import (Tuple, Generic, Callable)

def example() -> Tuple[Int, String]:
    return doubleGet(lambda xs: xs[1], [1, 2, 3], ["1", "2"])

X = TypeVar('X')
Y = TypeVar('Y')
A = TypeVarForAll?('A')
F = TypeVar('F', bound=type)

def doubleGet(
        f: Callable[F(A), A]
        fx: F(X),
        fy: F(Y)
        ): -> Tuple[X, Y]
    return (f(fx), f(fy))
