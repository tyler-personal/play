def example():
    return double_get(lambda xs: xs[1], [1, 2, 3], ["1", "2"])


def double_get(f, fx, fy):
    return f(fx), f(fy)


print(example())

