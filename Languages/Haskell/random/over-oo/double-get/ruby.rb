def example()
  doubleGet -> (xs) { xs[1] }, [1, 2, 3], ["1", "2"]
end

def doubleGet(f, fx, fy)
  [f.call(fx), f.call(fy)]
end

puts example

