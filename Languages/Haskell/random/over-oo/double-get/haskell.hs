{-# LANGUAGE RankNTypes, UnicodeSyntax, ExistentialQuantification #-}
{-# LANGUAGE KindSignatures, DataKinds, TypeApplications #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
import Data.Maybe

example ∷ (Int, String)
example = doubleGet (!! 1) [1, 2, 3] ["1", "2"]

example2 ∷ (String, Int)
example2 = doubleGet fromJust (Just "yeet") (Just 2)

-- Looking for a way to generalize this over X gets

-- get ∷ (∀ a. f a → a) → f x → x
-- get f = f

-- get :: (∀ a. f a -> a) -> f x -> f y -> (x, y)
doubleGet ∷ (∀ a. f a → a) → f x → f y → (x, y)
doubleGet f fx fy = (f fx, f fy)

tripleGet ∷ (∀ a. f a → a) → f x → f y → f z → (x, y, z)
tripleGet f fx fy fz = (f fx, f fy, f fz)

main = do
  num1 <- readLn
  num2 <- readLn
  print $ num1 + num2

-- I like this file.
