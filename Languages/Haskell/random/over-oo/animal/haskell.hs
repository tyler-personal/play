{-# LANGUAGE FlexibleInstances, UndecidableInstances #-}
module HaskellOverOOP where

class Animal a where
  running :: a -> Bool

class Bipedal a where
  kneeling :: a -> Bool

class Swimmer a where
  afraid :: a -> Bool

checkOld a = do
  print $ f (kneeling a) (running a)
  print $ if afraid a
    then "you a big baby"
    else "well that was expected"
  where
    f True True = "You're kneeling and running"
    f False False = "Maybe do something"
    f _ _ = "Wow you're normal"

class Checker a where
  check :: a -> IO ()

--instance (Animal a, Bipedal a, Swimmer a) => Checker a where
 -- check a = print "Wow you're all three!"
instance Animal a => Checker a where
  check a = print "Okay so you can't swim and you're not bipedal, interesting."

f = putStrLn "shaddup KAYLE!"

-- Concrete run
data John = John

instance Animal John where running _ = True
instance Bipedal John where kneeling _ = False
instance Swimmer John where afraid _ = True

main = check John

