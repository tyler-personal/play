from abc import ABC

class Animal(ABC):
    def running(self): pass

class Bipedal(ABC):
    def kneeling(self): pass

class Swimmer(ABC):
    def afraid(self): pass

def check(a):
    def f(x, y):
        if x and y:
            return "You're kneeling and running"
        elif not (x and y):
            return "Maybe do something"
        else:
            return "Wow you're normal"

    print(f(a.kneeling, a.running))
    if a.afraid:
        print("You're a big baby")
    else:
        print("Well that was expected")

# Conrete run
class John(Animal, Bipedal, Swimmer):
    def running(self):
        return True

    def kneeling(self):
        return False
    
    def afraid(self):
        return True

check(John())

