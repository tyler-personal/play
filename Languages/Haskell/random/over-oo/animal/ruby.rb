def check(a)
  puts case %w[running kneeling swimming crying].map { |x| a.respond_to?(x) ? a.x : nil }
  when [nil, nil, nil, nil]
    "Wow you're nothing"
  when [_, nil, nil, nil]
    "okay, you can run, that's cool"
  when [false, true, nil, nil]
    "why are you kneeling instead of running?"
  # etc.
  else
    "mmkay I don't get you"
  end
end

class John
  running = true
end

class JoeTheDolphin
  swimming = true
  crying = true
end

class PrayingMan
  kneeling = true
end

[PrayingMan, JoeTheDolphin, John].map { |x| check(x) }

puts John.new().running
