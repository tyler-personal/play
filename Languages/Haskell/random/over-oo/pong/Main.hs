data Position a = Position a a deriving (Show, Eq)

data Velocity a = Velocity a a deriving (Show, Eq)

data Ball a = Ball (Position a) (Velocity a) deriving (Show, Eq)

createBall x y = Ball (Position x y) (Velocity 0 0)

main = print $ last balls
  where balls = map (\x -> createBall x x) [0..5]
