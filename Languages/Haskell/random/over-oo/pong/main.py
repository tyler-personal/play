class Position:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return f"Position(x={self.x}, y={self.y})"

    def __eq__(self, other):
        if not isinstance(other, Position):
            return False
        return self.x == other.x and self.y == other.y

class Velocity:
    def __init__(self, x = 0, y = 0):
        self.x = x
        self.y = y

    def __str__(self):
        return f"Velocity(x={self.x}, y={self.y})"

    def __eq__(self, other):
        if not isinstance(other, Velocity):
            return False
        return self.x == other.x and self.y == other.y

class Ball:
    def __init__(self, position, velocity = Velocity()):
        self.position = position
        self.velocity = velocity

    def __str__(self):
        return f"Ball(position={self.position}, velocity={self.velocity})"

    def __eq__(self, other):
        if not isinstance(other, Ball):
            return False
        return self.position == other.position \
           and self.velocity == other.velocity

def createBall(x, y):
    return Ball(Position(x, y))

if __name__ == '__main__':
    balls = [createBall(x, x) for x in range(6)]
    print(balls[-1])
