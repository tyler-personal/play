{-# LANGUAGE TypeApplications, UnicodeSyntax #-}
import Control.Monad

readLines ∷ Read a ⇒ Int → IO [a]
readLines x = replicateM x readLn

main :: IO ()
main = do
  names ← readLines @String 4
  print "The longest name is: "
  print . maximum $ map length names

  numbers ← readLines @Int 10
  print "The smallest number is: "
  print . minimum $ numbers
