def readLines(x, f):
    return [f(input()) for _ in range(x)]

if __name__ == 'main':
    names = readLines(4, str)
    print("The longest name is: ")
    print(max([len(n) for n in names]))

    numbers = readLines(10, int)
    print("The smallest number is: ")
    print(min(numbers))
