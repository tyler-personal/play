main = do
  let row = replicate 3 ""
  let board = replicate 3 row

  print board

  let board' = ("X" : drop 1 (head board)) : drop 1 board

  print board'
