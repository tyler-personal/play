items = [False, 3, "potato", 4, True, "7", 4.0, 2.0]

intCount = 0
boolCount = 0
strCount = 0
otherCount = 0

for item in items:
    if isinstance(item, int):
        intCount += 1
    elif isinstance(item, bool):
        boolCount += 1
    elif isinstance(item, str):
        strCount += 1
    else:
        otherCount += 1

print(
    f"intCount: {intCount}, boolCount: {boolCount}, "
    f"strCount: {strCount}, otherCount: {otherCount}"
)
