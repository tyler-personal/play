module Animal where
import Data.Function (on)

data Gender = Male | Female deriving Eq

class Species s where
    gender :: s -> Gender

matable :: Species a => a -> a -> Bool
matable = on (/=) gender

data Human = Man | Woman
data Canine = Dog | Bitch

instance Species Human where
    gender Man = Male
    gender Woman = Female

instance Species Canine where
    gender Dog = Male
    gender Bitch = Female

bark :: Canine -> String
bark Dog = "woof"
bark Bitch = "wow"

speak :: Human -> String -> String
speak Man = ("The man says " ++)
speak Woman = ("The woman says " ++)
