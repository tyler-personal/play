{-# LANGUAGE DataKinds, TypeOperators, PolyKinds, RankNTypes, GADTs #-}
import Prelude hiding (map)
import GHC.TypeLits

data Vect n a where
  Nil :: Vect 0 a
  (:-) :: a -> Vect n a -> Vect (n + 1) a

map :: (a -> b) -> Vect n a -> Vect n b
map _ Nil = Nil
map f (x :- xs) = f x :- map f xs

append :: Vect n a -> Vect m a -> Vect (n + m) a
append Nil Nil = Nil
-- technically xs Nil and Nil ys are redundant
append xs Nil = xs
append Nil ys = ys
--append (x :- xs) ys = x :- append xs ys

main = print 4
