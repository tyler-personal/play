module LambdaList where

type Set a = a -> Bool

emptySet :: Set a
emptySet _ = False

insert' :: Eq a => a -> Set a -> Set a
insert' newValue set futureValue = futureValue == newValue || set futureValue

insert a s b = a == b || s b

x = 1 + x

addOne x = 1 + addOne x

