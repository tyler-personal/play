{-# LANGUAGE NoMonomorphismRestriction, ExtendedDefaultRules, FlexibleContexts #-}
import Data.Function
import Control.Applicative
import Data.List

average1 xs = sum xs / fromIntegral (length xs)

average2 = liftA2 (/) sum (fromIntegral . length)

average3 = (/) <$> sum <*> fromIntegral . length

average4 = (/) <$> sum <*> genericLength

average5 xs = sum xs / genericLength xs

main = undefined
