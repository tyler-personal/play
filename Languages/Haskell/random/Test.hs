{-# LANGUAGE FlexibleContexts, ParallelListComp, NoMonomorphismRestriction, TupleSections #-}
import Data.Traversable

main = print $ map show [1..5]

ra = addDiffs3 test1Values test1Values
raa = addDiffs3 test3Values test3Values
(.*) = (.) . (.)

addDiffs :: Num a => [a] -> [a] -> a
addDiffs = ((.) . (.)) (sum . map (uncurry (-))) zip

addDiffs0 = (sum . map (uncurry (-))) .* zip

addDiffs1 x y = sum . map (uncurry (-)) $ zip x y

addDiffs2 x y = sum . map (\(a, b) -> a - b) $ zip x y

addDiffs3 x y = sum [a - b | a <- x | b <- y]

addDiffs4 x y = sum [a - b | (a, b) <- zip x y]

sub :: Num a => a -> a -> a
sub x y = x - y

test1Values :: [Int]
test1Values = [1,2,3,4]

test3Values :: [Double]
test3Values = [0, 1.2, 2, 3]

x = map (, 4) [1..10]

f :: [String] -> Int
f = foldl (\len str -> len + length str) 0

f2 = foldr ((+) . length) 0

f3 = sum . fmap length

numbers = merge [1..] myNameInfinitely

myNameInfinitely = "yeet" : myNameInfinitely

merge (x:xs) (y:ys) = (x, y) : merge xs ys
