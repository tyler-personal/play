{-# LANGUAGE UnicodeSyntax #-}
{-# LANGUAGE TypeApplications #-}

import Control.Monad.State
import Control.Monad (void)
import Control.Exception (SomeException, catch)

type Stack = [Int]

main = void $ runStateT code [1..]

code :: StateT Stack IO ()
code = do
  x <- pop
  num <- lift getNum
  lift $ print (num * 2)
  lift $ print x
  y <- pop
  lift $ print y
  code

getNum :: IO Int
getNum = do
  catch readLn handler

  where
    handler :: SomeException -> IO Int
    handler _ = do
      putStrLn "That's not a number."
      getNum



pop :: StateT Stack IO Int
pop = do
    (x:xs) <- get
    put xs
    return x

