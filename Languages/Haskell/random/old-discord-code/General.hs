module General where

import Data.List.Split
import Control.Monad

(|>) x f = f x

fLast f n = reverse . f n . reverse

dropLast = fLast drop
dropFinal = dropLast 1

mapStartWhile check f xs =  map f start ++ end
  where
    start = takeWhile check xs
    end = take (length xs - length start) xs

mapLastWhile check f xs = start ++ (map f end)
  where
    end = fLast takeWhile check xs
    start = take (length xs - length end) xs

splitWhenLast _ [] = ([], [])
splitWhenLast check xs = splitWhen check xs |>
  \l -> case l of
    [] -> (xs, [])
    _ -> ((join $ dropFinal l), last l)

splitAfterLast check xs = splitWhenLast check xs |>
  \(ys, zs) -> case ys of
    [] -> ([], zs)
    _ -> (dropFinal ys, [last ys] ++ zs)
