module Test where

{-@ sumOfAll :: Num a => {xs: [a] | len xs > 0} -> a @-}
sumOfAll :: Num a => [a] -> a
sumOfAll = sum

main = print $ sumOfAll ([] :: [Int]) 
