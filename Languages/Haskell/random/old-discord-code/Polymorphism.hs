{-# LANGUAGE MultiParamTypeClasses #-}
module Polymorphism where

data Animal = Dog | Cat

say Dog = "dog barks"
say Cat = "cat meows"

main = do
  print $ say Dog


class Animal2 a where
say2 :: String -> String

class (Animal2 a) => Human a where


data Dog2 = Dog2
instance Animal2 Dog2 where
say2 s = "dog barks " ++ s


fib = [0, 1] ++ zipWith (+) fib (tail fib)
