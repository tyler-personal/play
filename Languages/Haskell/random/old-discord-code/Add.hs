module Add where

data Digit =
  Zero | One | Two | Three | Four | Five | Six | Seven | Eight | Nine
  deriving (Eq, Ord, Enum, Show)

(|>) x f = f x

add xs ys
  | all (== Zero) ys = xs
  | otherwise = add (successor xs) (predecessor ys)

sub xs ys
  | all (== Zero) ys = xs
  | otherwise = sub (predecessor xs) (predecessor ys)

successor = move One Zero Nine succ
predecessor = move Zero Nine Zero pred

move emptyNum min max change digits = dropWhile (== Zero) $ case startReversed of
  [] -> emptyNum : end
  (x:xs) -> reverse xs ++ [change x] ++ end
  where
    (end, startReversed) = span (== max) (reverse digits) |>
      \(maxes, other) -> (map (const min) maxes, other)
    

