module GeneralTest where

import Control.Exception.Assert
import General

claim b = assert b "Success"

generalTest = do
  print $ claim $ (splitWhenLast (== 7) xs) == ([1, 2, 3, 1, 4], [7, 3, 4])
  print $ claim $ (splitWhenLast (== 6) xs) == ([1, 2, 3, 1, 4, 7, 3, 4], [])
  print $ "splitWhenLastTest Success"
  print $ claim $ (splitAfterLast (== 7) xs) == ([1, 2, 3, 1, 4, 7], [3, 4])
  print $ "splitWhenAfterTest Success"

xs :: [Int]
xs = [1, 2, 3, 1, 4, 7, 3, 4]

