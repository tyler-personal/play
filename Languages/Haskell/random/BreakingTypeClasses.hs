{-# LANGUAGE GADTs, RecordWildCards #-}
module BreakingTypeClasses where

data ItemRepo a where
  ItemRepo :: Item a =>
    { getItem :: Int -> IO (a)
    , saveItem :: a -> IO ()
    } -> ItemRepo a

class Item a where
  id :: a -> Int
  display :: a -> String

data User = User
  { userID :: Int
  , name :: String
  , favoriteNum :: Int
  }

data Task = Task
  { taskID :: Int
  , description :: String
  }

instance Item User where
  id = userID
  display User {..} = name ++ "\n\tFavorite Number: " ++ num
    where num = show favoriteNum

instance Item Task where
  id = taskID
  display = description

getItemRoute ::Item a => ItemRepo a -> IO ()
getItemRoute repo = do
  id <- param "id"
  item <- getItem repo id
  json $ display item

param :: String -> IO a
param = undefined

json :: String -> IO ()
json = undefined
