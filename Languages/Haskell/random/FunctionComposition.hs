module FunctionComposition where

doubleLen :: String -> String -> Int
doubleLen [] [] = 0
doubleLen (_:xs) ys = 1 + doubleLen xs ys
doubleLen xs (_:ys) = 1 + doubleLen xs ys

add :: Int -> Int -> String
add x y = show $ x + y

(./.) :: (b -> c) -> (a1 -> a2 -> b) -> a1 -> a2 -> c
(./.) = (.) . (.)

f = add ./. doubleLen

