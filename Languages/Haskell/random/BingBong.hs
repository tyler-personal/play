module BingBong where

type BingBong = Int
type BongBing = String
type Bing = IO
type Bong = BingBong

bongBong :: BongBing -> BingBong
bongBong bongBong = read bongBong

bingBing :: BingBong -> Bong -> BingBong
bingBing = (+)

bongBing :: Bong -> BingBong -> BingBong
bongBing bong bing = bingBing bong bing

bong :: Bong -> Bing ()
bong bing = print bing

bongTheBing :: Bing BingBong
bongTheBing = bongBong <$> getLine

bingTheBong :: BingBong -> BingBong -> Bong
bingTheBong bingBongBing bongBingBing = bingBing bongBingBing bingBongBing

main :: Bing ()
main = do
  bingBingBing <- bongTheBing
  bongBongBong <- bongTheBing
  bong $ bingTheBong bingBingBing bongBongBong
