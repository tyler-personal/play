{-# LANGUAGE NamedFieldPuns, RecordWildCards, TypeApplications #-}
import Numeric

formatFloat floatNum numOfDecimals =
  showFFloat (Just numOfDecimals) floatNum ""

data Stats = Stats
  { money :: Double
  , loanYears :: Int
  , monthlyCost :: Double
  , growthRate :: Double
  }

processYears :: Stats -> IO Double
processYears stats@Stats{..} = if loanYears == 0
  then return money
  else processYears newStats
  where
    newStats = stats
      { money = (money * growthRate) - (monthlyCost * 12)
      , loanYears = loanYears - 1
      }

main = do
  money <- get @Double "How much money do you have?"
  monthlyCost <- get @Double "What is the monthly cost?"
  loanYears <- get @Int "How many years do you want to process?"
  growthRate <- get @Double "What's the growth rate?"

  mainRun <- processYears Stats { money, monthlyCost, loanYears, growthRate }
  secondRun <- processYears Stats { money, monthlyCost, loanYears, growthRate=0 }
  putStrLn $ "Money left: " ++ formatFloat mainRun 2
  putStrLn $ "Money left without growth: " ++ formatFloat secondRun 2
  return ()

get :: Read a => String -> IO a
get str = do
  putStrLn str
  read <$> getLine

-- get' :: Read a => (String -> String) -> String -> IO a
-- get' f str = do
--   putStrLn str
--   line <- getLine
--  read (f line)
