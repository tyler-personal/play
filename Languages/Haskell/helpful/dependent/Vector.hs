{-# LANGUAGE DataKinds, TypeFamilies, TypeOperators #-}
{-# LANGUAGE GADTs #-}

module Vector where

import Prelude hiding (last, length, map, reverse, zip, sum, repeat)
import Base

-- Basics
head :: Vect ('S n) a -> a
head (x :- _) = x

tail :: Vect ('S n) a -> Vect n a
tail (_ :- xs) = xs

uncons :: Vect ('S n) a -> (a, Vect n a)
uncons (x :- xs) = (x, xs)

last :: Vect ('S n) a -> a
last (x :- Nil) = x
last (_ :- y :- ys) = last $ y :- ys

append :: Vect n a -> Vect m a -> Vect (n :+ m) a
append Nil ys = ys
append (x :- xs) ys = x :- append xs ys

appendOnSecond :: Vect n a -> Vect m a -> Vect (m :+ n) a
appendOnSecond xs Nil = xs
appendOnSecond xs (y :- ys) = y :- appendOnSecond xs ys

(+++) = append

length :: Vect n a -> SNat n
length Nil = SZ
length (_ :- xs) = SS (length xs)

mkVect :: a -> Vect ('S 'Z) a
mkVect a = a :- Nil

-- Transformations
map :: (a -> b) -> Vect n a -> Vect n b
map _ Nil = Nil
map f (x :- xs) = f x :- map f xs

repeat :: Vect n a -> SNat m -> Vect (m :* n) a
repeat _ SZ = Nil
repeat xs (SS n) = xs +++ repeat xs n

repeatByLength :: Vect n a -> Vect (n :* n) a
repeatByLength xs = repeat xs (length xs)

-- filter :: (a -> Bool) -> Vect n a -> Vect m a
-- filter _ Nil = Nil

-- reverse :: Vect n a -> Vect n a
-- reverse Nil = Nil
-- reverse (x :- xs) = appendOnSecond b a
--   where
--     -- f SZ _ = f SZ xs
--     -- f n (x :- xs) = toVect (SS n) x +++ f (SS n) xs
--
--     a = mkVect x
--     b = reverse xs

zip :: Vect n a -> Vect n b -> Vect n (a, b)
zip Nil Nil = Nil
zip (x :- xs) (y :- ys) = (x, y) :- zip xs ys

zipWith :: (a -> b -> c) -> Vect n a -> Vect n b -> Vect n c
zipWith f xs ys = map (uncurry f) $ zip xs ys

toList :: Vect n a -> [a]
toList Nil = []
toList (x :- xs) = x : toList xs

-- Reductions
sum :: Num a => Vect n a -> a
sum Nil = 0
sum (x :- xs) = x + sum xs

dotProduct :: Num a => Vect n a -> Vect n a -> a
dotProduct Nil Nil = 0
dotProduct (x :- xs) (y :- ys) = (x * y) + dotProduct xs ys

-- Partial Functions
toVect :: SNat n -> [a] -> Vect n a
toVect SZ [] = Nil
toVect SZ _ = error "invalid length"
toVect _ [] = error "invalid length"
toVect (SS n) (x : xs) = x :- toVect n xs

