{-# LANGUAGE GADTs, DataKinds #-}
module Matrix where

import Prelude hiding (map, zip, flip, tail, head, zipWith, transpose, sum, uncons)
import Base
import Vector (zip, map, tail, head, zipWith, dotProduct, sum, uncons)

-- transpose22 :: Matrix ('S n) ('S m) a -> Matrix ('S m) ('S n) a
-- transpose22 x = map head x :- transpose22 (map tail x)

transpose :: Matrix n m a -> Matrix m n a
transpose (Nil :- _) = Nil
transpose (x :- xs) = qq :- transpose r
  where
    t = x :- xs
    qq = map head t
    r = map tail t

-- flip :: Matrix n m a -> Matrix m n a
-- flip (x :- xs) = map (\(a,b) -> a :- b) $ zip x (flip xs)

-- multiply :: Num a => Matrix m n a -> Matrix n p a -> Matrix m p a
-- multiply xs Nil = Nil
-- multiply xs (y :- ys) = f (transpose xs) :- multiply xs ys
--   where
--     f Nil = Nil
--     f (z :- zs) = dotProduct z y :- f zs
-- multiply :: Num a => Matrix n m a -> Matrix p n a -> Matrix m p a
-- multiply xs (y :- ys) = f (transpose xs) :- multiply xs ys
--   where
--     f (z :- zs) = dotProduct z y :- f zs

multiply :: Num a => Matrix m n a -> Matrix n p a -> Matrix m p a
multiply xs ys = map (\x -> map (sum . zipWith (*) x) (transpose ys)) xs
