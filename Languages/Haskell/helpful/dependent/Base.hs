{-# LANGUAGE DataKinds, TypeFamilies, TypeOperators #-}
{-# LANGUAGE GADTs, StandaloneDeriving, UndecidableInstances #-}

module Base where
import Data.List (transpose)

data Nat = Z | S Nat

instance Num Nat where
  fromInteger 0 = Z
  fromInteger n = S . fromInteger $ n - 1

  a + Z = a
  a + (S b) = S a + b

  a * Z = Z
  a * (S b) = (a * b) + a

  abs n = n
  negate n = n

  signum Z = 0
  signum n = 1

instance Show Nat where
  show n = show $ natToInt n

natToInt Z = 0
natToInt (S n) = 1 + natToInt n

data Vect n a where
  Nil :: Vect 'Z a
  (:-) :: a -> Vect n a -> Vect ('S n) a

type family   (n :: Nat) :+ (m :: Nat) :: Nat
type instance 'Z         :+ m           = m
type instance 'S n       :+ m           = 'S (n :+ m)

type family   (n :: Nat) :- (m :: Nat) :: Nat
type instance 'Z         :- m           = Z
type instance n          :- 'Z          = n
type instance 'S n       :- 'S m        = n :- m

type family  (n :: Nat)  :* (m :: Nat) :: Nat
type instance 'Z         :* m           = Z
type instance 'S n       :* m           = (:+) m (n :* m)

data SNat n where
  SZ :: SNat 'Z
  SS :: SNat n -> SNat ('S n)

infixl 6 :+
infixr 5 :-

deriving instance Eq a   => Eq   (Vect n a)
deriving instance Show a => Show (Vect n a)

type Matrix n m a = Vect n (Vect m a)

-- IGNORE BELOW
transpose2 :: [[a]] -> [[a]]
transpose2 ([] : _) = []
transpose2 x = map head x : transpose2 (map tail x)

zip2 :: [a] -> [b] -> [(a,b)]
zip2 [] [] = []
zip2 (x : xs) (y : ys) = (x, y) : zip2 xs ys

multiply a b = [[sum $ zipWith (*) aa bb | bb <- transpose b] | aa <- a]

m2 a b = map (\aa -> map (sum . zipWith (*) aa) (transpose b)) a
