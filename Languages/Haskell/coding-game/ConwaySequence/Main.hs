{-# LANGUAGE TypeApplications #-}
import Prelude hiding (sequence)
import Data.List (group)

type Line = [Int]

getNumber :: IO Int
getNumber = read @Int <$> getLine

sequence :: Line -> [Line]
sequence xs = xs : (sequence . flatten . countOccurrences $ xs)

countOccurrences :: Eq a => [a] -> [(Int, a)]
countOccurrences xs = map (\ys -> (length ys, head ys)) (group xs)

flatten :: [(a, a)] -> [a]
flatten [] = []
flatten ((x,y):xs) = x : y : flatten xs

display :: Show a => [a] -> String
display = unwords . map show

main :: IO ()
main = do
  startNumber <- getNumber
  lineNumber <- getNumber

  let line = sequence [startNumber] !! (lineNumber - 1)
  putStrLn $ display line
