from typing import List, TypeVar, Tuple
from collections import Counter

Line = List[int]
A = TypeVar('A')

def getNumber() -> int:
    return int(input())

def sequence(
        line: Line,
        lineNumber: int,
        currentLineNumber: int = 0) -> List[Line]:
    if currentLineNumber == lineNumber:
        return [line]
    else:
        return [line] + sequence(
            flatten(countOccurrences(line)),
            lineNumber,
            currentLineNumber + 1)

def countOccurrences(xs: List[A]) -> List[Tuple[int, A]]:
    return list(map(lambda ys: (len(ys), ys[0]), group(xs)))

def flatten(xs: List[Tuple[A, A]]) -> List[A]:
    if len(xs) == 0:
        return []
    else:
        return [xs[0][0], xs[0][1]] + flatten(xs[1:])

def display(xs: List[A]) -> str:
    return " ".join(list(map(lambda x: str(x), xs)))

def group(xs: List[A]) -> List[List[A]]:
    return [[k] * v for k, v in Counter(xs).items()]

if __name__ == '__main__':
    startNumber = getNumber()
    lineNumber = getNumber()

    line = sequence([startNumber], lineNumber - 1)[-1]
    print(display(line))
