{-# LANGUAGE TypeApplications, NamedFieldPuns, RecordWildCards #-}
module Main where

import Control.Monad.State
import System.IO
import Control.Monad
import Data.Maybe (isNothing, fromJust)
import Data.List (find)

executeCodingGameRequirement = hSetBuffering stdout NoBuffering

readAll :: Read a => String -> [a]
readAll = map read . words

readNums = readAll @Int <$> getLine

-- TODO: Try to refactor Exit/Elevator to just Floor
data Exit = Exit { exitFloor :: Int, exitPosition :: Int }
data Elevator = Elevator { elevatorFloor :: Int, elevatorPosition :: Int }
data Clone = Clone { cloneFloor :: Int, clonePosition :: Int, cloneDirection :: String }

data GameInfo = GameInfo{ elevators :: [Elevator], exit :: Exit }
data GameState = GameState { startPosition :: Maybe Int }
data GameLoopInfo = GameLoopInfo { clone :: Clone }

main :: IO ()
main = do
  executeCodingGameRequirement
  [floorCount, width, roundsMax, exitFloor, exitPosition, cloneCount, _, elevatorCount] <- readNums

  elevators <- replicateM elevatorCount $ do
    [elevatorFloor, elevatorPosition] <- readNums
    return Elevator { elevatorFloor, elevatorPosition }

  let exit = Exit { exitPosition, exitFloor }
  let gameInfo = GameInfo { elevators, exit }
  let gameState = GameState { startPosition = Nothing }

  void $ runStateT (gameLoop gameInfo) gameState

gameLoop :: GameInfo -> StateT GameState IO ()
gameLoop gameInfo@GameInfo{..} = do
    GameState{..} <- get
    GameLoopInfo{clone=clone@Clone{..}} <- lift gameLoopInfo

    when (isNothing startPosition) $
      put GameState { startPosition = Just clonePosition }

    lift . putStrLn $ determineOutputType clone exit startPosition

    gameLoop gameInfo

    where
      findElevator cloneFloor = fromJust $ find (\e -> elevatorFloor e == cloneFloor) elevators
      determineOutputType clone@Clone{..} exit@Exit{..} startPosition
        | exitFloor == cloneFloor = exitOutput exit clone
        | cloneFloor >= 0 = floorOutput (findElevator cloneFloor) clone startPosition
        | otherwise = "WAIT"

gameLoopInfo :: IO GameLoopInfo
gameLoopInfo = do
    input <- words <$> getLine
    let cloneFloor = read @Int $ head input
    let clonePosition = read @Int $ input !! 1
    let cloneDirection = input !! 2
    return GameLoopInfo { clone=Clone { cloneFloor, clonePosition, cloneDirection } }

-- TODO: After Elevator/Exit merge, make the last 3 guards of the following 2 functions the same function
floorOutput Elevator{..} Clone{..} startPosition
  | isNothing startPosition = "WAIT"
  | fromJust startPosition == clonePosition = "WAIT"
  | cloneDirection == "RIGHT" && elevatorPosition < clonePosition = "BLOCK"
  | cloneDirection == "LEFT"  && elevatorPosition > clonePosition = "BLOCK"
  | otherwise = "WAIT"

exitOutput Exit{..} Clone{..}
  | cloneDirection == "RIGHT" && exitPosition < clonePosition = "BLOCK"
  | cloneDirection == "LEFT" && exitPosition > clonePosition = "BLOCK"
  | otherwise = "WAIT"
