import System.IO
import Control.Monad

codingGameSetup = hSetBuffering stdout NoBuffering

getNumber :: IO Int
getNumber = read <$> getLine

main :: IO ()
main = do
  codingGameSetup
  r <- getNumber
  l <- getNumber

  putStrLn "answer"
