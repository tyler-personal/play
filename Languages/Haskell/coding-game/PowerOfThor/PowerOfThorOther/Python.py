from collections import namedtuple

Board = namedtuple('Board', 'x y end_x end_y')
Move = namedtuple('Move', 'direction board')

def get_numbers(str):
    return [int(x) for x in str.split()]

def game(b):
    f = lambda m: [m] if (m.direction == "") else [m] + f(calculate(m.board))
    return f(calculate(b))

def calculate(b):
    def new_b(x, y):
        return Board(x, y, b.end_x, b.end_y)

    if b.x > b.end_x and b.y > b.end_y:
        return Move("NW", new_b(b.x-1, b.y-1))
    elif b.x > b.end_x and b.y < b.end_y:
        return Move("SW", new_b(b.x-1, b.y+1))
    elif b.x < b.end_x and b.y > b.end_y:
        return Move("NE", new_b(b.x+1, b.y-1))
    elif b.x < b.end_x and b.y < b.end_y:
        return Move("SE", new_b(b.x+1, b.y+1))
    elif b.x > b.end_x:
        return Move("W", new_b(b.x-1, b.y))
    elif b.x < b.end_x:
        return Move("W", new_b(b.x-1, b.y))
    elif b.y > b.end_y:
        return Move("N", new_b(b.x, b.y-1))
    elif b.y < b.end_y:
        return Move("S", new_b(b.x, b.y+1))
    else:
        return Move("", b)

if __name__ == '__main__':
    n = get_numbers(input())
    board = Board(n[2], n[3], n[0], n[1])
    for move in game(board):
        print(move.direction)
