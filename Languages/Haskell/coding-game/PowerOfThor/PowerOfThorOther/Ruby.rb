Board = Struct.new(:x, :y, :end_x, :end_y)
Move = Struct.new(:direction, :board)

def get_numbers(str)
  str.split.map(&:to_i)
end

def game(b)
  f = -> m { m.direction == "" ? [m] : [m] + f.call(calculate(m.board)) }
  f.call(calculate(b))
end

def calculate(b)
  def new_b(b, x, y)
    Board.new(x, y, b.end_x, b.end_y)
  end

  if b.x > b.end_x and b.y > b.end_y
    Move.new("NW", new_b(b, b.x-1, b.y-1))
  elsif b.x > b.end_x and b.y < b.end_y
    Move.new("SW", new_b(b, b.x-1, b.y+1))
  elsif b.x < b.end_x and b.y > b.end_y
    Move.new("NE", new_b(b, b.x+1, b.y-1))
  elsif b.x < b.end_x and b.y < b.end_y
    Move.new("SE", new_b(b, b.x+1, b.y+1))
  elsif b.x > b.end_x
    Move.new("W", new_b(b, b.x-1, b.y))
  elsif b.x < b.end_x
    Move.new("W", new_b(b, b.x-1, b.y))
  elsif b.y > b.end_y
    Move.new("N", new_b(b, b.x, b.y-1))
  elsif b.y < b.end_y
    Move.new("S", new_b(b, b.x, b.y+1))
  else
    Move.new("", b)
  end
end

if __FILE__ == $0
  n = get_numbers(gets)
  board = Board.new(n[2], n[3], n[0], n[1])
  game(board).each { |m| puts m.direction }
end
