class Board(val x: Int, val y: Int, val endX: Int, val endY: Int)
class Move(val direction: String, val board: Board)

fun getNumbers(s: String) = s.split(" ").map { it.toInt() }

fun game(b: Board): List<Move> {
    fun f(m: Move): List<Move> = when {
        m.direction == "" -> listOf(m)
        else -> listOf(m) + f(m.board.calculate())
    }
    return f(b.calculate())
}

fun Board.calculate(): Move {
    fun newB(x: Int, y: Int) = Board(x, y, endX, endY)

    return when {
        x > endX && y > endY -> Move("NW", newB(x-1, y-1))
        x > endX && y < endY -> Move("SW", newB(x-1, y+1))
        x < endX && y > endY -> Move("NE", newB(x+1, y-1))
        x < endX && y < endY -> Move("SE", newB(x=x+1, y=y+1))
        x > endX -> Move("W", newB(x-1, y))
        x < endX -> Move("E", newB(x+1, y))
        y > endY -> Move("N", newB(x, y-1))
        y < endY -> Move("S", newB(x, y+1))
        else -> Move("", this)
    }
}

fun main() {
    val n = getNumbers(readLine()!!)
    val board = Board(n[2], n[3], n[0], n[1])
    game(board).forEach { println(it.direction) }
}