import Control.Monad

data Board = Board { x :: Int, y :: Int, endX :: Int, endY :: Int }
data Move = Move { direction :: String, board :: Board }

getNumbers :: String -> [Int]
getNumbers = map read . words

game :: Board -> [Move]
game = f . calculate
  where
    f m@Move{..}
      | direction == "" = [m]
      | otherwise = m : f (calculate board)

calculate :: Board -> Move
calculate b@Board{..}
  | x > endX && y > endY = Move "NW" b { x=x-1, y=y-1 }
  | x > endX && y < endY = Move "SW" b { x=x-1, y=y+1 }
  | x < endX && y > endY = Move "NE" b { x=x+1, y=y-1 }
  | x < endX && y < endY = Move "SE" b { x=x+1, y=y+1 }
  | x > endX = Move "W" b { x=x-1 }
  | x < endX = Move "E" b { x=x+1 }
  | y > endY = Move "N" b { y=y-1 }
  | y < endY = Move "S" b { y=y+1 }
  | otherwise = Move "" b

main :: IO ()
main = do
  n <- getNumbers <$> getLine
  let board = Board (n!!2) (n!!3) (n!!0) (n!!1)
  forM_ (game board) (putStrLn . direction)
