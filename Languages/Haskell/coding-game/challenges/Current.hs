module Main where

import Data.Char
import Safe

main = do
  x <- getLine
  y <- getLine
  putStrLn $ format x y

format3 value style = loop 0 0 ""
  where
    loop valueIndex styleIndex result =
      case (value !!! valueIndex, style !! styleIndex) of
        (Nothing, _)       -> result
        (Just a, Just 'X') -> loop (valueIndex+1) (styleIndex+1) $ result ++ [toUpper a]
        (Just a, Just 'x') -> loop (valueIndex+1) (styleIndex+1) $ result ++ [toLower a]
        (_, Just b)        -> loop valueIndex     (styleIndex+1) $ result ++ [b]
        (_, Nothing)       -> loop valueIndex     0                result

      where
        (!!!) = atMay

format value style = f 0 0 ""
  where
    f x y acc = case (value `atMay` x, style `atMay` y) of
      (Nothing, _)       -> acc
      (Just a, Just 'X') -> f (x+1) (y+1) $ acc ++ [toUpper a]
      (Just a, Just 'x') -> f (x+1) (y+1) $ acc ++ [toLower a]
      (_, Just b)        -> f x     (y+1) $ acc ++ [b]
      (_, Nothing)       -> f x     0       acc

{-@ format :: v:String -> {f:String | length f > length v} -> String @-}
formatOld value style  = f 0 0 ""
  where
    f x y acc = case (value `atMay` x, style `atMay` y) of
      (Nothing, _) -> acc
      (Just a, Just 'X') -> ff (+1) (+1) $ toUpper a
      (Just a, Just 'x') -> ff (+1) (+1) $ toLower a
      (_, Just b)        -> ff (+0) (+1) b
      (_, Nothing)       -> ff ((-) x) (+0) []

      where ff ix iy a = f (ix x) (iy y) (acc ++ [a])


--   putStr . map snd $ scanl f (0, ' ') (zip x y)
--   putStr . map snd $ scanl ff (0, ' ') (x, y)

-- ff (i,c) (x,y) = case b of
--   'X' -> (i+1, toUpper a)
--   'x' -> (i+1, toLower a)
--   _ -> (i,b)
--   where
--     a = x !! i
--     b = y !! i
-- f (i,c) (a,'X') = (i+1, toUpper a)
-- f (i,c) (a,'x') = (i+1, toLower a)
-- f (i,c) (a,b) = (i,b)
--
