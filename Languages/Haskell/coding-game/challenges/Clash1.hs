{-# LANGUAGE TypeApplications #-}
import Control.Monad

main = do
  n <- read @Int <$> getLine
  replicateM_ n . putStrLn . fmap change <$> getLine

change '0' = '-'
change n = n
