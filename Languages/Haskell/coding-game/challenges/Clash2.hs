{-# LANGUAGE TypeApplications #-}
import System.IO
import Control.Monad

main :: IO ()
main = do
    hSetBuffering stdout NoBuffering -- DO NOT REMOVE

    -- Auto-generated code below aims at helping you parse
    -- the standard input according to the problem statement.

    input_line <- getLine
    let n = read input_line :: Int

    replicateM n $ do
        row <- getLine
        putStrLn $ map change row
        return ()
    return ()

change '-' = '-'
change '0' = '-'
change '1' = '1'
