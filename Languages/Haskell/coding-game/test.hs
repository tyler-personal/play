module Test where

s 1=[1]
s n
  |even n=n:s(div n 2)
s n=n:s(3*n+1)

main = do
  n <- readLn
  let x = length $ s n
  print.length$ s x
