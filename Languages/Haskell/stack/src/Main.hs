{-# LANGUAGE OverloadedStrings, ExtendedDefaultRules, NoMonomorphismRestriction #-}
module Main where
import Data.List
import Safe
import Network.WebSockets
import Control.Monad
import Data.Text
import Sockets

{-@ test :: xs:[a] -> {i:Nat | i < len xs} -> a @-}
test list index = list !! index

yeet :: Connection -> IO ()
yeet connection = forever $ do
  message <- receiveData connection
  sendTextData connection $ message `append` ", yeet"

main = start

main2 = do
  print $ test [2, 3, 4] 1
  print $ headDef "yeet" []
  putStrLn "hello world"
