{-# LANGUAGE NoMonomorphismRestriction, ExtendedDefaultRules, NamedFieldPuns #-}
module Sockets where
import Data.Monoid ((<>))
import Prelude hiding (putStrLn, null)
import Network.WebSockets
import Control.Concurrent
import Data.Char (isPunctuation, isSpace)
import Data.Text (Text, isPrefixOf, null)
import qualified Data.Text as T
import Data.Text.IO (putStrLn)
import Control.Monad
import Control.Exception (finally)

data Client = Client
  { name :: Text
  , connection :: Connection
  }

type ServerState = [Client]

mkState :: ServerState
mkState = []

clientCount :: ServerState -> Int
clientCount = length

addClient :: Client -> ServerState -> ServerState
addClient client clients = client : clients

removeClient :: Client -> ServerState -> ServerState
removeClient client = filter ((/= name client) . name)

clientExists :: Client -> ServerState -> Bool
clientExists client = any ((== name client) . name)

broadcast :: Text -> ServerState -> IO ()
broadcast message clients = do
  putStrLn message
  forM_ clients $ \c -> sendTextData (connection c) message

start :: IO ()
start = do
  state <- newMVar mkState
  runServer "192.168.1.32" 8001 $ application state

application :: MVar ServerState -> ServerApp
application state pending = do
  connection <- acceptRequest pending
  forkPingThread connection 30
  message <- receiveData connection
  clients <- readMVar state
  case message of
    _ | invalidPrefix -> send "Invalid prefix."
      | invalidName -> send "Name cannot contain punctuation, whitespace, or be empty"
      | clientExists client clients -> send "User already exists"
      | otherwise -> flip finally disconnect $ do
        modifyMVar_ state $ \s -> do
          let s' = addClient client s
          send "Welcome!"
          broadcast (name client <> " joined") s'
          return s'
        talk connection state client
      where
        prefix = "I am "
        client = Client
          { name = T.drop (T.length prefix) message
          , connection
          }
        invalidPrefix = not (prefix `isPrefixOf` message)
        invalidName = any ($ name client) [null, T.any isPunctuation, T.any isSpace]

        send :: Text -> IO ()
        send = sendTextData connection
        disconnect = do
          s <- modifyMVar state $ \s ->
            let s' = removeClient client s in return (s', s')
          broadcast (name client <> " disconnect") s

talk :: Connection -> MVar ServerState -> Client -> IO ()
talk connection state client = forever $ do
  message <- receiveData connection
  readMVar state >>= broadcast (name client <> ": " <> message)
