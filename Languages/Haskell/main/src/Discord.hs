{-# LANGUAGE RecordWildCards #-}

module Discord where
import Network.Discord
import Data.Text(Text, isPrefixOf, strip)
import Data.Text.Conversions
import Pipes
import Data.List(stripPrefix)
import Data.Maybe
import Data.List.Split

token :: String
token = "NDc5Nzk5ODYyNzU0Mjc5NDI1.D3meVw.Xty3zrpoiMqefSbSPVfC79h1IJk"

reply :: Message -> Text -> Effect DiscordM ()
reply Message{messageChannel=channel} content = fetch' $
  CreateMessage channel content Nothing

startDiscordBot :: [IO ()]
startDiscordBot = map (\(str, f) -> runBot (Bot token) $
  with MessageCreateEvent $ \msg@Message{..} ->
    when (str `isPrefixOf` messageContent) $ f msg) commands

args :: String -> String -> [String]
args message command = filter (/= "") $ splitOn " " $ fromText $ strip $
  convertText $ fromMaybe message $ stripPrefix command message

commands :: [(Text, Message -> Effect DiscordM ())]
commands = [("!ping", (`reply` "Testing abstracted solution HASKELL"))]
