module Lib where
import Data.Default
import Data.List
import Data.Maybe
import Data.List.Split
import Data.Text (strip)

args :: String -> String -> [String]
args message command = filter (/= "") $ splitOn " " $ strip $
  fromMaybe message $ stripPrefix command message
