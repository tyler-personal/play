{-# LANGUAGE QuasiQuotes, TemplateHaskell, OverloadedStrings, DataKinds, NoMonomorphismRestriction #-}
{-# OPTIONS_GHC -fplugin=Language.Java.Inline.Plugin #-}
module Main where
import Language.Java.Inline
import Language.Java

import qualified Language.C.Inline as C
import Data.Text (Text)

C.include "<stdio.h>"
C.include "<math.h>"

main :: IO ()
main =
--  x <- [C.exp| int{ printf("Some number: %.2f\n", cos(0.5)) } |]
  withJVM [] $ do
    message <- reflect ("Hello World!" :: Text)
    [java| {
      javax.swing.JOptionPane.showMessageDialog(null, $message);
    }|]
--  putStrLn $ show x ++ " characters printed."
