{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_glue2 (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/tylerwbrown/Code/Play/Languages/Haskell/glue2/.stack-work/install/x86_64-linux-nix/lts-12.26/8.4.4/bin"
libdir     = "/home/tylerwbrown/Code/Play/Languages/Haskell/glue2/.stack-work/install/x86_64-linux-nix/lts-12.26/8.4.4/lib/x86_64-linux-ghc-8.4.4/glue2-0.1.0.0-2hTsVeyLwFH2VeP0iCx8Zx-glue2"
dynlibdir  = "/home/tylerwbrown/Code/Play/Languages/Haskell/glue2/.stack-work/install/x86_64-linux-nix/lts-12.26/8.4.4/lib/x86_64-linux-ghc-8.4.4"
datadir    = "/home/tylerwbrown/Code/Play/Languages/Haskell/glue2/.stack-work/install/x86_64-linux-nix/lts-12.26/8.4.4/share/x86_64-linux-ghc-8.4.4/glue2-0.1.0.0"
libexecdir = "/home/tylerwbrown/Code/Play/Languages/Haskell/glue2/.stack-work/install/x86_64-linux-nix/lts-12.26/8.4.4/libexec/x86_64-linux-ghc-8.4.4/glue2-0.1.0.0"
sysconfdir = "/home/tylerwbrown/Code/Play/Languages/Haskell/glue2/.stack-work/install/x86_64-linux-nix/lts-12.26/8.4.4/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "glue2_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "glue2_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "glue2_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "glue2_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "glue2_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "glue2_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
