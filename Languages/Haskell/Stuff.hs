module Stuff where
import Control.Monad

check val a b c d f g h = val `elem` [(a `f` b) `g` c `h` d, a `f` (b `g` c) `h` d, a `f` b `g` (c `h` d)]

f = mappedOpts
opts = replicateM 3 [((+), "+"), ((-), "-"), ((/), "/"), ((*), "*")]
optsDisplayed [(f,x), (g,y), (h,z)] = x ++ " " ++ y ++ " " ++ z
filteredOpts a b c d = filter (\[(f,x), (g,y), (h,z)] -> check 36 a b c d f g h) opts
mappedOpts a b c d = map optsDisplayed (filteredOpts a b c d)

main = print 4
