package com.tyler.playground.frontend.services.rpc

import com.tyler.playground.shared.rpc.client.MainClientRPC
import com.tyler.playground.shared.rpc.client.chat.ChatNotificationsRPC

class RPCService(notificationsCenter: NotificationsCenter) extends MainClientRPC {
  override val chat: ChatNotificationsRPC =
    new ChatService(notificationsCenter.msgListeners, notificationsCenter.connectionsListeners)
}