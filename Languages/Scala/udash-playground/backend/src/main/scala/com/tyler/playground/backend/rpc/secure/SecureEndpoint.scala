package com.tyler.playground.backend.rpc.secure

import com.tyler.playground.backend.rpc.secure.chat.ChatEndpoint
import com.tyler.playground.backend.services.DomainServices
import com.tyler.playground.shared.model.auth.UserContext
import com.tyler.playground.shared.rpc.server.secure.SecureRPC
import com.tyler.playground.shared.rpc.server.secure.chat.ChatRPC

class SecureEndpoint(implicit domainServices: DomainServices, ctx: UserContext) extends SecureRPC {
  import domainServices._

  lazy val chatEndpoint = new ChatEndpoint

  override def chat(): ChatRPC = chatEndpoint
}
