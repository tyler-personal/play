package com.tyler.playground.backend.rpc

import com.tyler.playground.backend.rpc.auth.AuthEndpoint
import com.tyler.playground.backend.rpc.i18n.TranslationsEndpoint
import com.tyler.playground.backend.rpc.secure.SecureEndpoint
import com.tyler.playground.backend.services.DomainServices
import com.tyler.playground.shared.model.auth.{UserContext, UserToken}
import com.tyler.playground.shared.model.SharedExceptions
import com.tyler.playground.shared.rpc.server.MainServerRPC
import com.tyler.playground.shared.rpc.server.open.AuthRPC
import com.tyler.playground.shared.rpc.server.secure.SecureRPC
import io.udash.i18n.RemoteTranslationRPC
import io.udash.rpc._

class ExposedRpcInterfaces(implicit domainServices: DomainServices, clientId: ClientId) extends MainServerRPC {
  // required domain services are implicitly passed to the endpoints
  import domainServices._

  private lazy val authEndpoint: AuthRPC = new AuthEndpoint

  // it caches SecureEndpoint for a single UserToken (UserToken change is not an expected behaviour)
  private var secureEndpointCache: Option[(UserToken, SecureEndpoint)] = None

  private def secureEndpoint(implicit ctx: UserContext): SecureRPC = {
    secureEndpointCache match {
      case Some((token, endpoint)) if token == ctx.token =>
        endpoint
      case None =>
        val endpoint = new SecureEndpoint
        secureEndpointCache = Some((ctx.token, endpoint))
        endpoint
    }
  }

  override def auth(): AuthRPC = authEndpoint

  override def secure(token: UserToken): SecureRPC = {
    authService
      .findUserCtx(token)
      .map(ctx => secureEndpoint(ctx))
      .getOrElse(throw SharedExceptions.UnauthorizedException())
  }

  override def translations(): RemoteTranslationRPC = TranslationsEndpoint
}