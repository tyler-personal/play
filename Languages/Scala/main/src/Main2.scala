import scala.collection.mutable

object Main2 extends App {
  val xs = List(1,2,3,4)
  val ys = List(0,1,2,3)

  val result = xs.zip(ys).map(x => x._1 - x._2).sum
  println(result)

  def addDifferences(xs: List[Number], ys: List[Number]) =
    xs.zip(ys).map(x => x._1.doubleValue() - x._2.doubleValue()).sum

//  def squares(data: List[Int]): List[Int] = {
//    val listo = mutable.MutableList[Int]()
//  }
}