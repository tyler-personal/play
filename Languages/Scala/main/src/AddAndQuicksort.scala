sealed trait Nat
case object Z extends Nat
case class S(n: Nat) extends Nat

import language.experimental.macros

object AddAndQuicksort extends App {
  def plus: (Nat, Nat) => Nat = {
    case (Z, y) => y
    case (S(k), y) => S(plus(k, y))
  }

  def sub: (Nat, Nat) => Nat = {
    case (x, Z) => x
    case (Z, y) => y
    case (S(a), S(b)) => sub(a, b)
  }

  def qs[T <: Ordered[T]]: List[T] => List[T] = {
    case List() => List()
    case x :: xs => qs(xs.filter(_ < x)) ++ List(x) ++ qs(xs.filter(_ >= x))
  }

  println(plus(S(Z), S(Z)))
}