case class P(var o: P = null, var c: Int = 0)

object A extends App {
  Console.readLine()
  val s = scala.collection.mutable.ListBuffer(P(), P())

  Console.readLine.split(" ").map(_.toInt).foreach(n => {
    var o = s(n)
    s.append(P(o))

    while (o != null) {
      o.c += 1
      o = o.o
    }
  })

  print(s.drop(1).map(_.c).mkString(" "))
}

