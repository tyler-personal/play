import scala.collection.mutable.ListBuffer

object CSES_Two extends App{
  Console.readLine()
  val slaves = scala.collection.mutable.
    ListBuffer[Person](Person(), Person())

  Console.readLine.split(" ").map(_.toInt).foreach(n => {
    var owner = slaves(n)
    slaves.append(Person(owner))

    while (owner != null) {
      owner.count += 1
      owner = owner.owner
    }
  })

  println(slaves.drop(1).map(_.count).mkString(" "))
}
