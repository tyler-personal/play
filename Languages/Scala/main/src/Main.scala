import scala.collection.mutable.ListBuffer

case class Person(var owner: Person = null, var count: Int = 0)

object Main extends App {
  Console.readLine()
  val slaves = ListBuffer(Person(), Person())

  Console.readLine.split(" ").map(_.toInt).foreach(n => {
    var owner = slaves(n)
    slaves.append(Person(owner))

    while (owner != null) {
      owner.count += 1
      owner = owner.owner
    }
  })

  println(slaves.drop(1).map(_.count).mkString(" "))
}
