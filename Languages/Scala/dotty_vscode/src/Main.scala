enum Nat {
  case Z
  case S(n: Nat)
}

def plus: (Nat, Nat) => Nat = {
  case (Nat.Z, y) => y
  case (Nat.S(k), y) => Nat.S(plus(k, y))
}

def test(x: Tuple2[String, Int]) = { 
  println("test")
}
object Main extends App {
  List(1, 2, 3).contains("YEET")
  // println(plus(Nat.S(Nat.Z), Nat.S(Nat.Z)))
  "Test".write
  testTopLevelFunction()
}

// #1 Top level functions are valid now
def testTopLevelFunction() = println("Big boi yatted")

// #2 Easier extension methods
def (s: String) write = println(s)

// #2 Latest Scala version best extension methods
implicit class ExtendedString(val value: String) extends AnyVal {
  def write2 = println(value)
}