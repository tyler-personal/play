import scala.io.StdIn.readLine
import scala.math.Ordering.Implicits._

@main def main(): Unit =
  val numbers = List.fill(3)(readLine("Enter num: ").toInt)
  val nums = List(6,4,7)
  println(quicksort(nums))
//  checkIfFree(numbers)

import java.util.stream.Collectors
import java.util.stream.Stream

def quicksort[T <: Comparable[T]]: List[T] => List[T] = {
  case Nil => List()
  case x :: xs =>
    val less = quicksort(xs.filter(_ <= x))
    val more = quicksort(xs.filter(_ > x))
    less ++ List(x) ++ more
}

//def checkIfFree[T <: Comparable[T]](numbers: List[T]): Unit =
//  numbers.map(num => if (num == 0) "free" else "not free")
//    .foreach(println)