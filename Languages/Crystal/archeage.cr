ground_grain = 0.0439
cucumber = 0.0288
hereafter_stone = 0.30

sandeep_local_pack = (ground_grain * 160) + (cucumber * 15) + hereafter_stone

def x_per_hour(x, minutes)
  run_count = 60.0 / minutes
  x * run_count
end

def profit_per_hour(cost, gross, minutes)
  gross_per_hour = x_per_hour(gross, minutes)
  cost_per_hour = x_per_hour(cost, minutes)
  puts "Profit: #{gross_per_hour - cost_per_hour}, Cost: #{cost_per_hour}"
end

puts sandeep_local_pack
profit_per_hour sandeep_local_pack, 16.74, 9.0
profit_per_hour sandeep_local_pack, 19.45, 9.0
profit_per_hour sandeep_local_pack, 14.48, 5.3

