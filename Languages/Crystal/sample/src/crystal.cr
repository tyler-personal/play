require "../lib/discordcr/src/discordcr"
require "http/server"

module Crystal
  server = HTTP::Server.new do |context|
    context.response.content_type = "text/plain"
    context.response.print "Hello world, got #{context.request.path}!"
  end

  puts "Listening on http://127.0.0.1:8080"
  server.listen(8080)
  client = Discord::Client.new(token: "token", client_id: 471)
end
