const funcs = [];
for (var i = 0; i < 3; i++) {
  funcs[i] = () => console.log(`My value: ${i}`);
}

for (let j = 0; j < 3; j++) {
  funcs[j]();
}
