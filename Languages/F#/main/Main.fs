module Main

let convertString str =
    sprintf "Hello, %s!" str

[<EntryPoint>]
let main argv =
    let names = ["Test"; "Aye"]
    
    names
        |> List.map convertString 
        |> List.iter (fun greeting -> printfn "%s" greeting)
    0