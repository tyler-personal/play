module fconsole.Old

let main2 argv =
    let names = ["Yeet"; "Yatt"]
    
    names
        |> List.map (fun x -> sprintf "Yeet %s" x)
        |> List.iter (fun x -> printf "%s" x)
    
    0 // return an integer exit code
