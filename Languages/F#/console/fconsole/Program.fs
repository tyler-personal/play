﻿type Nat = Z | S of Nat

let rec plus = function
    | Z, y -> y
    | S k, y -> S (plus (k, y))
    
let rec sub = function
    | x, Z -> x
    | Z, y -> Z
    | S a, S b -> sub (a, b)
    
let rec qs = function
    | [] -> []
    | x :: xs -> qs (List.filter ((<) x) xs) @ [x] @ qs (List.filter ((>=) x) xs)

[<EntryPoint>]
let main argv =
    System.Console.WriteLine("Result: {0}", plus (Z, S(Z)))
    0
