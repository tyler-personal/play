import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

interface Functor<T> {
    <R> Functor<R> map(Function<T, R> f);
}

record FList<T> (T... items) implements Functor<T> {
    @Override
    public <R> FList<R> map(Function<T, R> f) {
        return new FList<>((R[]) Arrays.stream(items).map(f).toArray());
    }
}

//record FOption<T> (T value) implements Functor<T>{
//    FOption() { this(null); }
//
//    @Override
//    public <R> FOption<R> map(Function<T, R> f) {
//        if (value == null)
//            return
//        return new FList<>(items.stream().map(f).collect(Collectors.toList()));
//    }
//}