//package Rafcord;
//
//import java.util.Random;
//
//class Circum {
//    int max_velocity = 15;
//    float radius, mass, ref = 0.5, springness = 0.9999;
//    PVector  point = new PVector(0, 0);
//    PVector  acceleration = new PVector(0, 0);
//    PVector  velocity = new PVector(new Random().nextInt(max_velocity) + 0.05, -1 * (new Random().nextInt(max_velocity) + 0.05));
//    PVector gre = new PVector(0, 0);
//
//    Circum() {
//        mass = 5 * new Random().nextFloat() + 1;
//        radius = mass * 4;
//    }
//
//    Circum(float x_, float y_, float r_, float m_) {
//        this();
//        point.x = x_;
//        point.y = y_;
//        radius = r_;
//        mass = m_;
//    }
//
//    void border() {
//        //Checking if the object is outside the displayed window
//        if (point.y - radius <= 0 || point.y + radius >= height) velocity.y *= -springness;
//        if (point.x - radius <= 0 || point.x + radius >= width) velocity.x *= -springness;
//    }
//
//    void setSpeed(PVector force) {
//        PVector f = force.copy();
//        acceleration.add(PVector.div( f, mass));
//        velocity.add(acceleration);
//        point.add(velocity);
//        gre = acceleration.copy();
//
//        // If out of bounds
//        if (point.y - radius <= 0)
//            point.y = 0 + radius;
//
//        else if (point.y + radius >= height)
//            point.y = height - radius ;
//
//        if (point.x - radius <= 0)
//            point.x = 0 + radius;
//
//        else if (point.x + radius >= width)
//            point.x = width - radius;
//
//        acceleration.mult(0);
//    }
//
//    void drawing() {
//        //drawing an object
//        fill((map(velocity.y, -max_velocity, max_velocity, 50, 255)), (map(gre.y, -2, 2, 50, 100)), (map(point.y, 0, width, 1, 255)) );
//        stroke((map(point.x, 0, height, 100, 255)), (map(gre.x, -2, 2, 50, 100)), (map(velocity.x, -max_velocity, max_velocity, 50, 255)));
//        circle(point.x, point.y, 2 * radius);
//        //drawing the direction of the setSpeed vector
//        strokeWeight(2);
//        stroke(255, 255, 255);
//        PVector dir = velocity.copy();
//        dir.normalize().mult(radius);
//        dir = PVector.add(dir, point);
//        line(point.x, point.y, dir.x, dir.y);
//    }
//}