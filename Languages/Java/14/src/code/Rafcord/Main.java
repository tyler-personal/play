//package Rafcord;
//
//import java.util.*;
//import java.util.Random;
//public class Main {
//    Random randa = new Random();
//    ControlP5 cp5;
//
//    boolean  field = false;//true -  central field , false -  homogeneous field
//    int control = 5, control2 = control, i, control3 = control2+1, ile = 1, ile_pend =1, current;
//    float mass = 1000, radius = 20, density, G_const = 0.6673, total_mass = 0, a1 = 0;
//    ArrayList<Circum> cir = new ArrayList<Circum>(control);
//    PVector momentum[] = new PVector[control3];
//    PVector  inic = new PVector(0.0000, 0.0000);
//
//    void setup() {
//        //fullScreen(P2D, 1);
//        size(1000, 800, P2D);
//        stroke(255);
//        background(255);
//
//        for (int i = 0; i <control; i++)
//        { //initial object creation
//            mass = 10;
//            total_mass += mass;
//            radius = 10;
//            cir.add(new Circum(0, 0, radius, mass));
//        }
//
//        setting();
//    }
//
//    void setting()
//    {
//        float pozX = 0, pozY = 0;
//
//        for (int i = control - 1; i >= 0; i--)
//        {
//            cir.remove(i);
//        }
//
//        if ( field == true)
//        {//creation of random objects participating in central field
//            for (int i = 0; i <control2; i++)
//            {
//                mass = 20 * randa.nextFloat() + 4;
//                total_mass +=mass;
//                radius = mass  * 2;
//                pozX = (width * new Random().nextFloat()) - radius;
//                pozY =  (height * new Random().nextFloat()) - radius;
//
//                cir.add(new Circum(pozX, pozY, radius, mass));
//                cir.get(i).velocity = new PVector(0, 0);
//            }
//            control = control2;
//        } else if ( field == false)
//        {//creation of random objects participating in homogeneous field
//            for (int i = 0; i<control2; i++)
//            {
//                mass = 8 * randa.nextFloat() + 4;
//                radius = mass  * 2;
//                pozX = (width * new Random().nextFloat()) - radius;
//                pozY =  (height * new Random().nextFloat()) - radius;
//
//                cir.add(new Circum( pozX, pozY, radius, mass));
//                cir.get(i).setSpeed(inic);
//            }
//        }
//    }
//
//    void drawBorders()
//    {
//        strokeWeight(6);
//        stroke(200, 0, 10);
//        line(0, height, width, height);
//        line(0, 0, width, 0);
//        line(0, 0, 0, height);
//        line(width, 0, width, height);
//        strokeWeight(1);
//    }
//
//    void keyPressed()
//    {
//
//        if ((key >= 'A' && key <= 'Z') || (key >= 'a' && key <= 'z'))
//        {
//
//            if (key == 'f' ||key == 'F')
//            {
//                field = false; // f - homogeneous field
//                setting();
//            } else if (key == 'v' ||key == 'V')
//            {
//                field = true; //v - central field
//                setting();
//            } else if (key == 'r' ||key == 'R')
//            {
//                background(255); //R - reset
//                setting();
//            }
//        }
//    }
//
//    void collision(Circum con1, Circum con2, float dist_, float min_, PVector lock)
//    {
//        float u1, u2, distance = dist_, min_dyst = min_;
//
//        // Defining the X axis
//        PVector dirX = lock.copy();
//        dirX.normalize();
//        // Defining the Y axis
//        PVector dirY = new PVector(dirX.y, -dirX.x);
//
//        // X coordinates of velocityocities
//        float vx1 = dirX.dot(con1.velocity);
//        float vx2 = dirX.dot(con2.velocity);
//        // Y coordinates of velocityocities
//        float vy1 = dirY.dot(con1.velocity);
//        float vy2 = dirY.dot(con2.velocity);
//
//        // Applying the collision to X coordinates
//        u1 = (2 * vx2 * con2.mass + vx1 * (con1.mass - con2.mass)) / (con1.mass + con2.mass);
//        u2 = (2 * vx1 * con1.mass + vx2 * (con2.mass - con1.mass)) / (con1.mass + con2.mass);
//
//        // Turning velocityocities back into vectors
//        PVector velocity1 = PVector.mult(dirX, u1);
//        PVector velocity2 = PVector.mult(dirX, u2);
//        velocity1.add(PVector.mult(dirY, vy1));
//        velocity2.add(PVector.mult(dirY, vy2));
//
//        con1.velocity = velocity1;
//        con2.velocity = velocity2;
//
//        float distanceCorrection = (min_dyst-distance)/2.0;
//        PVector correctionVector = lock.normalize().mult(distanceCorrection);
//        con2.point.add(correctionVector);
//        con1.point.sub(correctionVector);
//    }
//
//    void checkForCollision()
//    {
//        for (int i = 0; i<cir.size() -1; i++)
//        {
//            for (int j = i + 1; j<cir.size(); j++)
//            {
//                //calculating distance between object
//                PVector lengthFrom_i_to_j= PVector.sub( cir.get(j).point, cir.get(i).point);
//                float oldDist = lengthFrom_i_to_j.mag();
//                float min_dyst = cir.get(j).radius + cir.get(i).radius;
//                //checking for collision
//                if (oldDist <= min_dyst)
//                {
//                    collision(cir.get(i), cir.get(j), oldDist, min_dyst, lengthFrom_i_to_j);
//                }
//            }
//        }
//    }
//
//    void centralField()
//    {
//        float dem = 0, dividerx = 0, dividery = 0, oldDist, newDist;
//        PVector grav_atract = new PVector(0, 0);
//        ArrayList<PVector> reverseGravity = new ArrayList<PVector>(cir.size());
//
//        for (int i = 0; i < cir.size(); i++)
//        {
//            reverseGravity.add(new PVector(0, 0));
//        }
//
//        for (int i = 0; i<cir.size() - 1; i++)
//        {
//            //Calculation of the mass center
//            dem += cir.get(i).mass;
//            dividerx += cir.get(i).mass * cir.get(i).point.x;
//            dividery += cir.get(i).mass * cir.get(i).point.y;
//
//            for (int j = i + 1; j<cir.size(); j++)
//            {
//
//                //calculating distance between object
//                PVector lengthFrom_i_to_j= PVector.sub( cir.get(j).point, cir.get(i).point);
//                PVector lengthFrom_j_to_i= PVector.sub( cir.get(i).point, cir.get(j).point);
//
//                newDist = lengthFrom_i_to_j.mag();
//                lengthFrom_i_to_j.normalize();
//                lengthFrom_j_to_i.normalize();
//
//                //adding gravity
//                float strength = (G_const * cir.get(i).mass * cir.get(j).mass)/(newDist * newDist);
//                lengthFrom_i_to_j.mult(strength );
//                lengthFrom_j_to_i.mult(strength );
//
//                reverseGravity.get(j).add(lengthFrom_j_to_i);
//
//                grav_atract.add(lengthFrom_i_to_j);
//            }
//
//            grav_atract.add( reverseGravity.get(i));
//            cir.get(i).setSpeed(grav_atract);
//            grav_atract.mult(0);
//
//            for (int k = 0; k<reverseGravity.size(); k++)
//            {
//                reverseGravity.get(k).mult(0);
//            }
//
//            checkForCollision();
//            cir.get(i).border();
//            cir.get(i).drawing();
//        }
//
//        //Drawing a center of mass
//        pushMatrix();
//        strokeWeight(6);
//        fill(50, 0, 255 );
//        stroke(255, 0, 50);
//        point((dividerx/dem), (dividery/dem));
//        popMatrix();
//    }
//
//    void homogeneousField()
//    {
//        PVector  air_replica = new PVector(0.004, 0.004);
//        PVector  grav = new PVector(0.00, 3.00);
//        PVector air;
//
//        for (int i = 0; i<cir.size(); i++)
//        {//Adding gravity
//            cir.get(i).setSpeed(grav);
//            air = air_replica.copy();
//
//            if (cir.get(i).velocity.x>0 ||cir.get(i).velocity.y>0)
//            {
//                air.mult(-1);
//            }
//            //adding motion resistance
//            cir.get(i).setSpeed(air);
//        }
//
//        checkForCollision();
//
//        for (int k = 0; k<cir.size(); k++)
//        {
//            cir.get(k).border();
//            cir.get(k).drawing();
//        }
//    }
//
//    void draw() {
//        background(200);
//        drawBorders();
//
//        if (field)
//        {
//            centralField();
//        } else
//        {
//            homogeneousField();
//        }
//        pushMatrix();
//        textSize(20);
//        textLeading(22);
//        fill(10, 10, 10);
//        text(" It doesn't matter what size the letters are.\n F - homogeneous field\n V - central field\n R - reset", 10, 20);
//        popMatrix();
//    }
//}
