package jeffwong123;

public class Mountain {

    private String name;
    private String range;
    private String country;
    private int height;
    private String hemisphere;

    public Mountain() {

    }

    public Mountain(String name, String range, String country, int height, String hemisphere) {
        setName(name);
        setRange(range);
        setCountry(country);
        setHeight(height);
        setHemisphere(hemisphere);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public String getRange() {
        return range;
    }

    public void setCountry(String contry) {
        this.country = contry;
    }

    public String getCountry() {
        return country;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getHeight() {
        return height;
    }

    public void setHemisphere(String hemisphere) {
        this.hemisphere = hemisphere;
    }

    public String getHemisphere() {
        return hemisphere;
    }

    public String getNameAndHeight() {
        return name + " " + Integer.toString(height);

    }

    public String getAll() {
        return name + " " + range + " " + country + " " + Integer.toString(height) + " " + hemisphere;
    }

}