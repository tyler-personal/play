package jeffwong123;

public class MountainClient {

    public static void main(String[] args) {
        Mountain mount1 = new Mountain("Mount Everest", "Mahalangur Himalaya", "china", 8848, "North");

        var client = new MountainServer();
        client.addMountain(mount1);
        client.addMountain(new Mountain("Lhotse", "Mahalangur Himalaya", "china", 8516, "North"));
        client.addMountain(new Mountain("Kamet", "Garhwal Himalaya", "India", 7756, "North"));
        client.addMountain(new Mountain("Mount Warning", "Tweed Range", "Australia", 1155, "South"));
        String all = client.returnAll();
        System.out.println(all);
        String aMountain = client.getMountainByHemisphere("North");
        System.out.println(aMountain);
        String bMountain = client.getMountainByCountry("china");
        System.out.println(bMountain);
        String cMountain = client.getMountainByNRC("Mount Everest", "Mahalangur Himalaya", "china");
        System.out.println(cMountain);



    }
}
