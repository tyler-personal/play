package jeffwong123;

//This is the server Mountain

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MountainServer {

    private ArrayList<String> mountainRange = new ArrayList<>();
    private ArrayList<String> mountainCountry = new ArrayList<>();
    private List<Mountain> mountains = new ArrayList<>();
    private boolean check;

    private final Object syncLock = new Object();

    public MountainServer() {
    }

    public String addMountain(String name, String range, String country, int height, String hemisphere) {
        synchronized (syncLock) {
            List<String> test = mountains.stream()
                    .filter(m -> m.getName().equals(name)
                            && m.getRange().equals(range)
                            && m.getCountry().equals(country)
                            && m.getHeight() == height
                            && m.getHemisphere().equals(hemisphere))
                    .map(m -> m.getName())
                    .collect(Collectors.toList());
            check = test.toString().equals(name);
            String result;
            if (!check) {//problem here
                Mountain mountain = new Mountain(name, range, country, height, hemisphere);
                if (!mountainRange.contains(range)) {
                    mountainRange.add(range);
                }
                if (!mountainCountry.contains(country)) {
                    mountainCountry.add(country);
                }
                mountains.add(mountain);
                result = "Mountain successfully added";

            } else {
                result = "Mountain already exist";

            }
            return result;
        }
    }

    public String addMountain(Mountain mountain) {
        synchronized (syncLock) {
            String result;
            if (!check) {//problem here
                mountains.add(mountain);
                result = "Mountain successfully added";
            } else {
                result = "Mountain already exist";
            }
            return result;
        }
    }

    public String deleteMountain(String name, String range, String country) {
        synchronized (syncLock) {
            //get size before delete
            int mountainBefore = mountains.size();
            //deletes mountain
            mountains.removeIf(m -> m.getName().equals(name)
                    && m.getRange().equals(range)
                    && m.getCountry().equals(country));
            String result;
            //get size after delete
            int mountainAfter = mountains.size();
            if (mountainBefore == mountainAfter) {
                result = "Such Mountain does not exist";
            } else {
                result = "Mountain removed";
            }
            return result;
        }
    }

    public String updateMountain(Mountain Mountain) {
        synchronized (syncLock) {
            String result;
            String name = Mountain.getName();
            String range = Mountain.getRange();
            String country = Mountain.getCountry();
            int height = Mountain.getHeight();
            //saves a list before deleteing
            List<Integer> mountainBefore = mountains.stream()
                    .filter(m -> m.getName().equals(name)
                            && m.getRange().equals(range)
                            && m.getCountry().equals(country))
                    .map(m -> m.getHeight())
                    .collect(Collectors.toList());
            //delete item
            mountains.stream()
                    .filter(m -> m.getName().equals(name)
                            && m.getRange().equals(range)
                            && m.getCountry().equals(country))
                    .forEach(m -> m.setHeight(height));
            //saves a list after deleteing
            List<Integer> mountainAfter = mountains.stream()
                    .filter(m -> m.getName().equals(name)
                            && m.getRange().equals(range)
                            && m.getCountry().equals(country))
                    .map(m -> m.getHeight())
                    .collect(Collectors.toList());
            //check if before and after are equal
            boolean isEqual = mountainBefore.equals(mountainAfter);
            if (isEqual) {//problem here
                result = "Such mountain does not exist";
            } else {
                result = "Mountain info updated";
            }
            return result;
        }
    }

    public String updateMountain(String name, String range, String country, int height) {
        synchronized (syncLock) {
            //saves a list before update
            List<Integer> mountainBefore = mountains.stream()
                    .filter(m -> m.getName().equals(name)
                            && m.getRange().equals(range)
                            && m.getCountry().equals(country))
                    .map(m -> m.getHeight())
                    .collect(Collectors.toList());
            //updates list
            mountains.stream()
                    .filter(m -> m.getName().equals(name)
                            && m.getRange().equals(range)
                            && m.getCountry().equals(country))
                    .forEach(m -> m.setHeight(height));
            //saves a list after update
            List<Integer> mountainAfter = mountains.stream()
                    .filter(m -> m.getName().equals(name)
                            && m.getRange().equals(range)
                            && m.getCountry().equals(country))
                    .map(m -> m.getHeight())
                    .collect(Collectors.toList());
            //compare and check if before and after are equal
            boolean isEqual = mountainBefore.equals(mountainAfter);
            String result;
            if (isEqual) {//problem here
                result = "Such mountain does not exist";
            } else {
                result = "Mountain info updated";
            }
            return result;
        }
    }

    public String getMountainByNRC(String name, String range, String country) {
        synchronized (syncLock) {
            List<Integer> result = mountains.stream()
                    .filter(m -> m.getName().equals(name)
                            && m.getRange().equals(range)
                            && m.getCountry().equals(country))
                    .map(m -> m.getHeight())
                    .collect(Collectors.toList());
            if (result.isEmpty()) {
                return "Such mountain doesnt exist";
            } else {
                return result.toString();
            }
        }
    }

    public String getMountainByRC(String range, String country) {
        synchronized (syncLock) {
            List<String> result = mountains.stream()
                    .filter(m -> m.getRange().equals(range)
                            && m.getCountry().equals(country))
                    .map(m -> m.getNameAndHeight())
                    .collect(Collectors.toList());

            if (result.isEmpty()) {
                return "Such mountain doesn't exist";
            } else {
                return result.toString();
            }
        }
    }

    public String getMountainByCountry(String country) {
        synchronized (syncLock) {
            List<String> result = mountains.stream()
                    .filter(m -> m.getCountry().equals(country))
                    .map(m -> m.getNameAndHeight())
                    .collect(Collectors.toList());
            if (result.isEmpty()) {
                return "Such mountain doesn't exist";
            } else {
                return result.toString();
            }
        }
    }

    public String getMountainByHemisphere(String hemisphere) {
        synchronized (syncLock) {
            List<String> result = mountains.stream()
                    .filter(m -> m.getHemisphere().equals(hemisphere))
                    .map(m -> m.getName())
                    .collect(Collectors.toList());

            if (result.isEmpty()) {
                return "Such mountain doesn't exist";
            } else {
                return result.toString();
            }

        }

    }

    public String returnAll() {
        synchronized (syncLock) {
            List<String> result = mountains.stream()
                    .map(m -> m.getAll())
                    .collect(Collectors.toList());
            return result.toString();
        }

    }


}