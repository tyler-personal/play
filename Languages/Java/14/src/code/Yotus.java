interface Formula<P> { }

record Prop<P>(P p) implements Formula<P> { }
record And<P>(Formula<P> x, Formula<P> y) implements Formula<P> { }
record Not<P>(Formula<P> p) implements Formula<P> { }

public class Yotus {
    public static void main(String[] args) {
        var data = new Prop<>(4);
        var dog = new Dog();
        dog.numberOfLegs = 3;
    }

    public static <P> void run(Formula<P> formula) {
        if (formula instanceof Prop f) {
            // code
        } else if (formula instanceof And f) {
            // code
        } else if (formula instanceof Not f) {
            // code
        }
    }
}

final class Dog {
    int numberOfLegs = 4;
}

