import java.lang.constant.Constable;
import java.lang.constant.ConstantDesc;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import static java.util.stream.IntStream.range;
import java.util.stream.Stream;

public class Java14Main {
    public static int getNumberInClosedRange(int lowBound, int highBound) {
        return (int) (Math.random() * (highBound - lowBound + 1)) + lowBound;
    }

    record Mountain(String name, String range, String country, int height) {}

    <T extends Number & Comparable<? extends T> & Constable & ConstantDesc> List<T> nums() {
        return null;
        //return (List<T>) List.of(27.74, 999, 21.99);
    }
    public static void main(String[] args) {
        int average1 = 0;
        int average2 = 0;
        int average3 = 0;

        int lowestX = Integer.MAX_VALUE;
        int lowestY = Integer.MAX_VALUE;
        int lowestZ = Integer.MAX_VALUE;

        int highestX = Integer.MIN_VALUE;
        int highestY = Integer.MIN_VALUE;
        int highestZ = Integer.MIN_VALUE;

        int min = 0;
        int max = 2;
        range(0, 10).forEach(System.out::println);
        for (int i = 0; i < 100_000; i++) {
            int x = (int) (Math.random() * (max - min)) + min;
            int y = new Random().nextInt(max - min) + min;
            int z = getNumberInClosedRange(min, max);

            if (x < lowestX)
                lowestX = x;
            if (x > highestX)
                highestX = x;
            if (y < lowestY)
                lowestY = y;
            if (y > highestY)
                highestY = y;
            if (z < lowestZ)
                lowestZ = z;
            if (z > highestZ)
                highestZ = z;
            average1 += x;
            average2 += y;
            average3 += z;
        }
        System.out.println(average1 / 100_000.0);
        System.out.println(average2 / 100_000.0);
        System.out.println(average3 / 100_000.0);

        System.out.println(lowestX);
        System.out.println(lowestY);
        System.out.println(lowestZ);
        System.out.println(highestX);
        System.out.println(highestY);
        System.out.println(highestZ);
        var nums = List.of(27.74, 999, 21.99);
        var items = Map.of("yeet", 27.74, "New yac", 999, "xx", 21.99);

        var mountains = List.of(
            new Mountain("Potato Mountain", "Great Ranges", "Murica", 40),
            new Mountain("Runner Mountain", "Runnin Hills", "ChINA", 80),
            new Mountain("Oofsted Mountain", "yachty humps", "Canada", 30)
        );

        var rangesOver35 = mountains.stream()
                .filter(m -> m.height > 35)
                .map(m -> m.range)
                .collect(Collectors.toList());

        rangesOver35.forEach(System.out::println);
        System.out.println();
        mountains.forEach(System.out::println);
    }

    public static <T extends Enum<T>> Optional<T> getEnum(Class<T> type, String value) {
        return EnumSet.allOf(type).stream()
                    .filter(e -> e.name().equals(value))
                    .findFirst();
    }
    enum Cases {
        SEVERE, MILD, NORMAL
    }

    static String[] repeat(String[] items, int times) {
        var newItems = new String[items.length * times];
        for (int i = 0; i < items.length; i++)
            for (int j = 0; j < times; j++)
                newItems[(i * times) + j] = items[i];
        return newItems;
    }

    static List<String> repeat2(List<String> items, int times) {
        var newItems = new ArrayList<String>();
        items.forEach(item -> {
            for (int i = 0; i < times; i++)
                newItems.add(item);
        });
        return newItems;
    }
}

