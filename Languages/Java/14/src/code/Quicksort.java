import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

//    static <T extends Comparable<T>> List<Comparable<T>> q(List<Comparable<T>> l) {
//        return l.isEmpty() ? List.of() :
//                q(l.subList(1, l.size()).stream().filter(x -> l.get(0).compareTo(x) >= 0).collect(Collectors.toList()))
//    }
public class Quicksort {
    public static <T extends Comparable<T>> List<T> quicksort(List<T> _xs) {
        if (_xs.isEmpty())
            return List.of();

        var x = _xs.get(0);
        var xs = _xs.subList(1, _xs.size());

        var less = quicksort(xs.stream()
                .filter(y -> y.compareTo(x) < 0)
                .collect(Collectors.toList()));
        var more = quicksort(xs.stream()
                .filter(y -> y.compareTo(x) >= 0)
                .collect(Collectors.toList()));

        return Stream.of(less.stream(), Stream.of(x), more.stream())
                .flatMap(Function.identity())
                .collect(Collectors.toList());
    }
}
//     static <T extends Comparable<T>> List<T> q(List<T> l) { return l.isEmpty() ? List.of() : Stream.concat(Stream.concat(q(l.subList(1, l.size()).stream().filter(x -> x.compareTo(l.get(0)) <= 0).collect(Collectors.toList())).stream(), Stream.of(l.get(0))), q(l.subList(1, l.size()).stream().filter(x -> x.compareTo(l.get(0)) > 0).collect(Collectors.toList())).stream()).collect(Collectors.toList()); }