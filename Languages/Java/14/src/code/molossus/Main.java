//package molossus;
//
//import java.util.function.Function;
//
//interface TermVisitor<B, C> {
//    <Domain, Range> C lambda(Variable<Domain> binder, Term<Range> body);
//    <A> C apply(Term<Function<A, B>> f, Term<A> x);
//    C global(Global<B> global);
//    C constant(Constant<B> constant);
//    C local(Variable<B> binder);
//}
//
//interface Term<A> {
//    <C> C visit(TermVisitor<A, C> visitor);
//}
//
//record LambdaTerm<A,B>(Variable<Domain> binder, Term<Range> body) implements Term<Function<A, B>> {
//
//}
//
