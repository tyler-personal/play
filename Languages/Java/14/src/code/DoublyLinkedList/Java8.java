package DoublyLinkedList;

import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Java8<T> {
    Node head;

    class Node {
        T value;
        @Nullable Node prev;
        @Nullable Node next;

        Node(T value, @Nullable Node prev, @Nullable Node next) {
            this.value = value;
            this.prev = prev;
            this.next = next;
        }
    }


    void push(T value) {
        Node node = new Node(value, null, head);

        if (head != null)
            head.prev = node;

        head = node;
    }

    T pop() {
        if (head == null)
            return null;

        Node node = head;
        head = head.next;

        return node.value;
    }

    void append(T value) {
        Node node = new Node(value, null, null);

        if (head == null) {
            head = node;
            return;
        }

        Node last = Stream.iterate(head, n -> n.next != null, n -> n.next)
                .reduce((a,b) -> b)
                .get();
        last.next = node;
        node.prev = last;
    }

    @Override
    public String toString() {
        return Stream.iterate(head, Objects::nonNull, node -> node.next)
                .map(Node::toString)
                .collect(Collectors.joining(", "));
    }


    public static void main(String[] args) {
        Java8<Integer> list = new Java8<>();
        list.append(3);
        list.push(2);
        list.push(1);
        list.append(4);
        System.out.println(list);
    }
}
