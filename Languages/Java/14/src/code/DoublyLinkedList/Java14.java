package DoublyLinkedList;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Java14<T> {
//    record Node <T> (T value, @Nullable Node<T> prev, @Nullable Node<T> next) {};
//    Node<T> push(@Nullable Node<T> node, T value) {
//        if (node == null)
//            return new Node<>(value, null, null);
//        var newNode = new Node<>(value, null, new Node<>(node.value, newNode, node.next));
//        return newNode;
//    }
    static class Node<T> {
        T value;
        @Nullable Node<T> prev;
        @Nullable Node<T> next;

        Node(T value, @Nullable Node<T> prev, @Nullable Node<T> next) {
            this.value = value;
            this.prev = prev;
            this.next = next;
        }

        @Override
        public String toString() {
            return "Node{value=" + value + ", prev=" + prev + ", next=" + next + '}';
        }
    }

    Node<T> push(@Nullable Node<T> node, T value) {
        if (node == null)
            return new Node<>(value, null, null);
        var newNode = new Node<>(value, null, null);
        newNode.next = new Node<>(node.value, newNode, node.next);
        return newNode;
    }

    Node<T> append(Node<T> node, T value) {
        if (node == null)
            return new Node<>(value, null, null);
        else if (node.next == null)
            return new Node<>(value, node, null);
        else
            return new Node<>(value, node.prev, append(node.next, value));
    }

    String showNodes(Node<T> node) {
        return Stream.iterate(node, Objects::nonNull, n -> n.next)
                .map(Node::toString)
                .collect(Collectors.joining(", "));
    }


    public static void main(String[] args) {
        Java8<Integer> list = new Java8<>();
        list.append(3);
        list.push(2);
        list.push(1);
        list.append(4);
        System.out.println(list);
    }
}
