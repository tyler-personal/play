#include <stdio.h>

int add3(int x) {
  return x + 3;
}

int twice(int f(int), int v) {
  return f(f(v));
}

int main() {
  printf("%d\n", twice(int (int x) { return x + 3 }, 7));
}
